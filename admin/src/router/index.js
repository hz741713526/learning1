import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'
import CategoryEdit from '../views/CategoryEdit'
import CategoryList from '../views/CategoryList'
import ItemEdit from '../views/ItemEdit'
import ItemList from '../views/ItemList'
import HeroEdit from '../views/HeroEdit'
import HeroList from '../views/HeroList'
import ArticleEdit from '../views/ArticleEdit'
import ArticleList from '../views/ArticleList'
import AdEdit from '../views/AdEdit'
import AdList from '../views/AdList'
import AdminUserEdit from '../views/AdminUserEdit'
import AdminUserList from '../views/AdminUserList'
import Login from '../views/Login'

Vue.use(VueRouter)
const router = new VueRouter({
  routes : [
    {
      path:'/login',name :'login',component: Login, meta :{ isPublic : true}
    },
    {
      path: '/',
      name: 'main',
      component: Main,
      children:[
        {path:'/Categories/create', component:CategoryEdit},
        {path:'/Categories/list', component:CategoryList},
        {path:'/Categories/edit/:id', component:CategoryEdit, props:true},
  
        {path:'/Items/create', component:ItemEdit},
        {path:'/Items/list', component:ItemList},
        {path:'/Items/edit/:id', component:ItemEdit, props:true},
  
        {path:'/heroes/create', component:HeroEdit},
        {path:'/heroes/list', component:HeroList},
        {path:'/heroes/edit/:id', component:HeroEdit, props:true},
  
        {path:'/articles/create', component:ArticleEdit},
        {path:'/articles/list', component:ArticleList},
        {path:'/articles/edit/:id', component:ArticleEdit, props:true},
  
        {path:'/ads/create', component:AdEdit},
        {path:'/ads/list', component:AdList},
        {path:'/ads/edit/:id', component:AdEdit, props:true},
  
        {path:'/admin_users/create', component:AdminUserEdit},
        {path:'/admin_users/list', component:AdminUserList},
        {path:'/admin_users/edit/:id', component:AdminUserEdit, props:true},
      ]
    },
    
  ]
})
  

router.beforeEach((to, from, next) => {
  // to and from are both route objects. must call `next`.
  //不是公开访问的页面和没有token都会进入login
  if(!to.meta.isPublic && !localStorage.token){
    return next('/login')
  }
  next()
})
export default router
