import Vue from 'vue'
import App from './App.vue'
import './plugins/element.js'
import router from './router'
import './style.css'
Vue.config.productionTip = false

//将http添加到vue原型中，即可在任意处使用this.$http来调用 baseURL  baseURL:'http//localhost:3000/admin/api'
import http from './http'
Vue.prototype.$http = http
//将这个代码块定义到vue全局中
Vue.mixin({
  computed:{
    uploadUrl(){
      return this.$http.defaults.baseURL + '/upload'
    }
  },
  methods:{
    getAuthHeaders(){
      return {
        Authorization : `Bearer ${localStorage.token || ''}`
      }
    }
  }
})
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
