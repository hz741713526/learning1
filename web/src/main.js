import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import './assets/iconfont/iconfont.css'
import './style.scss'
import router from './router/index'


import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'
import Card from './components/Card.vue'
import ListCard from './components/ListCard.vue'
import axios from 'axios'
import echarts from 'echarts'

Vue.prototype.$echarts = echarts
Vue.prototype.$http = axios.create({
  baseURL:'http://localhost:3000/web/api'
})
Vue.use (VueAwesomeSwiper,/*{default global options}*/)
Vue.component('m-list-card',ListCard)
Vue.component('m-card', Card)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
