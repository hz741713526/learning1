

module.exports=app =>{
    const express = require('express')
    const AdminUser = require('../../models/AdminUser')
    const jwt= require('jsonwebtoken')
    const assert= require('http-assert')
    const router =express.Router({
        mergeParams:true//导入父级参数到子级配置
    })
    const Category = require('../../models/Category')
    //创建资源
    router.post('/', async(req,res)=>{
     const model = await req.Model.create(req.body)
     res.send(model)
    })
    //查询所有资源,加上了中间件处理
    router.get('/', async(req,res)=>{
        const queryOptions = {}
        if (req.Model.modelName==='Category'){
            queryOptions.populate = 'parent'
        }
        const items = await req.Model.find().setOptions(queryOptions).limit(500)
        res.send(items)
    })
    //获取某个详情页
    router.get('/:id', async(req,res)=>{
        const model = await req.Model.findById(req.params.id)
        res.send(model)
    })
    //修改某个详情页
    router.put('/:id', async(req,res)=>{
        const model = await req.Model.findByIdAndUpdate(req.params.id, req.body)
        res.send(model)
    })
    //删除某个详情页
    router.delete('/:id', async(req,res)=>{
        await req.Model.findByIdAndDelete(req.params.id, req.body)
        res.send({
            success:true
        })
    })
    //登录授权中间件
    const authMiddleware = require('../../middlewares/auth')
    //express的中间件功能，该中间件必须有req,res,next三个参数，next确保执行完成后 继续执行下面的程序：将'地址'挂载到路由上之前对其用这个中间件处理       
    const resourceMiddleware = require('../../middlewares/resource')

    app.use('/admin/api/rest/:resource', authMiddleware(), resourceMiddleware(), router)
    //express 本身 无法获取上传文件的数据，multer中间件专门处理上传数据
    //express 中没有req.file multer中间件增加req.file
    const multer = require('multer')
    const upload = multer({dest:__dirname + '/../../uploads'})
    app.post('/admin/api/upload', authMiddleware(), upload.single('file'), async(req,res)=>{
        const file = req.file
        file.url = `http://localhost:3000/uploads/${file.filename}`
        res.send (file)
    })

    app.post('/admin/api/login', async(req,res)=>{
        const{username, password} = req.body//解构赋值
        //1根据用户名找用户 ,由于密码被散列加密不可以通过密码去找用户信息
        const user= await AdminUser.findOne({username:username}).select('+password')
        assert (user,422,'用户名不存在')
        const isValid = require ('bcrypt').compareSync(password, user.password)
        assert (isValid,422,'密码错误')
        //3返回token
        const token = jwt.sign({id: user._id,}, app.get('secret'))
        res.send({token})
    })
    //错误处理函数
    app.use(async(err,req,res,next)=>{
        res.status(err.statusCode || 500).send({
            message: err.message
        })
    })
}