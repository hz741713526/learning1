const Item = require('../../models/Item')

module.exports= app =>{
    const router = require('express').Router()
    const mongoose = require('mongoose')
    const Article =mongoose.model('Article')
    const Category =mongoose.model('Category')
    const Hero =mongoose.model('Hero') 
    const Item =mongoose.model('Item')
    const Ad = mongoose.model('Ad')
    //广告获取接口
    router.get('/ads/list', async(req,res)=>{
      const ads = await Ad.findOne({
        name:'首页轮播图'
      })
      res.send (ads)
    }) 
    router.get('/news/init',async(req,res)=>{
        const parent =await Category.findOne({
            name:'新闻分类'
        })
        const cats = await Category.find().where({
            parent:parent          
        }).lean()
        const newsTitles = [" 恭喜TES夺得2020德玛西亚杯冠军！ ", "英雄麦克疯：过关斩将，剑指桂冠", "2020德玛西亚杯决赛宣传片：英雄，决赛之路", "2020德玛西亚杯宣传片：英雄，不问出处", "2020德玛西亚杯抽签分组出炉", "2020德玛西亚杯参赛名单", "创造历史！电子竞技首次成为亚运会正式比赛项目"," 游戏环境公示及处罚名单12月25日 ", "12月25日免费英雄更换公告", "至臻终章2020活动说明", "12月23日凌晨2点全区停机维护公告", "游戏环境公示及处罚名单12月18日", "2020年12月17日 不停机更新公告", "迎2020全明星 战队限定皮肤限时销售"," TOP5：knight中单薇恩收割团战 ", "2020德杯落幕TES夺冠，广州黄埔区发力电竞游戏产业", "掌盟独家专访Jackeylove：Zhuo是个蛮不错的小孩子", "恭喜knight斩获2020德玛西亚杯决赛FMVP！", "恭喜TES夺得2020德玛西亚杯冠军！", "2020德玛西亚杯决赛开幕式表演", "掌盟看德杯：解说十一带你直击德杯现场"," 【从小白到Uzi】进阶篇，AD的中期思路和怎么搭配辅助 ", "【线霸养成记】杰斯——你的英雄池需要一个全能英雄。", "职业选手怎么玩：莽夫打野重现MLXG皇子打法攻略", "【从小白到Uzi】功能性英雄篇，教你如何四两拨千斤", "楚钧亲笔：第一艾克的赖线技巧，教你一招对线永不崩盘！", "【苏御】看完必上大分！打野核心知识二十条", "【苏御】看完必上大分！中单核心知识二十条"," 英雄联盟海怪大盘点，泰坦派克为何成为水鬼？ ", "电竞百科全书：亚运会冠军教练，堪称LPL活化石的那个男人", "徐老师来巡山297：十秒死三次，塞恩水泉不知所措", "盟物堂：祖安十年老玩家为爱发电 用糖自制远古巨龙霸气外露", "【造萌宗狮】你学会娜美的“星座魔法”了嘛？粘土自制手办。", "徐老师讲故事145：女帝皮肤故事解析 一个只有女英雄的世界", "万万没看到：重忆十年冰雪节"]
        const newsList = newsTitles.map(title =>{
            const randomCats = cats.slice(0).sort((a,b) => Math.random()-0.5)
            return{
                categories:randomCats.slice(0,2),
                title:title
            }
        })
        await Article.deleteMany({})
        await Article.insertMany(newsList)
        res.send(newsList)
    })
    router.get('/news/list', async(req,res)=>{
        const parent = await Category.findOne({
            name: '新闻分类'
        })
        //聚合查询，可以同时执行好几次查询，最终得到想要的结果，聚合查询在程序看来只执行了一次，因此速度会很快
        //match 相当于mysql里的 where， 聚合查询可以理解为 先查A，查到后在A的基础上查B
        const cats = await Category.aggregate([
            {$match:{ parent: parent._id}},
            {
                $lookup:{
                from:'articles',
                localField:'_id',
                foreignField:'categories',
                as:'newsList'
                }
            },
            {
                $addFields:{
                    newsList:{$slice: ['$newsList',5]}
                }
            }
        ])
        cats.map(cat=>{
            cat.newsList.map(news=>{
                news.categoryName = cat.name
                return news
            })
        })
        // const subCats = cats.map(v=> v.id)
        // cats.unshift({
        //     name:'综合(All)',
        //     newsList:await Article.find().where({
        //         categories:{ $in: subCats}
        //     }).limit(5).lean()
        // })
        res.send(cats)
    })
    //录入英雄 测试接口 仅在第一次录入时使用 后续请不要使用此接口
    router.get('/heroes/init', async(req,res)=>{
        await Hero.deleteMany({})
        const rawData = [
          {
            "name": "战士",
            "heroes": [
              {
                "title": "狂战士",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Olaf.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big2000.jpg",
                "story": "奥拉夫是一股无坚不摧的毁灭之力，战斧在手的他别无所求，只想光荣地死在战斗中。奥拉夫来自弗雷尔卓德的海岸半岛洛克法，他曾在一次占卜预言中听闻自己将安详地死去——这是懦夫的命运，也是对他们族人的莫大侮辱。于是，为了追寻另外一种结局，他在狂怒的驱动下在这片土地上暴跳横行，屠杀了数十名伟大的战士和传说中的野兽，希望能够找到能够阻止自己的对手。如今他是凛冬之爪部族残酷的执法者，希望在即将到来的大战中找到自己的终结",
                "usageTips": "奥拉夫可以在生命垂危时组合使用狂战之怒、残暴打击和诸神黄昏，来变得不可思议地强大",
                "battleTips": "在诸神黄昏的持续期间里，奥拉夫防御伤害的能力会降低，但是免疫控制技能。因此，如果你无法摆脱处在诸神黄昏状态下的奥拉夫的话，就尽量和队友一起集火掉他吧。",
                "name": "奥拉夫",
                "defense": "5",
                "magic": "3",
                "difficulty": "3",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Olaf_Passive.png",
                    "title": "狂战之怒",
                    "description": "奥拉夫每损失1%的生命值，就会增加1%的攻击速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OlafAxeThrowCast.png",
                    "title": "逆流投掷",
                    "description": "奥拉夫将战斧投至目标区域，对战斧穿过的敌人造成80/125/170/215/260(+100% 额外物理伤害)物理伤害，并减少他们29/33/37/41/45%的速度，最多持续2.5秒。斧子飞行的距离越远，减速效果的持续时间越长，但持续时间不会短于1.5秒。\n\n如果奥拉夫将斧头捡起，那么该技能的冷却将减少4.5秒。\n\n冷却时间（秒）: 7\n法力值消耗: 60\n范围: 1000"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OlafFrenziedStrikes.png",
                    "title": "残暴打击",
                    "description": "在6秒的持续时间内，奥拉夫获得14/16/18/20/22%生命偷取，并且他的攻击速度会提升55/65/75/85/95%。\n\n在技能持续期间，奥拉夫每损失2%生命值，就会获得1%的治疗效果加成。\n\n冷却时间（秒）: 16\n法力值消耗: 30\n范围: 700"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OlafRecklessStrike.png",
                    "title": "鲁莽挥击",
                    "description": "奥拉夫狂野地挥舞他的双斧，对他的目标造成70/115/160/205/250(+50% 攻击力)真实伤害。这个技能的消耗相当于此技能所造成伤害的30%，但如果此技能将目标击杀，则会返还所有施法消耗。\n\n普通攻击会减少鲁莽挥击1秒冷却时间。\n\n冷却时间（秒）: 11/10/9/8/7\n法力值消耗: 消耗生命值\n范围: 325"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OlafRagnarok.png",
                    "title": "诸神黄昏",
                    "description": "被动：奥拉夫获得20/30/40护甲和20/30/40魔法抗性。\n\n主动：奥拉夫移除身上的所有控制效果，并在6秒的持续时间里免疫任何限制技能。奥拉夫还会在跑向敌方英雄时获得20/45/70%移动速度加成，持续1秒。技能持续期间里，奥拉夫会损失此技能被动部分的抗性加成，并获得15/20/25（+30%AD）攻击力。\n\n冷却时间（秒）: 100/90/80\n法力值消耗: 无消耗\n范围: 400"
                  }
                ],
                "data": {
                  "hp": "597.2400",
                  "hpperlevel": "93.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.9000",
                  "mp": "315.6000",
                  "mpperlevel": "42.0000",
                  "mpregen": "7.4660",
                  "mpregenperlevel": "0.5750",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6940",
                  "attackspeedperlevel": "2.7000",
                  "armor": "35.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "德邦总管",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/XinZhao.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big5000.jpg",
                "story": "赵信是效忠于光盾王朝的坚毅勇士。他曾经沦落于诺克萨斯的角斗场，在无数次角斗中得以幸存。被德玛西亚军队解救以后，他便发誓为这群勇敢的解放者贡献生命和忠诚。最称手的三爪长枪相伴，赵信现在为自己的第二祖国而战，一往无前，暴虎冯河。",
                "usageTips": "赵信是优秀的团战发起者。冲在阵型的前方来开始战斗，并使用你的终极技能来造成最大的伤害。",
                "battleTips": "赵信是个强劲的团战发起者，他的冲锋和终极技能都能够对附近敌方单位造成伤害。 在他使用终极技能时要和队友分散。",
                "name": "赵信",
                "defense": "6",
                "magic": "3",
                "difficulty": "2",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/XinZhaoP.png",
                    "title": "果决",
                    "description": "每第三次攻击或使用风斩电刺命中敌人时，都会造成额外15%AD物理伤害并回复（（10-112【取决于等级】） +10%AD+40%AP）生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XinZhaoQ.png",
                    "title": "三重爪击",
                    "description": "赵信的下3次普攻造成额外的（20/28/36/44/52+40%额外AD）物理伤害并使他的其它技能的冷却时间缩短1秒。第3次普攻还会击飞目标0.75秒。\n\n冷却时间（秒）: 7/6.5/6/5.5/5\n法力值消耗: 30\n範圍: 375"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XinZhaoW.png",
                    "title": "风斩电刺",
                    "description": "赵信进行斩击，造成(30/40/50/60/70+30%AD)物理伤害，随后进行刺击，造成（40/75/110/145/180+80%AP）物理伤害和持续1.5秒的50%减速。\n\n冷却时间（秒）: 12/11/10/9/8\n法力值消耗: 45\n範圍: 900"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XinZhaoE.png",
                    "title": "无畏冲锋",
                    "description": "赵信对一名敌人发起冲锋，造成（50/75/100/125/150+60%AP）魔法伤害和持续0.5秒的30%减速。\n赵信自身还会获得40/45/50/55/60%攻击速度，持续5秒。\n\n冷却时间（秒）: 12\n法力值消耗: 50\n範圍: 650"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XinZhaoR.png",
                    "title": "新月护卫",
                    "description": "赵信环绕自身释放一次横扫，造成（75/175/275+100%额外AD）外加15%当前生命值的物理伤害并击退所有敌人，但不会击退被赵信的最后一次攻击或无畏冲锋命中的目标。\n随后，赵信会免疫来自横扫范围外侧敌人的伤害，持续3秒。每次攻击和使用技能都会使这个效果的持续时间提升0.3秒。\n\n\n冷却时间（秒）: 120/110/100\n法力值消耗: 100\n範圍: 500"
                  }
                ],
                "data": {
                  "hp": "570.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.7000",
                  "mp": "273.8000",
                  "mpperlevel": "35.0000",
                  "mpregen": "7.2560",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "66.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6450",
                  "attackspeedperlevel": "3.5000",
                  "armor": "35.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "无畏战车",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Urgot.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big6000.jpg",
                "story": "厄加特曾一度是诺克萨斯强大的处刑人，但这个让他为之杀人如麻的帝国，最后却背叛了他。铁链束缚着他，并迫使他在一个新的地方懂得了力量的真正意义——祖安地下深处的监牢矿坑——“沉钩”。后来的一场灾难让祖安城中混乱肆虐，厄加特也借机破土而出，在祖安的地下犯罪世界傲视群雄。曾经奴役他的铁链，现在是他折磨猎物的工具，他会用枪火洗礼自己新的家园，肃清那些不配苟活的人，将祖安铸成一座痛苦的熔炉。",
                "usageTips": "命中【腐蚀电荷】或【鄙弃】就能用【净除】来锁定目标——这是一种可让多条腿快速地连续发射的好办法。",
                "battleTips": "厄加特严重依赖他的几条腿来打击对手，而这些腿都有独立的冷却时间并且只会在他攻击腿所面对的方向上的敌人时贡献火力。避免吃到多条腿的火力。",
                "name": "厄加特",
                "defense": "5",
                "magic": "3",
                "difficulty": "8",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Urgot_Passive.png",
                    "title": "回响烈焰",
                    "description": "厄加特的6条腿上各有一门加农炮。攻击和净除时将会发射面对着他的那门腿炮，造成40%AD外加（2-6%基于等级）最大生命值的物理伤害。每条腿的这个效果各有（30-2.5基于等级）秒冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UrgotQ.png",
                    "title": "腐蚀电荷",
                    "description": "厄加特发射一个爆炸性的电荷，对爆炸波及到的敌人造成（25/70/115/160/205+70%AD）物理伤害和持续1.25秒的45%减速。\n\n冷却时间（秒）: 12/11/10/9/8\n法力值消耗: 80\n範圍: 800"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UrgotW.png",
                    "title": "净除",
                    "description": "被动：厄加特的其它技能会将命中的最后一名敌方英雄标记5秒。\n\n主动：厄加特开始用他的链枪朝着相距最近的敌人开火，优先选择被标记的敌人。他每秒攻击目标3次并且每次射击造成（12+20/24/28/32/36%AD）物理伤害。厄加特可以在开火时移动，并且拥有40%减速抗性，但会失去125移动速度。\n\n这个技能在点满技能等级后，可无限持续并且能切换开启状态和关闭状态。\n\n冷却时间（秒）: 12/9/6/3/0\n法力值消耗: 40/30/20/10/0\n範圍: 490"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UrgotE.png",
                    "title": "鄙弃",
                    "description": "厄加特向前冲锋，获得持续4秒的（60/80/100/120/140+150%额外AD+150%额外生命值）护盾值。命中的第一个英雄会被晕眩并被投向厄加特的身后。厄加特碰撞到的所有敌人都会受到（90/120/150/180/210+100%额外AD）物理伤害。\n\n冷却时间（秒）: 16/15.5/15/14.5/14\n法力值消耗: 60/70/80/90/100\n範圍: 475"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UrgotR.png",
                    "title": "超越死亡的恐惧",
                    "description": "厄加特发射一个炼金钻头来刺穿命中的第一个敌方英雄。造成（100/225/350+50%额外AD）物理伤害和持续4秒的减速，幅度为每1%已损失生命值提供1%。\n\n如果目标的生命值在被刺穿状态下跌至25%生命值以下，厄加特就可以再次施放这个技能，来压制目标并将其拖过来。在接触到厄加特后，目标会被击杀并且附近敌人会被恐惧1.5秒。\n\n冷却时间（秒）: 100/85/70\n法力值消耗: 100\n範圍: 2500"
                  }
                ],
                "data": {
                  "hp": "585.0000",
                  "hpperlevel": "88.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.7000",
                  "mp": "340.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "7.2500",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.7500",
                  "armor": "36.0000",
                  "armorperlevel": "4.2500",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "330.0000",
                  "attackrange": "350.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "正义天使",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kayle.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big10000.jpg",
                "story": "在符文战争最激烈的时刻，一位巨神星灵诞下了凯尔，她传承了母亲的荣耀，在圣火飞翼的承载下为了正义而战。她和孪生妹妹莫甘娜曾是德玛西亚的保护神——但凡人反反复复的失败最终让凯尔的幻想破灭，彻底抛弃了这个国度。但关于她用火焰长剑惩罚不义之徒的传说依然在流传，关于她终将回归的希望也依然在持续……",
                "usageTips": "对高伤害输出的友军使用【R圣裁之刻】可以让其进行更加自由的输出，从而扭转战局。",
                "battleTips": "【圣裁之刻】只能让凯尔或她的友军免疫伤害。减速和晕眩技能仍可用。因此，如果他们太过激进，就诱捕他们。",
                "name": "凯尔",
                "defense": "6",
                "magic": "7",
                "difficulty": "7",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kayle_P.png",
                    "title": "登神长阶",
                    "description": "凯尔的攻击会随着她提升等级和分配技能点而获得增强。\n\n-等级1-狂热：普通攻击可使攻击速度提升（6+1%AP）%，持续5秒，最多可叠加5层。在叠满时，凯尔变成昂扬状态，获得8%移动速度。\n\n-等级6-升腾：攻击距离提升至525\n\n-等级11-炽诚：昂扬攻击会激起焰浪，造成（15/20/25/30/35+25%AP+10%额外AD魔法伤害）\n\n-等级16-超然：永久变成昂扬状态，攻击距离提升至575\n\n焰浪附带法术特效并且能够暴击。他们的伤害基于[E]星火符刃的技能等级。。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KayleQ.png",
                    "title": "耀焰冲击",
                    "description": "凯尔召唤一把星界之剑，它会停留在命中的第一个敌人身上。\n\n星界之剑对目标及其身后的敌人造成（60/100/140/180/220+50%AP+60%额外AD）魔法伤害，26/32/38/44/50%减速效果，持续2秒，并击碎15%的护甲和魔法抗性，持续4秒。\n\n\n冷却时间（秒）: 12/11/10/9/8\n法力值消耗: 70/75/80/85/90\n範圍: 900"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KayleW.png",
                    "title": "星界恩典",
                    "description": "凯尔和目标友方英雄被治愈（60/90/120/150/180+30%AP）生命值并获得（24/28/32/36/40%+0.08%AP）移动速度，持续2秒。\n\n冷却时间（秒）: 15\n法力值消耗: 90/100/110/120/130\n範圍: 900"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KayleE.png",
                    "title": "星火符刃",
                    "description": "被动：攻击造成（15/20/25/30/35+20%AP+10%额外AD）额外魔法伤害。\n\n主动：凯尔的下次攻击变为远程并且造成目标（8/9/10/11/12%+0.02%AP）已损失生命值的额外魔法伤害。这个攻击会在凯尔到达11级时升级，使它可以在命中目标时爆炸，对附近敌人造成伤害。\n\n冷却时间（秒）: 8/7.5/7/6.5/6\n法力值消耗: 无消耗\n範圍: 550"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KayleR.png",
                    "title": "圣裁之刻",
                    "description": "凯尔令一名友方英雄免疫伤害2秒。\n\n接着，在一段施放时间后，她会净化该英雄周围的区域并造成（200/350/500+80%AP+100%额外AD）魔法伤害给附近的敌人。\n\n在释放圣裁之刻时，凯尔可以移动但不能攻击。\n\n冷却时间（秒）: 160/120/80\n法力值消耗: 100/50/0 \n範圍: 900"
                  }
                ],
                "data": {
                  "hp": "600.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "5.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "330.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "50.0000",
                  "attackdamageperlevel": "2.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.5000",
                  "armor": "26.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "34.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "无极剑圣",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/MasterYi.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big11000.jpg",
                "story": "易师锤炼身体、磨砺心智，直至身心合一。尽管他将暴力作为不得已的选择，但他优雅迅猛的剑法总是让这一手段显得尤为快捷。作为无极之道最后的门徒，易大师致力于这个门派的传承，用七度洞悉目镜搜寻着最有资格的人，寻找潜在的新弟子。",
                "usageTips": "试着在一名敌方英雄前使用阿尔法突袭来攻击小兵，这样在技能结束的时候，你就处于一个安全距离的位置了。",
                "battleTips": "如果易大师试图用无极剑道补兵，则连续几次攻击他，令其不得不消耗法力使用冥想回复生命值。",
                "name": "易",
                "defense": "4",
                "magic": "2",
                "difficulty": "4",
                "attack": "10",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/MasterYi_Passive1.png",
                    "title": "双重打击",
                    "description": "易大师每第4次攻击时将进行双重打击。第二段攻击造成50%AD物理伤害。\n第二段攻击算是一次常规攻击。它能够暴击并施加攻击特效。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AlphaStrike.png",
                    "title": "阿尔法突袭",
                    "description": "易变得【不可被选取】并对他目标附近的敌人发起快速打击，造成（25/60/95/130/165+100AD）物理伤害给第4击之后命中的所有敌人。\n\n如果没有其他目标，那么这个技能可以重复打击相同敌人，每次后续打击造成30%AD物理伤害。\n\n\n这个技能可以暴击，造成额外的（15/36/57/78/99+60%AD）物理伤害\n野怪受到75/100/125/150/175额外伤害\n攻击可以使这个技能的冷却时间减少1秒\n易将出现在初始目标旁边\n\n冷却时间（秒）: 18/17/16/15/14\n法力值消耗: 50/55/60/65/70\n範圍: 600"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Meditate.png",
                    "title": "冥想",
                    "description": "易开始引导，减少即将到来的60/62.5/65/67.5/70%伤害并在4秒里持续回复共（120/200/280/360/440+100%AP）生命值。这个治疗效果可基于易大师的已损失生命值而提升，最多可提升至100%。\n\n易在引导时将获得双重打击的层数并暂停无极剑道和高原血统的持续时长\n伤害减免效果在作用于来自防御塔的伤害时会减半。在引导结束后，伤害减免效果会存留0.25秒，但效能会降低。\n\n冷却时间（秒）: 28\n法力值消耗: 50\n範圍: 20"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/WujuStyle.png",
                    "title": "无极剑道",
                    "description": "易的攻击造成额外的（18/26/34/42/50+35%额外AD）真实伤害，持续5秒。\n\n冷却时间（秒）: 18/17/16/15/14\n法力值消耗: 无消耗\n範圍: 20"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Highlander.png",
                    "title": "高原血统",
                    "description": "被动：在参与击杀后，易大师的普通技能的剩余冷却时间会减少70%。\n\n主动：获得35/45/55%移动速度，25/45/65%攻击速度，并且免疫减速效果，持续7秒。每次参与击杀后都会使此技能的持续时间延长7秒。\n\n冷却时间（秒）: 85\n法力值消耗: 100"
                  }
                ],
                "data": {
                  "hp": "598.5600",
                  "hpperlevel": "92.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.6500",
                  "mp": "250.5600",
                  "mpperlevel": "42.0000",
                  "mpregen": "7.2560",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "66.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6790",
                  "attackspeedperlevel": "2.0000",
                  "armor": "33.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "355.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "祖安怒兽",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Warwick.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big19000.jpg",
                "story": "沃里克是一头游猎于祖安灰色小巷的怪兽。他的身体接受了痛苦的实验并发生了变异，融合了精密复杂的储液舱和药泵，向他的血管中注入炼金合成的愤怒激素。他从阴影中一跃而出，猎杀那些在城市最深处肆虐的罪犯。沃里克会被鲜血吸引，血腥味让他失去理智。没有哪个沾血的人能够逃过他的猎杀。",
                "usageTips": "【Q野兽之口】会跟随敌人。在你按住这个技能键时，即使敌人用了奔跑、突进或传送技能，沃里克也会跟随他们出现在相应位置。",
                "battleTips": "沃里克普攻会在他生命值较低时治疗他。把你的控制技能留到他残血时用，不要给他反击的机会。",
                "name": "沃里克",
                "defense": "5",
                "magic": "3",
                "difficulty": "3",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/WarwickP.png",
                    "title": "血之饥渴",
                    "description": "沃里克的攻击造成额外的(8-42)魔法伤害。如果沃里克的生命值低于50%，那么他会回复(8-42)生命值。如果他的生命值低于25%，那么他会回复(24-126)生命值。\n额外伤害：12-46 (在1-18级时) (+15%额外攻击力) (+10%法术强度)\n技能说明BUG修复：更新了技能说明，旧版说明错误地标注了血之饥渴的伤害为8-42而非10-44;治疗量应等于预期伤害值而非实际伤害值;如果生命值低于25%，治疗效果为300%而非250%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/WarwickQ.png",
                    "title": "野兽之口",
                    "description": "秒放：沃里克向前猛扑并撕咬，造成(120%AD +100%AP)外加(6%/7%/8%/9%/10%)最大生命值的魔法伤害并回复(30%/45%/60%/75%/90%)实际伤害值的生命值。\n蓄力：沃里克猛扑并用它的狼嘴紧咬目标，并跃到目标身后，在紧咬目标时，沃里克会跟随目标的所有移动。在释放他的蓄力后，他会造成等额伤害和治疗效果。\n冷却时间：6\n法力消耗：50/60/70/80/90"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/WarwickW.png",
                    "title": "鲜血追猎",
                    "description": "被动：沃里克可以感知到低于50%生命值的英雄们，朝他们移动时获得(35%/40%/45%/50%/55%)移动速度并在对抗他们时获得(70%/80%/90%/100%/110%/)攻击速度。这些加成在对抗低于20%生命值的敌人们时会提升至3倍。\n主动：沃里克可以暂时感知所有敌人，并在对抗相距最近的敌人时获得这个技能的被动效果，持续8秒，无论该目标有多少生命值。沃里克无法在与英雄战斗时使用这个技能。\n在没有敌人被感知到时，这个技能的冷却时间速率翻倍。\n冷却时间：120/105/90/75/60\n法力消耗：70"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/WarwickE.png",
                    "title": "远祖嗥叫",
                    "description": "沃里克获得持续2.5秒的(35%/40%/45%/50%/55%)伤害减免。在它结束后，沃里克会发出嗥叫，恐惧附近的敌人1秒。沃里克可以再次释放来提前结束这个技能。\n冷却时间：15/14/13/12/11\n法力消耗：40"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/WarwickR.png",
                    "title": "无尽束缚",
                    "description": "沃里克跳跃一大段距离(随他的移动速度增长)，并压制碰撞到的第一个敌方英雄1.5秒并且自身处在引导状态。再次期间他持续造成共(175/350/525 +167%额外AD)魔法伤害并施加3次攻击特效。沃里克在引导期间会回复100%实际伤害值的生命值。\n冷却时间：110/90/70\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "550.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "4.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "280.0000",
                  "mpperlevel": "35.0000",
                  "mpregen": "7.4660",
                  "mpregenperlevel": "0.5750",
                  "attackdamage": "65.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6380",
                  "attackspeedperlevel": "2.3000",
                  "armor": "33.0000",
                  "armorperlevel": "3.2000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "蛮族之王",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Tryndamere.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big23000.jpg",
                "story": "桀骜不羁和无尽的愤怒驱使着泰达米尔在弗雷尔卓德所向披靡。他曾公开挑战北方最伟大的战士，让自己为未来更黑暗的时代做好准备。这位愤怒的野蛮人一直都在为自己被灭绝的氏族寻求复仇，不过最近他与阿瓦洛萨部族的战母艾希联手，并在她的族群中重新安家。他非人的力量与毅力是众人皆知的传奇，而且也为他和他的新盟友带来了无数次奇迹般的胜利。",
                "usageTips": "推迟激活无尽怒火是促使敌方英雄过度激进地想来杀死你的有效方式。",
                "battleTips": "尽早的骚扰泰达米尔，使其无法击杀小兵、无法通过嗜血杀戮技能回复生命值。",
                "name": "泰达米尔",
                "defense": "5",
                "magic": "2",
                "difficulty": "5",
                "attack": "10",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Tryndamere_Passive.png",
                    "title": "战斗狂怒",
                    "description": "泰达米尔击中一个单位获得5点怒气；每次暴击获得10点怒气，击杀一个单位额外获得10点怒气。在脱离战斗8秒后，泰达米尔每秒损失5点怒气。每点怒气增加+(0.40)%暴击几率。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TryndamereQ.png",
                    "title": "嗜血杀戮",
                    "description": "被动：泰达米尔嗜血成性，获得(5/10/15/20/25)点攻击力，每损失1%生命值额外增加(0.15/0.20/0.25/0.30/0.35)点攻击力。\n主动：泰达米尔消耗怒气，回复(30/40/50/60/70 +30%AP)，每1点怒气额外回复(0.5/0.95/1.4/1.85/2.3)+(1.2%AP)\n冷却时间：12"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TryndamereW.png",
                    "title": "蔑视",
                    "description": "减少身边敌方英雄(20/35/50/65/80)攻击力，持续4秒，并且背对泰达米尔的敌人还会被减速(30%/37.5%/45%/52.5%/60%)\n冷却时间：14"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TryndamereE.png",
                    "title": "旋风斩",
                    "description": "泰达米尔挥舞大剑冲向目标，对冲锋路线上所有敌人造成(80/110/140/170/200 + 130%AD + 80%AP)物理伤害并产生怒气。\n泰达米尔每次暴击减少旋风斩的冷却时间1秒。这个冷却缩减会在攻击英雄时提升至2秒。\n冷却时间：12/11/10/9/8"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UndyingRage.png",
                    "title": "无尽怒火",
                    "description": "泰达米尔在5秒内对死亡免疫，生命最低为(30/50/70)，并瞬间获得(50/75/100)怒气。\n冷却时间：110/100/90"
                  }
                ],
                "data": {
                  "hp": "625.6400",
                  "hpperlevel": "98.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.9000",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "69.0000",
                  "attackdamageperlevel": "3.7000",
                  "attackspeed": "0.6700",
                  "attackspeedperlevel": "2.9000",
                  "armor": "33.0000",
                  "armorperlevel": "3.1000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "武器大师",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Jax.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big24000.jpg",
                "story": "无论是各种兵器的技法，还是刻薄的挖苦嘲讽，贾克斯都无人能及，他是目前已知的最后一名艾卡西亚武器大师。曾经，故乡的人们狂妄自大地引来了虚空，结果导致家园被夷为平地。在那之后，贾克斯和他的同胞发誓要保护仅存的一切。如今，世界上的魔法再次涌起，沉睡的威胁也再次被触动。于是贾克斯开始在瓦洛兰漫游，手握艾卡西亚的最后光明，考验他遇到的每一名战士，寻找可与自己分庭抗礼的强者，并肩作战。",
                "usageTips": "贾克斯的跳斩能对友军施放，包括侦查守卫，你可以利用这个逃离敌人的追杀。",
                "battleTips": "贾克斯可以在短时间内躲闪掉所有即将到来的普通攻击，并在结束后晕眩周围的敌军。请等到他的躲闪结束后再发起攻击。",
                "name": "贾克斯",
                "defense": "5",
                "magic": "7",
                "difficulty": "5",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Armsmaster_MasterOfArms.png",
                    "title": "无情连打",
                    "description": "贾克斯的普通攻击会提高他(3.50%/5.0%/6.50%/8.0%/9.50%/11%/)的攻击速度，持续2.5秒。此效果最多可堆叠8层，并且会逐层衰减。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JaxLeapStrike.png",
                    "title": "跳斩",
                    "description": "贾克斯跳向目标单位，如果目标是敌人则会攻击，造成(65/105/145/185/225 +100%额外AD +60%AP)物理伤害。\n冷却时间：8/7.5/7/6.5/6\n法力消耗：65"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JaxEmpowerTwo.png",
                    "title": "蓄力一击",
                    "description": "贾克斯给他的武器充能，使他的下次攻击或[Q跳斩]造成额外(40/75/110/145/180 +60%AP)魔法伤害。\n冷却时间：7/6/5/4/3\n法力消耗：30"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JaxCounterStrike.png",
                    "title": "反击风暴",
                    "description": "贾克斯进入一个最多持续2秒的防御姿态，能够躲闪掉一切即将到来的普通攻击，并且减少25%来自范围技能的伤害。\n2秒之后，或再次激活此技能后，贾克斯会将周围的敌人打晕1秒，并对他们造成(55/80/105/130/155 +50%额外AD)物理伤害。\n贾克斯每躲掉一次攻击，则反击风暴的伤害值会提升20%(最多可提升100%的伤害)。\n冷却时间：14/12.5/11/9.5/8\n法力消耗：50/60/70/80/90"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JaxRelentlessAssault.png",
                    "title": "宗师之威",
                    "description": "被动：每滴3次连续的攻击会造成(100/140/180 +70%AP)额外魔法伤害。\n主动：贾克斯增强他的决心，提升(30/50/70 + 50%额外AD)护甲和 (30/50/70+ 20%法强)魔法抗性，持续8秒。\n冷却时间：80\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "592.8000",
                  "hpperlevel": "85.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "338.8000",
                  "mpperlevel": "32.0000",
                  "mpregen": "7.5760",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.3750",
                  "attackspeed": "0.6380",
                  "attackspeedperlevel": "3.4000",
                  "armor": "36.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "刀锋舞者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Irelia.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big39000.jpg",
                "story": "诺克萨斯对艾欧尼亚的占领催生了许多英雄，但没有谁像纳沃利的艾瑞莉娅一般令人意外。她将家乡的古老舞艺化为战技，以精心修习的优雅身姿操控着致命的刀丛。在她证明了自己的战斗实力后，被众人推举为反抗军的领袖和首脑，为了守卫家园而奋斗至今。",
                "usageTips": "在你前往目标英雄的途中，先用【利刃冲击】攻击低生命的小兵，再用【利刃冲击】攻击目标英雄，便可瞬间移动很远的距离",
                "battleTips": "多关注【艾欧尼亚狂热】的层数，并且试图在它刚消失时与艾瑞莉娅开战。",
                "name": "艾瑞莉娅",
                "defense": "4",
                "magic": "5",
                "difficulty": "5",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Irelia_Passive.png",
                    "title": "艾欧尼亚热诚",
                    "description": "艾瑞莉娅在用一个技能命中一名敌人时，会获得可一层持续6秒的效果（最大5层）。每层效果为她提升8%/12%/16%攻击速度，并在满层时她的攻击会造成额外的15~66(1-18级 每级+3)（+25%*AD）魔法伤害。\n\n攻击一名英雄或大型野怪，会刷新效果的持续时间。艾瑞莉娅每命中一名英雄都会获得一层效果，命中多个小兵时仍获得一层效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IreliaQ.png",
                    "title": "利刃冲击",
                    "description": "艾瑞莉娅冲向一名敌人，造成5/25/46/65/85（+60%*AD）物理伤害并回复自身12%/14%/16%/18%/20%*AD生命值。如果敌人阵亡或者重心不稳，那么会刷新冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IreliaW.png",
                    "title": "距破之舞",
                    "description": "开始蓄力：艾瑞莉娅进入防御姿态，至多持续1.5秒，变得无法行动但使所受的物理伤害降低（50+7%*AP）%。\n\n释放：艾瑞莉娅放出她的刀锋，造成10/25/40/55/70（+50%*AD +40%*AP）物理伤害，基于蓄力时间至多提升至20/50/80/110/140（+100%*AD +80%*AP）。\n\n释放将在蓄力结束时自动发生且无法打断。在蓄力0.75秒后达到最大伤害提升。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IreliaE.png",
                    "title": "比翼双刃",
                    "description": "艾瑞莉娅扔出一把刀锋至地上，并且至多可在3.5秒里再次施放。\n\n再次施放：艾瑞莉娅扔出第二把刀锋，随后两把刀锋会朝着彼此飞行，对沿途的敌人造成0.75秒晕眩和80/125/170/215/260（+80%*AP）魔法伤害。英雄和大型野怪会进入重心不稳状态，持续5秒。\n\n艾瑞莉娅在3.5秒后会在她所在的区域自动再次施放。艾瑞莉娅能在被控制效果影响时再次施放。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IreliaR.png",
                    "title": "先锋之刃",
                    "description": "艾瑞莉娅发射出一阵刀锋弹幕，造成125/250/375（+70%*AP）魔法伤害并使英雄和大型野怪重心不稳，持续5秒。刀锋弹幕会在命中第一个英雄后绽放成一个牢笼，持续2.5秒。牢笼会造成125/250/375（+70%*AP）魔法伤害并减速90%，持续1.5秒。"
                  }
                ],
                "data": {
                  "hp": "580.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.8500",
                  "mp": "350.0000",
                  "mpperlevel": "30.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6560",
                  "attackspeedperlevel": "2.5000",
                  "armor": "36.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "200.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "海洋之灾",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Gangplank.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big41000.jpg",
                "story": "诡计多端、心狠手辣的普朗克是被废黜的强盗之王，他令人恐惧的名号广达远至。他一度是港口城市比尔吉沃特的统治者。虽然现在他的威权已经不再，但人们相信这只会让他变得更加可怖。普朗克若是知道有人要从他手中抢走比尔吉沃特，必然会大肆血洗这座城市。而如今有了火枪、弯刀，还有一桶桶的火药，他决心要夺回自己失去的东西",
                "usageTips": "枪火谈判可以附带像冰霜之锤或黑色切割者的攻击附带效果。",
                "battleTips": "枪火谈判可以造成高额的物理伤害。如果敌人的普朗克表现不错，那么提供护甲的物品将对你大有裨益。",
                "name": "普朗克",
                "defense": "6",
                "magic": "4",
                "difficulty": "9",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Gangplank_Passive.png",
                    "title": "烈火审讯",
                    "description": "每过15秒，普朗克的下一次近战攻击就会点然目标，在2.5秒里持续造成共55-225(100% 额外AD)额外真实伤害(对防御塔造成一半伤害)，并为普朗克提供30%移速加成，持续两秒。\n摧毁一个火药桶时会刷新烈火审讯并为普朗克提供移动速度。\n【Q枪火谈判】不会激活烈火审讯"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GangplankQWrapper.png",
                    "title": "枪火谈判",
                    "description": "发一颗子弹，造成20/45/70/95/120+(100%AD)物理伤害(可以暴击并施加攻击特效)。\n如果这个技能击杀了目标，那么普朗克会掠夺3/4/5/6/7额外金币和4/5/6/7/8银蛇币。(可在商店中用银蛇币来升级R加农炮幕)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GangplankW.png",
                    "title": "坏血病疗法",
                    "description": "普朗克吃掉大量柑橘类水果，来解除所有限制效果并治疗自身50/75/100/125/150(+0.9*AP)生命值+15%的已损失生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GangplankE.png",
                    "title": "火药桶",
                    "description": "在一个区域放置一个普朗克和敌人们都可以攻击的火药桶，持续25秒。\n如果普朗克摧毁了一个火药桶，那么那个火药桶会爆炸，对范围内的敌人造成相当于这次攻击伤害的物理伤害(无视40%护甲)和40%/50%/60%/70%/80%的减速效果，持续2秒。爆炸会对敌方英雄造成80/105/130/155/180额外物理伤害。\n当一个火药桶爆炸时，会让与这个火药桶有重叠区域的其他火药桶产生连环爆炸(伤害不会叠加)。\n火药桶的生命值每2/1/0.5秒衰减一次。(衰减速率在7级和13级时提升)\n火药桶在爆炸时会施加【Q枪火谈判】的掠夺效果\n充能时间：18/16/14/12/10"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GangplankR.png",
                    "title": "加农炮幕",
                    "description": "给普朗克的船发射信号弹，让它在8秒里朝着目标区域发射共12波加农炮弹。每波炮弹造成40+(0.1*AP)魔法伤害并使敌人减速30%，持续0.5秒。\n用【Q枪火谈判】收集银蛇币，然后在商城中升级此技能。\n每波伤害：40/70/100"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "82.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "282.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.5000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "3.2000",
                  "armor": "35.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "巨魔之王",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Trundle.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big48000.jpg",
                "story": "特朗德尔是一个粗鄙且狡猾的巨魔，性格非常顽劣。没有什么东西不能被他打到屈服认输，甚至是弗雷尔卓德本身。他的领土意识极强，任何进入他领地的蠢蛋都会被他追杀。巨大的臻冰棍棒随时伺候。他会让敌人感到刺骨寒冷，并且用锯齿状的冰柱刺穿他们，最后在他们血溅冰原的时候放声大笑。",
                "usageTips": "特朗德尔擅长在冰封领域内战斗。试着把敌人引到此区域内。",
                "battleTips": "特朗德尔在特定地形下作战能力超强。试着让他离开冰封领域。",
                "name": "特朗德尔",
                "defense": "6",
                "magic": "2",
                "difficulty": "5",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Trundle_Passive.png",
                    "title": "国王的贡品",
                    "description": "每当附近有一个敌方单位死亡，特朗德尔都会获得相当于其2%-7%最大生命值的治疗效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TrundleTrollSmash.png",
                    "title": "利齿撕咬",
                    "description": "特朗德尔的下次攻击会撕咬目标，造成20/40/60/80/100(+110%/120%/130%/140%/150%)物理伤害和短暂的减速效果。\n该攻击会增加特朗德尔20/25/30/35/40攻击力，持续5秒，而敌人则减少此数值一半的攻击力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/trundledesecrate.png",
                    "title": "冰封领域",
                    "description": "特朗德尔用寒冰覆盖目标区域8秒，同时获得20%/28%/36%/44%/52%移动速度，20%/40%/60%/80%/100%攻击速度，并且会从所有的治疗和回复手段中获得25%的额外治疗效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TrundleCircle.png",
                    "title": "寒冰之柱",
                    "description": "特朗德尔在目标位置召唤一根持续6秒的寒冰之柱，令所有人无法通过，并减缓附近敌方单位20%/30%/40%/50%/60%移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TrundlePain.png",
                    "title": "强权至上",
                    "description": "特朗德尔吸取目标20%/27.5%/35%(+0.02%AP)最大生命值(算作魔法伤害)和40%护甲和魔抗，其中一半会立刻吸取掉，另一半会在5秒里持续偷取。护甲和魔法抗性会在吸取结束的4秒后返还给目标。"
                  }
                ],
                "data": {
                  "hp": "616.2800",
                  "hpperlevel": "96.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "281.6000",
                  "mpperlevel": "45.0000",
                  "mpregen": "7.5080",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6700",
                  "attackspeedperlevel": "2.9000",
                  "armor": "37.0000",
                  "armorperlevel": "2.7000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "荒漠屠夫",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Renekton.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big58000.jpg",
                "story": "雷克顿是一位来自恕瑞玛炙热沙漠中的面目可怖、野蛮狂怒的飞升者。他曾经是帝国最受尊敬的战士，带领恕瑞玛的军队取得过无数次胜利。然而，在帝国陨落以后，雷克顿被困在了沙漠之下，慢慢地，在世界变迁的同时，雷克顿丧失了理智。现在他重获自由，但却被一个执念吞噬：找到并杀死他的哥哥内瑟斯，因为疯狂之中的他坚信内瑟斯是害他经受数百年黑暗束缚的罪魁祸首。",
                "usageTips": "横冲直撞非常适合骚扰。先释放技能突入，触发后使用直撞返回安全的位置。",
                "battleTips": "在雷克顿有足够怒气时，要格外小心，因为它通常标志着他准备发起攻击了。",
                "name": "雷克顿",
                "defense": "5",
                "magic": "2",
                "difficulty": "3",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Renekton_Passive.png",
                    "title": "怒之领域",
                    "description": "雷克顿每次攻击能获得5点怒气。拥有50点以上的怒气,其技能效果色得到提升,但每次使用技能都会消耗50点怒气。不作战时，雷克顿秒损失4点怒气。当雷克顿生命值低于50%时,获得的怒气增加50%。\n冷却时间：0秒"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RenektonCleave.png",
                    "title": "暴君狂击",
                    "description": "雷克顿挥动武器,对附近敌人造成65/100/135/170/205物理伤害,每击中一个小兵提供3/4.5/6/7.5/9(+0.04额外AD)点治疗量和2.5点怒气,每击中一个英雄提供12/18/24/30/36(+0.16额外AD)点治疗量和10点怒气.消耗50点怒气:伤害增加至100/150/200/250/300.治疗量提高为每击中一个小兵提供9/13.5/18/22.5/27(+0.08额外AD)点，每击中一个英雄提供36/54/72/90/108(+0.24额外AD)点治疗量和10点怒气(装备AD加成:0.08)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RenektonPreExecute.png",
                    "title": "冷酷捕猎",
                    "description": "提供50攻击距离加成。雷克顿的下次攻击挥动武器2次，造成物理伤害(10/30/50/70/90+攻击力的150%)并晕眩敌人0.75秒，攻击英雄将产生10点怒气.该攻击可附带特效。消耗50点怒气:雷克顿挥动武器3次，破坏目标现存的护盾，并造成物理伤害(15/45/75/105/135+攻击力的225%)并晕眩敌人1.25秒。(AD加成:0.75)\n蓝耗：无消耗或50怒气"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RenektonSliceAndDice.png",
                    "title": "横冲直撞",
                    "description": "横冲:雷克顿冲向前,造成30/60/90/120/150物理伤害,并生成怒气。若击中敌人则可以在4秒内使用直撞。直撞:雷克顿冲向前,造成40/70/100/130/160(+0.9AD)额外物理伤害。直撞―消耗50点怒气:伤害增加至70/115/160/205/250(+1.35额外AD)。被击中的敌人护甲减少25%/27.5%/30%/32.5%/35%，持续4秒。攻击非英雄目标每个目标产生2.5点怒气，攻击英雄目标每个目标产生10点怒气(装备AD加成:0.9)\n蓝耗：无消耗或50怒气"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RenektonReignOfTheTyrant.png",
                    "title": "终极统治",
                    "description": "立即获得20点怒气,同时雷克顿集聚黑暗能量,持续15秒，获得250/500/750生命值(扭曲丛林生命值加成降低)。激活时，会每秒对周围敌人造成40/80/120扭曲丛林伤害会降低)魔法伤害,并且会每秒获得5点怒气。(AP加成:0.1)"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "87.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "69.0000",
                  "attackdamageperlevel": "3.7500",
                  "attackspeed": "0.6650",
                  "attackspeedperlevel": "2.7500",
                  "armor": "35.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "德玛西亚皇子",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/JarvanIV.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big59000.jpg",
                "story": "皇子嘉文四世是皇家的血脉，意味着他便是德玛西亚的下一任国王。他自小被寄予厚望，有朝一日能够成为德玛西亚的楷模，而如此沉重的负担令他的心中充满了挣扎。在战场上，他英勇无畏的气势和一往无前的决心鼓舞着全军上下，显现出身为人主的真实才干。",
                "usageTips": "你可以配合使用巨龙撞击和德邦军旗，离开终极技能形成的环形障碍。",
                "battleTips": "远离嘉文四世和他的军旗之间的路径，以免被击飞。",
                "name": "嘉文四世",
                "defense": "8",
                "magic": "3",
                "difficulty": "5",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/JarvanIV_MartialCadence.png",
                    "title": "战争律动",
                    "description": "嘉文四世的攻击会造成(8%目标当前生命值,最大值400)的额外物理伤害，这个效果对同一目标6秒内无法再次触发"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JarvanIVDragonStrike.png",
                    "title": "巨龙撞击",
                    "description": "用嘉文四世的长矛穿透敌人，造成90/130/170/210/250(+1.2额外AD)物理伤害,减少路径上所有敌人10/14/18/22/26%的护甲持续3秒。如果该技能指向德邦军旗,则嘉文四世会被引向军旗,并击飞沿途的所有敌人。即使是在被禁锢时，这个位移效果仍然能够触发(AD加成:1.2额外AD)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JarvanIVGoldenAegis.png",
                    "title": "黄金圣盾",
                    "description": "暂时获得一个拥有60/85/110/135/160(+嘉文四世最大生命值的1.5%/每命中敌方一名英雄)点生命值的护盾，持续5秒，同时减缓周围敌人15/20/25/30/35%的速度,持续2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JarvanIVDemacianStandard.png",
                    "title": "德邦军旗",
                    "description": "被动:获得20/22.5/25/27.5/30%攻击速度加成.主动:投掷一柄德邦军旗,对敌人造成80/120/160/200/240魔法伤害,嘉文获得双倍攻速加成,并使附近友军获得被动效果同样数值的攻速，持续8秒。其中,军旗的视野半径为700(AP加成:0.8)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JarvanIVCataclysm.png",
                    "title": "天崩地裂",
                    "description": "勇猛地跃向敌人，并对被【R天崩地裂】框住的所有单位造成200/325/450物理伤害，并在目标周围形成环形障碍,持续3.5秒。再次激活，将使障碍倒塌。(装备AD加成:1.5)"
                  }
                ],
                "data": {
                  "hp": "570.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.7000",
                  "mp": "300.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "6.5000",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "3.4000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.5000",
                  "armor": "34.0000",
                  "armorperlevel": "3.6000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "齐天大圣",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/MonkeyKing.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big62000.jpg",
                "story": "悟空是一个瓦斯塔亚族的机灵鬼，用自己的力量、灵敏和机智迷惑对手并抢得先机。机缘巧合让他结识了一位剑客并与之成为一生的挚友，这位剑客被人称作易大师。后来，悟空就成为了古老武术门派“无极”的最后一位弟子。如今，附魔长棍傍身的悟空，目标是让艾欧尼亚免遭崩溃的命运。",
                "usageTips": "配合使用腾云突击和真假猴王能够快速攻击敌人并在他们报复之前撤退。",
                "battleTips": "孙悟空通常会在使用腾云突击后使用真假猴王。保留技能，确保你击打的是孙悟空真身。",
                "name": "孙悟空",
                "defense": "5",
                "magic": "2",
                "difficulty": "3",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/MonkeyKingStoneSkin.png",
                    "title": "金刚不坏",
                    "description": "在1-18级时获得5-9护甲,每5秒回复0.5%最大生命值,每当孙悟空或者其分身命中一名敌方英雄或命中野怪时，额外增益提升50%，持续5秒(最多可叠加10次，总额外增益500%)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MonkeyKingDoubleAttack.png",
                    "title": "粉碎打击",
                    "description": "美猴王悟空用惊人的速度挥动他的金箍棒砸向目标，施放时间随攻击速度而改变。攻击造成额外20/45/70/95/120 (+0.5额外攻击力)，并3秒内降低目标的10/15/20/25/30%的物理防御。每当孙悟空或其分身用普通攻击或者技能造成伤害，【Q-粉碎打击】的冷却时间缩短0.5秒"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MonkeyKingDecoy.png",
                    "title": "真假猴王",
                    "description": "(隐形效果)美猴王悟空用他的敏捷身手戏耍他的敌人，进行一段位移(不可穿墙)进入隐身状态1秒并留下一个不可控制的幻象，孙悟空的分身会模仿孙悟空的普通攻击和终极技能,但是仅造成35/40/45/50/55%伤害。分身将会尝试去攻击真身近期攻击过的敌人们。分身存在时使用【Q-粉碎打击】,分身的下一次普通攻击将被强化分身存在时使用【E-腾云突击】,分身将获得额外攻击速度分身存在时使用【R-大闹天宫】,分身将开始旋转，且若敌方英雄没有被真身的【R-大闹天宫】击飞,分身将可以击飞这些英雄(AP加成:0.6)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MonkeyKingNimbus.png",
                    "title": "腾云突击",
                    "description": "美猴王悟空用他的筋斗云冲向敌方目标,如果目标附近有敌方英雄则悟空会最多分出2个分身来攻击目标,分身造成80/110/140/170/200(+0.8法术强度)魔法伤害。使用后悟空增加40/45/50/55/60%攻速,持续5秒。(额外AP加成:0.8)\n\n【新增】现在，【腾云突击】对野怪的伤害提升50%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MonkeyKingSpinToWin.png",
                    "title": "大闹天宫",
                    "description": "美猴王悟空展开他的金箍棒,不断旋转，对身周敌人每秒造成4-8%最大生命值(+1.1总攻击力)点伤害并且击飞目标,悟空可以在8秒内再次施放终极技能。第二段施放将再次击飞敌人，击飞持续0.6秒，旋转持续2秒。悟空在技能释放期间获得20%额外移动速度。在施放技能0.5秒后可取消旋转，也可通过其他技能取消旋转，在旋转期间，来自【E-腾云突击】的攻击速度加成的持续时间将刷新。(AD加成:1.1)"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "4.0000",
                  "hpregenperlevel": "0.6500",
                  "mp": "300.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.6500",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.7110",
                  "attackspeedperlevel": "3.0000",
                  "armor": "31.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "28.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "盲僧",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/LeeSin.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big64000.jpg",
                "story": "李青是艾欧尼亚古老武术的大师，讲原则、重信义的他能将神龙之灵的精粹运用自如，助他面对任何挑战。虽然他多年前便已双目失明，但这位武僧依然献出自己的全部力量，用生命捍卫家园，抵御任何胆敢打破这里神圣均衡的人。所有因他安静冥想的举动而掉以轻心的敌人都将品尝他燃烧的拳头和炽烈的回旋踢。",
                "usageTips": "施放猛龙摆尾前先用天音波，这样李青可以用回音击瞬间接近目标。",
                "battleTips": "团战时猛龙摆尾有很强的输出。尽量分散站位以减少受到的冲击。",
                "name": "李青",
                "defense": "5",
                "magic": "3",
                "difficulty": "6",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/LeeSinPassive.png",
                    "title": "疾风骤雨",
                    "description": "李青施放技能后,接下来的2次基础攻击增加40%攻击速度并回复能量，第一次攻击回复20/30/40 (于1/7/13级)点,第二次回复10/15/20 (于1/7/13级)点。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BlindMonkQOne.png",
                    "title": "天音波/回音击",
                    "description": "天音波:李青发出刺耳的声波定位敌人，对首个敌人造成55/80/105/130/155(+1.0)物理伤害。如果天音波击中敌人，李青在接下来3秒可施放回音击。回音击:李青冲向被天音波击中的敌人，造成55/80/105/130/155(+1.0)物理伤害,并将基于目标已损失生命值，对其造成最高100%伤害提升。(额外AD加成:1.0)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BlindMonkWOne.png",
                    "title": "金钟罩/铁布衫",
                    "description": "金钟罩:李青冲向友军,提供护盾保护自己和目标免受55/110/165/220/275伤害,持续2秒。如果施放在李青或一位友军英雄身上,那么冷却时间减少50%。施放金钟罩后接下来3秒，李青可施放铁布衫。铁布衫:李青高强度的训练让他们在战斗中激发潜能。李青可以在5秒内获得10/15/20/25/30%生命偷取和法术吸血。(AP加成:0.8AP加成:0.8)\n能量消耗:50/50/50/50/50\n能量/30/30/30/30/30"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BlindMonkEOne.png",
                    "title": "天雷破/摧筋断骨",
                    "description": "天雷破:李青撞击地面,施放冲击波,造成100/140/180/220/260(+装备物理攻击)点魔法伤害,并使被击中单位暴露,持续4秒。如果天雷破击中敌人，李青可以在接下来的3秒内施放催筋断骨。催筋断骨:李青致残被天雷破侦查到的敌人，持续4秒，减少敌人的移动速度20%/30%/40%/50%/60%。持续期间受影响单位的移动速度会逐渐恢复正常。(额外AD加成:1.0)\n能量消耗:50/50/50/50/50\n能量/30/30/30/30/30"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BlindMonkRKick.png",
                    "title": "猛龙摆尾",
                    "description": "李青用强力的回旋踢击退敌方英雄,对目标以及被目标撞到的任何敌人造成175/400/625物理伤害。被目标撞到的敌方英雄会被短暂击飞。每当目标与一名敌方英雄产生碰撞时，这名敌方英雄就会受到额外物理伤害,伤害值相当于目标额外生命值的12/15/18%。(额外AD加成:2.0)"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.7000",
                  "mp": "200.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "50.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "70.0000",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6510",
                  "attackspeedperlevel": "3.0000",
                  "armor": "33.0000",
                  "armorperlevel": "3.7000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "水晶先锋",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Skarner.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big72000.jpg",
                "story": "斯卡纳是一只身形庞大的水晶蝎，来自于恕瑞玛的一处隐秘的山谷。作为古老的壳人族，斯卡纳和他的同胞因卓然的智慧和与大地深切的联系而闻名。他们的灵魂都得到过生命水晶的加持，所以祖先的记忆与现世的思绪都被完好地保存了下来。在久远的过去，壳人族为了躲避神秘的魔法灾难而进入了长眠，然而现在，新的威胁唤醒了斯卡纳。作为整个部族中唯一醒来的人，他将拼尽全力保护其余的同胞，免遭任何人的迫害。",
                "usageTips": "你的普通攻击会减少【Q水晶横扫】的冷却时间，因此可以在两次【Q水晶横扫】之间填充普通攻击，来最大化伤害输出。",
                "battleTips": " 只要斯卡纳的【W水晶蝎甲】释放的护盾存在，他的移动速度和就会得到提升。打破他的蝎甲，就能减缓他的移动速度。",
                "name": "斯卡纳",
                "defense": "6",
                "magic": "5",
                "difficulty": "5",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Skarner_Passive.png",
                    "title": "水晶尖塔",
                    "description": "斯卡纳的出场会导致数座水晶尖塔出现在遍布地图的若干特定地点上。双方队伍可以通过靠近水晶尖塔来占据它们。一旦某座水晶尖塔被占据后，在接下来的15秒内，另一队都无法再次占据那座水晶尖塔。在己方队伍控制的水晶尖塔附近时，斯卡纳会获得70-120 (1-18级）点移动速度加成，43~160% (1-18级)攻击速度加成，并且每秒回复2%的最大法力值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SkarnerVirulentSlash.png",
                    "title": "水晶横扫",
                    "description": "斯卡纳对附近所有敌人造成20%总攻击力(+1/1.5/2/2.5/3%目标最大生命值)攻击力的物理伤害,若击中一个目标，则斯卡纳会充盈水晶能量,持续5秒.\n在斯卡纳充盈着水晶能量时,下次水晶横扫将造成额外15%总攻击力(+1/1.5/2/2.5/3%目标最大生命值)(+0.3法术强度)伤害。普通攻击会使【Q水晶横扫】的冷却时间减少0.25秒(目标时敌方英雄的话，则为1秒)(AD加成:0.8)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SkarnerExoskeleton.png",
                    "title": "水晶蝎甲",
                    "description": "斯卡纳的护盾能吸收10/11/12/13/14%最大生命值的伤害，持续6秒。护盾存在时,斯卡纳会在3秒的持续时间里获得逐步提升的移动速度加成，这个移动速度加成可提升至16/20/24/28/32%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SkarnerFracture.png",
                    "title": "晶体破碎",
                    "description": "斯卡纳召唤一道晶状能量的爆裂波,对命中的所有敌人造成40/65/90/115/140(+0.4x法术强度)魔法伤害，同时还为目标施加持续5秒的【水晶毒素】效果，使斯卡纳对目标的下次普攻造成30/50/70/90/110额外物理伤害和1.2秒晕眩。用【E晶体碎裂】和【R晶状毒刺】将敌人水晶化之后，都会在控制效果持续期间为斯卡纳提供【水晶充能】(在已激活的水晶尖塔附近时所获得的增益状态)，并使【E晶体破碎】的冷却时间减少(AP加成:0.4)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SkarnerImpale.png",
                    "title": "晶状毒刺",
                    "description": "斯卡纳压制敌人1.75/1.75/1.75秒，造成20/60/100(+0.5)魔法伤害，同时在技能的开始和结束阶段造成相当于斯卡纳60%总攻击力的额外物理伤害。在此期间,斯卡纳能自由移动并拖拽敌人。此效果结束后，斯卡纳的目标将会受到额外100/150/200(+0.5)魔法伤害。(AP加成:0.5)"
                  }
                ],
                "data": {
                  "hp": "601.2800",
                  "hpperlevel": "90.0000",
                  "hpregen": "9.0000",
                  "hpregenperlevel": "0.8500",
                  "mp": "320.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.2060",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "65.0000",
                  "attackdamageperlevel": "4.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1000",
                  "armor": "38.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "沙漠死神",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Nasus.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big75000.jpg",
                "story": "内瑟斯是一位庄严威武的犬首人身飞升者，在古代恕瑞玛帝国时期，是被沙漠子民敬仰为半神的英雄人物。作为知识的守护者和无双的战术家，他用高绝的智慧引导着古代恕瑞玛帝国在数百年间走向了繁荣伟大。帝国陨落以后，他开始了自我放逐，成为了人们口中缥缈的传说。现在，恕瑞玛古城已经再一次崛起，他也随之回归，并决心绝不让它再度陨落。",
                "usageTips": "如果你防御力低下，人们就会集中攻击你，就算是你在终极技能的持续时间中。即使你要走物攻路线，也要尽量购买一些提高生存能力的装备。",
                "battleTips": "内瑟斯容易被放风筝（远程英雄保持距离让对手不能攻击到自己，但自己能攻击到对手）。尽量不要与满状态的内瑟斯交战。",
                "name": "内瑟斯",
                "defense": "5",
                "magic": "6",
                "difficulty": "6",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Nasus_Passive.png",
                    "title": "吞噬灵魂",
                    "description": "内瑟斯获得12%/18%24%(1-18级)生命偷取加成。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NasusQ.png",
                    "title": "汲魂痛击",
                    "description": "内瑟斯的下次攻击将造成30/50/70/90/110（+100%*AD）物理伤害。\n\n如果敌方单位死于【汲魂痛击】，则【汲魂痛击】永久增加3伤害。这个加成会在对付英雄、大型小兵和大型野怪时提升至12。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NasusW.png",
                    "title": "枯萎",
                    "description": "内瑟斯使目标英雄衰老，持续5秒，减少其35%移动速度，在持续期间减速效果逐渐提升至47%/59%/71%/83%/95%。该目标被减少的攻击速度为该数值的一半。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NasusE.png",
                    "title": "灵魂烈焰",
                    "description": "内瑟斯在目标区域施放灵魂烈焰,造成55/95/135/175/215（+60%*AP）魔法伤害。\n接下来的5秒，削弱区域内敌人25%/ 30%/ 35%/ 40%/ 45%护甲并每秒造成11/19/27/35/43（+12%*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NasusR.png",
                    "title": "死神降临",
                    "description": "沙漠风暴赐予内瑟斯力量，使他在15秒的持续时间内获得300/450/600生命值加成，以及 40/55/70护甲与魔法抗性。\n\n在风暴肆虐时，他每秒对周围目标造成目标3%/4%/5%（+0.01%*AP）最大生命值的魔法伤害(每秒的伤害上限为240)，并且【汲魂痛击】的冷却时间缩短50%。"
                  }
                ],
                "data": {
                  "hp": "561.2000",
                  "hpperlevel": "90.0000",
                  "hpregen": "9.0000",
                  "hpregenperlevel": "0.9000",
                  "mp": "325.6000",
                  "mpperlevel": "42.0000",
                  "mpregen": "7.4400",
                  "mpregenperlevel": "0.5000",
                  "attackdamage": "67.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6380",
                  "attackspeedperlevel": "3.4800",
                  "armor": "34.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "兽灵行者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Udyr.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big77000.jpg",
                "story": "乌迪尔不只是一个人而已；他体内承载着四个原始兽灵的不羁能量。在与这些兽灵的野性进行心灵感应时，乌迪尔可以驾驭它们独特的力量：猛虎让他矫健凶猛，灵龟为他提供韧劲，巨熊是蛮力的源头，而凤凰为他带来永恒的烈焰。结合它们的能量，乌迪尔就能击退那些妄图危害自然秩序的人。",
                "usageTips": "切换龟灵状态后，护盾吸收的伤害是计算过魔抗和护甲之后的。因此，购买防御道具可以大大地增加你的存活力。",
                "battleTips": "乌迪尔通常从野区出现，偷袭兵线上的玩家。在重要位置放置侦查守卫来掌握他的动向。",
                "name": "乌迪尔",
                "defense": "7",
                "magic": "4",
                "difficulty": "7",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Udyr_MonkeysAgility.png",
                    "title": "灵猴敏捷",
                    "description": "乌迪尔在使用一个技能后会获得5移动速度和10%攻击速度，持续5秒，最多可叠加3次。\n\n乌迪尔拥有4个基础技能，可用来在各个姿态之间切换。变更姿态会立刻结束上个姿态的被动部分和攻击特效部分，并使所有技能进入为期1.5秒的冷却时间。\n\n在16级时，乌迪尔可以将他的技能升到第6级。\n己方夺取的每条云端亚龙都会使这个1.5秒的冷却时间减少5%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UdyrTigerStance.png",
                    "title": "猛虎姿态",
                    "description": "姿态：乌迪尔的第一次攻击和之后的每第三次攻击，将对目标进行一次猛虎打击，在2秒内造成额外的30/60/90/120/150/180（+110%/125%/140%/155%/170%/185%*AD）物理伤害。后续对该目标进行的猛虎打击会刷新这个效果并立刻造成剩余的伤害值。\n\n主动：乌迪尔提升30%/40%/50%/60%/70%/80%攻击速度，持续5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UdyrTurtleStance.png",
                    "title": "灵龟姿态",
                    "description": "姿态：乌迪尔的第一次攻击和之后的每第三次攻击，将回复(2.5%*生命值)生命值，至多可基于乌迪尔的已损失生命值提升至(5%*生命值)。\n\n主动:乌迪尔获得60 /95 /130/165/200 /235（+50%*AP）护盾值，持续5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UdyrBearStance.png",
                    "title": "巨熊姿态",
                    "description": "姿态：乌迪尔的攻击造成1秒晕眩效果。该效果在5秒内无法重复作用于相同目标。\n\n主动：乌迪尔获得15%/20%/25%/30%/35%/40%移动速度，并无视单位体积碰撞，持续2/2.25/2.5/2.75/3/3.25秒。\n\n乌迪尔在使用这个技能后的首次攻击会略微提升距离。\n乌迪尔在这个姿态下的攻击无法被取消。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UdyrPhoenixStance.png",
                    "title": "火凤姿态",
                    "description": "姿态：乌迪尔的第一次攻击和之后的每第三次攻击，将朝目标释放一团烈焰，造成60/110/160/210/260/310（+70%*AP）魔法伤害。\n\n主动：乌迪尔释放脉冲烈焰，在4秒里对附近敌人持续造成共50/100/150/200/250/300（+100%*AP）魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "594.0000",
                  "hpperlevel": "99.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "271.0000",
                  "mpperlevel": "30.0000",
                  "mpregen": "7.5000",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "66.0000",
                  "attackdamageperlevel": "5.0000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.6700",
                  "armor": "34.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "不屈之枪",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Pantheon.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big80000.jpg",
                "story": "曾经被战争星灵附体的阿特瑞斯，在他体内那股天界力量被弑杀的时候，以凡人的身份活了下来，即便是那裂空摘星的一击，也无法让他屈服。不久以后，他学会了拥抱自己凡性的力量，以及凡性之中顽强的韧劲。如今的阿特瑞斯作为重生的潘森，反抗神性。他坚不可摧的意志在战场上化为火焰，注入那些曾经属于星灵的武具中。",
                "usageTips": "【矢志不退】会在5次技能或普攻后激活——提前做好计划，可使你在战斗中激活一次以上的【矢志不退】。 先用【Q贯星长枪】消耗敌人，然后再跳过去将其处决。 如果一名敌人即将走出你的【E神佑枪阵】的范围，你可以再次施放这个技能来立刻用圣盾进行猛击。",
                "battleTips": "潘森的【E神佑枪阵】会让他免疫来自前方的伤害。绕到他背后或者耐心等待吧。 如果你的生命值较低，就要当心潘森的靠近了——投掷版的【Q贯星长枪】会处决生命值较低的敌人。 【R大荒星陨】会在潘森抵达前的几秒作出警告。利用这段时间来跑出范围或制订一个计划来处理他。",
                "name": "潘森",
                "defense": "4",
                "magic": "3",
                "difficulty": "4",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Pantheon_Passive.PantheonVGU.png",
                    "title": "矢志不退",
                    "description": "在5次技能或攻击后，潘森的下一个基础技能会得到强化。\n【新增】现在，潘森回城之后将获得5层被动技能层数"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PantheonQ.png",
                    "title": "贯星长枪",
                    "description": "秒放：潘森刺出他的长枪，对命中的敌人们造成70/100/130/160/190（+115%*额外AD）物理伤害。返还60%的【Q贯星长枪】冷却时间。\n\n蓄力：潘森挥出他的长枪，对命中的首个敌人造成70/100/130/160/190（+115%*额外AD）物理伤害并对后续目标造成的伤害减少50%。\n\n【贯星长枪】会暴击低于20%生命值的敌人，造成155/230/305/380/455（+230%*额外AD）物理伤害作为替代。\n\n矢志不退加成：造成额外的20~240(1-18级)（+115%*额外AD）物理伤害。\n\n对小兵的伤害减少30%。\n\n提前施放BUG修复：修复了一个bug，该bug曾导致，潘森在施放Q技能期间被控制技能命中时，Q技能无法施放但仍然会进入冷却阶段"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PantheonW.png",
                    "title": "斗盾跃击",
                    "description": "潘森跃向一个目标，造成60/100/140/180/220（+100%*AP）物理伤害和1秒晕眩效果。\n\n矢志不退加成：潘森的下一次攻击会打击3次，总共造成120-165%(1-18级)物理伤害。\n\nBUG修复：现在，当潘森用带有【矢志不退】加成的W技能命中敌方单位的同时立刻使用另一个技能时，下一次普通攻击将正确触发【矢志不退】的加成效果"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PantheonE.png",
                    "title": "神佑枪阵",
                    "description": "潘森架起他的圣盾并激怒一个选定方向上的敌人1.5秒，变得免疫来自所选方向的伤害并在持续期间造成100%*AD物理伤害。\n\n最后一击会以潘森的圣盾进行猛击，造成55/105/155/205/255（+150%*额外AD）物理伤害。\n\n矢志不退加成：当潘森用圣盾进行猛击之后，他会消耗【矢志不退】层数，以获得持续1.5秒的60%移动速度加成\n\n再次施放可提前结束(潘森仍然会以圣盾猛击作为最后一击)。\n潘森在向后或者向侧边移动时至多会被减速25%。\n对小兵的伤害会减少50%。\n\n【已移除】E技能不再格挡来自防御塔的伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PantheonR.png",
                    "title": "大荒星陨",
                    "description": "潘森聚集他的力量以跃至空中，然后在几秒之后如陨石一般落向目标区域。对一条直线上的敌人最多造成300/500/700（+100%*AP）魔法伤害(最多降低50%，极限值出现于区域边缘)。\n\n【R大荒星陨】会立刻使【矢志不退】准备就绪。\n\n【新增】潘森获得10/20/30%护甲穿透\n【新增】现在，潘森在着陆前会先扔出一支可造成50%减速的长枪，并在其小范围内造成长枪伤害(未强化版)\n【新增】现在，当【大荒星陨】开始引导时，着陆范围指示器会同时对潘森和队友可见"
                  }
                ],
                "data": {
                  "hp": "580.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "10.0000",
                  "hpregenperlevel": "0.6500",
                  "mp": "317.1200",
                  "mpperlevel": "31.0000",
                  "mpregen": "7.3560",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "2.9500",
                  "armor": "40.0000",
                  "armorperlevel": "3.7500",
                  "spellblock": "28.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "355.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "铁铠冥魂",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Mordekaiser.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big82000.jpg",
                "story": "两度被杀，三度重生。莫德凯撒是一位来自远古纪元的残酷军阀，他使用死灵巫术将无数灵魂禁锢在永恒的奴役中。现在几乎无人记得他早期的那些征服战争，也无人知道他有多强大的力量，但也有一些古老的灵魂认得他，而他们一直都在担心有一天他会回来，同时统治生者和死者。",
                "usageTips": "你的防守手段就是进攻。保持不停战斗来积攒更厚的【不坏之身】护盾。 用相同技能命中多个英雄，能有助于快速激活【黑暗起兮】。 对低血量的敌人使用【轮回绝境】可以确保击杀并将其属性保持到打完一场团战。",
                "battleTips": "莫德凯撒在与英雄作战时会积攒一个强力的伤害光环，所以尽量与他保持距离。 他造成的伤害能够转化为一层厚实的护盾，并且消耗它来治疗生命值。 【轮回绝境】将使你与你的队友们彻底隔开。一旦如此，尽量留着机动技能来逃离莫德凯撒的魔掌。",
                "name": "莫德凯撒",
                "defense": "6",
                "magic": "7",
                "difficulty": "4",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MordekaiserPassive.MordeVGU.png",
                    "title": "黑暗起兮",
                    "description": "在用3次普通技能或攻击命中英雄或野怪后，莫德凯撒会将自身笼罩在负能量的遮蔽中。这个遮蔽每秒造成5~15(1-18级)（+30%*AP）加上1%~5%(1-18级)最大生命值的魔法伤害并为他提供3%移动速度。\n\n此外，莫德凯撒的攻击会造成额外的（40%*AP）魔法伤害。\n\n伤害光环和移动速度持续4秒并在用技能或攻击命中一名英雄或野怪时刷新。这个技能每秒最多对野怪造成28~164(1-18级)伤害。\n"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MordekaiserQ.png",
                    "title": "破灭之锤",
                    "description": "莫德凯撒用夜陨猛砸地面，造成75/95/115/135/155（+60%*AP +5~139(1-18级)）魔法伤害，如果这个技能只命中了一个敌人，则伤害提升至120%/125%/130%/135%/140%。\n\n 命中单个敌人的伤害提升：30/35/40/45/50%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MordekaiserW.png",
                    "title": "不坏之身",
                    "description": "被动：莫德凯撒将他造成的35%伤害和他受到的15%伤害储存起来。\n\n主动：莫德凯撒获得相当于已储存伤害值的护盾。他可以再次施放这个技能来回复相当于 40%/42.5%/45%/47.5%/50%剩余护盾值的生命值。\n\n最小护盾值：5%*生命值\n最大护盾值：30%*生命值\n\n护盾值会持续衰减。\n非英雄伤害的储存率会减少75%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MordekaiserE.png",
                    "title": "断魂一扼",
                    "description": "被动：莫德凯撒获得5%/7.5%/10%/12.5%/15%法术穿透。\n主动：莫德凯撒将敌人拉往他的方向，并造成80/95/110/125/140（+60%*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MordekaiserR.png",
                    "title": "轮回绝境",
                    "description": "莫德凯撒将自身和一个敌方英雄放逐至死者领域7秒，并在期间内偷取该英雄10%的核心属性。\n\n如果莫德凯撒在死者领域击杀了该敌人，那么他会吞噬目标的灵魂，保留他所偷取的属性，直到目标复活为止。\n已偷取的核心属性包括:法术强度，攻击力，攻击速度，护甲，魔法抗性和最大生命值。"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "5.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.0000",
                  "armor": "37.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "牧魂人",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Yorick.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big83000.jpg",
                "story": "约里克所在的教团早已被世人忘却，而他也成为了最后的幸存者。说不上是诅咒抑或是祝福，他拥有操控死者的能力。与他一同被困在暗影岛上的，只有逐渐腐败的尸体，还有他引到自己身边终日尖啸的死灵。约里克怪异的举止下掩藏着的却是他决绝的憧憬：在破败之咒的阴影下解放他的家园。",
                "usageTips": "室女将会协助你进行战斗，所以要明智地选择你的目标。你可以让室女去单带一路，但要慎重，因为她是你战斗力的重要组成部分。",
                "battleTips": "量削减约里克的仆从数量，然后再跟他交战。只需要一次普攻或一个单体技能就能解决掉一个雾行者。",
                "name": "约里克",
                "defense": "6",
                "magic": "4",
                "difficulty": "6",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Yorick_P.png",
                    "title": "牧魂人",
                    "description": "附近每阵亡12/6/2(1-18级)名敌人，约里克就会升起一座坟墓。英雄和大型野怪在阵亡时总会留下以坐坟墓。\n\n这些坟墓可以被用来通过约里克的其它技能来召唤雾行者，雾行者拥有99~184(1-18级)（+15%*生命值）生命值和2~99(1-18级)（+30%*AD）攻击力，并且约里克在一个区域内至多可控制4个。\n\n雾行者一旦进入战斗，就会沿着战线推进下去。\n约里克的召唤单位们所受的来自群体技能和效果的伤害降低50%。约里克的召唤单位们所受的来自野怪的伤害降低50%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YorickQ.png",
                    "title": "临终仪式",
                    "description": "基础技能：约里克的下次攻击将造成30/55/80/105/130（+40%*AD）物理伤害并回复12~82(1-18级)生命值，或在约里克低于50%生命值时回复24~164(1-18级)生命值。如果这个攻击击杀了目标，就会留下一座坟墓。\n\n当附近有3个或以上的坟墓时，如果【临终仪式】已被使用，那么约里克就能再次施放这个技能来从附近的所有坟墓中唤起雾行者。\n\n再次施放：约里克从坟墓中唤起雾行者。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YorickW.png",
                    "title": "暗灵缠身",
                    "description": "约里克召唤一道灵墙来阻挡敌人的道路，但不影响友军。灵墙能被攻击，并且会在2/2/3/3/4次命中后消失。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YorickE.png",
                    "title": "哀伤之雾",
                    "description": "约里克扔出一小团黑雾，来造成15%当前生命值的魔法伤害（最小伤害：70/105/140/175/210），以及持续2秒的30%减速，并标记英雄和野怪4秒。\n\n约里克和他的召唤单位们在朝着标记移动时会获得20%移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YorickR.png",
                    "title": "海屿悼词",
                    "description": "约里克召唤迷雾室女和2/3/4个雾行者。室女拥有300/1000/3000（+70%*生命值）生命值，造成0/10/40（+50%*AD）魔法伤害，并从附近阵亡的敌人身上唤起雾行者。\n\n当约里克对室女的目标造成伤害时，他将额外造成3%/6%/9%最大生命值的魔法伤害。这个效果有2秒的冷却时间。\n\n10秒后，约里克可以再次施放这个技能来放走室女，派她去相距最近的一条战线。"
                  }
                ],
                "data": {
                  "hp": "580.0000",
                  "hpperlevel": "100.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.8000",
                  "mp": "300.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.5000",
                  "mpregenperlevel": "0.7500",
                  "attackdamage": "62.0000",
                  "attackdamageperlevel": "5.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.0000",
                  "armor": "39.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "德玛西亚之力",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Garen.png",
                "banner": "作为一名自豪而高贵的勇士，盖伦将自己当做无畏先锋中的普通一员参与战斗。他既受到同袍手足的爱戴，也受到敌人对手的尊敬——尤其作为尊贵的冕卫家族的子嗣，他被委以重任，守卫德玛西亚的疆土和理想。他身披抵御魔法的重甲，手持阔剑，时刻准备着用正义的钢铁风暴在战场上正面迎战一切操纵魔法的狂人。",
                "story": "作为一名自豪而高贵的勇士，盖伦将自己当做无畏先锋中的普通一员参与战斗。他既受到同袍手足的爱戴，也受到敌人对手的尊敬——尤其作为尊贵的冕卫家族的子嗣，他被委以重任，守卫德玛西亚的疆土和理想。他身披抵御魔法的重甲，手持阔剑，时刻准备着用正义的钢铁风暴在战场上正面迎战一切操纵魔法的狂人。",
                "usageTips": " 如果【审判】的范围只有一个敌人，那么它就能造成最大伤害值。为了有效率地换血，走位时请尽量确保【审判】的范围内只有一个敌方英雄。",
                "battleTips": "【审判】只会在攻击单个目标时造成最大伤害。如果无法走出该技能的范围，就穿过友方小兵来降低所受伤害。",
                "name": "盖伦",
                "defense": "7",
                "magic": "1",
                "difficulty": "5",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Garen_Passive.png",
                    "title": "坚韧",
                    "description": "盖伦每5秒回复2%~10%(1-18级)最大生命值，前提是他在之前的8秒内未受到伤害或被敌方技能命中。\n\n小兵和非史诗级野怪的伤害不会中断这个回复。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GarenQ.png",
                    "title": "致命打击",
                    "description": "盖伦移除身上的所有减速效果并获得30%移动速度，持续1/1.65/2.3/2.95/3.6秒。\n\n他的下次攻击会使目标沉默1.5秒并造成30/ 60/ 90 /120/150（+150%*AD）物理伤害。\n\n强化攻击会在4.5秒后消散。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GarenW.png",
                    "title": "勇气",
                    "description": "被动：击杀一个单位会永久提供0.25护甲和0.25魔法抗性，最多可达30。满层时，盖伦获得(10%额外)护甲和(10%额外魔抗)魔法抗性。\n\n主动：盖伦加固他的勇气2/2.75/3.5/4.25/5秒，使即将到来的伤害减少30%。他还会获得70/95/120/145/170（+20%额外生命值）护盾值和60%韧性,持续0.75秒。\n\n当前已获得的护甲和魔法抗性:0.00/30"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GarenE.png",
                    "title": "审判",
                    "description": "盖伦快速地旋舞大剑，持续3秒，每旋转一圈造成4/8/12/16/20（+0~8(1-18级) +32%/34%/36%/38%/40%*AD)物理伤害，共旋转9圈。相距最近的敌人每圈转而受到5/10/15/20/25（+0~10(1-18级) +40%/42.5%/45%/47.5%/50%*AD）物理伤害。\n\n被6圈旋转命中的敌方英雄会损失25%护甲，持续6秒。\n\n旋转圈数可通过来自装备和等级的攻击速度来提升。\n这个技能可以暴击，造成33%额外伤害，即：造成(133%每一圈的伤害)物理伤害（对相距最近的目标的暴击伤害为(133%相距最近的敌人每圈受到的伤害)）.\n对野怪造成150%伤害。\n这个技能可以被提前取消，并且如果这么做，则会缩短剩余的冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GarenR.png",
                    "title": "德玛西亚正义",
                    "description": "盖伦召唤德玛西亚之力来击杀他的敌人，造成150/300/450外加20%/25%/ 30%已损失生命值的真实伤害。"
                  }
                ],
                "data": {
                  "hp": "620.0000",
                  "hpperlevel": "84.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "66.0000",
                  "attackdamageperlevel": "4.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.6500",
                  "armor": "36.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "0.7500",
                  "movespeed": "340.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "放逐之刃",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Riven.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big92020.jpg",
                "story": "曾担任诺克萨斯军队剑士长的锐雯，如今在那片她曾要征服的土地上流浪。她通过自己信念的力量和野蛮的行事风格在军中不断晋升，因此获得了一把传奇的符文之刃和自己的战团作为奖赏——然而在艾欧尼亚的前线上，锐雯对祖国的信仰遭到了考验，并最终粉碎。她切断了与帝国的一切关联，在分崩离析的世界中找寻自己的归宿，即使纷飞的谣言传说着诺克萨斯已经重铸",
                "usageTips": "锐雯的折翼之舞会朝着你在施法时的鼠标悬停位置施放。如果你想用这招穿插你的敌人，请确保你的敌人们处在锐雯和你的鼠标悬停位置之间。",
                "battleTips": "锐雯的机动性非常出色，但单个技能不能使她移动得太远。在技能间隙使用束缚/沉默会极大削减她的影响。",
                "name": "锐雯",
                "defense": "5",
                "magic": "1",
                "difficulty": "8",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/RivenRunicBlades.png",
                    "title": "符文之刃",
                    "description": "锐雯的技能会为她的剑刃充能至多3层。\n\n她的普通攻击会消耗充能层数来造成额外的（30％*AD）物理伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RivenTriCleave.png",
                    "title": "折翼之舞",
                    "description": "锐雯向前直冲，发起突袭。这个技能可以再次施放另外的2段。\n\n第一段和第二段：向前斩击，对接触到的所有敌人造成15/35/55/75/95（+45%/50%/55%/60%/65%*AD）物理伤害。\n第三段:跃向空中，随后猛击地面，造成15/35/55/75/95（+45%/50%/55%/60%/65%*AD）物理伤害，并且以冲击点为中心，将周围的敌人击飞。\n\n锐雯将会以你鼠标悬停的单位为目标，假如鼠标悬停处没有目标，锐雯会朝着她现在面向的区域发起突击。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RivenMartyr.png",
                    "title": "震魂怒吼",
                    "description": "锐雯的剑爆发出一股符文能量，来震慑住周围的敌人，对他们造成55/85/115/145/175（+100%*额外AD）物理伤害和0.75秒的晕眩效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RivenFeint.png",
                    "title": "勇往直前",
                    "description": "锐雯朝着你的鼠标悬停位置进行一次快速冲刺，并且武装上一层护盾，格挡将要来临的最多95/125/155/185/215（+100%*额外AD）伤害，护盾持续1.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RivenFengShuiEngine.png",
                    "title": "放逐之锋",
                    "description": "锐雯的武器激荡着圣灵的能量，在15秒的持续时间里，给予她20％的攻击力加成（20％*AD），并提高她的攻击距离，以及伤害类技能的施法范围，并拥有一次使用疾风斩的能力。\n\n疾风斩：锐雯挥出一道冲击波，能够对被它命中的敌人造成100/150/200（+60%*额外AD）到300/450/600（+180%*额外AD）额外物理伤害，敌人的伤势越重，则该技能的伤害值越高。"
                  }
                ],
                "data": {
                  "hp": "560.0000",
                  "hpperlevel": "86.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.5000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.5000",
                  "armor": "33.0000",
                  "armorperlevel": "3.2000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "龙血武姬",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Shyvana.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big102000.jpg",
                "story": "希瓦娜是一只魔法生物，心中有一块燃烧不灭的符文碎片。虽然她时常以人的形象出现，但她真正的形态是一条威猛的巨龙，可以用龙息烈焰吞噬敌人。希瓦娜曾拯救过皇子嘉文四世的性命，如今她心神不安地在皇子的卫队中效力，力图在多疑的德玛西亚人中求得接纳。",
                "usageTips": "希瓦娜的攻击能够给她的全部技能带来好处。提供攻击速度的装备对于她而言比其他英雄更有价值。",
                "battleTips": "希瓦娜的所有基础技能在魔龙形态下都能攻击多个敌人。因此，在与她作战时不要聚成一团。",
                "name": "希瓦娜",
                "defense": "6",
                "magic": "3",
                "difficulty": "4",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/ShyvanaReinforcedScales.png",
                    "title": "龙族之怒",
                    "description": "希瓦娜对龙类野怪造成20%额外伤害。她获得5护甲和魔法抗性，并且被希瓦娜或她的友军击败的每条龙都会为她提供额外的5护甲和魔法抗性。\n\n希瓦娜会将云霄亚龙的终极技能冷却缩减转化为额外的怒气生成。\n总护甲和魔法抗性加成:5\n\n极地大乱斗(嚎哭深渊)：每当希瓦娜或其队友击杀了炮兵/超级小兵时，希瓦娜还将永久获得护甲值和魔法抗性。此外，每当希瓦娜通过敌方半区的治疗包获得治疗效果时，永久获得1护甲值，1魔法抗性，且怒气回复效果提升0.05\n极限闪击 (百合与莲花的神庙)：每当希瓦娜或其队友击杀了附近的史诗级野怪、野怪、峡谷先锋时，希瓦娜永久获得护甲值和魔法抗性，此外，希瓦娜的怒气回复效果提升0.05"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShyvanaDoubleAttack.png",
                    "title": "龙牙突刺",
                    "description": "希瓦娜的下次攻击会进行两次打击，分别造成（100％*AD）和（20%/35%/50%/65%/80%*AD）物理伤害。\n\n当龙牙突刺尚未冷却完毕时，每次普攻会使剩余的冷却时间减少0.5秒。\n\n魔龙形态：龙牙突刺会撕裂希瓦娜面前的所有单位。\n\n龙牙突刺的第二次攻击将造成相当于希瓦娜20%/35%/50%/65%/80%总攻击力的伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShyvanaImmolationAura.png",
                    "title": "烈火燎原",
                    "description": "希瓦娜每秒对周围的敌人造成20/32/45/57/70（+20%*额外AD）魔法伤害，并且移动速度提高30%/35%/40%/45%/50%，这个移速加成会在3秒里持续减少。\n\n在烈火燎原激活时，希瓦娜的普攻会对附近的敌人造成5/8/11.25/14.25/17.5（+5%*额外AD）魔法伤害并使此移速加成的持续时间延长1秒。\n\n魔龙形态：烈火燎原会随体型增长。\n\n【烈火燎原】的最大持续时长为7秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShyvanaFireball.png",
                    "title": "烈焰吐息",
                    "description": "希瓦娜放出一枚火球，在命中一名敌方英雄时停下。所有被命中的敌人会受到60/100/140/180/220（+70％*AP +30％*额外AD）魔法伤害并被标记5秒。\n\n希瓦娜的普通攻击在命中被标记的目标时，会造成相当于目标3.75%最大生命值的附加魔法伤害。\n\n魔龙形态：烈焰吐息会在碰撞或是抵达目标位置时爆炸，造成100~160(1-18级)（+30％*AP +30％*AD）额外魔法伤害并灼烧地面4秒。在灼烧之地上的敌人会每秒受到60~120(1-18级)（+20％*AP +10％*AD）魔法伤害。\n\n在对抗野怪时，烈焰吐息每次命中时的附加伤害封顶值为150。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShyvanaTransformCast.png",
                    "title": "魔龙降世",
                    "description": "主动效果：希瓦娜变成一条巨龙，获得150/250/350生命值并且飞向目标区域。沿途的敌人会受到150/250/350（+100％*AP）魔法伤害，并被震向她的目标区域。\n\n被动效果：希瓦娜每秒产生1/1.5/2点怒气。普通攻击产生2点怒气。\n\n冷却时间（s）：需要100怒气以激活\n消耗：5怒气/秒"
                  }
                ],
                "data": {
                  "hp": "595.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.8000",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "66.0000",
                  "attackdamageperlevel": "3.4000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.5000",
                  "armor": "38.0000",
                  "armorperlevel": "3.3500",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "无双剑姬",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Fiora.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big114000.jpg",
                "story": "菲奥娜是全瓦洛兰最可怕的决斗家。她以雷厉风行、狡黠聪慧闻名于世，同样著名的还有她舞弄自己蓝钢佩剑的矫健。菲奥娜出生在德玛西亚王国的劳伦特家族，她从父亲的手中接管了家业，并在一场丑闻风波中将家族拯救于灭亡的边缘。虽然劳伦特家威严不再，但菲奥娜却一直在不懈地努力，希望重振家族荣耀，让劳伦特这个名字重回德玛西亚名望贵族之列。",
                "usageTips": "在【决斗之舞】的帮助下，菲奥娜特别擅长进行快速换血。攻击破绽后的移动速度加成，既能用来毫发无伤地逃离，又能用来发起下次攻击。",
                "battleTips": "在对菲奥娜施放禁锢类技能时要特别小心。如果她的【劳伦特心眼刀】还在可用状态，那么她就能将这类技能的效果反过来对付你。",
                "name": "菲奥娜",
                "defense": "4",
                "magic": "2",
                "difficulty": "3",
                "attack": "10",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Fiora_P.png",
                    "title": "决斗之舞",
                    "description": "菲奥娜识别出敌方英雄身上的破绽。用一次攻击或技能命中这个破绽会造成额外的(0.025 +0.045*额外AD)最大生命值的真实伤害，并为菲奥娜提供在2秒里持续衰减的20%移动速度，并回复(40-115)生命值。\n菲奥娜会在15秒后或是击中一个破绽后找出一个新破绽。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FioraQ.png",
                    "title": "破空斩",
                    "description": "菲奥娜向一个方向进行突刺并刺击相距最近的敌人、守卫或建筑物，造成(70/80/90/100/110 +(0.95/1/1.05/1.1/1.15)*额外AD)物理伤害。这次打击会优先攻击破绽和将被此打击击杀的单位。\n如果菲奥娜命中了一个敌人，那么这个技能的冷却时间就会缩短50%。\n这次打击会施加攻击特效。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FioraW.png",
                    "title": "劳伦特心眼刀",
                    "description": "菲奥娜招架即将到来的所有伤害、限制效果和有害效果，持续0.75秒，然后刺击。\n刺击会对命中的第一个敌方英雄造成(110/150/190/270/270 +100%AP)魔法伤害，并使目标的攻击速度和移动速度减速50%，持续2,秒。如果菲奥娜招架掉一个定身效果，那么被刺击的敌人会被晕眩而非减速。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FioraE.png",
                    "title": "夺命连刺",
                    "description": "菲奥娜的下两次攻击获得(50%/60%/70%/80%/90)攻击速度。第一次攻击造成持续1秒的30%减速。第二次攻击必定会产生暴击，造成(160%/170%/180%/190%/200%)伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FioraR.png",
                    "title": "无双挑战",
                    "description": "被动：决斗之舞的移动速度加成提升至(30%/40%/50%)。\n主动：菲奥娜标出一名英雄身上的全部4处破绽，最多可造成(10% +0.18*额外AD)最大生命值的真实伤害并在目标附近时获得决斗之舞的移动速度加成。\n如果菲奥娜在8秒内命中了全部4处破绽，或者在目标死前至少命中一处，那么菲奥娜就会为周围的友方英雄每秒恢复(80/110/140 +60%额外AD)生命值，持续2到5秒，命中的破绽数越多则持续时间越久。"
                  }
                ],
                "data": {
                  "hp": "550.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "300.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6900",
                  "attackspeedperlevel": "3.2000",
                  "armor": "33.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "战争之影",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Hecarim.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big120000.jpg",
                "story": "赫卡里姆是人与兽的幽灵混合体，身上的诅咒让他永世都只能不停地蹂躏践踏生者的灵魂。在福光岛被暗影吞噬之际，这位自豪的骑士被破败之咒的毁灭能量彻底湮没，连同他的骑兵团和他们胯下的坐骑。现在，符文之地上只要有黑雾出现的地方，就会有他率军冲锋的鬼影，在屠杀中狂欢，用铁蹄摧残脚下的敌人。",
                "usageTips": "【W恐惧之灵】会在周围敌人受到伤害时恢复赫卡里姆的生命值，即使这些伤害是赫卡里姆的队友造成的。在大型团战中施放这个技能，可以最大化地提升赫卡里姆的生存能力。",
                "battleTips": "赫卡里姆可以凭借着恐惧之灵从附近敌人身上吸取生命值，但是不太抗揍，把爆发技能扔给他吧！",
                "name": "赫卡里姆",
                "defense": "6",
                "magic": "4",
                "difficulty": "6",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Hecarim_Passive.png",
                    "title": "征战之路",
                    "description": "赫卡里姆获得(12% 额外移速)攻击力。\n攻击路随额外移动速度而增长。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HecarimRapidSlash.png",
                    "title": "暴走",
                    "description": "赫卡里姆猛劈周围的敌人，造成(60/102/144/186/228 +70%额外AD)物理伤害。如果这个技能命中了敌人，那么他会获得一层效果，来使这次技能冷却时间降低1秒并提升此技能5%伤害，持续8秒。这个效果至多可达2层。\n对小兵造成(36/61.2/86.4/111.6/136.8+42%额外AD)基础伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HecarimW.png",
                    "title": "恐惧之灵",
                    "description": "赫卡里姆在4秒里对附近的敌人们持续造成共(80/120/160/200/240 +80%AP)魔法伤害，并治疗自身相当于这些敌人30%所受伤害(所有来源)的生命值。\n赫卡里姆从小兵或野怪处获得的治疗效果无法超过(90/120/150/180/210)生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HecarimRamp.png",
                    "title": "毁灭冲锋",
                    "description": "赫卡里姆变为幽灵状态，并获得25%移动速度，在4秒里持续提升至75%。他的下次攻击会击退目标并造成(35/65/95/120/155 +50%额外AD)到(70/130/190/250/310 +100额外AD)物理伤害。击退距离和伤害随此技能期间的位移距离而增长。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HecarimUlt.png",
                    "title": "暗影冲击",
                    "description": "赫卡里姆召唤幽魂骑兵并向前冲锋，造成(150/250/350 +100%AP)魔法伤害。赫卡里姆会在他冲锋的终点释放一道冲击波，来使敌人们恐惧0.75到2秒，随冲锋距离提升。\n赫卡里姆将移动至目标位置，但幽魂骑兵们总会冲完全程。"
                  }
                ],
                "data": {
                  "hp": "580.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "277.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "6.5000",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "66.0000",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6700",
                  "attackspeedperlevel": "2.5000",
                  "armor": "36.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "诺克萨斯之手",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Darius.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big122000.jpg",
                "story": "提到诺克萨斯力量的象征，帝国上下没有人能比德莱厄斯这名久经沙场的指挥官更加适合。他从无名小卒逐渐成长为诺克萨斯之手，劈开了无数敌人的身躯 —— 其中也不乏诺克萨斯自己人。他从不怀疑自己执行的公义，也从不会在举起战斧后迟疑。作为崔法利军团的领导者，德莱厄斯的任何对手都不用指望他手下留情。",
                "usageTips": "你对目标施加的【出血】效果越多，【诺克萨斯断头台】的伤害越高。使用【诺克萨斯之力】来造成最大伤害。",
                "battleTips": " 德莱厄斯的脱战能力极度有限。如果你在对抗他时占据了优势，就将他拖进你的节奏吧。",
                "name": "德莱厄斯",
                "defense": "5",
                "magic": "1",
                "difficulty": "2",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Darius_PassiveBuff.png",
                    "title": "出血",
                    "description": "德莱厄斯的攻击和伤害会使目标出血，在5秒里持续造成共(13-30 +30%额外AD)物理伤害，至多可叠加5层。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DariusCleave.png",
                    "title": "大杀四方",
                    "description": "德莱厄斯举起他的斧头然后挥舞一圈，斧刃造成(50/80/110/140/170 +100%/110%/120%/130%/140%AD)物理伤害并且斧柄造成(17.5/28/38.5/49/59.5 +35%/38.5%/42%/45.5%/49%AD)伤害。被斧柄命中的敌人不会被施加出血。\n德莱厄斯每用斧刃命中一个敌方英雄或大型野怪就会回复15%已损失生命值，至多回复45%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DariusNoxianTacticsONH.png",
                    "title": "致残打击",
                    "description": "德莱厄斯的下次攻击造成(140%/145%/150%/155%/160%AD)物理伤害和持续1秒的90%减速。\n如果这个技能击杀了目标，那么会 返还其法力消耗并缩短其50%的冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DariusAxeGrabCone.png",
                    "title": "无情铁手",
                    "description": "被动：德莱厄斯获得(15%/20%/25%/30%/35%)护甲穿透。\n主动：德莱厄斯用他的斧头进行钩拽，造成拖拽、击飞和持续1秒的40%减速。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DariusExecute.png",
                    "title": "诺克萨斯断头台",
                    "description": "德莱厄斯跳向一名敌人并进行一次致命猛击，造成(100/200/300 +75%额外AD)真实伤害。目标身上每有一层出血，这个技能就会造成额外的20%伤害。至多可达200%伤害。\n如果这个技能击杀了目标，那么德莱厄斯就可以在20秒内再次施放这个技能。这个技能在技能等级达到3时，即可无需消耗法力并在击杀时完全刷新技能的冷却时间。"
                  }
                ],
                "data": {
                  "hp": "582.2400",
                  "hpperlevel": "100.0000",
                  "hpregen": "10.0000",
                  "hpregenperlevel": "0.9500",
                  "mp": "263.0000",
                  "mpperlevel": "37.5000",
                  "mpregen": "6.6000",
                  "mpregenperlevel": "0.3500",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "5.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.0000",
                  "armor": "39.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "未来守护者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Jayce.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big126000.jpg",
                "story": "杰斯是一位天才发明家，他为了守护皮尔特沃夫和这座城市对于进步的追求贡献了毕生的心血。杰斯依靠手中的海克斯科技变形锤，还有自己的力量、勇气和出众的智慧守护着他的故乡。虽然整座城市都将他视为英雄，但他却并没有处理好这样的声名。即便如此，杰斯的心之所向依旧纯良，即使是那些嫉妒他天赋的人，也会真心实意地感谢他给进步之城带来的保护。",
                "usageTips": "要记得频繁地切换姿态。这么做会强化你的攻击，并使你的移动速度得到爆发性提高。",
                "battleTips": "杰斯可以在近战攻击与远程攻击之间自由切换。注意观察他的姿态和武器颜色，以获悉他的攻击方式。",
                "name": "杰斯",
                "defense": "4",
                "magic": "3",
                "difficulty": "7",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Jayce_Passive.png",
                    "title": "海克斯科技电容",
                    "description": "在施放一次变形后，杰斯会提高40移动速度，并且暂时无视单位的碰撞体积，持续1.25秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JayceToTheSkies.png",
                    "title": "苍穹之跃 / 电能震荡",
                    "description": "跳向一名敌人，对附近敌人造成(55/95/135/175/215/255 +120%额外AD)物理伤害，并减少这些敌人(30%/35%/40%/45%/50%/55%)的移动速度，持续2秒。\n冷却时间：16/14/12/10/8/6\n法力消耗：40\n\n发射一团电球，在命中一名敌人或者到达最大射程后爆炸，对爆炸区域内的所有敌人造成(55/110/165/220/275/330 +120%额外AD)物理伤害。\n冷却时间：8\n法力消耗：55/60/65/70/75/80"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JayceStaticField.png",
                    "title": "闪电领域 / 超能电荷",
                    "description": "被动：在战锤形态下，每次攻击会获得(6/8/10/12/14/16)法力。\n主动：创造一个电子光环，在4秒的持续时间里对周围的的敌人造成(100/160/220/280/340/400 +100%AP)魔法伤害。\n冷却时间：10\n\n杰斯的活力暴涨，将攻击速度提升至最大值，此效果在3次攻击后消失，这些攻击会造成(70%/78%/86%/94%/102%/110%AD)的伤害。\n冷却时间：13/11.4/9.8/8.2/6.6/5"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JayceThunderingBlow.png",
                    "title": "雷霆一击 / 加速之门",
                    "description": "造成目标的(8%/10.4%/12.8%/15.2%/17.6%/20% +100%额外AD)最大生命值的魔法伤害，并将目标击退一小段距离。(对野怪的最大伤害值为200)\n冷却时间：20/18/16/14/12/10\n\n展开一个持续4秒的加速之门，所有穿过它的友军都会得到(30%/35%/40%/45%/50%/55%)的移动速度加成，加速效果持续3秒(加速效果会在持续时间里逐渐消逝)。\n如果电能震荡穿过这个门发射，那么它的弹道速度和射程都会得到提高，并且伤害会提升40%。\n冷却时间：16"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JayceStanceHtG.png",
                    "title": "墨丘利之炮 / 墨丘利之锤",
                    "description": "主动：将墨丘利之锤变为墨丘利之炮，同时获得新技能和远程攻击能力。\n炮形态的下一次攻击将让目标的护甲和魔法抗性减少(10%/25%)，持续5秒。\n\n将墨丘利之炮变为墨丘利之锤，同时获得新技能且护甲和魔法抗性提高(5-35)。\n锤形态的下一次攻击将造成额外的(25-145 +25%额外AD)魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "560.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "375.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "54.0000",
                  "attackdamageperlevel": "4.2500",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "3.0000",
                  "armor": "27.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "迷失之牙",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Gnar.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big150000.jpg",
                "story": "纳尔是一个原始约德尔人，讨人喜爱的小个子突然发起脾气来，就会变成一头巨大的野兽，脑海中只剩下破坏的念头。纳尔被臻冰冻结了数千年，如今他重获自由。这个面目全非的世界，在他充满好奇的眼里处处都是新鲜奇妙。因为纳尔在危险中会特别兴奋，所以他会随便抓起任何东西丢向自己的敌人，无论是他的骨齿回力标，还是手边的大房子。",
                "usageTips": "掌握好你的怒气，是非常重要的。尽量掌握好你的变形时机，来让你能从两个形态中获得最大的收益。",
                "battleTips": "纳尔在从大变小后的15秒里，不能获得怒气。利用这个机会来和对面开战吧。",
                "name": "纳尔",
                "defense": "5",
                "magic": "5",
                "difficulty": "8",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Gnar_Passive.png",
                    "title": "狂怒基因",
                    "description": "纳尔在战斗状态下，以及进行攻击时都会产生怒气。在怒气达到最大值时，他的下一个技能将会使他变形，改变他的技能和属性，持续15秒。\n小型纳尔：+0移动速度，+225攻击距离，+6攻击速度。\n巨型纳尔：+100生命值，+4护甲，+4魔法抗性，+6基础攻击力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GnarQ.png",
                    "title": "投掷回力标 / 投掷顽石",
                    "description": "小型纳尔:扔出一个回力标，回力标能造成(5/45/85/125/165 +115%AD)物理伤害，以及(30/35/40/45/50%)的移动减速效果，减速持续2秒。回力标在命中第一个敌人后会折返， 对后续目标造成50%伤害。每个敌人只会被击中一次。接住回力标会减少此技能40%冷却时间。\n巨型纳尔:扔出一颗石头，石头会在命中敌人后停下，对被命中目标附近的所有敌人造成(5/45/85/125/165 +140%AD)物理伤害和减速效果。捡起石头，会减少此技能70%冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GnarW.png",
                    "title": "亢奋 /  痛殴",
                    "description": "小型纳尔:被动:对相同目标的每第三次攻击或施法，都会造成额外的(10/20/30/40/50 +100%AP)+目标最大生命值的(6%/8%/10%/12%/14%/)的魔法伤害，并为纳尔提供30%移动速度加成，该移速加成会在3秒里持续衰减(对野怪的最大伤害: 300)。\n巨型纳尔:使一个范围内的敌人晕眩1.25秒，并造成(25/55/85/115/145 +100%AD)物理伤害。\n纳尔在从巨型变成小型时，会获得[W亢奋]的移动速度加成。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GnarE.png",
                    "title": "轻跳 / 猛踏",
                    "description": "小型纳尔：跳到一个区域，获得(40%/45%/50%/55%/60%)攻击速度，持续6秒。如果纳尔落到一个目标上， 那么他会继续向更远处弹跳。造成(50/85/120/155/190 +6%生命值)物理伤害并且如果落到敌方身上，还会对这个敌人造成短暂的减速效果。\n巨型纳尔：跳到一个区域，并在着陆时对附近的所有敌人造成(80/115/150/185/220 +6%生命值)物理伤害。正好位于纳尔落点处的敌人将会被短暂减速。\n如果[E猛踏]被用来变形，那么纳尔将仍能进行弹跳。\n距离：675"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GnarR.png",
                    "title": "呐啊！",
                    "description": "小型纳尔：被动: W亢奋的移动速度加成提升至(45%/60%/75%)。\n巨型纳尔：将附近的所有敌人朝着一个特定方向震击，造成(200/300/400 +100%AP +50%额外AD)物理伤害 和(45%/50%/55%)减速效果，减速持续(1.25/1.5/1.75)秒。被震到墙上的敌人会承受此技能150%的伤害，并受到晕眩效果而不是减速效果。"
                  }
                ],
                "data": {
                  "hp": "510.0000",
                  "hpperlevel": "65.0000",
                  "hpregen": "4.5000",
                  "hpregenperlevel": "1.7500",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "59.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "6.0000",
                  "armor": "32.0000",
                  "armorperlevel": "2.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "疾风剑豪",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Yasuo.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big157000.jpg",
                "story": "亚索是一个百折不屈的艾欧尼亚人，也是一名身手敏捷的御风剑客。这位生性自负的年轻人，被误认为杀害长老的凶手——由于无法证明自己的清白，他出于自卫而杀死了自己的哥哥。虽然长老死亡的真相已然大白，亚索还是无法原谅自己的所作所为。他在家园的土地上流浪，只有疾风指引着他的剑刃。",
                "usageTips": "你可以将敌方小兵作为踏前斩的跳板，来追杀本已逃掉的敌人；你也可以直接突进到对方英雄身上，并将敌方小兵作为撤退时的跳板。",
                "battleTips": " 当亚索的斩钢闪连续命中两次时，他的下次斩钢闪就会形成一道旋风。要观察他的增益栏，并且留心聆听相关音效，做好躲避这招的准备。",
                "name": "亚索",
                "defense": "4",
                "magic": "4",
                "difficulty": "10",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Yasuo_Passive.png",
                    "title": "浪客之道",
                    "description": "百折不屈:亚索在移动的同时会积攒剑意—移动得越快，剑意的获取就越快。当剑意槽被充满时，在受到来自英雄或野怪的伤害的同时，亚索获得一层吸收(120一605)伤害(随英雄等级成长)的护盾。\n向死而生:亚索的暴击几率提升150%，但暴击伤害会降低10%。\n这个效果的计算顺序处在所有其它的暴击几率修正效果之后。\n额外暴击几率转化为攻击力的转化比：每1%暴击几率转化为0.4攻击力"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YasuoQ1Wrapper.png",
                    "title": "斩钢闪",
                    "description": "向前出剑，造成(20/45/70/95/120 +60%AD)物理伤害。\n在命中时，斩钢闪会获得层旋风烈斩效果， 持续6秒。在积攒2层旋风烈斩效果后，斩钢闪会形成一阵能够击飞敌人的旋风。\n斩钢闪被视为普通攻击:它可以暴击，附带攻击特效，会被控制效果所中断，并且它的冷却时间和施法时间都会从攻击速度上获得收益。\n如果在突进的过程中施放斩钢闪，那么斩钢闪就会呈环状出剑。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YasuoW.png",
                    "title": "风之障壁",
                    "description": "形成一个持续4秒的气流之墙，来阻挡敌方的飞行道具。\n宽度：320/390/460530/600"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YasuoE.png",
                    "title": "踏前斩",
                    "description": "向目标敌人突进，造成(60/70/80/90/100 +60%AP +20%额外AD)魔法伤害。每次施法都会使你的下一次突进的基础伤害提升25%，最多提升至50%。\n在10秒内无法对相同敌人重复施放。\n如果在途径的过程中施放斩钢闪，那么斩钢闪就会呈环状出剑。\n每位单位的冷却时间：10/9/8/7/6"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YasuoR.png",
                    "title": "狂风绝息斩",
                    "description": "闪烁到一个浮空的敌方英雄身边，造成(200/350/500 +150%额外AD)物理伤害，并使范围内的所有浮空的敌人在空中多停留1秒。获得满额的剑意值，但会重置旋风\n烈斩的层数。\n在接下来的15秒里，亚索的暴击会获得50%的额外护甲穿透加成——这个效果能够无视目标的来自装备、增益、符文的护甲值。"
                  }
                ],
                "data": {
                  "hp": "523.0000",
                  "hpperlevel": "87.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.9000",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6970",
                  "attackspeedperlevel": "2.5000",
                  "armor": "30.0000",
                  "armorperlevel": "3.4000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "青钢影",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Camille.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big164000.jpg",
                "story": "卡蜜尔是菲罗斯家族的首席密探，游走于法律已然失效的边缘地带，小心地维护着皮尔特沃夫这台机器和其下的祖安，保证一切都能顺畅地运转。灵活而精准的她认为，任何浮皮潦草的技术都是必须被禁绝的丑陋行径。她的心智也如同身下的刀刃一般锐利。为了追求极致，她对自己进行了大幅度的海克斯人体增强手术。这也让很多人不禁怀疑，她根本就是一台纯粹的机器，而不是一个女人。",
                "usageTips": "尽量先等你的队友吸引敌方队伍的注意力，然后再用【E钩索】接近敌方脆弱的后排英雄们。",
                "battleTips": "卡蜜尔的护盾只能对一种伤害类型生效，所以尽量趁她无法抵挡你的伤害时对她发起攻击。",
                "name": "卡蜜尔",
                "defense": "6",
                "magic": "3",
                "difficulty": "4",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Camille_Passive.png",
                    "title": "适应性防御",
                    "description": "卡密尔对敌方英雄发起的攻击会基于该英雄的伤害类型(物理或魔法)提供专门吸收该类型伤害大(20%生命值)护盾，持续2秒。这个效果有(16-10)秒的冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CamilleQ.png",
                    "title": "精准礼仪",
                    "description": "卡蜜尔的下次攻击造成额外的(20%/25%/30%/35%/40%AD)物理伤害并为她提供20%移动速度，持续1秒。这个技能可以在接下来的3.秒里无消耗地再次施放。\n如果再次施放的攻击与第一段攻击相距至少1.5秒，那么额外伤害会提升至(40%AD)，并且该次攻击的(40%—100%)伤害转化为真实伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CamilleW.png",
                    "title": "战术横扫",
                    "description": "卡蜜尔蓄力然后切割，造成(70/100/130/160/190 +60%额外AD)物理伤害 。\n被外沿命中的敌人会被减速80%，减速效果会在2秒里持续衰减，并会受到额外的(6%/6.5%/7%/7.5%/8% +0.033*额外AD)最大生命值的物理伤害。卡蜜尔会回复对敌方英雄们的100%实际额外伤害值的生命值。\n这个技能对非史诗级野怪造成的伤害减少50%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CamilleE.png",
                    "title": "钩索",
                    "description": "卡蜜尔发射一个可附着在地形上的钩索，并将卡蜜尔拉到钩索处，并使此技能可以在1秒内再次施放。\n再次施放：卡蜜尔从墙体上跳下，与命中的第一个敌方 英雄进行碰撞。在着陆时，她会对附近的敌人造成(60/95/130/165/200 +75%额外AD)物理伤害并对附近的敌方英雄造成0.75秒晕眩。朝着敌方英雄突进时，突进距离翻倍并在碰撞后提供(40%/45%/50%/55%/60%)攻击速度，持续5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CamilleR.png",
                    "title": "海克斯最后通牒",
                    "description": "卡蜜尔暂时变得无法被选取，并且跳向一个敌方英雄，打断引导状态并将该英雄锁定在一个区域内，并且该英雄怎样都无法逃出这个区域，持续(2.5/3.25/4)秒。其它敌人会\n被震开。她的攻击会对目标敌人造成额外的((5/10/15)加上(4%/6%/8%)当前生命值的魔法伤害)。\n这个技能会在卡蜜尔离开该区域后结束。"
                  }
                ],
                "data": {
                  "hp": "575.6000",
                  "hpperlevel": "85.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.8000",
                  "mp": "338.8000",
                  "mpperlevel": "32.0000",
                  "mpregen": "8.1500",
                  "mpregenperlevel": "0.7500",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "2.5000",
                  "armor": "35.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "暴怒骑士",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kled.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big240000.jpg",
                "story": "无畏而且无赖的约德尔人克烈是诺克萨斯的意志化身、帝国士兵的仰慕偶像、长官眼里的定时炸弹、贵族鄙夷的送死小卒。很多军士都说，克烈参与了有史以来军团所挑起的每一场征战，“获得”了军中的每一份头衔，而且从来没有却步于任何一次战斗。虽然传闻总是不可全信，但至少有一件事毋庸置疑：只要克烈骑着胆小的斯嘎尔冲进战场，他的战斗便是为了保住所拥有的一切……或是抢走他想要的一切。",
                "usageTips": "【W暴烈秉性】的最后一次命中会造成比前三次命中更多的伤害——一定要打中喔！",
                "battleTips": "在克烈处于非骑乘状态下时，要注意观察他的勇气槽——当勇气槽到达100%时，他就会重回骑乘状态并回复可观的生命值。",
                "name": "克烈",
                "defense": "2",
                "magic": "2",
                "difficulty": "7",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kled_P.png",
                    "title": "怯战蜥蜴斯嘎尔",
                    "description": "骑乘状态：斯嘎尔会替克烈承受伤害。当斯嘎尔的生命值耗尽时，会导致克烈进入非骑乘状态。额外生命值属性只会施加给斯嘎尔。\n非骑乘状态：克烈更换若干技能。他在朝敌方英雄移动时会获得185移动速度，但普攻只会对敌方英雄造成80%伤害。\n克烈可通过击杀敌方英雄和小兵、攻击敌方英雄、建筑物和史诗级野怪来回复勇气。在勇气到达100时，克烈会骑着80%生命值的斯嘎尔重回骑乘状态。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KledQ.png",
                    "title": "飞索捕熊器",
                    "description": "克烈扔出一个捕熊器，来钩住命中的第一个敌方英雄或大型野怪并造成(30/55/80/105/130 +60%额外AD)物理伤害，并提供真实视野。对沿途的小兵造成150%伤害。\n如果克烈在被钩住的目标附近呆1.75秒，那么他就会造成(60/110/160/210/260 +120%额外AD)物理伤害，将该目标拽向他，然后使该目标减速(40%/45%/50%/55%/60%)，持续1.5秒，并施加60%重伤效果，持续5秒。\n非骑乘状态:变成[随身手枪]，一个可以回复勇气的远程技能。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KledW.png",
                    "title": "暴烈秉性",
                    "description": "被动:当克烈进行普攻时，他会陷入狂热状态，获得150%攻击速度，持续4次攻击或4秒。[W暴烈秉性]随后会进入冷却期。\n第四击会造成额外物理伤害，数值相当于(20/30/40/50/60)加上目标(4.5%/5%/5.5%/6%/6.5%+(5%额外AD))最大生命值(对野怪的最大伤害: 200)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KledE.png",
                    "title": "比武",
                    "description": "克烈发起突进，对沿途的敌人造成(35/60/85/110/135 +60%额外AD)物理伤害。无法穿过墙体。\n如果比武命中了一名敌方英雄或大型野怪，那么克烈就会获得50%移动速度，持续1秒并获得目标的真实视野。他可以在3秒内再次施放这个技能来回身突进，穿过初始目标，同时造成等额伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KledR.png",
                    "title": "冲啊——！！",
                    "description": "克烈朝着一个位置发起冲锋，同时留下一条定向加速的轨迹。在冲锋时，克烈会获得一个持续增长的护盾，护盾生命值最高可达(200/300/400 +300%额外AD)并在冲锋结束后持续2秒\n斯嘎尔会撞击命中的首个敌方英雄，并基于移动距离最多造成目标(12%/15%/18% )最大生命值的物理伤害。"
                  }
                ],
                "data": {
                  "hp": "340.0000",
                  "hpperlevel": "70.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "65.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.5000",
                  "armor": "35.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "皮城执法官",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Vi.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big254000.jpg",
                "story": "蔚曾经是祖安黑街上的破坏分子。她性格急躁、脾气火爆、凶神恶煞，对权威满心不屑。蔚从小到大都是孤身一人，所以练就了一身生存的本能，也培养了一种恶毒刻薄的幽默感。现在，蔚与皮尔特沃夫守卫一起合作，维护着皮城的安宁。靠着她手上的一副巨型海克斯科技拳套，无论是铜墙铁壁还是心理防线都不在话下。",
                "usageTips": "一记完全充能的【Q强能冲拳】会造成双倍伤害。它在追击并了结溃散的敌人时非常有效。",
                "battleTips": "蔚会在使用终极技能时免疫控制效果。记得把你的位移技能留到她的冲锋完成之后使用。",
                "name": "蔚",
                "defense": "5",
                "magic": "3",
                "difficulty": "4",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/ViPassive.png",
                    "title": "爆裂护盾",
                    "description": "蔚在用一个已激活的技能命中一名敌人后，会获得88 = (15%生命值)护盾值，持续3秒。这个技能有16=(16 一12)秒的冷却时间，每当蔚触发爆弹重拳时，剩余的冷却时间就会减少3秒"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViQ.png",
                    "title": "强能冲拳",
                    "description": "开始蓄力:蔚开始积蓄一次强力的冲拳，自身减速15%。\n释放:蔚向前冲刺，基于蓄力时间造成(55/80/ 105/ 130/ 155 +70% 额外AD)到(110/ 160/210/260/310 +140% 额外AD)物理伤害，并对命中的所有敌人施加爆弹重拳。蔚会在与一名敌方英雄碰撞时停下，并将其击退。\n命中的非英雄单位会被拉向蔚。\n如果蓄力被打断，那么这个技能会进入为期3秒的冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViW.png",
                    "title": "爆弹重拳",
                    "description": "被动:对相同目标的每第三次攻击会造成额外(4%/5.5%/7%/8.5%/10% +0.029% 额外AD)最大生命值的物理伤害，移除目标20%护甲，并为蔚提供(30%/37.5%/ 45%/52 5% 160%)攻击速度，持续4秒。它还可缩短爆裂护盾3秒冷却时间。最大生命值伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViE.png",
                    "title": "透体之劲",
                    "description": "蔚的下次攻击对目标和目标后方的敌人造成 (10/30/501 70/90 +110AD +90% Ap )物理伤害。这个技能有2层充能(12.96/ 11.57 /10.19/8.8/ 7.41秒充能时间)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViR.png",
                    "title": "天霸横空烈轰",
                    "description": "蔚挑出一名敌方英雄，将其显形并不可阻挡地向其冲刺。在触碰到该英雄后，蔚会将其击飞1.25秒，并造成 (150/325/500 +110% 额外AD)物理伤害。\n任何与蔚产生碰撞的敌人都会受到伤害、被震开井晕眩0.75秒。"
                  }
                ],
                "data": {
                  "hp": "585.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "10.0000",
                  "hpregenperlevel": "1.0000",
                  "mp": "295.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.6500",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "2.0000",
                  "armor": "30.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "暗裔剑魔",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Aatrox.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big266000.jpg",
                "story": "亚托克斯和他的同胞们曾是恕瑞玛对抗虚空之地时满载荣耀的守护者一族，最终却变成了符文之地的一个更大的威胁，并且仅被击败于被诡诈的致命巫术。但在被囚禁了数个世纪后，亚托克斯率先找到重获自由之法，那就是对那些蠢得妄图尝试挥舞那把含有他灵魂精华的神奇武器的愚妄之徒进行腐蚀和转化。现在，凭借偷来的血肉躯体，他以一种近似他之前形态的凶残外表行走于符文之地中，寻求着一次毁天灭地且久未兑现的复仇。",
                "usageTips": "在施放【暗裔利刃】时使用【暗影冲决】可提升你命中敌人的几率。 诸如【恶火束链】或你友军的定身效果等控制技能，将有助于你【暗裔利刃】的起手。 在你确保能够逼迫对方一战时施放【大灭】。",
                "battleTips": "亚托克斯的攻击非常具有波段性，所以要利用好时间来规避他的命中区域。 亚托克斯的【恶火束链】在朝着边界或亚托克斯奔跑时会更容易离开。 在亚托克斯使用他的终极技能时，保持距离以防止他复活。",
                "name": "亚托克斯",
                "defense": "4",
                "magic": "3",
                "difficulty": "4",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Aatrox_Passive.png",
                    "title": "赐死剑气",
                    "description": "亚托克斯的攻击造成额外的(5%12%)最大生命值物理伤害并回复100%\n实际伤害值的生命值，对小兵时降低至25%。这个效果有 (24-12)秒冷却时间。\n对英雄和大型野怪进行的攻击和技能会使这个效果的冷却时间缩短2秒。用暗裔之刃的边缘命中时会使这个效果的冷却时间缩短4秒。\n对野怪最多造成100伤害。\n用技能命中多个目标时也仅会缩短一次冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AatroxQ.png",
                    "title": "暗裔利刃",
                    "description": "亚托克斯猛砸他的巨剑，造成 ( 10/30/50/ 70/90+60%AD )物理伤害。如果敌人被边缘命中，还会被暂时击飞并转而受到 (15 +90%AD)伤害。 这个技能可以再次施放两段，每段都会改变形状并比，上一段多造成25%伤害。\n对小兵造成55% ( 55% )伤害。对野怪的击飞时长翻倍。\n总攻击力收益：60%/65%/ 70%/ 75%/ 80%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AatroxW.png",
                    "title": "恶火束链",
                    "description": "亚托克斯发射一道锁链，对命中的第个敌人 造成持续1. 5秒的25%减速和56 (30/40/50/60/ 70+40%AD)物理伤害。英雄和大型野怪需要在1.5秒内离开被影响的区域，否则就会被拖拽到中心并再次受到等额伤害。\n对小兵造成双倍伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AatroxE.png",
                    "title": "暗影冲决",
                    "description": "被动:亚托克斯获得20%/22.5%/25%127.5%/30%全能吸血(对英雄)。\n主动:亚托克斯向前突进。他可以在其它技能的前摇阶段使用这个技能。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AatroxR.png",
                    "title": "大灭",
                    "description": "亚托克斯显现他真正的恶魔形态，恐惧附近的小兵3秒并获得在10秒里持续衰减的60%/80%/100%移动速度。在此期间，他还会获得20%/30%/40%攻击力并提升50%/75%/100%自我治疗效果。\n参与击杀英雄后会使这个效果的持续时间延长5秒并刷新移动速度效果。\n持续时间最多只会提升到10秒。"
                  }
                ],
                "data": {
                  "hp": "580.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "3.0000",
                  "hpregenperlevel": "1.0000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "5.0000",
                  "attackspeed": "0.6510",
                  "attackspeedperlevel": "2.5000",
                  "armor": "38.0000",
                  "armorperlevel": "3.2500",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "海兽祭司",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Illaoi.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big420000.jpg",
                "story": "俄洛伊的体格强横无比，却唯独会在她的坚定信仰面前屈身。作为大海兽的先知，她挥舞着一个巨大的金色神像，将敌人的灵魂抽离体外，完全击毁他们对现实的感知。所有对“娜伽卡波洛丝的真者”发起挑战的人，很快就会发现，俄洛伊从来不单打独斗——蟒行群岛的神明会与她并肩作战。",
                "usageTips": "触手是一种极好的力量来源。没有它们的帮助就不要进行战斗。",
                "battleTips": "避免被【越界信仰】击中己方多人，从而减少俄洛伊生成的触手数量。",
                "name": "俄洛伊",
                "defense": "6",
                "magic": "3",
                "difficulty": "4",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Illaoi_P.png",
                    "title": "古神先知",
                    "description": "俄洛伊彰显她神祗的风采，如果附近没有任何触手存在的话，就会在附近地形上生成一条触手(20=(20 - 7 )秒冷却时间)。触手不会自行攻击，但可通过俄洛伊的技能来让它们猛击。\n\n猛击造成92 = (10- 180 +120%AD +40%AP )物理伤害 。如果猛击对1名以 上的敌方英雄造成伤害，那么俄洛伊就会治疗自身5%的已损失生命值。\n当多条触手同时命中一名敌方英雄时，除第一条 之外的触手都只造成50%伤害。触手会持续到被击杀，或是俄洛伊不在其范围内长达30秒时消散。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IllaoiQ.png",
                    "title": "触手猛击",
                    "description": "被动:猛击伤害提升10%/ 15%/ 20%/ 25% I 30%% (当前提升: 9伤害)。\n\n主动:俄洛伊挥舞她的神像，使一条触手向前猛击 。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IllaoiW.png",
                    "title": "严酷训诫",
                    "description": "俄洛伊的下次攻击会使她跃向目标，造成额外的 [ 3%/3.5% /4%/4.5%/5% ] +0.02%AD最大生命值的物理伤害。当她进行打击时，附近的触手会对目标进行猛击。\n\n百分比生命值伤害的最小值为20/30/40/50/60。对抗野怪时，这个伤害的封顶值为300。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IllaoiE.png",
                    "title": "灵魂试炼",
                    "description": "俄洛伊从一名敌方英雄身上扯出灵魂，持续7秒。灵魂能像英雄一样被攻击， 并且所受的(25%/30%/35%/40%/45% +0.08%AD)伤害 会传递给本体。\n\n如果灵魂死亡或者离本体过远，那么目标会被标记10秒，并被减速80%，持续1.5秒。被标记的敌人将每5秒生成一条触手。\n\n触手会每(5 - 3)秒自动猛击次灵魂和被标记的敌人。\n\n被标记的敌人会被短时间显形。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IllaoiR.png",
                    "title": "过界信仰",
                    "description": "俄洛伊将她的神像砸入地面，对附近的敌人造成 ( 150/ 250/ 350 +50% 额外AD)物理伤害并且每命中一个敌方英雄就会生成一条触手。\n\n在接下来的8秒里，触手无法被选取，猛击变快50%， 并且[ W严酷训诫)的基础冷却时间降至2秒。\n\n来自灵魂试炼的灵魂如果被这个技能命中，也会生成一条触手。"
                  }
                ],
                "data": {
                  "hp": "585.6000",
                  "hpperlevel": "95.0000",
                  "hpregen": "9.5000",
                  "hpregenperlevel": "0.8000",
                  "mp": "300.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.5000",
                  "mpregenperlevel": "0.7500",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "5.0000",
                  "attackspeed": "0.5710",
                  "attackspeedperlevel": "2.5000",
                  "armor": "35.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "封魔剑魂",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Yone.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big777000.jpg",
                "story": "生前，他是永恩，是亚索同母异父的哥哥，是故乡剑术道场的知名弟子。但当他死在弟弟手下以后，却发现自己被精神领域中的恶毒灵体所侵扰，不得不用它自己的刀剑将它弑杀。如今，被诅咒的永恩戴上了它的恶魔面具，开始了不懈的追猎，他要猎尽所有同种的恶魔，从而查清自己究竟成为了什么。",
                "usageTips": "永恩的第二次攻击总能触发魔法伤害，在使用技能的时候不要忘记穿插使用普通攻击",
                "battleTips": "永恩的E技能会回到用原地，所以在必要的时候可以在E技能原地等待他回来，与他继续交战",
                "name": "永恩",
                "defense": "0",
                "magic": "0",
                "difficulty": "0",
                "attack": "0",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/YonePassive.Yone.png",
                    "title": "狩人之道",
                    "description": "永恩手持双剑，所以他的每第二次攻击可造成50%魔法伤害、\n永恩的暴击几率提升150%，但暴击伤害会降低10%。\n这个效果的计算顺序处在所有其它的暴击几率修正效果之后。\n额外暴击几率转化为攻击力的转化比：每1%暴击几率转化为0.4攻击力"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YoneQ.png",
                    "title": "错玉切",
                    "description": "永恩向前出剑，造成(20/4060/80/100 +100%AD)物理伤害。在命中时，错玉切回获得一层可叠加的效果，持续6秒。在积攒2层效果后，错玉切会使永恩向前冲刺，并带着一阵能够击飞敌人0.75秒的旋风，造成(20 +100%AD)物理伤害。\n这个技能可以暴击，施加攻击特效，会被控制技能打断，并且它的冷却时间和施放时间可通过攻击速度来缩短。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YoneW.png",
                    "title": "凛神斩",
                    "description": "永恩向前顺劈，造成(5/10/15/20/25)+目标(5.5%/6%/6.5%/7%/7.5%)最大生命值的物理伤害和目标(5.5%/6%/6.5%/7%/7.5%)最大生命值的魔法伤害。\n如果永恩命中了敌方单位，那么他会获得(40-60 +60%额外AD)护盾，持续1.5秒。每命中一名敌方英雄，护盾生命值都会提升。\n命中的首个敌方英雄会使护盾生命值提升100%，命中的所有后续敌方英雄都会使护盾生命值提升25%。\n这个技能的冷却时间和施放时间可通过 攻击速度来缩短。\n对小兵的最小总伤害值为(40-410)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YoneE.png",
                    "title": "破障之锋",
                    "description": "永恩进入灵体形态5秒，期间会将身体留在原地，并获得10%不断提升的移动速度。\n当灵体形态结束时，永恩会返回他的身体，并再次造成伤害，伤害值相当于他在此期间对英雄造成伤害的(25%/27.5%/30%/32.5%/35%)\n再次施放：提前结束灵体形态。\n在此期间，移动速度会从10%不断提升至30%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YoneR.png",
                    "title": "封尘绝念斩",
                    "description": "永恩对一条路径上的所有敌人进行打击，造成(100/200/300 +40%AD)物理伤害和(100/200/300 +40%AD)魔法伤害，传送至命中的最后一名敌方英雄身后，并将命中的所有敌人击飞向永恩。\n如果没有命中任何敌方英雄，那么永恩将闪烁至技能的最大距离处。"
                  }
                ],
                "data": {
                  "hp": "0.0000",
                  "hpperlevel": "0.0000",
                  "hpregen": "0.0000",
                  "hpregenperlevel": "0.0000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "0.0000",
                  "attackdamageperlevel": "0.0000",
                  "attackspeed": "0.0000",
                  "attackspeedperlevel": "0.0000",
                  "armor": "0.0000",
                  "armorperlevel": "0.0000",
                  "spellblock": "0.0000",
                  "spellblockperlevel": "0.0000",
                  "movespeed": "0.0000",
                  "attackrange": "0.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "腕豪",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Sett.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big875000.jpg",
                "story": "在与诺克萨斯的战争结束之后，艾欧尼亚的地下王国日渐兴起，瑟提在其中脱颖而出，成为了一方霸主。虽然他一开始只是纳沃利的搏击场里的无名小卒，但他有着一身蛮力，而且极其耐打，很快就名声鹊起。等到当地的搏击手尽数被他击败之后，瑟提靠着一腔勇武，掌控了自己曾经浴血奋战的搏击场。",
                "usageTips": "腕豪的W技能在拳意值达到最大时会拥有最大的护盾量和最高的真实伤害",
                "battleTips": "注意走位不要被腕豪的E技能拉倒，否则几乎无法躲避接下来的【蓄意轰拳】连招的真实伤害",
                "name": "瑟提",
                "defense": "5",
                "magic": "1",
                "difficulty": "2",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Sett_P.Sett.png",
                    "title": "沙场豪情",
                    "description": "瑟提的攻击会交替使用左重拳和右重拳。右重会跟着左重拳快速打出，并造成额外的(5-90 +50%额外AD)物理伤害。\n瑟提的每5%已损失生命值会为他提供额外的(0.2-2)生命回复。\n瑟提如果在2秒内没有打出右拳，那么就会重置回左拳。右重拳的攻击速度是左重拳的8倍。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Sett_Q.Sett.png",
                    "title": "屈人之威",
                    "description": "瑟提手痒难耐，渴望打架，在朝着敌方 英雄移动时获得30%移动速度，持续1.5秒。\n此外，瑟提的下两次攻击造成额外的(10/20/30/40/50)外加(1%/1.5%/2%2.5%/3% +1%)最大生命值的的物理伤害。\n攻击总会以一左一右的方式进行。每次重拳对小兵和野怪最多造成400伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Sett_W.Sett.png",
                    "title": "蓄意轰拳",
                    "description": "被动：瑟提将受到的100%伤害储存为豪意，储量上限为(50%生命值)。豪意在停止受到伤害的4秒后快速衰减。\n主动：瑟提将所有豪意迸发出来，获得100%迸发豪意值的护盾值，护盾值会在3秒里持续衰减。瑟提随后会轰出一记重拳，对中央的所有敌人造成(80/100/120/140/160)外加(25% +10%额外AD)迸发豪意值的真实伤害。不在中央的敌人会受到物理伤害最为替代。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Sett_E.Sett.png",
                    "title": "强手裂颅",
                    "description": "瑟提将他两侧的敌人猛撞到一起，造成(50/70/90/110/130 +60%AD)物理伤害并使他们减速50%，持续0.5秒。如果瑟提在每侧都至少擒住了一名敌人，那么所有敌人还会被晕眩1秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Sett_R.Sett.png",
                    "title": "叹为观止",
                    "description": "瑟提擒住一名敌方英雄，并在扛着改英雄前行时将其压制，然后将该英雄猛摔到地上，对着陆点附近的敌人造成(200/300/400)外加(40%/50%/60%)被擒英雄的额外生命值的物理伤害和持续1秒的99%减速。敌人离瑟提着陆的位置越远，受到的伤害越低。"
                  }
                ],
                "data": {
                  "hp": "600.0000",
                  "hpperlevel": "93.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.7500",
                  "armor": "33.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
            ]
          },
          {
            "name": "法师",
            "heroes": [
              {
                "title": "黑暗之女",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Annie.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big1000.jpg",
                "story": "既拥有危险夺命的能力，又拥有小大人儿的可爱模样，安妮是一名掌握着深不可测的占火魔法的幼女魔法师。安妮生活在诺克萨斯北边的山脚下，即使是在这种地方，她也仍然是魔法师中的异类。她与火焰的紧密关系与生俱来，最初是伴随着喜怒无常的情绪冲动出现的，不过后来她学会了如何掌握这些“好玩的小把戏”。其中她最喜欢的就是召唤她亲爱的泰迪熊提伯斯——一头狂野的守护兽。安妮已经迷失在了永恒的天真里。她在黑暗的森林中游荡，始终寻觅着能陪自己玩耍的人。",
                "usageTips": "安妮的终极必杀技和晕眩技能一起使用能够扭转局势。",
                "battleTips": "安妮召唤的巨熊提伯斯能烧伤他附近的敌方单位。切记远离被召唤出的提伯斯。",
                "name": "安妮",
                "defense": "3",
                "magic": "10",
                "difficulty": "6",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Annie_Passive.png",
                    "title": "嗜火",
                    "description": "在施放4次技能后，安妮的下一个伤害技能将会造成1.25/1.5/1.75秒（1级/6级/11级）晕眩"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AnnieQ.png",
                    "title": "碎裂之火",
                    "description": "安妮投出一团火球，造成（80/115/150/185/200+80%AP）魔法伤害。如果目标阵亡，那么安妮会返还法力值并缩短50%冷却时间。\n\n冷却时间（秒）: 4\n法力值消耗: 60/65/70/75/80\n范围: 625"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AnnieW.png",
                    "title": "焚烧",
                    "description": "安妮投射出一阵火焰波，造成（70/115/160/205/250+85%AP）魔法伤害。\n\n冷却时间（秒）: 8\n法力值消耗: 70/80/90/100/110\n范围: 600"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AnnieE.png",
                    "title": "熔岩护盾",
                    "description": "安妮为她自己或一名友方英雄提供持续3秒的（40/90/140/190/240+40%AP）护盾值，以及在1.5秒里持续衰减的20%-50% (在1-18级时)移动速度。当护盾处于激活状态时，敌人对护盾进行攻击时会受到（20/30/40/50/60+20%）魔法伤害。\n\n冷却时间（秒）: 14/13/12/11/10\n法力值消耗: 40\n范围: 800"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AnnieR.png",
                    "title": "提伯斯之怒",
                    "description": "安妮召唤她的小熊提伯斯，造成（150/275/400+75%AP）魔法伤害。在接下来的45秒里，提伯斯会灼烧附近的敌人，每秒造成（20/30/40+12%AP）魔法伤害，并且它的攻击会造成（50/75/100+15%AP）魔法伤害。安妮可以再次施放这个技能来控制提伯斯。\n\n如果安妮阵亡，提伯斯会暴怒，获得275%攻击速度和在3秒里持续衰减的100%移动速度。\n\n冷却时间（秒）: 120/100/80\n法力值消耗: 100\n范围: 600\n额外双抗：30/50/70\n额外生命值：0/900/1800"
                  }
                ],
                "data": {
                  "hp": "524.0000",
                  "hpperlevel": "88.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "418.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "50.4100",
                  "attackdamageperlevel": "2.6250",
                  "attackspeed": "0.5790",
                  "attackspeedperlevel": "1.3600",
                  "armor": "19.2200",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "625.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "卡牌大师",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/TwistedFate.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big4000.jpg",
                "story": "崔斯特·菲特是一名声名狼藉的纸牌高手和诈骗惯犯，世界上任何有人烟的地方都有他施展魅力和赌艺的足迹，让那些富人和痴人既羡慕又嫉恨。他很少会认真起来干一件事，总是用一抹轻蔑的微笑和一副漫不经心的随性面对每一天。无论面对什么情况，崔斯特的袖子里永远都会藏着一张王牌。",
                "usageTips": "潜行角色通常会在生命值较低时逃离战斗。利用命运技能发现潜行目标，并将其消灭。",
                "battleTips": "如果你的生命值很低，小心敌方的崔斯特使用命运技能来击杀你。",
                "name": "崔斯特",
                "defense": "2",
                "magic": "6",
                "difficulty": "9",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Cardmaster_SealFate.png",
                    "title": "灌铅骰子",
                    "description": "在击杀了一名单位后，崔斯特会投掷他的“幸运”骰，随机获得1到6的额外赏金。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/WildCards.png",
                    "title": "万能牌",
                    "description": "崔斯特掷出三张牌，各造成（60/105/150/195/240+65%AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PickACard.png",
                    "title": "选牌",
                    "description": "崔斯特开始洗牌，允许他再次施放来从3张牌中锁定一张，并且强化他的下次攻击。\n蓝牌造成（40/60/80/100/120+100%AD+90%AP）魔法伤害并回复50/75/100/125/150法力。\n红牌对附近的敌人造成（30/45/60/75/90+100%AD+60%AP）魔法伤害和持续2.5秒的30/35/40/45/50%减速。\n金牌造成（15/22.5/30/37.5/45+100%AD+50%AP）魔法伤害和1/1.25/1.5/1.75/2秒晕眩。\n\n冷却时间（秒）: 8/7.5/7/6.5/6\n法力值消耗: 40/55/70/85/100\n範圍: 200"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CardmasterStack.png",
                    "title": "卡牌骗术",
                    "description": "被动：每第4次攻击造成额外的65/90/115/140/165（+50%AP）魔法伤害。\n\n崔斯特还会获得20/25/30/35/40%攻击速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Destiny.png",
                    "title": "命运",
                    "description": "崔斯特专注于他的卡牌，提供地图上所有敌方英雄的真实视野，持续6/8/10秒。\n\n当命运技能被激活，再次使用该技能可以在1.5秒后将崔斯特传送到5500码以内的任何地方。\n\n冷却时间（秒）: 180/150/120\n法力值消耗: 100\n範圍: 5500"
                  }
                ],
                "data": {
                  "hp": "534.0000",
                  "hpperlevel": "94.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "333.0000",
                  "mpperlevel": "19.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "52.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6510",
                  "attackspeedperlevel": "3.2200",
                  "armor": "21.0000",
                  "armorperlevel": "3.1500",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "诡术妖姬",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Leblanc.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big7000.jpg",
                "story": "即使是在秘密团体黑色玫瑰的成员内部，乐芙兰也同样保持神秘，而乐芙兰这个名字也只是众多化名之一。这个皮肤惨白的女人自从诺克萨斯建国初期就开始操纵大小人物，推动事态发展。这位女法师能用魔法制造自己的镜像，她可以出现在任何地点、任何人面前、甚至同时现身于许多地方。乐芙兰永远都在暗处密谋策划，而她真正的动机和她变换不定的身份一样令人难以捉摸。",
                "usageTips": "魔影谜踪和故技重施一起释放，可以让对方难以猜出你要回到哪一个魔影谜踪的法阵上。",
                "battleTips": "乐芙兰的终极技能可以在她的技能施放期间，或是，少见地在一个遥远的位置创造一个假的乐芙兰。",
                "name": "乐芙兰",
                "defense": "4",
                "magic": "10",
                "difficulty": "9",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeBlancP.png",
                    "title": "镜花水月",
                    "description": "当乐芙兰跌至40%最大生命值以下时，她会隐形1秒并创造一个幻像（不能造成伤害），幻像最多可持续8秒。\n镜花水月有1分钟冷却时间"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeblancQ.png",
                    "title": "恶意魔印",
                    "description": "乐芙兰投出一个魔印，造成55/80/105/130/155(+40%AP)魔法伤害并标记目标3.5秒。\n\n用任一其它技能对被标记的目标造成伤害时，都会引爆魔印并造成55/80/105/130/155 (+40% 魔法输出)魔法伤害。\n\n冷却时间（秒）: 6\n法力值消耗: 50/60/70/80/90\n範圍: 700"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeblancW.png",
                    "title": "魔影迷踪",
                    "description": "乐芙兰位移至目标区域，对目标区域附近的敌人造成75/115/155/195/235(+60% 魔法输出)魔法伤害。\n\n在接下来的4秒内，她可以再次激活【魔影迷踪】来回到乐芙兰的初始位置。\n\n冷却时间（秒）: 18/16/14/12/10\n法力值消耗: 60/75/90/105/120\n範圍: 600"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeblancE.png",
                    "title": "幻影锁链",
                    "description": "乐芙兰抛出一个锁链来束缚命中的第一个敌人，造成50/70/90/110/130(+30% 魔法输出)魔法伤害并提供目标的真实视野。\n\n如果目标持续被束缚1.5秒，则乐芙兰会将其禁锢1.5秒，并造成附加的80/120/160/200/240(+70% 魔法输出)魔法伤害。\n\n冷却时间（秒）: 14/13.25/12.5/11.75/11\n法力值消耗: 50\n範圍: 925"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeblancR.png",
                    "title": "故技重施",
                    "description": "乐芙兰故技重施，将她放过的上一个技能再施放一次。\n\n故技重施版【恶意魔印】和【幻影锁链】造成（70/140/210+40%AP）魔法伤害，相应地，它们的印记和禁锢效果造成（140/280/420+80%AP）伤害。\n\n故技重施版【魔影迷踪】造成（150/300/450+75%AP）魔法伤害。\n\n冷却时间（秒）: 60/45/30\n法力值消耗: 无消耗\n範圍: 25000"
                  }
                ],
                "data": {
                  "hp": "528.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "334.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "54.8800",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.4000",
                  "armor": "21.8800",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "猩红收割者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Vladimir.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big8000.jpg",
                "story": "弗拉基米尔是一个渴望凡人鲜血的魔鬼，早在诺克萨斯帝国建立之初就开始干涉帝国的内政。他的血巫术不仅能超越自然规律延长他的寿命，而且还能让他随心所欲地控制其他人的身体和思想。在诺克萨斯贵族奢华的沙龙聚会上，这个能力让他围绕自己建立了狂热的教众，而在底层的后巷里，这个能力则让他吸干敌人的鲜血。",
                "usageTips": " 鲜血转换会在治疗弗拉基米尔之前立即对敌人造成伤害，令它成为游戏中最好的最后一击方法之一。",
                "battleTips": "尽量在【R血之瘟疫】引爆前干掉弗拉基米尔，否则每个被感染的英雄都会为弗拉基米尔提供治疗效果。",
                "name": "弗拉基米尔",
                "defense": "6",
                "magic": "8",
                "difficulty": "7",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/VladimirP.png",
                    "title": "血色契约",
                    "description": "每40生命值加成给予弗拉基米尔1法术强度，每1法术强度给予弗拉基米尔1.4生命值（不会自我循环叠加）。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VladimirQ.png",
                    "title": "鲜血转换",
                    "description": "弗拉基米尔汲取目标的生命力，造成（80/100/120/140/160+60%AP）魔法伤害并回复（20/25/30/35/40+35%AP）生命值。在使用2次这个技能后，弗拉基米尔会获得10%移动速度，持续0.5秒，并强化这个技能的下一次使用，持续2.5秒。\n\n这个技能的强化版会造成（148/185/222/259/296+111%AP）魔法伤害并回复额外的（30-200基于等级）加上（5%+0.04%AP）已损失生命值。\n\n冷却时间（秒）: 9/8/7/6/5\n法力值消耗: 无消耗\n範圍: 600"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VladimirSanguinePool.png",
                    "title": "血红之池",
                    "description": "弗拉基米尔化成一滩持续2秒的血池，获得37.5%持续衰减的移动速度，持续1秒，并进入无法被选取和幽灵状态，同时使血池中的敌人减速40%。\n\n弗拉基米尔在期间会持续造成（80/135/190/245/300+10%额外生命值）魔法伤害，并为弗拉基米尔回复相当于15%实际伤害值的生命值。\n\n冷却时间（秒）: 28/25/22/19/16\n法力值消耗: 消耗20%当前生命值\n範圍: 350"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VladimirE.png",
                    "title": "血之潮汐",
                    "description": "开始蓄力：弗拉基米尔积攒一个蓄血库，至多消耗（8%生命值）生命值。在攒满后，弗拉基米尔会被减速20%。\n\n释放：弗拉基米尔对附近的敌人释放一股血弹激流，基于蓄力时间造成（30/45/60/75/90+1.5%生命值+35%AP）至（60/90/120/150/180+6%生命值+80%AP）魔法伤害。如果这个技能蓄力了至少1秒，那么它还会使目标减速40/45/50/55/60%，持续0.5秒。\n\n冷却时间（秒）: 13/11/9/7/5\n法力值消耗: 引导过程消耗8%最大生命值\n範圍: 600"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VladimirHemoplague.png",
                    "title": "血之瘟疫",
                    "description": "弗拉基米尔生成一个剧毒瘟疫，使他的目标们所受伤害(所有来源)提高10%，持续4秒。在它消散后，弗拉基米尔对所有被感染的目标造成（150/250/350+70%AP）魔法伤害。如果弗拉基米尔命中了一名敌方英雄，那么就会治疗自身（150/250/350+70%AP）生命值，此后每命中一名敌方英雄会治疗自身额外的（75/125/175+35%AP）生命值。\n\n冷却时间（秒）: 150/135/120\n法力值消耗: 无消耗\n範圍: 625"
                  }
                ],
                "data": {
                  "hp": "537.0000",
                  "hpperlevel": "96.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "2.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "55.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.0000",
                  "armor": "23.0000",
                  "armorperlevel": "3.3000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "450.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "远古恐惧",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/FiddleSticks.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big9000.jpg",
                "story": "某物已在符文之地苏醒，远古且可怖。它潜伏于人类世界的边缘，被浓烈的妄想所吸引，以受害者的惊骇为食，人们将这永恒的恐怖之物称为费德提克。这个狂乱的拼凑之物挥舞着镰刀收割恐惧，摧毁不幸与之相逢的人的神志。当心乌鸦的声响，或是那近乎人形的怪物所发出的呓语吧……费德提克已经归来。",
                "usageTips": "当费德提克第一次使用技能命中时会造成恐惧效果",
                "battleTips": "在战场上千万留意这家伙的动态，突然跳出的稻草人必然会影响团战的天平",
                "name": "费德提克",
                "defense": "3",
                "magic": "9",
                "difficulty": "9",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FiddleSticks_Passive.png",
                    "title": "巫骇草人",
                    "description": "费德提克的饰品会被替换为【草间人】。\n草间人会随着费德提克的升级而获得强度提升，不断降低其冷却时间并提升其持续时间。\n6级：草间人在被布置后会使附近的敌方守卫显形6秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FiddleSticksQ.png",
                    "title": "恐惧",
                    "description": "被动：在非战斗状态且未被敌人看见，或假扮成一个【草间人】时，如果用一个技能对敌人造成了伤害，那么会使该敌人恐惧1秒。\n\n主动：恐惧一个目标1/1.25/1.5/1.75/2秒并造成相当于目标6/7/8/9/10%(+1%每50法术强度)当前生命值(最小值：40/60/80/100/120)的魔法伤害。\n\n如果目标近期被恐惧过，那么则会转而造成相当于目标12/14/16/18/20%(+2%每50法术强度)当前生命值(最小值：80/120/160/200/240)的魔法伤害。\n\n对野怪的最大伤害值为400。\n\n冷却时间（秒）: 15/14.5/14/13.5/13\n法力值消耗: 65\n範圍: 575"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FiddleSticksW.png",
                    "title": "五骨丰登",
                    "description": "吸取附近敌人们的灵魂，每秒造成（60/90/120/150/180+35%AP）魔法伤害，持续2秒，此外最终一段伤害造成相当于目标12%已损失生命值的伤害。\n\n费德提克治疗自身相当于30%折前伤害的生命值，对小兵的治疗效果降低至15%。\n\n如果进行了一次完整的引导，或是不剩任何东西可供吸取，那么就会返还60%冷却时间。\n\n对小兵的伤害降低至50%\n\n冷却时间（秒）: 9/8.5/8/7.5/7\n法力值消耗: 60/65/70/75/80\n範圍: 650"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FiddleSticksE.png",
                    "title": "夜割",
                    "description": "释放黑魔法至目标位置，在一个新月形区域内造成（70/105/140/175/210+50%AP）魔法伤害和持续1.25秒的30%减速效果。\n\n新月中央的敌人们在减速期间还会被沉默。\n\n冷却时间（秒）: 10/9/8/7/6\n法力值消耗: 40/45/50/55/60\n範圍: 850"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FiddleSticksR.png",
                    "title": "群鸦风暴",
                    "description": "引导1.5秒，然后闪烁到目标地点，同时放出成群的杀人鸦，每秒对范围内的所有敌方单位造成魔法伤害。\n\n此效果持续，最多造成125/225/325(+225%AP)总魔法伤害。\n\n冷却时间（秒）: 140/110/80\n法力值消耗: 100\n範圍: 800"
                  }
                ],
                "data": {
                  "hp": "580.4000",
                  "hpperlevel": "92.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "500.0000",
                  "mpperlevel": "28.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "55.3600",
                  "attackdamageperlevel": "2.6250",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1100",
                  "armor": "34.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "480.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "符文法师",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ryze.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big13000.jpg",
                "story": "瑞兹是符文之地广为人知的最老练的法师之一。他生于远古，饱经风霜，肩负着不可承受之重任。这位大法师的武器，是他无可摧折的决心和丰富的秘法学识，他一生都在寻找着世界符文 ——它们是令这世界从无到有、万物成形的原生魔法所留下的碎片。他一定要找到所有这些神秘的字符，以免落入恶人之手，因为瑞兹知道它们可能给符文之地带来怎样的浩劫。",
                "usageTips": "使用【Q超负荷】的被动来最大化伤害或移动速度。 【E法术涌动】的冷却时间很短，可以利用这点来将【涌动】效果散播到多个敌人身上。 在【R曲境折跃】的充能期间，瑞兹可以移动并施放其它技能，并且这么做不会导致传送门被取消。",
                "battleTips": "对那些身上带有【涌动】效果的人来说，瑞兹尤为危险。",
                "name": "瑞兹",
                "defense": "2",
                "magic": "10",
                "difficulty": "7",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Ryze_P.png",
                    "title": "奥术专精",
                    "description": "瑞兹的技能会造成基于他额外法力值加成的额外伤害，并且每100法术强度可使他的最大法力值提升5%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RyzeQ.png",
                    "title": "超负荷",
                    "description": "被动：【符文禁锢】和【法术涌动】可重置【超负荷】的冷却时间，并且填充一层符文(最多可至2层)，持续4秒。如果填满2层符文，那么【超负荷】会释放所有符文层数，来提供28%移动速度，持续2秒。\n\n主动：释放一次魔爆来对命中的第一个敌人造成（75/100/125/150/175+45%AP+3%法力值）魔法伤害。如果目标身上有涌动效果，那么【超负荷】会消耗该效果，来造成10%的提升伤害并弹射至附近带有涌动效果的敌人身上。\n\n冷却时间（秒）: 6\n法力值消耗: 40\n範圍: 1000"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RyzeW.png",
                    "title": "符文禁锢",
                    "description": "立刻造成（80/110/140/170/200+60%AP+4%法力值）魔法伤害并减速一名敌人35%，持续1.5 秒。如果目标身上带有涌动效果，那么它会转而被禁锢，并在此过程中消耗掉涌动效果。\n\n冷却时间（秒）: 13/12/11/10/9\n法力值消耗: 40/55/70/85/100\n範圍: 615"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RyzeE.png",
                    "title": "法术涌动",
                    "description": "发射一团法球来造成（60/80/100/120/140+30%AP+2%法力值）魔法伤害，并散播涌动效果至目标和所有附近的敌人，持续4 秒。\n\n如果试图施加涌动至一名已有该效果的敌人，那么该效果会进一步散播。\n\n冷却时间（秒）: 3.25/3/2.75/2.5/2.25\n法力值消耗: 40/55/70/85/100"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RyzeR.png",
                    "title": "曲境折跃",
                    "description": "被动：【超负荷】的伤害加成对带有涌动效果的目标时会提升至40%。\n\n主动：瑞兹会开启一个传送门来通向另一个位置。在2秒后，站在传送门附近的所有友军会被传送至该位置。\n\n如果瑞兹陷入无法施法或移动的状态，那么【曲境折跃】会被取消。\n\n冷却时间（秒）: 210/180/150\n法力值消耗: 100\n範圍: 3000"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "98.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.8000",
                  "mp": "300.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "1.0000",
                  "attackdamage": "56.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1120",
                  "armor": "22.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "34.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "堕落天使",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Morgana.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big25000.jpg",
                "story": "在天界与凡间双重本性的夹缝中左右为难的莫甘娜，束缚了自己的双翼并拥抱了人性，还将自己的痛苦和怨恨施加给那些失信和堕落之人。她抗拒一切在她眼中不公正的法律和传统，并从德玛西亚的黑影中投出保护的盾牌和暗焰的锁链，在别人想要打压的暗处为真理而战。最重要的是，莫甘娜坚信，即使是被放逐、被遗弃的人，也可能有朝一日东山再起。",
                "usageTips": "增强生存力的道具可以使莫甘娜在联合使用黑暗之盾与灵魂镣铐时变得极难杀死。",
                "battleTips": "莫甘娜经常需要使用【Q暗之禁锢】来发起其他进攻，把小兵当护盾，从而躲避【Q暗之禁锢】。 【E黑暗之盾】让莫甘娜免疫限制效果，但这个护盾可以被魔法伤害给灌爆。",
                "name": "莫甘娜",
                "defense": "6",
                "magic": "8",
                "difficulty": "1",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/FallenAngel_Empathize.png",
                    "title": "灵魂吸取",
                    "description": "莫甘娜的技能会治疗自身，数额相当于这些技能对英雄、大型小兵和野怪造成的实际伤害的20%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MorganaQ.png",
                    "title": "暗之禁锢",
                    "description": "莫甘娜掷出一团星火来禁锢命中的第一个敌人(2/2.25/2.5/2.75/3)秒，并造成(80/135/190/245/300 +90%AP)魔法伤害。\n冷却时间：10\n法力消耗：50/55/60/65/70"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MorganaW.png",
                    "title": "折磨之影",
                    "description": "莫甘娜点燃一个区域5秒，每秒对区域内的敌人造成(12/22/32/42/52 (基于敌方英雄已失去的生命值) +14%AP)魔法伤害，基于目标的已损失生命值，伤害最多可提升至170%。 \n这个技能对于非史诗级野怪造成150%伤害。\n冷却时间：12\n法力消耗：70/85/100/115/130"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MorganaE.png",
                    "title": "黑暗之盾",
                    "description": "莫甘娜为一名友方英雄提供(80/135/190/245/300 +70%)魔法护盾，持续5秒。护盾会挡住限制和定身效果，直至被打破为止。\n冷却时间：26/24/22/20/18\n法力消耗：80"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MorganaR.png",
                    "title": "灵魂镣铐",
                    "description": "莫甘娜将她自己与附近的敌方英雄用锁链拷在一起，造成(150/225/300 +70%AP)魔法伤害并使其减速20%/。在3秒后，仍未打破锁链的敌人会受到(150/225/300 +70%AP)魔法伤害并被晕眩1.5秒。\n在施放时，莫甘娜在朝着被她拷住的敌人移动时会获得5%移动速度。\n莫甘娜会获得所有受到这个技能影响的敌人们的真实视野。\n冷却时间：120/110/100\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "560.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.4000",
                  "mp": "340.0000",
                  "mpperlevel": "60.0000",
                  "mpregen": "11.0000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "56.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.5300",
                  "armor": "25.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "450.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "死亡颂唱者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Karthus.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big30000.jpg",
                "story": "卡尔萨斯是湮灭的使者，是不死的亡灵。从来都是未见其恐怖身影，先闻其鬼魅挽歌。活着的人惧怕那些永世不得超生的亡灵，但卡尔萨斯却在亡灵的存在中只看到了美丽和纯洁，这是生与死的完美融合。当卡尔萨斯从暗影岛获得新生的时候，他决心要担任不死亡灵的使徒，把死亡的欣喜带给所有凡人。",
                "usageTips": "让友军帮忙，告诉你何时使用安魂曲，从而击杀在不同线的对手。",
                "battleTips": "卡尔萨斯死后能够施放短暂的技能。远离他的尸体以保安全。",
                "name": "卡尔萨斯",
                "defense": "2",
                "magic": "10",
                "difficulty": "7",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Karthus_Passive.png",
                    "title": "死亡契约",
                    "description": "在死亡时，卡尔萨斯会化为一个灵体，让他能够免费地施放技能，持续7秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarthusLayWasteA1.png",
                    "title": "荒芜",
                    "description": "在指针悬停处施放一次有延迟的爆炸魔法，对范围内的每个敌人造成45/65/85/105/125(+35%*AP )魔法伤害。\n\n如果范围内只有1个敌人，那么此技能会造成双倍伤害。\n\n对野怪造成85%伤害。\n\n冷却时间（s）：1\n法力消耗 ：20/26/32/38/44"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarthusWallOfPain.png",
                    "title": "痛苦之墙",
                    "description": "创造一个持续5秒的墙体。穿过墙体的敌人会被减少15%的魔法抗性，以及40%/50%/60%/70%/80%的移动速度，持续5秒(但减速效果会随时间持续衰减)。\n\n墙体宽度 ：800/900/1000/1100/1200\n冷却时间（s）：15\n法力消耗 ：70"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarthusDefile.png",
                    "title": "亵渎",
                    "description": "被动：当卡尔萨斯击杀一个单位时，他会回复20/27/34/41/48法力。\n\n主动：每秒消耗30/42/54/66/78法力，来对附近的所有敌人造成每秒30/50/70/90/110(+20%*AP)魔法伤害。\n\n冷却时间（s）：0.5\n法力消耗 ：30/42/54/66/78"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarthusFallenOne.png",
                    "title": "安魂曲",
                    "description": "在引导3秒后，卡尔萨斯就会对所有敌方英雄(无视距离)造成200/350/500(+75%*AP ）魔法伤害。\n\n冷却时间 （s）：200/180/160\n法力消耗 ：100"
                  }
                ],
                "data": {
                  "hp": "528.0000",
                  "hpperlevel": "87.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "467.0000",
                  "mpperlevel": "30.5000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "45.6600",
                  "attackdamageperlevel": "3.2500",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1100",
                  "armor": "20.8800",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "450.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "冰晶凤凰",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Anivia.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big34000.jpg",
                "story": "艾尼维亚是一个充满仁慈的飞禽形态灵体，曾经历过无数次诞生、死亡与重生的轮回，始终眷顾着弗雷尔卓德。她是生于凛冽冰风中的半神，可以操控冰雪的元素之力，阻挡那些胆敢侵犯她家园的人。艾尼维亚指引并保护着北方贫瘠土地上的人类部落，而这些人类也将她奉为希望的象征和重大变革的预兆。她的每一次奋战都不遗余力，因为她知道，自己的记忆将会超越牺牲，长久地留存，而她也将在崭新的明天中重生。",
                "usageTips": "艾尼维亚在释放冰川风暴时极度依赖法力。试着出一些法力道具或击杀苍蓝雕纹魔像获得它的增益效果。",
                "battleTips": "当艾尼维亚在兵线上时试着gank他。人越多，越容易杀死她的凤凰蛋。",
                "name": "艾尼维亚",
                "defense": "4",
                "magic": "10",
                "difficulty": "10",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Anivia_P.png",
                    "title": "寒霜涅槃",
                    "description": "艾尼维亚受到致命伤害后化身为凤凰蛋，回复满血并获得-40/-25/-10/5/20护甲和魔抗。如果她能存活6秒，则获得重生。\n\n冷却时间240秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FlashFrost.png",
                    "title": "寒冰闪耀",
                    "description": "艾尼维亚发射出一个巨型冰块，造成50/70/90/110/130 (+25%法术强度)魔法伤害。在到达距离终点时，冰块会爆炸，晕眩敌人1.1/1.2/1.3/1.4/1.5秒并造成70/105/140/175/210 (+50%法术强度) 魔法伤害。还会使这些敌人冰冻3秒，并使敌人减速20%。\n\n艾尼维亚可以在冰块飞行途中再次施放这个技能来将其提前引爆。\n\n【新增】现在，【Q – 寒冰闪耀】会对途经的所有敌方单位施加【冰冻】效果\n【已更新】现在，【Q – 寒冰闪耀】爆炸效果的实际位置已更新，不再像之前一样略微靠后"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Crystallize.png",
                    "title": "寒冰屏障",
                    "description": "艾尼维亚召唤出一道宽400/500/600/700/800码的无法通过的冰墙。冰墙会在融化前持续5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Frostbite.png",
                    "title": "霜寒刺骨",
                    "description": "艾尼维亚用一阵刺骨的寒风冲击一名敌人，造成 60/90/120/150/180 (+60%法术强度)魔法伤害。如果目标身上带有冰冻效果，则艾尼维亚会造成100/150/200/250/300（+100%*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GlacialStorm.png",
                    "title": "冰川风暴",
                    "description": "激活：艾尼维亚在目标区域召唤出一阵夹杂着冰与雹的强雨，来使敌人减速20%/30%/40%并每秒造成30/45/60（+12.5%*AP）魔法伤害。风暴会在1.5秒里持续提升规模。\n\n当风暴完全形成后，它会施加冰冻效果，使敌人减速30%/45%/60%，并且每秒造成120/180/240（+37.5%*AP） 魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "480.0000",
                  "hpperlevel": "82.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "495.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "51.3760",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.6800",
                  "armor": "21.2200",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "600.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "虚空行者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kassadin.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big38000.jpg",
                "story": "在世界上最黑暗的地方，卡萨丁切出了一道燃烧的裂口，他知道自己已经时日无多。他曾是恕瑞玛地区的向导和冒险家，后来选择在恕瑞玛南方平静的部落中安家落户——直到那一天，他的村庄被虚空吞噬。卡萨丁发誓要报仇雪恨，于是整合了许多秘法器物和禁忌之术，以便应对前方的险阻。最后，卡萨丁动身前往艾卡西亚的废土，准备面对任何虚空的造物，寻找那位自封的先知，玛尔扎哈。",
                "usageTips": "卡萨丁有多条装备路线，他可以通过法力和法术强度装备来做一个法师，或者出冷却缩减和魔法抗性的装备来对抗对方的法师。",
                "battleTips": "卡萨丁需要6次充能（周围有英雄施放技能）才能施放减【E能量脉冲】，使目标减速。如果他升级了该技能，你最好明智地使用你的技能，因为你在施放技能的同时会为卡萨丁充能。",
                "name": "卡萨丁",
                "defense": "5",
                "magic": "8",
                "difficulty": "8",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kassadin_Passive.png",
                    "title": "虚空之石",
                    "description": "卡萨丁所受的魔法伤害减少15%，并无视单位的碰撞体积。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NullLance.png",
                    "title": "虚无法球",
                    "description": "卡萨丁发射虚无法球，对目标造成65/95/125/155/185（+70%*AP）魔法伤害并打断目标的技能引导。\n\n满溢的能量会环绕于卡萨丁，提供一个可以吸收40/70/100/130/160（+30%*AP）魔法伤害的护盾，护盾持续1.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NetherBlade.png",
                    "title": "虚空之刃",
                    "description": "被动：卡萨丁的普通攻击从虚空中吸取法力，造成20（+10%*AP）额外魔法伤害。\n\n主动：卡萨丁给他的虚空之刃充能，使他的下次普通攻击造成70/95/120/145/170（+80%*AP）额外魔法伤害，并回复4% /4.5% / 5% /5.5%/6%的已损失法力值（如果攻击的对象是敌方英雄，那么法力回复提升至20%/22.5%/25% /27.5%/30%）。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ForcePulse.png",
                    "title": "能量脉冲",
                    "description": "卡萨丁会从临近的每次施法中吸收能量，附近每有一次技能施放，就会获得一层效果。\n\n当效果到达6层后，卡萨丁就可以使用能量脉冲，来对前方锥形范围内的敌人造成\n80/105/130/155/180（+80%*AP）魔法伤害以及50%/60%/70%/80%/90%减速效果，减速持续1秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RiftWalk.png",
                    "title": "虚空行走",
                    "description": "卡萨丁传送到附近的目标区域，着陆时对身边的所有敌方单位造成80/100/120（+2%*最大法力值 +40%*AP）魔法伤害。\n\n每在15秒内连续施放一次虚空行走，法力消耗就会翻倍，每层效果造成额外的40/50/60（+1%*最大法力值 +10%*AP）魔法伤害，最多可叠加4层。"
                  }
                ],
                "data": {
                  "hp": "576.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "397.6000",
                  "mpperlevel": "67.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "58.8520",
                  "attackdamageperlevel": "3.9000",
                  "attackspeed": "0.6400",
                  "attackspeedperlevel": "3.7000",
                  "armor": "19.0000",
                  "armorperlevel": "2.8000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "邪恶小法师",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Veigar.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big45000.jpg",
                "story": "维迦是热衷于黑暗巫术的大师。几乎没有哪个凡人敢碰的恐怖力量，他却能敞开怀抱。作为拥有自由精神的班德尔城居民，他渴望突破约德尔魔法的边界，于是转而研究那些被隐藏数千年的秘法文字。现在他已经变成了一个偏执的生物，对宇宙的奥秘无限向往。人们总会低估维迦的力量。虽然维迦发自心底地觉得自己是邪恶的，不过他的一些心底的原则也的确会让人质疑他的深层动机。",
                "usageTips": "维迦极度依赖和冷却缩减。买一些增加以上属性的装备，会令你的被动技能更有效，并能让你可以施放更多的黑暗祭祀。",
                "battleTips": "黑暗物质能造成巨大伤害，但可以避免。留意声音和视觉指示，了解技能施放的时间和地点。",
                "name": "维迦",
                "defense": "2",
                "magic": "10",
                "difficulty": "7",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Veigar_Entropy.png",
                    "title": "超凡邪力",
                    "description": "维迦的技能会在命中地方英雄时为他提供一层超凡邪力。参与击杀英雄后会提供额外的5层。\n每层超凡邪力会为维迦提供1法术强度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VeigarBalefulStrike.png",
                    "title": "黑暗祭祀",
                    "description": "维迦释放一束黑暗能量，对命中的头2个敌人造成80/120/160/200/240(+60%AP)魔法伤害。\n用此技能击杀一个单位，还会为维迦提供一层超凡邪恶效果。英雄、大型小兵和大型野怪会提供2层。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VeigarDarkMatter.png",
                    "title": "黑暗物质",
                    "description": "维迦召唤黑暗物质将从天而降，造成100/150/200/250/300(+100%)魔法伤害。\n每50层超凡邪力会使这个技能的冷却时间减少10%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VeigarEventHorizon.png",
                    "title": "扭曲空间",
                    "description": "维迦扭曲空间创造一个牢笼来使途径的敌人晕眩1.5/1.75/2/2.25/2.5秒。牢笼持续3秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VeigarR.png",
                    "title": "能量爆裂",
                    "description": "维迦用源力魔法引爆了目标一名敌方英雄，造成175/250/325(+75%AP)到350/500/650(+150%AP)魔法伤害，伤害提升幅度基于目标的已损失生命值。目标生命值在33%以下即可造成最大伤害。"
                  }
                ],
                "data": {
                  "hp": "505.0000",
                  "hpperlevel": "94.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "490.0000",
                  "mpperlevel": "26.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "52.0000",
                  "attackdamageperlevel": "2.7000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.2400",
                  "armor": "23.0000",
                  "armorperlevel": "3.7500",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "诺克萨斯统领",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Swain.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big50000.jpg",
                "story": "斯维因是一位高瞻远瞩的帝国统领，他指挥着帝国的战团在前线冲杀。虽然他在艾欧尼亚的战场上受到了重伤，但他靠着一只新生的恶魔手臂以及无情的意志夺得了诺克萨斯的权力。如今，这位统领正在向着只有他一人能看见的黑暗进军。",
                "usageTips": "在兵线上时，尽量试着从一个安全的距离上利用【解脱之手】的穿透伤害来消耗敌人。 【帝国视界】本身非常难以命中敌人，所以要时刻留意地图各处的遭遇战，来对那些被干扰了注意力或被控制的敌人使用，这样就能更易命中敌人。",
                "battleTips": "高机动性可以克制斯维因的所有基础技能：【解脱之手】是对越近的敌人造成越多伤害，【帝国视界】拥有一个非常久的延迟，而【永不复行】则必须是在返程途中才会特别危险。 购买任一带有【重伤】效果的装备，即在斯维因开启【恶魔升华】期间更易将他击杀。",
                "name": "斯维因",
                "defense": "6",
                "magic": "9",
                "difficulty": "8",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Swain_P.png",
                    "title": "狂食鸦群",
                    "description": "狂食：斯维因可以右键点击一名被定身的敌方英雄来将其拉向他，同时抽取一层魂屑并造成25-110(+30%AP)魔法伤害(10秒冷却时间)。\n鸦群：敌方英雄在死亡时会释放一片魂屑，斯维因可收集它们来恢复21-37生命值。每收集1层魂屑可使斯维因的最大生命值永久提升5."
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SwainQ.png",
                    "title": "解脱之触",
                    "description": "斯维因释放5道恶魔之力，这种力量可以穿透命中的目标。被命中的敌人受到55/75/95/115/135(+0.4*AP)魔法伤害，每道额外的恶魔之力多造成15%/20%/25%/30%/35%/伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SwainW.png",
                    "title": "帝国视界",
                    "description": "斯维因睁开恶魔之眼，来对最多5500/6000/6500/7000/7500距离处的敌人造成80/120/160/200/240(+0.7*AP)魔法伤害和25%/35%/45%/55%/65%的减速效果，减速持续2.5秒。\n被命中的英雄会为斯维因提供一片魂屑并显形4/5/6/7/8秒。\n对小兵时，伤害会减少50%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SwainE.png",
                    "title": "永不复行",
                    "description": "斯维因睁开恶魔之眼，来对最多5500/6000/6500/7000/7500距离处的敌人造成80/120/160/200/240(+0.7*AP)魔法伤害和25%/35%/45%/55%/65%的减速效果，减速持续2.5秒。\n被命中的英雄会为斯维因提供一片魂屑并显形4/5/6/7/8秒。\n对小兵时，伤害会减少50%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SwainR.png",
                    "title": "恶魔升华",
                    "description": "斯维因释放内心的恶魔，持续12秒，获得125/200/275生命值并使从魂屑中收集的额外生命值翻倍。每一秒他会从附近的敌方英雄、小兵和中立野怪身上吸取生命值，以造成35/50/65(+14%AP)魔法伤害,并治疗自身20/35/50(+16%AP)生命值。在一段延迟，并吸取了任意数额的生命值后，斯维因会获得释放[恶魔耀光]的能力。\n恶魔耀光：斯维因释放一道魂焰新星来造成相当于100/150/200(+50%)外加他37.5%已治疗生命值(伤害总和至多为200/300/400(+100%AP)的魔法伤害并结束他的变身。\n斯维因可以从非英雄单位身上吸取生命值，以获得相当于10%常规数值的治疗效果。"
                  }
                ],
                "data": {
                  "hp": "525.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.6500",
                  "mp": "468.0000",
                  "mpperlevel": "28.5000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "58.0000",
                  "attackdamageperlevel": "2.7000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1100",
                  "armor": "22.7200",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "蜘蛛女皇",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Elise.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big60000.jpg",
                "story": "诺克萨斯帝国最古老城市的地下深处，有一处不见天日的禁地，这是夺命的掠食者伊莉丝的宫殿。当她还是一个凡人的时候，她是曾经显赫一时的家族女主人，但是自从被一个卑鄙的半神咬伤之后，她就化身成了美丽的不死异类，一个形似蜘蛛的生物，用蛛网诱捕毫无防备的猎物。为了永葆青春，伊莉丝现在喜欢捕食那些无戒心、无信仰的人，而这世上也鲜少有人能够抗拒她的诱惑。",
                "usageTips": "蜘蛛形态在收割残血敌人时非常有效；人类形态的神经毒素能对生命充沛的敌人造成更多伤害。",
                "battleTips": "在你的生命值很低时，伊莉丝的蜘蛛形态会更加危险，并且在你生命值很高时，她的人类形态会更有威胁。",
                "name": "伊莉丝",
                "defense": "5",
                "magic": "7",
                "difficulty": "9",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/ElisePassive.png",
                    "title": "蜘蛛女皇",
                    "description": "人类形态:当伊莉丝的技能命中一名敌人时,就会让一只小蜘蛛准备就绪。蜘蛛形态:伊莉丝召唤她的小蜘蛛们去攻击附近的敌人。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EliseHumanQ.png",
                    "title": "神经毒素 / 剧毒之蜇",
                    "description": "人类形态:造成相当于40/75/110/145/180加上目标当前生命4%的魔法伤害。对野怪最多造成75/100/125/150/175额外伤害。\n蜘蛛形态:用剧毒之蜇刺向一个目标，造成70/110/150/190/230加上相当于目标已损失生命值8%的魔法伤害。对野怪最多造成75/100/125/150/175额外伤害。剧毒之蛰会施加攻击特效。(AP加成:0.02 AP加成:0.03)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EliseHumanW.png",
                    "title": "自爆蜘蛛 / 掠行狂暴",
                    "description": "人类形态:召唤一个注满毒液的小蜘蛛[范围伤害为25%]爬向目标区域，并在靠近一个敌人或者3秒后爆炸，造成560/105/150/195/240 (+0.95AP)魔法伤害。\n蜘蛛形态:被动技能:小蜘蛛们获得5/10/15/20/25%攻击速度加成。主动技能:为伊莉丝和她的小蜘蛛们提升60/80/100/120/140%的攻击速度,持续3秒。自爆蜘蛛的视野半径为400. (AP加成:0.95)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EliseHumanE.png",
                    "title": "结茧 / 盘丝",
                    "description": "人类形态:将命中的第一个敌人晕眩并暴露,持续1.6/1.7/1.8/1.9/2秒。\n蜘蛛形态:对敌人施放:伊莉丝和她的小蜘蛛们拉升至空中，然后降落到目标敌人身上。对地面施放:伊莉丝和她的小蜘蛛们拉升至空中，并且无法被选取,持续2秒。她可以再次施放盘丝来降落到附近一名敌人身上。盘丝冷却时间为22/21/20/19/18秒。在落地后的5秒里，使伊莉丝蜘蛛形态下的普攻所附带的额外伤害和治疗效果提升40/55/70/85/100%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EliseR.png",
                    "title": "蜘蛛形态",
                    "description": "蜘蛛形态:伊莉丝变成一只凶恶的蜘蛛,获得蛛形技能并召唤她的小蜘蛛们。蜘蛛形态:伊莉丝的每次攻击造成10/20/30/40额外魔法伤害并治疗自身4/6/8/10(+0.1法术强度)的生命值。并且获得25移动速度。小蜘蛛的生命值:85~390(85/95/105/115/125/135/145/160/175/190/210/230/250/275/300/325/355/390)，小蜘蛛们击中造成10/15/20/25(+0.15AP)的魔法伤害,并且所受的来自群体技能的伤害会减少10/20/30/40%"
                  }
                ],
                "data": {
                  "hp": "534.0000",
                  "hpperlevel": "93.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "324.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "55.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.7500",
                  "armor": "27.0000",
                  "armorperlevel": "3.3500",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "发条魔灵",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Orianna.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big61000.jpg",
                "story": "奥莉安娜曾是一个拥有血肉之躯的好奇女孩，而现在则是全身上下部由发条和齿轮构成的科技奇观。祖安下层地区的一次事故间接导致了她身染重病，日渐衰竭的身体必须替换为精密的人造器官，一个接一个，直到全身上下再也没有人类的肉体。她给自己制作了一枚奇妙的黄铜球体，既是伙伴，也是保镖。如今她已经可以自由地探索壮观的皮尔特沃夫，以及更遥远的地方。",
                "usageTips": "【E指令：防卫】可用于自身，让魔偶更快回到你身边。配合使用【E指令：防卫】和【Q指令：攻击】能对敌人进行快速骚扰。",
                "battleTips": "奥莉安娜只能对魔偶所在的区域附近产生影响。利用这点来创造优势。",
                "name": "奥莉安娜",
                "defense": "3",
                "magic": "9",
                "difficulty": "7",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/OriannaPassive.png",
                    "title": "发条协奏",
                    "description": "奥莉安娜的魔偶是其施放技能和攻击的焦点。如果奥莉安娜和魔偶相距太远,魔偶会自动回到她身边。奥莉安娜的普通攻击会造成额外魔法伤害,伤害值为34加上法术强度15%.4秒内对同个目标的连续攻击会造成额外20%魔法伤害。最多可叠加2次。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrianaIzunaCommand.png",
                    "title": "指令：攻击",
                    "description": "奥莉安娜指挥他的魔偶，对目标区域发射，对沿路目标造成60/90/120/150/180魔法伤害。每击中一个目标，伤害减少10%(最少造成40%)指令结束后,魔偶会留在目标区域。(AP加成:0.5)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrianaDissonanceCommand.png",
                    "title": "指令：杂音",
                    "description": "奥莉安娜指挥她的魔偶，释放一股电脉冲，对周围敌军造成60/105/150/195/240魔法伤害。此电脉冲留下持续3秒的能量场，降低敌人30/35/40/45/50%的移动速度,增加友军30/35/40/45/50%的移动速度,持续2秒。此效果随时间减弱。(AP加成:0.7)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrianaRedactCommand.png",
                    "title": "指令：防卫",
                    "description": "被动:魔偶为附属的友军增加10/15/20/25/30护甲和魔法抗性。主动:奥莉安娜指挥她的魔偶前往并附属在友军英雄身上，保护他们在接下来2.5秒免受60/100/140/180/220伤害。魔偶经过的敌方单位受到护盾75%的伤害:60/90/120/150/180(被动AP加成:0.3主动AP加成:0.5)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrianaDetonateCommand.png",
                    "title": "指令：冲击波",
                    "description": "奥莉安娜指挥他的魔偶，在短暂的延迟后释放一股冲击波，对周围敌军造成200/275/350 (+0.8 AP)魔法伤害并将他们朝魔偶的方向击飞。(AP加成:0.8)"
                  }
                ],
                "data": {
                  "hp": "530.0000",
                  "hpperlevel": "91.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "418.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "40.3680",
                  "attackdamageperlevel": "2.6000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "3.5000",
                  "armor": "17.0400",
                  "armorperlevel": "3.0000",
                  "spellblock": "26.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "复仇焰魂",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Brand.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big63000.jpg",
                "story": "基根·诺和曾经是一名普通弗雷尔卓德部族居民，现在则变成了另一种生物，布兰德。他的身世警醒着后人，被更强大的力量所诱惑会带来什么后果。基根为了追寻传说中的世界符文，背叛了自己的同伴并将符文据为己有，就在那一瞬间，这个人彻底消失了。他的灵魂被彻底燃尽，他的身躯成为了活体烈焰的容器，如今，布兰德游荡在瓦洛兰，寻觅着其他的符文。他曾遭受的苦难，凡人活上十几辈子也未必能够经历，而他发誓此仇必报。",
                "usageTips": "你可以对敌方英雄附近的小兵使用技能点燃他们，再使用烈火燃烧来溅射到敌方英雄。",
                "battleTips": "布兰德连招前一定要施放一个技能。躲避火焰烙印或烈焰之柱能打乱他的节奏。",
                "name": "布兰德",
                "defense": "2",
                "magic": "9",
                "difficulty": "4",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/BrandP.png",
                    "title": "炽热之焰",
                    "description": "布兰德的技能会对他的目标施加【烈焰焚身】效果,在4秒里持续造成共3%目标最大生命值的魔法伤害,最多可叠加3次。如果布兰德击杀了身上带有【烈焰焚身】的敌人,那么他会回复20-40(于1-18级)法力值。当【炽热之焰】在一名英雄或大型野怪身上叠至三层时,它会变得不稳定。它会在2秒内爆炸，在目标附近的区域施加法术特效并造成目标最大生命值的10-14%(每100法术强度+0.02)伤害。效果对野怪造成120%伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BrandQ.png",
                    "title": "火焰烙印",
                    "description": "布兰德向前扔出火球,造成80/110/140/170/200 (+0.55AP)魔法伤害。炽热之焰:如果目标已被点燃，火焰烙印会晕眩敌人1.5秒。(AP加成:0.55)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BrandW.png",
                    "title": "烈焰之柱",
                    "description": "在短暂延迟后,布兰德朝目标区域释放烈焰之柱,对该区域内的敌方单位造成75/120/165/210/255魔法伤害.炽热之焰:被点燃的单位受到额外25%的伤害。(AP加成:0.6)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BrandE.png",
                    "title": "烈火燃烧",
                    "description": "布兰德引燃目标,对其造成70/95/120/145/170 (+0.45法术强度)魔法伤害。炽热之焰:烈火燃烧将会扩散到附近敌人并造成伤害。(AP加成:0.45)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BrandR.png",
                    "title": "烈焰风暴",
                    "description": "布兰德释放烈焰风暴,在单位间反弹并造成100/200/300 (+0.25AP)魔法伤害,最多反弹5次。炽热之焰:如果目标已被点燃,烈焰风暴在下次反弹时优先反弹英雄。将优先引爆敌方英雄身上的【炽热之印】并附加30/45/60%的短暂减速效果火焰之种可以弹跳返回至布兰德(弹跳返回至布兰德之后，火焰之种将在布兰德身上短暂附着然后继续弹跳,以限制0距离的瞬间爆发)(AP加成:0.25)"
                  }
                ],
                "data": {
                  "hp": "519.6800",
                  "hpperlevel": "88.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "469.0000",
                  "mpperlevel": "21.0000",
                  "mpregen": "10.6650",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "57.0400",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.3600",
                  "armor": "21.8800",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "机械公敌",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Rumble.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big68000.jpg",
                "story": "兰博是一个有脾气的约德尔青年发明家。他仅用自己的双手和一堆废铁，就造出了一架巨大的机甲，上面还搭载了电击鱼叉和燃烧火箭弹等超常规武器。虽然其他人可能对他的垃圾场发明嗤之以鼻，但兰博根本不在乎——毕竟，喷出铄金之火的枪口，在他自己手里。",
                "usageTips": "尝试让自己待在危险温度从而将技能效果最大化。如果技能使用过于频繁，很容易过热。",
                "battleTips": " 留心兰博的热量条。如果他处于过热状态，那么当他被沉默时，可以趁机击杀他。",
                "name": "兰博",
                "defense": "6",
                "magic": "8",
                "difficulty": "10",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Rumble_JunkyardTitan1.png",
                    "title": "机械重组",
                    "description": "兰博的技能都能使其获得热量。当兰博的热量达到50%时,他会处于危险温度状态，所有基础技能都会获得额外效果。当热量达到100，他会进入过热状态,让他的普通攻击造成额外25(+5*等级)(+0.3AP)魔法伤害，并且冷却的6秒内无法施放技能。进入过热状态时过热衰减时间:4秒过热衰减比率:1秒过热衰减数值:每跳10点"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RumbleFlameThrower.png",
                    "title": "纵火盛宴",
                    "description": "兰博朝敌人喷火,在3秒的持续时间里对锥形范围内的敌人造成75/135/195/255/315(+1.0*法术强度)魔法伤害，每秒造成25/45/65/85/105(+0.33法术强度)点伤害,对小兵造成60/65/70/75/80%伤害。危险温度加成:造成50%额外伤害，在3秒的持续时间里对锥型范围内的敌人共造成180/220/260/300/340(+1.1法术强度)魔法伤害,每秒造成37.5/67.5/97.5/127.5/157.5(+0.5法术强度)点魔法伤害(AP加成:1.0)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RumbleShield.png",
                    "title": "破碎护盾",
                    "description": "兰博开启一个持续1.5秒的能吸收860/95/130/165/200伤害的护盾。兰博还会额外增加15/20/25/30/35%(处于危险温度时22.5/30/37.5/45/52.5%)移动速度,持续1秒。危险温度加成:护盾生命值120/165/210/255/300，移动速度加成提升50%。(AP加成:0.5)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RumbleGrenade.png",
                    "title": "电子鱼叉",
                    "description": "以充能方式运作(最大2层,每层充能耗时6秒，消耗10热量)兰博用最多两把鱼叉射击敌人，造成60/85/110/135/160(+0.4*法术强度)魔法伤害，并减速敌人30/40/50/60/70%，持续2秒(可叠加)。危险温度加成:伤害和减速百分比提升50%。(AP加成:0.4)冷却:0.5/0.5/0.5/0.5/0.5"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RumbleCarpetBomb.png",
                    "title": "恒温灼烧",
                    "description": "兰博朝目标区域发射一排火箭,制造一条持续5秒的燃烧小径，每秒造成140/210/280魔法伤害,并且移动速度减少35%。通过点击并将鼠标直线拖动，你可以控制攻击的施放位置。(AP加成:1.75)"
                  }
                ],
                "data": {
                  "hp": "589.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "100.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "1.8500",
                  "armor": "31.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "魔蛇之拥",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Cassiopeia.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big69000.jpg",
                "story": "卡西奥佩娅是个夺命的生物，决心要操纵其他人屈服于自己的阴险意志之下。身为诺克萨斯贵族杜·克卡奥家族的最年轻、最漂亮的女儿，她冒险深入恕瑞玛古代墓穴找寻远古的力量。在墓穴中，她被恐怖的墓穴守卫咬中，在毒液的作用下变成了毒蛇外形的掠食者。狡猾而敏捷的卡西奥佩娅如今在夜幕的掩护下蜿蜒滑行，用阴森的凝视让敌人石化。",
                "usageTips": "对被石化凝视眩晕的野怪和单位施放双生毒牙，可以达到最大伤害效果。",
                "battleTips": "当你已经中了卡西奥佩娅的毒时，要小心她的双生毒牙。",
                "name": "卡西奥佩娅",
                "defense": "3",
                "magic": "9",
                "difficulty": "10",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Cassiopeia_Passive.png",
                    "title": "优雅蛇行",
                    "description": "蛇女每级获得4点移动速度成长,蛇女无法购买鞋类道具。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CassiopeiaQ.png",
                    "title": "瘟毒爆炸",
                    "description": "在短暂的延迟后,蛇女向目前区域注入毒素,命中的目标会在3秒内受到75/110/145/180/215(+0.9AP)魔法伤害。当Q技能命中一名英雄时，蛇女获得30/35/40/45/50%移动速度加成,减速效果会在接下来的2秒钟内衰减。(AP加成:0.9)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CassiopeiaW.png",
                    "title": "剧毒迷雾",
                    "description": "施法距离:700蛇女向前方施放毒云，并在碰到目标或者到达最大距离时生成持续5秒的毒雾。在毒雾中的敌方单位将被减速40/50/60/70/80%，减速效果随时间的增长而衰减,且每秒都会受到20/25/30/35/40(+0.15AP)点魔法伤害。此外，受到毒雾影响的所有敌方英雄将不能使用位移技能。(AP加成:0.15)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CassiopeiaE.png",
                    "title": "双生毒牙",
                    "description": "对目标造成52-120(基于等级)(+0.1AP)魔法伤害。如果目标被【E双生毒牙】所杀，或在此技能弹道的飞行期间阵亡，卡西奥佩娅获得60/65/70/75/80法力值。如果【E双生毒牙】命中了中毒的目标，那么它会浩成10/20En-o人二号 平J中每的害并为卡西奥佩娅治疗法术强度的12/14/16/18/20%生命值,如果【E双生毒牙】命中了中毒的小兵和小型野怪，卡西奥佩娅所获得的治疗效果减少75%。(AP加成:0.1 AP加成:0.6)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CassiopeiaR.png",
                    "title": "石化凝视",
                    "description": "卡西奥佩娅对前方所有敌人造成150/250/350(+0.5AP)点魔法伤害。眩晕面朝她的敌人2秒,减速背朝她的敌人40%。(AP加成:0.5)"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5000",
                  "mp": "350.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "53.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6470",
                  "attackspeedperlevel": "1.5000",
                  "armor": "20.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "328.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "大发明家",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Heimerdinger.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big74000.jpg",
                "story": "塞西尔·B·黑默丁格教授是一个才华横溢但古怪反常的约德尔科学家。他被称赞为皮尔特沃夫前所未见的最具革新意识和受人尊敬的发明家之一。他不间断地工作，孜孜不倦地尝试解答宇宙中最费解的难题，达到了神经痴迷的程度。尽管他的理论经常会看起来晦涩难懂，但他也曾制造出许多发明，堪称皮尔特沃夫最为惊奇，当然也同样危险的机械，他也在不断改进自己的发明，让它们更加高效。",
                "usageTips": "炮塔的布局是战斗中一个决定性因素。对于大多数敌人，扎堆放置炮塔来攻击敌人是最佳方法，但如果敌人有很多群伤技能，那么你的防御塔可能很快就会被摧毁。此外，将炮塔放置在草丛中，可以进行有利于己方的突袭。",
                "battleTips": "要当心黑默丁格的终极技能，因为他能用终极技能来化解当前他所遇到的大部分麻烦。一旦他交出终极技能，就可以准备击杀他了！",
                "name": "黑默丁格",
                "defense": "6",
                "magic": "8",
                "difficulty": "8",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Heimerdinger_Passive.png",
                    "title": "海克斯科技亲和",
                    "description": "黑默丁格离友军防御塔或他放置的防御塔300码范围以内时，他获得20%移动速度加成"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HeimerdingerQ.png",
                    "title": "H-28 G 进化炮台",
                    "description": "黑默丁格放置一座炮台。炮台会优先攻击黑默丁格的目标，以及正在攻击黑默丁格的敌人。在黑默丁格远离炮台时,炮台会进入休眠模式。黑默丁格每20秒生产—套炮台配置，并能同时持有3套配置。H-28G进化炮台属性:生命值:150攻击-加农炮:每次攻击造成6/9/12/15/18 (+0.35AP)魔法伤害。攻击-激光:每过90秒造成40/60/80/100/120 (+0.55）魔法伤害。W的每一发导弹击中一个敌人英雄会让附近的炮台获得20%充能，如果E的手雷击中一个敌人英雄会让充能变成100%(被大招强化后的W和E也会获得同样的效果)(AP加成:0.35 AP加成:0.55)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HeimerdingerW.png",
                    "title": "海克斯科技微型导弹",
                    "description": "黑默丁格以鼠标悬停处为焦点,呈扇形发射5枚导弹。每枚导弹造成60/90/120/150/180(+0.45法术强度)魔法伤害。敌方单位可以被1枚以上的导弹命中，但额外导弹仅造成20%的魔法伤害(最大伤害值:108/162/216/270/324(+0.81魔法伤害)小兵仅受到60%的伤害。鼠标悬停点离黑默丁格越近，则导弹的分布越分散。每有一枚火箭命中敌方英雄都会立即为附近炮台充能20%。(AP加成0.45)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HeimerdingerE.png",
                    "title": "CH-2电子风暴手雷",
                    "description": "黑默丁格向目标区域投掷震荡手雷，对爆炸范围内的所有敌方单位造成60/100/140/180/220(+0.6法术强度)魔法伤害和35/35/35/35/35%减速效果，减速持续2/2/2/2/2秒。位于爆炸区域中心的敌人还会被晕眩1.5秒。手雷命中敌方英雄，会让附近1000码内的所有炮台立即充能完毕(AP加成0.6)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HeimerdingerR.png",
                    "title": "升级！！！",
                    "description": "黑默丁格的下个基础技能免费施放，并拥有额外效果H-28Q尖端炮台:放置一个炮台，常规武器为加农炮，造成90/110/130(+0.35*法术强度)魔法伤害，特殊武器为激光,每过8秒造成100/140/180(+0.7*法术强度)，并且攻击可以使目标减速。海克斯科技导弹集群:发射4波导弹，每一波都能造成135/180/225(+0.45*法术强度)魔法伤害。在多枚导弹命中相同英雄或野怪时,第一枚之后的导弹会造成较少的伤害(最大伤害值:550/690/865(+1.83*法术强度))CH-3X闪电手雷:扔出一颗可弹跳三次的手雷,每次弹跳造成150/200/250(+0.6*法术强度)魔法伤害。晕眩和减速区域变大,并且减速幅度提升至80%(AP加成:0.7)"
                  }
                ],
                "data": {
                  "hp": "488.0000",
                  "hpperlevel": "87.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "385.0000",
                  "mpperlevel": "20.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "55.5360",
                  "attackdamageperlevel": "2.7000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.3600",
                  "armor": "19.0400",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "狂野女猎手",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Nidalee.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big76000.jpg",
                "story": "在密林深处长大的奈德丽是一位追猎大师，她可以变换形态，成为一只凶猛的美洲狮。她既不是真正的人类，也不是真正的野兽，但她会用精心布置的陷阱和灵活自如的标枪，凶狠地捍卫自己的领地不被任何人侵犯。她会先打击猎物的行动能力，然后再以大猫的形态上前扑杀。只有很少数人曾侥幸逃脱并活下来讲述一个狂野女猎手的故事，讲述她锐利的猎手本能，以及更加锐利的爪子。",
                "usageTips": "【W丛林伏击】的陷阱会根据猎物的当前生命值来造成伤害。把它们放在己方队伍的后排，就可以在对面冲锋时帮助你的队伍对敌方的前排英雄造成伤害。",
                "battleTips": "奈德丽的【Q推倒】会对被捕猎的目标造成额外伤害，但她需要先近身才能使用这个技能。把你的控制技能或防御技能留到她过来拿人头的时候再用吧。",
                "name": "奈德丽",
                "defense": "4",
                "magic": "7",
                "difficulty": "8",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Nidalee_Passive.png",
                    "title": "寻觅",
                    "description": "在穿越草丛进行移动时，奈德丽的移动速度会提升10%，持续2秒，并在朝着1400码内的出现在己方视野内的敌方英雄进行移动时，移动速度加成会提升至30%。\n\n在【Q标枪投掷】或【W丛林伏击】对敌方英雄或野怪造成伤害后，奈德丽会开始捕猎目标英雄，持续4秒，不断提供目标的真实视野。在此期间，奈德丽获得10%移动速度加成（在朝着被捕猎的目标移动时，移动速度加成提升至30%)并且她对该目标的第一次【Q推倒】和【W猛扑】会对这些目标造成强化伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JavelinToss.png",
                    "title": "标枪投掷 / 推倒",
                    "description": "人类形态：奈德丽投掷她的标枪，造成70/90/110/130/150 (+50%法术强度)魔法伤害。如果标枪投掷的行驶距离超过了奈德丽的普攻距离，那么标枪的伤害就会增长，在最远距离处的伤害可达210/270/330/390/450 (+150%法术强度)魔法伤害。\n\n美洲狮形态：奈德丽下次普通攻击将造成额外伤害，敌方目标的生命值越低，造成伤害越大。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Bushwhack.png",
                    "title": "丛林伏击 / 猛扑",
                    "description": "人类形态：奈德丽放置一个隐形的陷阱，最多可持续2分钟。陷阱会在被敌人踩中时触发，让该敌人显形并在4秒里持续承受共40/80/120/160/200（+20%*AP）魔法伤害。\n\n地图上可同时存在4个陷阱（最大陷阱数会在6,11,16级时提升）。\n\n美洲狮形态：奈德丽向目标方向猛扑，落地时对附近的敌人造成范围伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PrimalSurge.png",
                    "title": "野性奔腾 / 挥击",
                    "description": "人类形态：奈德丽为目标友方英雄治疗35/55/75/95/115（+33%*AP）到70/110/150/190/230（+65%*AP）生命值（基于该英雄的已损失生命值)，并使该英雄的攻击速度提升20%/30%/40%/50%/60%，持续7秒。\n\n美洲狮形态：奈德丽朝着目标方向进行爪击，对面前的敌人造成伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AspectOfTheCougar.png",
                    "title": "美洲狮形态",
                    "description": "人类形态：奈德丽变身为一只狂野的美洲狮，并且她的基础技能将变为推倒，猛扑和挥击。\n美洲狮形态：奈德丽变回人类形态。在人类形态下时，只要触发一次捕猎效果，就会刷新【R美洲狮形态】的冷却时间。\n\n推倒伤害：5/30/55/80\n推倒伤害增幅：1%/1.25%/1.5%/1.75% \n猛扑伤害：60/110/160/210\n挥击伤害：80/140/200/260\n强化版猛扑冷却时间（s）：3/2.5/2/1.5"
                  }
                ],
                "data": {
                  "hp": "545.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "295.6000",
                  "mpperlevel": "45.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6380",
                  "attackspeedperlevel": "3.2200",
                  "armor": "28.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "酒桶",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Gragas.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big79000.jpg",
                "story": "豪爽而且威严的古拉加斯是一位身宽体胖、吵闹喧哗的酿酒大师，只为找到最完美的那一口麦酒。他从未知的地方前来，在弗雷尔卓德纯洁的荒原上寻找稀有的酿酒原料，尝试着各种不同的酿制配方。他总是酩酊大醉而且冲动鲁莽，他挑起的斗殴事件堪称传奇，经常造成一整夜的狂欢和殃及池鱼的破坏。只要古拉加斯在某地现身，接踵而至的往往是饮酒和闹事——就按这个顺序。",
                "usageTips": "醉酒狂暴的伤害减免效果在开始引导时就会生效，因此尽量在即将受到伤害时使用它。",
                "battleTips": "古拉加斯能够用其终极必杀技击退所有人。注意不要被撞向他，或者更糟——被撞向敌方防御塔。",
                "name": "古拉加斯",
                "defense": "7",
                "magic": "6",
                "difficulty": "5",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/GragasPassiveHeal.png",
                    "title": "欢乐时光",
                    "description": "古拉加斯每次使用技能后都会喝一小杯，立即恢复(8%*生命值)生命值。这个效果有8秒冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GragasQ.png",
                    "title": "滚动酒桶",
                    "description": "古拉加斯滚动出一个酒桶，该酒桶会在4秒后爆炸，造成80/120/160/200/240（+70%*AP）魔法伤害到120/180/240/300/360（+105%*AP）魔法伤害和持续2秒的40%/45%/50%/55%/60%到60%/67.5%/75%/82.5%/90%减速。伤害和减速会基于该酒桶在爆炸前的时长来提升。\n\n古拉加斯可以再次施放这个技能来提前引爆该酒桶。\n对小兵造成70%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GragasW.png",
                    "title": "醉酒狂暴",
                    "description": "古拉加斯畅饮他的佳酿，使即将到来的伤害降低10%/12%/14%/16%/18%（+0.04%*AP），持续2.5秒。在完成他的畅饮后，他的下次攻击对目标及目标身边的敌人们造成额外的20/50/80 /110/140（+60%*AP）外加7%最大生命值的魔法伤害。\n\n对野怪最多造成300伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GragasE.png",
                    "title": "肉弹冲击",
                    "description": "0古拉加斯向前冲锋，撞击命中的第一名敌人，使附近的敌人击飞1秒并对其造成80/130/180/230/280（+60%*AP）魔法伤害。\n\n当古拉加斯撞到一个敌人时，这个技能的冷却时间会缩短3秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GragasR.png",
                    "title": "爆破酒桶",
                    "description": "古拉加斯猛抛他的酒桶，造成200/ 300/ 400（+80%*AP）魔法伤害并将敌人们从爆炸中心击退。\n\n敌人不可以被这个技能击退得越过墙体。"
                  }
                ],
                "data": {
                  "hp": "600.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5000",
                  "mp": "400.0000",
                  "mpperlevel": "47.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6750",
                  "attackspeedperlevel": "2.0500",
                  "armor": "35.0000",
                  "armorperlevel": "3.6000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "330.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "狂暴之心",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kennen.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big85000.jpg",
                "story": "凯南不仅是捍卫艾欧尼亚均衡的迅猛如雷电的执法者，而且还是均衡教派中唯一的一名约德尔人。虽然他身体小巧，浑身绒毛，但他能用手里剑的风暴和无限的热情迎接任何敌人的威胁。他和自己的师父慎一起在精神领域巡逻，使用雷电的能量痛击敌人。",
                "usageTips": "【E奥义！雷铠】可以用来切入战场，因为击中敌人所带来的能量回复可以使他稍后使用其他技能。",
                "battleTips": "当你有【忍法！雷缚印】减益时，在凯南附近要小心。如果你身上的【忍法！雷缚印】到达3层，就会被晕眩。",
                "name": "凯南",
                "defense": "4",
                "magic": "7",
                "difficulty": "4",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kennen_Passive.png",
                    "title": "【忍法！雷缚印】",
                    "description": "凯南的技能会施加一层持续6秒的标记。在3层标记时，敌人会被晕眩1.25秒，并且凯南会获得25能量。\n\n如果一名敌人在6秒内被这个技能晕眩了不止一次，那么该敌人会被转而晕眩0.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KennenShurikenHurlMissile1.png",
                    "title": "奥义！千鸟",
                    "description": "凯南投掷一枚手里剑，对命中的第一个敌人造成75/115/155/195/235（+75%*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KennenBringTheLight.png",
                    "title": "奥义！电刃",
                    "description": "被动：每第5次攻击造成额外的10/20/30/40/50（+20%*AP +60%/70%/80%/90%/100%*额外AD）魔法伤害。\n\n主动：凯南释放一道电能爆裂，对附近被施加了【忍法！雷缚印】的敌人们造成60/85/110/135/160（+80%*AP）魔法伤害。\n\n被动效果会施加【忍法！雷缚印】。\n攻击建筑时会叠加被动效果，但不会消耗被动效果。主动技能也将命中所有处在【秘奥义！万雷天牢引】的敌人。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KennenLightningRush.png",
                    "title": "奥义！雷铠",
                    "description": "凯南变成一个闪电球，获得100%移动速度，在穿过敌人时造成80/120/160/200/240（+80%*AP）魔法伤害，同时变为幽灵状态，持续2秒。如果凯南对至少一名敌人造成了伤害，那么他会获得40能量。\n\n在这个技能结束后，凯南会获得40%/50%/60%/70%/80%攻击速度，持续4秒。凯南可以通过再次施放来提前结束这个技能。\n\n【幽灵】状态的单位能够无视其它单位的碰撞体积。对小兵和野怪造成50%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KennenShurikenStorm.png",
                    "title": "秘奥义！万雷天牢引",
                    "description": "凯南释放一阵魔法风暴，每0.5秒对附近的所有敌人造成40/75/110（+20%*AP）魔法伤害，并且获得20/40/60护甲和20/40/60魔法抗性，持续3秒。后续的每次命中会对已被影响的敌人造成10%提升伤害。\n\n敌人只会被这个技能施加3层【忍法！雷缚印】。"
                  }
                ],
                "data": {
                  "hp": "541.0000",
                  "hpperlevel": "84.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.6500",
                  "mp": "200.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "50.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "48.0000",
                  "attackdamageperlevel": "3.7500",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.4000",
                  "armor": "29.0000",
                  "armorperlevel": "3.7500",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "虚空先知",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Malzahar.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big90000.jpg",
                "story": "玛尔扎哈是一名狂热的先知，将自己的全部奉献给一切生命的大一统。他坚信，近来新出现的虚空就是符文之地的救赎。在恕瑞玛的沙漠荒原上，他曾一度追随着脑海中的窃窃私语，一路来到古艾卡西亚。在这片废墟中，他窥见了虚空深处的黑暗核心，被赋予了新的力量和目标。玛尔扎哈现在视自己为迷途羔羊的牧人，将其他人带进畜栏，或是放出藏身地下的虚灵生物。",
                "usageTips": "使用【Q虚空召唤】和【R冥府之握】来刷新敌人身上的【E煞星幻象】的持续时间。",
                "battleTips": "当玛尔扎哈用技能命中了一个感染了【E煞星幻象】的敌人时，【E煞星幻象】的持续时间会得到刷新。",
                "name": "玛尔扎哈",
                "defense": "2",
                "magic": "9",
                "difficulty": "6",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Malzahar_Passive.png",
                    "title": "虚空穿越",
                    "description": "玛尔扎哈获得90%伤害减免并免疫限制和定身效果，并且在受到或格挡了一次那些效果后，会存留0.25秒。这个效果有30/24/18/12(1-18级)秒的冷却时间。\n\n承受伤害将重启冷却时间。\n来自小兵的伤害不会被减免，但不会触发或重启冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MalzaharQ.png",
                    "title": "虚空召唤",
                    "description": "玛尔扎哈开启两扇通往虚空的传送门，在短暂的延迟后，能量就会从虚空之中喷发，并且在两道传送门之间对流，对击中的敌人造成70/105/140/175/210（+65%*AP）魔法伤害并使其沉默1/1.25/1.5/1.75/2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MalzaharW.png",
                    "title": "虚空虫群",
                    "description": "被动：玛尔扎哈的其它技能在施放时会为他提供一层效果(最多可叠到2层)。\n\n主动：玛尔扎哈召唤一个虚灵，并且每层效果会额外召唤一个虚灵。虚灵持续8/8/9/9/10秒并且每次命中造成额外的5~65(1-18级)（+40%*额外AD +20%*AP）（+12/14/16/18/20）魔法伤害。\n\n虚灵对中了【煞星幻象】的线上小兵造成300%伤害。\n虚灵对史诗级野怪造成50%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MalzaharE.png",
                    "title": "煞星幻象",
                    "description": "玛尔扎哈给目标造成可怕的幻象，在4秒里对一名敌人持续造成共80/115/150/185/220（+80%*AP）魔法伤害。在此期间，对目标使用【虚空召唤】或【冥府之握】会刷新幻象的持续时间。\n\n如果目标被击杀，玛尔扎哈会获得（2%*法力值）法力并且幻象会传播给相距最近的敌人。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MalzaharR.png",
                    "title": "冥府之握",
                    "description": "玛尔扎哈压制一名敌方英雄并在2.5秒里持续造成共125/200/275（+80%*AP）魔法伤害。一个负能量地带会在他目标周围生成，在5秒里持续造成共10%（+0.025%*AP）最大生命值的魔法伤害。\n\n虚无地带每秒伤害：2%/3%/4%（0AP时）"
                  }
                ],
                "data": {
                  "hp": "537.0000",
                  "hpperlevel": "87.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "375.0000",
                  "mpperlevel": "27.5000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "55.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.5000",
                  "armor": "18.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "光辉女郎",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Lux.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big99000.jpg",
                "story": "拉克珊娜·冕卫出身自德玛西亚，一个将魔法视为禁忌的封闭国度。只要一提起魔法，人们总是带着恐惧和怀疑。所以拥有折光之力的她，在童年的成长过程中始终担心被人发现进而遭到放逐，一直强迫自己隐瞒力量，以此保住家族的贵族地位。虽然如此，拉克丝的乐观和顽强让她学会拥抱自己独特的天赋，现在的她正在秘密地运用自己的能力为祖国效力。",
                "usageTips": "如果你不能很好的施放曲光屏障, 要切记它达到最大射程后会返回。 试着利用返回的曲光屏障保护友军。",
                "battleTips": "拉克丝有多个强大的区域控制技能。试着分散并从不同方向攻击，这样她就无法锁定某个特定区域。",
                "name": "拉克丝",
                "defense": "4",
                "magic": "9",
                "difficulty": "5",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuxIlluminatingFraulein.png",
                    "title": "光芒四射",
                    "description": "拉克丝的伤害型技能会标记目标6秒。拉克丝的攻击会消耗该标记，以造成20~190(1-18级 每级+10) （+20％*AP）魔法伤害。\n\n终极闪光在命中敌人时将先消耗标记然后刷新标记。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuxLightBinding.png",
                    "title": "光之束缚",
                    "description": "拉克丝朝目标地点发射一团光球，束缚前2名敌人2秒并对每个敌人造80/125/170/215/260（+60％*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuxPrismaticWave.png",
                    "title": "曲光屏障",
                    "description": "拉克丝扔出她的魔杖，为它途经的友军提供45/65/85/105/125（+35％*AP）护盾。随后它会折返，为它途经的友军提供等额的护盾。\n\n两段护盾可以叠加。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuxLightStrikeKugel.png",
                    "title": "透光奇点",
                    "description": "拉克丝创造一个光明地带，显形该区域并使区域中的敌人减速25%/30%/35%/40%/45%。在5秒后或再次施放这个技能后，它会爆炸，造成60/105/150/195/240（+60％*AP）魔法伤害并减速额外的1秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuxMaliceCannon.png",
                    "title": "终极闪光",
                    "description": "拉克丝发射一束耀目的光能射线，对一条直线上的所有敌人造成300/400/500（+100％*AP）魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "490.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "480.0000",
                  "mpperlevel": "23.5000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "53.5400",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6690",
                  "attackspeedperlevel": "1.0000",
                  "armor": "18.7200",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "远古巫灵",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Xerath.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big101000.jpg",
                "story": "泽拉斯是古代恕瑞玛的巫师，飞升以后的他变成了一种奥术能量体，在魔法石棺的碎片之中涌动。数千年来，他被囚禁在沙漠之下，但最近恕瑞玛的崛起却将他从远古的牢笼中解放出来。对权力的疯狂渴求驱使着他，想要夺回他认为属于自己的东西，并取代世界上这些自命不凡的文明，让自己成为唯一受膜拜的偶像，统一整个世界。",
                "usageTips": "在敌人奔向或逃离你时施放【Q奥能脉冲】，命中率会比在敌人贴住你时高。",
                "battleTips": "泽拉斯的施法距离非常惊人，但只要被靠近了拼消耗的话，大部分英雄都可以从他身上占到便宜。",
                "name": "泽拉斯",
                "defense": "3",
                "magic": "10",
                "difficulty": "8",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Xerath_Passive1.png",
                    "title": "法力澎湃",
                    "description": "每过12秒，泽拉斯的下次对英雄发起的攻击就会回复60~390(1-18级)法力。如果攻击小兵或野怪，那么会回复30~195(1-18级)法力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XerathArcanopulseChargeUp.png",
                    "title": "奥能脉冲",
                    "description": "开始蓄力:泽拉斯开始积蓄一次奥术光束，自身逐步减速，至多可达50%。\n\n释放:泽拉斯发射光束，造成80/120/160/200/240（+75％*AP）魔法伤害。距离随蓄力时间提升。\n\n如果泽拉斯没有将光束释放，那么会返还一半的消耗。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XerathArcaneBarrage2.png",
                    "title": "毁灭之眼",
                    "description": "泽拉斯引爆目标区域的奥术能量，造成60/90/120/150/180（+60％*AP）魔法伤害并使敌人减速25%，持续2.5秒。区域中心的敌人会受到100.02/150.03/200.04/250.05/300.06（+100.02%*AP）魔法伤害，并被减速60%/65%/70%/75%/80%，减速效果会在2.5秒里持续衰减。\n\n强化版减速效果会持续衰减至25%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XerathMageSpear.png",
                    "title": "冲击法球",
                    "description": "泽拉斯发射一颗原生魔法之球，基于飞行距离使命中的第一个敌人晕眩至多2秒，并造成80/110/140/170/200（+45％*AP）魔法伤害。\n\n最小晕眩时长为0.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XerathLocusOfPower2.png",
                    "title": "奥术仪式",
                    "description": "泽拉斯飞升至他的真正形态并引导10秒。在此期间，他可以再次施放此技能至多3/4/5次。\n\n再次施放:泽拉斯施放一道魔法炮击，造成200/250/300（+45％*AP）魔法伤害。\n\n如果泽拉斯根本没有再次施放，那么这技能的冷却时间会降低至50%。"
                  }
                ],
                "data": {
                  "hp": "526.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "459.0000",
                  "mpperlevel": "22.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "54.7000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.3600",
                  "armor": "21.8800",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "九尾妖狐",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ahri.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big103000.jpg",
                "story": "符文之地的潜在力量和她有着与生俱来的共鸣。原始的魔法在她手中凝为魔法宝珠。瓦斯塔亚人阿狸醉心于玩弄猎物的情感，然后吞噬他们的生命精魄。虽然阿狸是天生的掠食者，但她却对猎物始终保存着一份同情，因为每一个被吞噬的灵魂，都伴随着他们生前的记忆。",
                "usageTips": "用【魅惑妖术】来启动你的连招，它将会使【欺诈宝珠】和【妖异狐火】更容易命中敌人。",
                "battleTips": "在终极技能【灵魄突袭】进入冷却阶段后，阿狸的生存能力可谓低得令人发指。",
                "name": "阿狸",
                "defense": "4",
                "magic": "8",
                "difficulty": "5",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Ahri_SoulEater2.png",
                    "title": "摄魂夺魄",
                    "description": "每当阿狸用技能命中敌人时，她就会获得一层效果，每个技能至多提供3层。当到达9层时，阿狸的下一个技能每命中一名敌人就会回复自身3/5/9/18（+9％*AP）生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AhriOrbofDeception.png",
                    "title": "欺诈宝珠",
                    "description": "阿狸投出然后收回她的宝珠，在放出时会沿途对敌人造成40/65/90/115/140（+35％*AP）魔法伤害，在收回时则会沿途对敌人造成40/65/90/115/140（+35％*AP）真实伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AhriFoxFire.png",
                    "title": "妖异狐火",
                    "description": "阿狸释放出3团狐火来追踪附近的敌人们并造成40/ 65 /90 /115/140（+30％*AP）魔法伤害，第一团之后的狐火降低至12/19.5/27/34.5/42（+9％*AP）伤害。她还会获得在1.5秒里持续衰减的40%移动速度。\n\n狐火的优先度顺序为:被魅惑妖术命中的英雄、被阿狸攻击过的敌人、然后是其他英雄。20%生命值以下的小兵会受到200%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AhriSeduce.png",
                    "title": "魅惑妖术",
                    "description": "阿狸献出红唇热吻，魅惑命中的首个敌人1.4/1.55/1.7/1.85/2秒并造成60/90/120/150/180（+40％*AP）魔法伤害。阿狸的技能对该目标造成的伤害会提升20%，持续3秒。\n\n这个技能会中止任何进行中的移动技能。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AhriTumble.png",
                    "title": "灵魄突袭",
                    "description": "阿狸灵活地突进，并对附近的敌人们发射3颗灵魄弹，优先选择英雄。这些灵魄弹每颗造成60/90/120（+35％*AP）魔法伤害。这个技能可以在10秒内再次施放至多2次，然后就会进入冷却阶段。"
                  }
                ],
                "data": {
                  "hp": "526.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "418.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "53.0400",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6680",
                  "attackspeedperlevel": "2.0000",
                  "armor": "20.8800",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "机械先驱",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Viktor.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big112000.jpg",
                "story": "他是崭新科技时代的领路先驱，将自己毕生精力奉献给了人类的进步。他是寻求人性启迪的理想主义者，信奉着唯有光荣进化才能实现人类全部的潜能。在钢铁与科学的加持之下，维克托狂热地追求着自己理想中的光明未来。",
                "usageTips": "E死亡射线】是一个给力的磨血技能，以及强大的区域封锁工具。将它与【W重力场】组合使用，来控制敌人的走位。",
                "battleTips": "要时刻注意维克托与你的距离。离对手越近，维克托的控场能力就越强。",
                "name": "维克托",
                "defense": "4",
                "magic": "10",
                "difficulty": "9",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Viktor_Passive.png",
                    "title": "光荣进化",
                    "description": "维克托在击杀小兵和野怪后会获得1个海克斯碎片，他在击杀炮兵后生成5个，在参与击杀英雄后生成25个，每100个碎片即可使维克托升级他的[海克斯核心]，从而将他的一个基础技能进化为增强版。在维克托升级了所有基础技能后，他会完善它的海克斯核心并增强[混乱风暴]。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViktorPowerTransfer.png",
                    "title": "虹吸能量",
                    "description": "维克托引爆一名敌方单位，造成(60/75/90/105/120 +40%AP)魔法伤害，同时为维克托生成一个最多可以吸收(115 +20%AP)伤害的护盾，持续2.5秒。\n维克托的下次普通攻击会造成(20/45/70/95/120 +60%AP +100%AD)额外魔法伤害。\n增强—增压：虹吸能量的护盾提升60%并且维克托获得30%移动速度，持续2.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViktorGravitonField.png",
                    "title": "重力场",
                    "description": "维克托放出一个持续4秒的重力限制设备，减缓敌人(30%/33%/37%/40%/45%)的移动速度，并且每0.5秒都会堆叠一次效果，在达到3层堆叠效果时，目标会被晕眩1.5秒。\n增强—磁化：维克托的非周期性机能会对敌人施加持续1秒的20%减速效果"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViktorDeathRay.png",
                    "title": "死亡射线",
                    "description": "维克托用他的机械臂发射一束混乱光线，笔直地切割战场，沿途对行进路上的敌人造成(70/110/150/190/230 +50%AP)魔法伤害\n增强—余震：射线照过的地方会产生爆炸，造成(20/50/80/110/140 +80%AP)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ViktorChaosStorm.png",
                    "title": "混乱风暴",
                    "description": "维克托朝目标区域发射一枚奇点，造成(100/175/250 + 50%AP)魔法伤害并打断敌人的引导技能。\n维克托可以在6.5秒里重新定位奇点，奇点会放电，每1秒对附近的敌人造成(65/105/145 +45%AP)魔法伤害，奇点在试图远离维克托时，移速会变慢。\n增强—完美风暴：混乱风暴的移动加快25%。"
                  }
                ],
                "data": {
                  "hp": "530.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.6500",
                  "mp": "405.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "53.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.1100",
                  "armor": "23.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "爆破鬼才",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ziggs.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big115000.jpg",
                "story": "炸弹越大越好，引线越短越好，带着这种喜好的约德尔人吉格斯就是天生的爆炸狂人。他曾是皮尔特沃夫一位发明家的助手，不过因为自己千篇一律的生活而感到无聊，后来和一个名为金克丝的蓝头发小疯子交上了朋友。疯狂的城中一夜过后，吉格斯接受了她的建议，搬到了祖安，在那里更加自由地探索自己着迷的东西。在他对于爆炸的无尽追寻过程中，一直恐吓着炼金男爵和普通市民之流。",
                "usageTips": "即使远离团战发生地，你也可以在远处用【R科学的地狱火炮】来帮助队友。",
                "battleTips": "别踩到吉格斯的地雷上！它们会使你减速，并且会让吉格斯的其它技能更易命中你。",
                "name": "吉格斯",
                "defense": "4",
                "magic": "9",
                "difficulty": "4",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/ZiggsPassiveReady.png",
                    "title": "一触即发",
                    "description": "每过12秒，吉格斯的下次普通攻击就会造成额外的(20-160 +50%AP)魔法伤害。这个技能会对建筑物造成(50-400 +125%)伤害。\n吉格斯每施放一次技能，这个效果的冷却时间就会缩短(4-6)秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZiggsQ.png",
                    "title": "弹跳炸弹",
                    "description": "吉格扔出一个会弹跳的炸弹，对敌人造成(85/130/175/220/265 +65%AP)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZiggsW.png",
                    "title": "定点爆破",
                    "description": "吉格斯扔出一个炸药包，会在4秒后或者在再次释放该技能后爆炸。爆炸会对敌人造成(70/105/140/175/210 +50%AP)魔法伤害，并将他们击退。吉格斯也会被震开，但不会受到伤害。\n[定点爆破会自动摧毁生命值低于(25%/27.5%/30%/32.5%/35%)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZiggsE.png",
                    "title": "海克斯爆破雷区",
                    "description": "吉格斯布下敌人一踩就炸的感应雷区，对踩中地雷的敌人造成(40/75/110/145/180 +30%AP)魔法伤害并减速(30%/35%/40%/45%/50%)，持续1.5秒。地雷持续10秒。\n第一个之后的地雷造成(16/30/44/58/72 +12%AP)伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZiggsR.png",
                    "title": "科学的地狱火炮",
                    "description": "吉格斯扔出他的终极造物，早爆炸范围的中心区域造成(300/450/600 +1.09995*AP)魔法伤害，或在边远地区造成(200/300/400 +0.7333*AP)魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "536.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "480.0000",
                  "mpperlevel": "23.5000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "54.2080",
                  "attackdamageperlevel": "3.1000",
                  "attackspeed": "0.6560",
                  "attackspeedperlevel": "2.0000",
                  "armor": "21.5440",
                  "armorperlevel": "3.3000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "冰霜女巫",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Lissandra.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big127000.jpg",
                "story": "丽桑卓的魔法将纯净的冰霜之力扭曲为某种黑暗而可怕的东西。她的黯冰之力不仅能冻结一切，还能将任何反抗之人刺穿并粉碎。在北部终日惊惶的居民中，人们只知道她是“冰霜女巫”。但事实却更为邪恶：丽桑卓是自然世界的腐化者，她的阴谋是要让全世界都进入到彻骨寒冷的冰河世纪。",
                "usageTips": "丽桑卓的技能范围要比大部分法师短。因此，要购买那些法术强度和生存属性兼备的装备，比如中娅沙漏和深渊面具，就能非常好地帮助她输出伤害的同时存活下去。",
                "battleTips": "阻止丽桑卓使用冰川之径的最佳方式，就是在她再次激活这招之前将她束缚住。",
                "name": "丽桑卓",
                "defense": "5",
                "magic": "8",
                "difficulty": "6",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Lissandra_Passive.png",
                    "title": "冰脉驱役",
                    "description": "当一名敌方英雄在丽桑卓附近阵亡时，该英雄会变成一个冰封奴仆。\n极度冰寒中粉碎，造成(120-520 +50%AP)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LissandraQ.png",
                    "title": "寒冰碎片",
                    "description": "扔出一杆冰矛，冰矛会在命中一名敌人后碎裂，对该名敌人造成(70/100/130/160/190 +80%AP)魔法伤害，以及(16%/19%/22%/25%/28%)，减速持续1.5秒。碎片会穿过目标，对被命中的其他敌人造成等量的伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LissandraW.png",
                    "title": "冰霜之环",
                    "description": "对周围的敌人造成(70/105/140/175/210 +70%AP)魔法伤害和持续(1.1/1.2/1.3/1.4/1.5)秒的束缚效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LissandraE.png",
                    "title": "冰川之径",
                    "description": "放出一团寒冰魔爪，对命中的敌人造成(70/105/140/175/210 +60%AP)魔法伤害。\n再次激活这个技能，就能让丽桑卓传送到寒冰魔爪的当前位置。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LissandraR.png",
                    "title": "冰封陵墓",
                    "description": "对敌人施放：让目标英雄冻结，使该目标晕眩1.5秒。\n对自己施放：丽桑卓用暗黑之冰将自己裹住，持续2.5秒，治疗自身(100/150/200 +30%AP)生命值。每损失1%生命值，这个治疗效果就会提升1%.这段期间里，丽桑卓免疫任何伤害且不可被选取，但也无法进行任何动作。\n随后暗黑之冰会从目标出散发寒光，对敌人造成(150/250/350 +75%AP)魔法伤害，暗黑之冰持续3秒，冰减缓的敌人(30%/45%/75%)的移动速度。"
                  }
                ],
                "data": {
                  "hp": "550.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "475.0000",
                  "mpperlevel": "30.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "53.0000",
                  "attackdamageperlevel": "2.7000",
                  "attackspeed": "0.6560",
                  "attackspeedperlevel": "1.3600",
                  "armor": "22.0000",
                  "armorperlevel": "3.7000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "皎月女神",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Diana.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big131000.jpg",
                "story": "黛安娜永远都佩着她的月刃 ，她是皎月教派的武士，不过她的教派在巨神峰周围地区几乎已经销声匿迹。黛安娜身穿闪烁着冬夜寒雪之光的铠甲，是皎月神力的凡间化身。她在巨神峰之巅与星灵精魄相融，不再是单纯的凡人。现在的她努力抗争着，寻找着神的启示，以及自己的力量和存在对于这个世界的意义。",
                "usageTips": "新月打击的命中是至关重要的，但没命中的话也别太担心。它的冷却时间很短，而且法力消耗很低。",
                "battleTips": "即使黛安娜不将月神冲刺与月光效果结合使用，她也可以打得非常有侵略性，但你可以在她没办法往回走的时候，用减速或者晕眩来教训她。",
                "name": "黛安娜",
                "defense": "6",
                "magic": "8",
                "difficulty": "4",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Diana_Passive_LunarBlade.png",
                    "title": "月银之刃",
                    "description": "在施放一个技能后，戴安娜从【月银之刃】中获得的攻击速度增至3倍，持续3秒\n每第三次攻击顺劈附近的敌人，造成20-250 (+0.4法术强度)额外魔法伤害\n在进行第三发普攻后，【月银之刃】不再叠加一层持续0.1秒的月光效果\n黛安娜获得(10%-40%)攻击速度，在施放一个技能后提升至(30%-120%)，持续3秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DianaArc.png",
                    "title": "新月打击",
                    "description": "黛安娜释放一道月能圆弧，造成(60/95/130/165/200 +70%AP)魔法伤害，并用月光标记3秒。\n月光会显形那些不在潜行状态的敌人们。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DianaOrbs.png",
                    "title": "苍白之瀑",
                    "description": "黛安娜生成3颗持续5秒的护体法球，可在接触时爆炸，每颗造成(18/30/42/54/66 +15%AP)魔法伤害，至多可达到(54/90/126/162/198 +45%AP)伤害。\n黛安娜也会获得相同持续时间的(30/45/60/75/90 +30%AP  +10%额外生命值)护盾值。\n当最后一颗法球爆炸后，他会获得额外的(30/45/60/75/90 +30%AP  +10%额外生命值)护盾值，并刷新持续时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DianaVortex.png",
                    "title": "月神冲刺",
                    "description": "黛安娜成为复仇之月的活化身，冲向一名敌人并造成(40/45/50/55/60 +40%AP)魔法伤害。如果目标被施加了月光，那么这个技能的冷却时间会被刷新。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DianaTeleport.png",
                    "title": "月之降临",
                    "description": "黛安娜拽入、减速(40%/50%/60%)、并显形附近的敌人2秒。\n如果黛安娜命中了至少一名敌方英雄，那么她就会召唤月亮，造成(200/300/400 +60%AP)魔法伤害外加(35/60/85 +15%AP)X第一个之外的被拖拽英雄数，至多可达(175/300/425 +75%AP)伤害。"
                  }
                ],
                "data": {
                  "hp": "570.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.8500",
                  "mp": "375.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "57.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.2500",
                  "armor": "31.0000",
                  "armorperlevel": "3.6000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "暗黑元首",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Syndra.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big134000.jpg",
                "story": "辛德拉是一位令人胆寒的艾欧尼亚魔法师，操纵着难以置信的力量。她在孩童时期就因为狂暴而不受控制的魔法而让村庄的长老们深感不安。长老们把她送到外地接受严密的监管和训练，但她发现所谓的训练其实是对自己能力的限制。辛德拉将自己感受到的背叛与痛楚融入暗黑法球，并发誓消灭所有想要控制她的人。",
                "usageTips": "为了最大化你的终极技能伤害，请在战场上存在许多暗黑法球时使用它。",
                "battleTips": "在与辛德拉对抗时，尽早购买鞋子，能帮你躲避她的大多数技能。",
                "name": "辛德拉",
                "defense": "3",
                "magic": "9",
                "difficulty": "8",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SyndraPassive.png",
                    "title": "卓尔不凡",
                    "description": "辛德拉的技能会在升至满级后获得额外的效果。\n暗黑法球：对英雄多造成25%/伤害。\n驱使念力：造成20%/额外真实伤害。\n弱者退散：宽度提升50%。\n能量倾泻：施法距离提升75。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SyndraQ.png",
                    "title": "暗黑法球",
                    "description": "辛德拉幻化出一颗暗黑法球，造成(70/105/140/175/210 +65%AP)魔法伤害。这个法球可以存留6秒，并且能被辛德拉的其他技能所操纵。\n这个技能可以在移动时施放。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SyndraW.png",
                    "title": "驱使念力",
                    "description": "辛德拉抓取一颗暗黑法球、一个敌方小兵或一只非史诗级野怪。她在抓取后的至多5秒内可以再次施放这个技能。\n再次施放：辛德拉将抓取到的单位猛掷出去，造成(70/110/150/190/230 +70%AP)魔法伤害和持续1.5秒的(25%/30%/35%/40%/45%)减速效果。\n如果没有选择任何目标，那么就会抓取相距最近的暗黑法球。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SyndraE.png",
                    "title": "弱者退散",
                    "description": "辛德拉投出一道原力波，击退敌人们和暗黑法球并造成(85/130/175/220/265 +60%AP)魔法伤害。\n被推挤的暗黑法球会晕眩敌人1.5秒并造成(85/130/175/220/265 +60%AP)魔法伤害。\n这个技能每次施放时只会对相同敌人造成一次伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SyndraR.png",
                    "title": "能量倾泻",
                    "description": "辛德拉动用她洪流般的能量，将环绕于她的3颗暗黑法球外加至多4颗之前生成的暗黑法球轰在一名敌方英雄身上。每颗暗黑法球造成(90140/190 +20%AP)魔法伤害(最大(630/980/1330 +140%AP)魔法伤害)"
                  }
                ],
                "data": {
                  "hp": "523.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "480.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "53.8720",
                  "attackdamageperlevel": "2.9000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.0000",
                  "armor": "24.7120",
                  "armorperlevel": "3.4000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "铸星龙王",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/AurelionSol.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big136000.jpg",
                "story": "奥瑞利安•索尔曾创造了奇迹般的群星，为无垠的荒芜太空布下他宏伟的恩典。而如今，他的威能却遭人设计，被迫服务于某个潜藏在深空中的帝国。为了重返铸星大道，奥瑞利安•索尔誓要夺回属于自己的自由。哪怕召星降怒，倾覆众生。",
                "usageTips": "你可以在使用【E星流横溢】带着【Q星河急涌】的星核一起飞行，从而让星核的爆炸范围得到可观的增长。",
                "battleTips": "尽量突破到他的星轨之内，这样他的护体行星就不会伤害到你了。",
                "name": "奥瑞利安索尔",
                "defense": "3",
                "magic": "8",
                "difficulty": "7",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/AurelionSol_Passive.png",
                    "title": "星原之准",
                    "description": "三颗行星环绕着奥瑞利安·索尔，造成(12-120)+(25%AP)魔法伤害，并秒杀生命值低于25的小兵。\n即使奥瑞利安·索尔在草丛时，敌人仍能看到护体行星。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AurelionSolQ.png",
                    "title": "星河急涌",
                    "description": "第一段施放：奥瑞利安·索尔创造一个新的星核，星核会随着时间成长，并为奥瑞利安·索尔提供20%移动速度加成。\n星核将在到达奥瑞利安·索尔的外轨时爆炸，同时施加(70/110/150/190/230 +65%AP)魔法伤害和持续0.55-2.2秒的晕眩效果。\n第二段施放：提前引爆星核。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AurelionSolW.png",
                    "title": "星穹暴涨",
                    "description": "被动：将护体行星的基础伤害提升(5/10/15/20/25)\n主动：奥瑞利安·索尔的护体行星们延展至外轨，转动得更快并造成140%伤害，总共造成(17-168)+(35%AP)魔法伤害。在再次激活或在3秒后，护体行星会缩回内轨并且奥瑞利安·索尔获得40%移动速度，在1.5秒里持续衰减。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AurelionSolE.png",
                    "title": "星流横溢",
                    "description": "朝着选定方向飞行(5500/6000/6500/7000/7500)码。只能在非战斗状态下施放。在飞行时，奥瑞利安·索尔既能看见隔墙的单位，也能被隔墙的单位看见。\n承受来自英雄或防御塔的伤害时会被强行着陆。\n腾飞速度：600/650/700/750/800"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AurelionSolR.png",
                    "title": "星弦高落",
                    "description": "喷吐出一道纯净的星火，造成(150/250/350 +70%AP)魔法伤害以及(40%/50%/60%)减速效果，持续2秒。\n星火还会将附近的敌人击退至奥瑞利安·索尔的外轨。"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "350.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "57.0000",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.3600",
                  "armor": "19.0000",
                  "armorperlevel": "3.6000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "暮光星灵",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Zoe.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big142000.jpg",
                "story": "调皮捣蛋、异想天开而且变化莫测，佐伊就是这一切的现实化身。作为巨神族的宇宙信使，她的出现就是惊天巨变的先兆。她甚至无需任何行为，只是单纯地出现在某个地方，就足以扭曲周围的奥术数学法则，从而扰乱现实的物理定律。有的时候还会带来浩劫与灾难，虽然她本身并无半点恶意。或许这就是为什么她对待自己的职责总是那么地漫不经心，给了她充足的时间用来玩游戏、捉弄凡人，或者自娱自乐。与佐伊的邂逅可能会给人带来欢乐与激励，但她的现身往往没这么简单，甚至常常意味着极大的危险。",
                "usageTips": "佐伊的【Q飞星乱入】能基于小星星已飞过的路程而造成更多伤害。朝你背后施放然后使它重新导向，就能造成成吨的伤害。 要用你最强的伤害来源来打醒目标，因为昏睡中的敌人会受到双倍伤害。 【E催眠气泡】可以越过墙体。找个地方藏起来，以便让你能从远处发起夺命攻势。",
                "battleTips": "佐伊的【Q飞星乱入】能基于小星星已飞过的路程而造成更多伤害。 佐伊在施放【R折返跃迁】后必须回到她的初始地点，而这个特性让她容易遭到反制。 【E催眠气泡】可以越过墙体。阻止佐伊隐藏在战争迷雾中，以防止她从暗中发起攻势。",
                "name": "佐伊",
                "defense": "7",
                "magic": "8",
                "difficulty": "5",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Zoe_P.png",
                    "title": "烟火四射！",
                    "description": "在施放一次技能后，佐伊的下次普通攻击将造成额外的(16-130 +20%AP)魔法伤害。\n强化攻击会立刻造成伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZoeQ.png",
                    "title": "飞星乱入！",
                    "description": "佐伊发射一颗飞星，基于它的飞行距离来对命中的第一个敌人造成不断提升的伤害((50/80/110/140/170) + 7-50 +60%AP)到((125/200/270/350/425) +18-125 +150%AP)魔法伤害。目标附近的的敌人会受到80%伤害。\n佐伊可以再次 施放这个技能来将这个飞弹重新导向到佐伊附近的一个位置。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZoeW.png",
                    "title": "窃法巧手",
                    "description": "被动：敌人在使用[召唤师技能]或[主动型装备]时会掉落一个技能碎片。特点小兵也会在被佐伊击杀后掉落一个技能碎片。佐伊可以拾取这些碎片来使用一次相应技能。\n被动：当佐伊施放这个技能或任一召唤师技能时，他会获得(30%/40%/50%/60%/70%)移动速度，持续(2/2.25/2.5/2.75/3)秒，并对她近期刚攻击过的目标投出3个飞弹那。每颗飞弹造成(24.975/34.965/44.955/54.945/64.935 +13.32%AP)魔法伤害。\n主动：施放一次来自佐伊已拾取的技能碎片的技能。\n技能碎片会在地上持续40秒。\n每颗飞弹那都会施加烟火四射！"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZoeE.png",
                    "title": "催眠气泡",
                    "description": "佐伊踢出一个气泡，可造成(60/100/140/180/220 +40%AP)魔法伤害，并会在未命中任何单位时如陷阱一般停留。在飞跃墙体后，气泡的距离会得到延长。\n在一段延迟后，被气泡命中的目标会陷入昏睡状态2秒。攻击和技能在命中目标时，都会将其惊醒并造成双倍伤害，最多可造成(60/100/140/180/220 +40%AP)真实伤害。\n在目标陷入昏睡状态之前，会进入困倦状态，被施加不断提升的减速效果(至多可达10%)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZoeR.png",
                    "title": "折返跃迁",
                    "description": "佐伊传送到附近的一个位置1秒。然后，佐伊会传送回原来的位置。在此期间，佐伊可以攻击和使用技能，但无法移动。\n在传送期间，佐伊的视线还可以越过墙体。"
                  }
                ],
                "data": {
                  "hp": "560.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.6000",
                  "mp": "425.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.6500",
                  "attackdamage": "58.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.5000",
                  "armor": "20.8000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "虚空之眼",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Velkoz.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big161000.jpg",
                "story": "人们不太确定维克兹是否是符文之地上出现的第一个虚空生物，但可以确定的是没有第二个虚空生物能超过他的残忍和精明。他的同类通常都会吞噬或者破坏周围的一切，然而维克兹却一直在仔细观察并研究这个物质世界，以及世界上这些奇怪的好战生物，寻找虚空可以利用的弱点。但维克兹绝不是个被动的观察者，他会用致命的电浆射线回击任何威胁，甚至还能扰动世界本身的基础构造。",
                "usageTips": "侧身发射等离子裂变，并让它在最大距离处分裂，就能让这个技能命中初始施法距离之外的敌人，这也是一个值得去掌握的技巧。",
                "battleTips": "如果在战斗中对维克兹不管不顾的话，他就会非常致命。尽量趁早将他集火掉。",
                "name": "维克兹",
                "defense": "2",
                "magic": "10",
                "difficulty": "8",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VelKoz_Passive.png",
                    "title": "有机体解构",
                    "description": "维克兹的技能会施加一层持续7秒的解构。带有3层效果的敌人会消耗所有层数，受到(33-169 +50%AP)真实伤害。\n维克兹的攻击会刷新解构的持续时间，但不会添加新的层数。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VelkozQ.png",
                    "title": "等离子裂变",
                    "description": "维克兹发射一束等离子，造成(80/120/160/200/240 +80%AP)魔法伤害和在(1/1.4/1.8/2.2/2.6)秒里持续衰减的70%减速。在到达它的距离终点，或是命中一个目标，或是再次施放此技能时，等离子束会分裂成新的两束并呈90度角发射出去。\n用这个技能击杀一个单位，就会返还50%法力消耗。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VelkozW.png",
                    "title": "虚空裂隙",
                    "description": "维克兹打开一道虚空裂隙，造成(30/50/70/90/110 +15%/AP)魔法伤害。然后裂隙会喷发，造成(45/75/105/135/165 +25%AP)魔法伤害。\n这个技能有2层充能时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VelkozE.png",
                    "title": "构造分解",
                    "description": "维克兹让一个区域爆炸，击飞敌人，并将近处的敌人略微击退。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VelkozR.png",
                    "title": "生命形态瓦解射线",
                    "description": "维克兹引导一道能量射线来跟随鼠标指针，在2.5秒里持续造成共(450/625/800 +125%AP)魔法伤害和20%减速。对近期被有机体解构伤害过的敌人们造成真实伤害作为替代。\n射线内的敌人会周期性地获得解构层数。"
                  }
                ],
                "data": {
                  "hp": "520.0000",
                  "hpperlevel": "88.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "469.0000",
                  "mpperlevel": "21.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "54.9379",
                  "attackdamageperlevel": "3.1416",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.3600",
                  "armor": "21.8800",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "岩雀",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Taliyah.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big163000.jpg",
                "story": "塔莉垭是一位来自恕瑞玛的游牧民族的法师，孩子的好奇与大人的责任两股力量同时拉扯着她。她曾踏遍瓦洛兰的山山水水，只为寻得控制己身异能的法门，不过最近，她已经回到了故乡，保护生她养她的部族。有些人把她的温柔善意当做是懦弱的表现，最终也为自己的愚鲁付出了代价。塔莉垭青春稚嫩的举止背后，是一颗敢于移山填海的雄心，和一个堪能倾世的灵魂。",
                "usageTips": "尝试用【W岩突】来把敌人扔到【E撒石阵】上。",
                "battleTips": "当塔莉垭在线上放下【E撒石阵】时，要注意躲开她的【W岩突】。如果她把你扔到雷区中，就会有各种石头来打断你的骨头。",
                "name": "塔莉垭",
                "defense": "7",
                "magic": "8",
                "difficulty": "5",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Taliyah_Passive.png",
                    "title": "浮石冲",
                    "description": "塔莉娅在墙壁附近奔跑时获得(12-40%)移动速度加成。只会在非战斗状态下触发。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaliyahQ.png",
                    "title": "石穿",
                    "description": "朝目标方向猛掷5块岩石，每块造成(70/95/120/145/170 +45%AP)魔法伤害。对相同敌人的后续命中只造成50%伤害。会留下持续45秒的掘石场。\n在掘石场上施放[Q石穿]时，只会猛掷1块岩石，但它的消耗减少至1法力。\n\n【已移除】后几发弹体对野怪造成的伤害不再降低"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaliyahW.png",
                    "title": "岩突",
                    "description": "塔莉垭选择一个区域作为目标。在短暂的延迟后，区域内的敌人会被推搡并受到(60/80/100/120/140 +40%AP)魔法伤害。\n你可以通过点击并拖动鼠标，来控制敌人将被推搡的方向。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaliyahE.png",
                    "title": "撒石阵",
                    "description": "布下一个能感应突进的陷阱区域，使区域内的敌人受到(50/75/100/125/150 +40%)魔法伤害和24%减速效果。在4秒后，陷阱会爆炸，再次造成伤害。\n敌方英雄在因突进、被拉扯或被推挤而经过撒石阵时就会触发陷阱，每触发一个陷阱就会受到(50/75/100/125/150 +30%)魔法伤害( 最多受到4个陷阱的伤害，第一个陷阱之外的后续陷阱造成的伤害降低15%)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaliyahR.png",
                    "title": "墙幔",
                    "description": "首次施放会创造一道石墙。再次施放就会立刻搭乘石墙。如果移动或受到来自英雄或防御塔的伤害，就会导致塔莉垭停下。\n[R墙幔]持续5秒。解除这个技能就会提前摧毁石墙。\n墙体长度：3000/4500/6000"
                  }
                ],
                "data": {
                  "hp": "532.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.7000",
                  "mp": "425.0000",
                  "mpperlevel": "30.0000",
                  "mpregen": "9.3350",
                  "mpregenperlevel": "0.8500",
                  "attackdamage": "58.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.3600",
                  "armor": "20.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "沙漠皇帝",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Azir.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big268000.jpg",
                "story": "阿兹尔是上古时期恕瑞玛的一位凡人皇帝，一位站在不朽神话巅峰的自豪之人。但他的狂妄引来了旁人的背叛，在最伟大胜利降临的那一刻将他杀害。而现在，数千年后，他重获新生并成为了力量无边的飞升者。阿兹尔的城市已经从黄沙之下崛起，他要让恕瑞玛恢复曾经的荣光。",
                "usageTips": "要牢记的是，除非你准备一波拼死敌人，否则不要把【W沙兵现身】的两个黄沙士兵都放出去。最好时刻把一个黄沙士兵留在手中，你可以用它来穿墙逃生，也可以在远离另一个士兵时放出来追加伤害。",
                "battleTips": " 阿兹尔依靠他的士兵来输出伤害，并且每移动一次士兵就要等待一段时间。尽量在他的士兵无法移动时来赚取优势。",
                "name": "阿兹尔",
                "defense": "3",
                "magic": "8",
                "difficulty": "9",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Azir_Passive.png",
                    "title": "恕瑞玛的传承",
                    "description": "阿兹尔可以从一座防御塔的废墟中召唤出太阳圆盘，但每180秒只能召唤1次。这座防御塔会获得 (15%AP)攻击力加成，但会在60秒里持续瓦解。\n\n点击一座防御塔的废墟，来激活[恕瑞玛的传承]。敌方的召唤水晶塔和水晶枢纽处塔对此技能免疫。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AzirQ.png",
                    "title": "狂沙猛攻",
                    "description": "阿兹尔把所有黄沙士兵派往一个地点。 黄沙士兵会对沿途的所有敌人造成( 70/90/ 110/ 130/ 150+0)魔法伤害和一层25%减速效果。这个减速效果持续1秒，并且可以叠加。\n\n被多个黄沙士兵击中的敌人不会受到额外伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AzirW.png",
                    "title": "沙兵现身",
                    "description": "被动:阿兹尔获得15%攻击速度。当阿兹尔拥有3或更多个黄沙士兵时，他会额外获得15%攻击速度，持续5秒。\n\n主动:阿兹尔召唤出一个持续10秒的黄沙士兵。在阿兹尔对一名敌人进行普攻时，如果该名敌人在某个黄沙士兵的攻击范围内，那么该黄沙士兵会代替阿兹尔来攻击，通过戳刺目标来造成60( +0)魔法伤害。被该黄沙士兵的戳刺所攻击到的其它敌人会受到25%的戳刺伤害。\n\n阿兹尔最多可储存2个黄沙士兵，并且每过8秒就会让一个新的黄沙士兵准备就绪。\n\n如果有多个沙漠士兵攻击相同的目标，那么除第一个之外的士兵只造成25%伤害。沙漠士兵可以攻击那些超出阿兹尔的普攻距离之外的目标。\n\n沙漠士兵在敌方防御塔附近时会以双倍速度消散。\n\n充能时间[ 8/ 7.5/ 7 /6.5/6 ]\n被动攻击速度[15%/ 25%/ 35% / 45%/ 55%]  \n 带着士兵时的攻速[15%/25%/35%/45%/55%]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AzirE.png",
                    "title": "流沙移形",
                    "description": "阿兹尔为自己武装上一层持续1. 5秒的护盾，最多可阻挡(80/ 120/ 160/ 200/ 240伤害，并且他会 突进到他的一个黄沙士兵身旁，对命中的敌人造成( 60/90/ 120/ 150/ 180)\n魔法伤害。\n\n如果阿兹尔命中了一名敌方英雄，那么他会中断他的冲刺并立刻使一个全新的黄沙士兵准备就绪。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AzirR.png",
                    "title": "禁军之墙",
                    "description": "阿兹尔召唤一道由禁军组成的人墙向前冲锋，击退沿途的敌人并造成(175/325/475)魔法伤害。然后这道由禁军组成的人墙会持续5秒。\n\n阿兹尔和他的友军可以自由穿过这道人墙。\n\n禁军之墙不会与阿兹尔的攻击或技能产生交互。\n士兵数量[6/7/8]"
                  }
                ],
                "data": {
                  "hp": "552.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "480.0000",
                  "mpperlevel": "21.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "52.0000",
                  "attackdamageperlevel": "2.8000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.0000",
                  "armor": "19.0400",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "翠神",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ivern.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big427000.jpg",
                "story": "半人半树的艾翁•荆足常在符文之地的丛林中游荡，所经之处无不生机盎然。他知晓自然界的种种秘密，无论是飞禽走兽还是游鱼虫孑，都与他交谊深厚。在逍遥四野的途中，艾翁会向路遇的人传授奇特的智慧，或是培植丰茂的丛林。时不时地，他也会向口风不严的蝴蝶托付自己所知的秘密。",
                "usageTips": "尽量用一记精妙的【Q根深敌固】接【E种豆得瓜】来帮助友军。",
                "battleTips": "艾翁的草丛能持续很长一段时间。要当心其中的埋伏！",
                "name": "艾翁",
                "defense": "5",
                "magic": "7",
                "difficulty": "7",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/IvernP.png",
                    "title": "森林之友",
                    "description": "艾翁不会攻击非史诗级的野怪。右键点击一个野怪来创造片小植被， 小植被会在(40 一1公)秒里持续成长。每片小植被花费 (132 一32会)生命值和、(149\n\n34公)法力。\n\n当一片小植被完全成长后，右键该处野怪即可送走他们，并给予金币和经验。惩戒一片小植被即可瞬间送走野怪。\n\n在5级或以上时，如果艾翁送走了一个带增益效果的野怪，那么就会创造一个额外的增益效果留给艾翁的友军们。如果名友军击杀 了一个带增益效果的野怪， 也会留下一个额外的增益效果给艾翁。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IvernQ.png",
                    "title": "根深敌固",
                    "description": "艾翁召唤-束荆棘，造成( 80/ 125/ 170/215/ 260 +0)魔法伤害并禁锢命中的第一个敌人，持续1.2秒。友军可以点击被禁锢的敌人来朝着该敌人突进，直到友军与该敌人的距离等于友军的攻击距离为止。\n禁锢时长[ 1.2/1.4/1.6/1.8/2 ]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IvernW.png",
                    "title": "揠苗助攻",
                    "description": "被动:在草丛中，艾翁的攻击造成( 30/37.5/45/52.5/60+0)额外魔法伤害。\n\n主动:艾翁种下一块草丛，持续30秒。在草丛被种下的3秒里，草丛会显示草丛中或草丛附近的视野。\n草丛充能[ 40/36/32/28/24 ]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IvernE.png",
                    "title": "种豆得瓜",
                    "description": "艾翁为一位友军加上护盾，可吸收最多(80/ 115/ 150/ 185/220+0)伤害。2秒后，护盾会爆炸，造成(70/95/ 120/ 145/ 170+0)魔法伤害并使敌人减速50%，持续2秒。\n\n也能施放在小菊身上。减速[50%/55%/60%/65%/70%]\n\n护盾值收益：90%法术强度"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/IvernR.png",
                    "title": "小菊！",
                    "description": "艾翁召唤出他的护卫好友一-小菊， 持续60秒。如果小菊对相同的敌方英雄进行了3次连续的攻击，那么她就会施放一道冲击波来将敌人击飞1秒(冷却时间: 3秒)。\n小菊拥有:\n生命值: (1250/ 2500/ 3750 +50%AP)\n-护甲和魔法抗性:  (15/40/90+5% AP)\n-攻击力: (30%/ 50%/ 70% +30% AP)\n基础攻击速度：0.7\n再次施放这个技能将指挥小菊前往一个 新的目标或位置."
                  }
                ],
                "data": {
                  "hp": "585.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.8500",
                  "mp": "450.0000",
                  "mpperlevel": "60.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.7500",
                  "attackdamage": "50.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "3.4000",
                  "armor": "27.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "330.0000",
                  "attackrange": "475.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "解脱者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Sylas.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big517000.jpg",
                "story": "在小城边沟镇长大的塞拉斯，如今却成了德玛西亚雄都的黑暗面的代表人物。当他还是个男孩的时候，人人避之唯恐不及的搜魔人发现他拥有感知法师的能力，便将他控制起来，利用这种能力来对付塞拉斯的同类。逃出生天之后的塞拉斯现在是一个坚定的抗命者，他要借助法师的力量摧毁自己曾经侍奉过的王国。遭到放逐而前来追随他的法师也与日俱增。",
                "usageTips": "等你或者敌人的血量都较低时再施放【弑君突刺】，会达到最佳效果。",
                "battleTips": "尽量在塞拉斯无法获取你的终极技能时与他作战。",
                "name": "塞拉斯",
                "defense": "4",
                "magic": "8",
                "difficulty": "5",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/SylasP.png",
                    "title": "破敌禁法",
                    "description": "在施放一次技能后，塞拉斯会储存一层[破敌禁法]效果(最多可叠3层)。塞拉斯的普攻将消耗一层效果并环绕自身挥舞他的盈能锁链来对目标造成(130%AD +25%AP)魔法伤害，并对附近的其他敌人造成(40%AD +20%AP)魔法伤害。当塞拉斯拥有一层[破敌禁法]效果时，他会获得80%攻击速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SylasQ.png",
                    "title": "锁链鞭击",
                    "description": "塞拉斯将锁链甩到一个X型区域内 ，锁链会在目标位置交叉并对敌人造成(40/60/80/100/120 +40%AP)魔法伤害和持续1.5秒的(15%)减速效果。\n在0.6秒后，魔法能量将从交叉点迸发，造成(60/115/170/225/280 +80%AP)伤害。\n迸发会对 小兵和野怪造成40%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SylasW.png",
                    "title": "弑君突刺",
                    "description": "塞拉斯带着魔法之力跃向一名敌人，造成(65/100/135/170/205 +85%AP)魔法伤害。\n如果对敌方英雄造成伤害，那么会基于塞拉斯的已损失生命值来治疗塞拉斯(30/60/90/120/150 +50%AP) - (60/120/180/240/300 +100%AP)生命值(在40%生命值时达到最大值)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SylasE.png",
                    "title": "潜掠/强掳",
                    "description": "塞拉斯突进至一个位置。\n再次施放：塞拉斯将 他的锁链向一个方向挥出，将他自身拉向命中的第一个敌人并造成(80/130/180/230/280 +100%AP)魔法伤害和0.35秒击飞效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SylasR.png",
                    "title": "其人之道",
                    "description": "塞拉斯朝一个已被选定的敌人甩出一条锁链，以使他自身可以施放该敌人的终极技能，效果与原技能一样，技能等级基于他自身的终极技能等级。\n从一名敌人处窃取技能后，会进入一段相当于200%/目标终极技能冷却时间的、只针对该名敌人的冷却阶段(基于塞拉斯的终极技能冷却时间，最小值为40秒)，期间无法再次窃取该名敌人的终极技能。\n塞拉斯会使用一部分法术强度来为那些纯靠攻击力收益的敌方技能增加伤害，转化率为0.4法术强度：1额外攻击力，相应的0.6法术强度：1总攻击力。"
                  }
                ],
                "data": {
                  "hp": "525.0000",
                  "hpperlevel": "115.0000",
                  "hpregen": "9.0000",
                  "hpregenperlevel": "0.9000",
                  "mp": "280.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6450",
                  "attackspeedperlevel": "3.5000",
                  "armor": "27.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.7500",
                  "movespeed": "340.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "万花通灵",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Neeko.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big518000.jpg",
                "story": "妮蔻来自一个早已迷失的瓦斯塔亚部落。她可以借用别人的外表来伪装自己，融入人群，甚至通过影响别人的情绪状态，一瞬间就能化敌为友。没人知道妮蔻到底在哪儿——或者到底是谁，但是想要为难她的人会立刻见识到她的本色，感受原始的精神魔法倾泻在自己身上的痛苦。",
                "usageTips": "你可以在[选项]菜单中将她的被动设置到热键上。默认键位是[SHIFT]+[F1~F5] 尽量选好你的【天生幻魅】的施放时机，没用好的话，就会引起敌人的警觉。",
                "battleTips": "在对抗妮蔻时，站在小兵后面是很危险的，因为【缠结倒刺】会变得更强力。 在妮蔻处于假扮状态时，【怒放】的警告视觉效果是不可见的。",
                "name": "妮蔻",
                "defense": "1",
                "magic": "9",
                "difficulty": "5",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Neeko_P.png",
                    "title": "天生幻魅",
                    "description": "妮蔻可以假扮成一名友方英雄。受到来自英雄的直接伤害、或是施放伤害型的技能都会打破这个假扮状态。\n冷却时间：(25-10)秒"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NeekoQ.png",
                    "title": "盛开花种",
                    "description": "妮蔻扔出一颗种子以造成(80/125/170/215/260 +50%AP)伤害。如果种子击杀了一个单位或命中了一个英雄或者大型野怪，那么它会再次盛开，造成(40/60/80/100/120 +20%AP)魔法伤害。最多可额外盛开2次。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NeekoW.png",
                    "title": "两生花影",
                    "description": "被动：每第三次攻击造成(50/70/90/110/130 +60%AP)额外魔法伤害并使妮蔻的移动速度提升(10%/15%/20%/25%/30%)，持续1秒。\n主动：妮蔻进入0.5秒的隐形状态，并投射出一个持续3秒的镜像。妮蔻和镜像会获得(20%/25%/30%/35%/40%)移动速度，持续3秒。\n在潜行时，镜像是不可被选取的。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NeekoE.png",
                    "title": "缠结倒刺",
                    "description": "妮蔻掷出一团倒刺，造成(80/115/150/185/220 +60%AP)魔法伤害和(0.7/0.9/1.1/1.3/1.5)秒的禁锢效果。\n倒刺会在命中一名敌方英雄后强化，长得更大、动的更快、并且禁锢时长变为(1.8/2.1/2.4/2.7/3)秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NeekoR.png",
                    "title": "怒放",
                    "description": "在1.25秒后，妮蔻会跃入空中。她获得一层可吸收(75/100/125 +75%AP)((+40/60/80 +40%)X附近敌方英雄数)伤害的护盾。\n在妮蔻着陆时，她会对附近的敌人造成(200/425/650 +130%AP)魔法伤害和1.25秒晕眩效果。\n在妮蔻处于假扮状态下，这个技能可以秘密地进行准备。"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.7500",
                  "mp": "450.0000",
                  "mpperlevel": "30.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "48.0000",
                  "attackdamageperlevel": "2.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.5000",
                  "armor": "21.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "含羞蓓蕾",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Lillia.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big876001.jpg",
                "story": "羞怯难当的仙灵小鹿莉莉娅在艾欧尼亚的森林中蹦跳着游荡。她躲藏在凡人视线的边缘，因为凡人的神秘一直都让她着迷，同时也让她害怕。莉莉娅要查清楚为什么他们的梦境再也无法抵达幻梦树。如今她带着一根魔法树枝游历艾欧尼亚，寻找人们未实现的梦境。只有通过梦境，莉莉娅才能绽放，并帮助其他人解开恐惧的郁结，找寻内心的闪光。呦——！",
                "usageTips": "小鹿每次使用技能以后都会获得移动速度，要利用好移动速度的加成",
                "battleTips": "要当心梦尘的状态，小鹿会使用R技能困住所有拥有梦尘状态的英雄",
                "name": "莉莉娅",
                "defense": "0",
                "magic": "0",
                "difficulty": "0",
                "attack": "0",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Lillia_Icon_Passive.Lillia.png",
                    "title": "梦满枝",
                    "description": "莉莉娅的技能会施加梦尘，在3秒里持续造成共(5%)最大生命值的魔法伤害。\n梦尘在持续时间内对野怪的最大总伤害为(40-100)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LilliaQ.png",
                    "title": "飞花挞",
                    "description": "被动：莉莉娅的技能在命中时会提供(7%/8%/9%/10%/11% +1%AP)移动速度，持续5秒，可叠加5层。\n主动：莉莉娅猛掷她的香炉，造成(30/45/60/75/90 +40%AP)魔法伤害并对外侧边缘的敌人造成额外的(30/45/60/75/90 +40%AP)真实伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LilliaW.png",
                    "title": "惊惶木",
                    "description": "莉莉娅蓄力以进行一次巨大打击，在一个区域内造成(70/85/100/115/130 +30%AP)魔法伤害。区域中心的敌人受到(210/255/300/345/390 +90%AP)伤害。\n莉莉娅无法用这个技能翻越地形。\n对小兵造成50%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LilliaE.png",
                    "title": "流涡种",
                    "description": "莉莉娅投出一枚种子，种子在着陆时会显形并造成(70/90/110/130/150 +40%AP)魔法伤害和(25%/30%/35%/40%/45%)减速效果，持续3秒，期间还会使目标显形。如果种子未能命中任何东西，那么它将继续滚动，直到命中墙体或者一名敌人为止。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LilliaR.png",
                    "title": "夜阑谣",
                    "description": "莉莉娅使所有带着梦尘的敌人陷入困倦状态1.5秒。此后，他们会陷入昏睡状态(2/2.5/3)秒。\n在被伤害惊醒时他们会受到额外的(100/150/200 +30%AP)魔法伤害。\n[困倦状态]的单位会在期间内受到持续提升的减速效果，然后陷入[昏睡状态]。\n[昏睡状态]的单位无法移动或行动，直到被一名 敌人用非周期性的伤害打醒为止。"
                  }
                ],
                "data": {
                  "hp": "0.0000",
                  "hpperlevel": "0.0000",
                  "hpregen": "0.0000",
                  "hpregenperlevel": "0.0000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "0.0000",
                  "attackdamageperlevel": "0.0000",
                  "attackspeed": "0.0000",
                  "attackspeedperlevel": "0.0000",
                  "armor": "0.0000",
                  "armorperlevel": "0.0000",
                  "spellblock": "0.0000",
                  "spellblockperlevel": "0.0000",
                  "movespeed": "0.0000",
                  "attackrange": "0.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
            ]
          },
          {
            "name": "坦克",
            "heroes": [
              {
                "title": "牛头酋长",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Alistar.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big12000.jpg",
                "story": "阿利斯塔一直都是威名远扬的巨力勇士，他要为自己被屠杀的氏族向诺克萨斯帝国复仇。虽然他曾被奴役，并被迫成为斗兽场中的角斗士，但他坚不可摧的意志使他免于沦为真正的野兽。现在，挣脱了奴役枷锁的他继续以受苦之人和弱者的名义战斗。他的愤怒，还有犄角、蹄子和拳头，都是他的武器。",
                "usageTips": "使用大地粉碎可以让你在更好的位置施放野蛮冲撞。",
                "battleTips": "阿利斯塔具有很强的破坏力也很结实，尝试着攻击更脆弱的输出型英雄才是更好的选择。",
                "name": "阿利斯塔",
                "defense": "9",
                "magic": "5",
                "difficulty": "7",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Alistar_E.png",
                    "title": "凯旋怒吼",
                    "description": "阿利斯塔在对敌方英雄造成晕眩或震移时，或附近有敌人阵亡时，会积攒他的怒吼层数。在7层时，他会治疗他自己（25-161基于等级）生命值并治疗所有附近的友方英雄（50-322基于等级）生命值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Pulverize.png",
                    "title": "大地粉碎",
                    "description": "阿利斯塔锤击地面，击飞敌人1秒并造成（60/105/150/195/240+50%AP）魔法伤害。\n\n冷却时间（秒）: 15/14/13/12/11\n法力值消耗: 65/70/75/80/85\n範圍: 365"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Headbutt.png",
                    "title": "野蛮冲撞",
                    "description": "阿利斯塔顶向一名敌人，将其击退并造成（55/110/165/220/275+70%AP）魔法伤害。\n\n冷却时间（秒）: 14/13/12/11/10\n法力值消耗: 65/70/75/80/85\n範圍: 650"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AlistarE.png",
                    "title": "践踏",
                    "description": "阿利斯塔践踏地面，变为幽灵状态并在5秒里持续对附近的敌人造成（80/110/140/170/200+40%AP）魔法伤害。每段践踏在至少伤害到一名敌方英雄时会为阿利斯塔提供一层效果。\n\n在5层效果时，阿利斯塔的下次攻击会晕眩目标1秒并造成额外的（35-290基于等级）魔法伤害。\n\n冷却时间（秒）: 12/11.5/11/10.5/10\n法力值消耗: 50/60/70/80/90\n範圍: 350"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FerociousHowl.png",
                    "title": "坚定意志",
                    "description": "阿利斯塔立刻净化掉所有限制效果并获得55/65/75%伤害减免，持续7秒。\n\n冷却时间（秒）: 120/100/80\n法力值消耗: 100\n範圍: 1"
                  }
                ],
                "data": {
                  "hp": "600.0000",
                  "hpperlevel": "106.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.8500",
                  "mp": "350.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.5000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "62.0000",
                  "attackdamageperlevel": "3.7500",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1250",
                  "armor": "44.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "330.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "正义巨像",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Galio.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big3000.jpg",
                "story": "光彩熠熠的德玛西亚城外，石巨像加里奥始终如一地守望着。他被创造出来是为了抵挡来犯的法师，但却经常要一动不动地矗立数十年，只有当强大的魔法力量出现时，他才会激活。而只要加里奥活动起来，他便会充分利用每一刻，品味荡气回肠的战斗和来之不易的守护人民的荣耀。可惜，他的胜利永远都喜忧参半，因为消灭魔法的同时，也消灭了他活跃力量的源泉。每一次胜利都会使他再次进入不知世事的休眠。",
                "usageTips": "你可以使用小地图上的友军图标来施放【R英雄登场】。 你可以利用【E正义冲拳】的小后跳来躲避敌方的技能。",
                "battleTips": "在加里奥施放【R英雄登场】时，可以趁加里奥还没跃至空中时打断这个技能。",
                "name": "加里奥",
                "defense": "10",
                "magic": "6",
                "difficulty": "5",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Galio_Passive.png",
                    "title": "巨像重击",
                    "description": "每过若干秒，加里奥的下次普通攻击对附近的敌人造成（（15-200）+100%AD+50%AP+60%额外魔抗）的魔法伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GalioQ.png",
                    "title": "战争罡风",
                    "description": "加里奥发射两道罡风，每道造成（70/105/140/175/210+75%AP）魔法伤害。\n当罡风汇聚时，它们会合成一股龙卷风，在2秒里持续造成魔法伤害，对每个敌人的伤害总额相当于该敌人（8+2.64%AP）%的最大生命值（对野怪最大伤害：200）。\n\n\n冷却时间（秒）: 12/11.5/11/10.5/10\n法力值消耗: 70/75/80/85/90\n範圍: 825"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GalioW.png",
                    "title": "杜朗护盾",
                    "description": "被动：如果加里奥在12秒内没有受到伤害，那么他就会获得一层护盾，可吸收8/11/14/17/20%生命值魔法伤害。\n\n首次释放：加里奥开始蓄力，并获得20/25/30/35/40（+5%魔抗）（+5%AP）%魔法伤害减免和相当于该数值50%物理伤害减免。在充能时加里奥自身会减速15%。\n\n再次释放：加里奥嘲讽附近的敌方英雄0.5到1.5秒，造成20/35/50/65/80（+30%AP）到60/105/150/195/240（+90%AP）魔法伤害，并使伤害减免的持续时长刷新至2秒。嘲讽的持续时长、伤害和距离取决于他的蓄力时长。\n\n冷却时间（秒）: 16/15/14/13/12\n法力值消耗: 50 \n範圍: 275"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GalioE.png",
                    "title": "正义冲拳",
                    "description": "加里奥挟着一股强劲向前猛冲，对命中的第一个敌方英雄击飞0.75秒并造成（90/130/170/210/250+90%AP）魔法伤害。沿途的其它敌人将受到（45/65/85/105/125+45%AP）魔法伤害。\n\n加里奥的猛冲将在命中地形时停下。\n\n\n冷却时间（秒）: 12/11/10/9/8\n法力值消耗: 50\n範圍: 650"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GalioR.png",
                    "title": "英雄登场",
                    "description": "加里奥选定一个友方英雄的位置作为他的着陆位置，并为该区域的所有友方英雄提供杜朗护盾的被动护盾，护盾持续5秒。然后加里奥会飞往他的着陆地带。\n\n当加里奥着陆时，他会对范围内所有敌人造成0.75秒击飞和（150/250/350+70%AP）魔法伤害。\n\n冷却时间（秒）: 200/180/160\n法力值消耗: 100 \n範圍: 4000/4750/5500"
                  }
                ],
                "data": {
                  "hp": "562.0000",
                  "hpperlevel": "112.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.8000",
                  "mp": "500.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "9.5000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "59.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.5000",
                  "armor": "24.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "亡灵战神",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Sion.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big14000.jpg",
                "story": "赛恩是一个来自先前时代的诺克萨斯战争英雄，曾经徒手掐死过一代德玛西亚国王。但他拒绝了死亡和湮灭，以活尸的状态继续为帝国效命。只要挡住他的去路，都会被他无差别地屠杀，敌我不分，足可证明他已经失去了从前的人性。即便如此，他腐朽的身体还是被钉进了粗糙的装甲，继续以丧心病狂的鲁莽冲上战场，在每一下巨斧的挥砍中艰难地回忆真正的自我。",
                "usageTips": "在【R蛮横冲撞】进行时，你只有非常微弱的转向能力，所以请先规划好冲撞的直线。",
                "battleTips": "即使会被赛恩的【Q残虐猛击】给命中，也要让赛恩提前放出这个技能，从而减少这个技能的影响力。",
                "name": "赛恩",
                "defense": "9",
                "magic": "3",
                "difficulty": "5",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Sion_Passive1.png",
                    "title": "死亡荣耀",
                    "description": "在被击杀后，赛恩会回光返照一段时间并且生命值会快速衰减。在此期间，他的攻击会变得超快，获得生命偷取，并且每次攻击会额外造成基于目标最大生命值的额外伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SionQ.png",
                    "title": "残虐猛击",
                    "description": "赛恩开始蓄力，并在释放时对他前方的一个区域进行一次强劲的猛击，对区域内的敌人造成伤害。如果他蓄力时间足够久，那么被命中的敌人还会受到击飞和晕眩效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SionW.png",
                    "title": "灵魂熔炉",
                    "description": "赛恩为自己套上护盾，并能在3秒后再次激活这个技能来对身边的敌人造成魔法伤害。在赛恩击杀敌人时，他会被动获得最大生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SionE.png",
                    "title": "杀手怒吼",
                    "description": "赛恩发射一个短程冲击波，来对命中的第一个敌人造成伤害、减速效果和破甲效果。如果冲击波命中的是小兵或者野怪，那么目标敌方单位还会被击退，对沿途的所有敌方单位造成伤害、减速和护甲击碎效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SionR.png",
                    "title": "蛮横冲撞",
                    "description": "赛恩朝着一个方向冲锋，持续提升移动速度。他可以朝着鼠标悬停处进行微小的转向。当他与一名敌人产生碰撞时，他会基于敌人们与碰撞点之间的距离造成伤害和击飞效果。"
                  }
                ],
                "data": {
                  "hp": "545.0000",
                  "hpperlevel": "73.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.8000",
                  "mp": "330.0000",
                  "mpperlevel": "42.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6790",
                  "attackspeedperlevel": "1.3000",
                  "armor": "32.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "雪原双子",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Nunu.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big20000.jpg",
                "story": "很久以前，曾有个小男孩，他要证明自己是个英雄，于是决定去杀掉一头凶猛的怪兽——但他却发现这个怪兽其实是个孤独的魔法雪人，而且他需要的只是一个朋友。雪人和男孩被古老的力量所连结，也因对雪球的共同爱好而玩到一起。努努和威朗普如今在弗雷尔卓德的土地上肆意撒欢打滚，为想象中的冒险注入鲜活的生命力。他们希望能够在前面的某个地方找到努努的母亲。如果他们能拯救她，或许他们就真的是英雄了。",
                "usageTips": "吞噬可以让努努呆在线上对抗敌方远程英雄。",
                "battleTips": "【史上最大雪球！】移动得非常快但无法快速转向，因此尽量不要沿直线逃跑，而是要进行突然、大角度的转向。",
                "name": "努努和威朗普",
                "defense": "6",
                "magic": "7",
                "difficulty": "4",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/NunuPassive.png",
                    "title": "弗雷尔卓德的召唤",
                    "description": "在对英雄/大型野怪或者建筑物造成伤害时，会为威朗普和他附近拥有最高攻击威胁的友军提供[弗雷尔卓德的召唤]，以提升20%攻击速度和10%移动速度，持续4秒。\n当威朗普处于[弗雷尔卓德的召唤]效果下时，他的普攻会造成(30%AD)物理伤害且拥有顺劈效果。\n当[弗雷尔卓德的召唤]源自一个大型野怪时，将自动作用于700码内的一名友军。当源自一个英雄、史诗级野怪或建筑物时，将自动作用于至多1000码内的一名友军。\n持续时间可叠加，但效果在10秒内无法从相同目标处重复触发。\n一个英雄的攻击威胁指的是该英雄的普攻造成的每秒伤害输出。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NunuQ.png",
                    "title": "吞噬",
                    "description": "威朗普撕咬一名敌人，在造成伤害的同时治疗自身。在生命值第一50%时，这个治疗效果可提升50%。\n野怪和小兵：造成(340/500/660/820/980)真实伤害并治疗(75/110/145/180/215 +90%AP +10% 额外生命值)生命值。\n英雄：造成(60/100/140/180/220 +5%额外生命值 +65%AP)魔法伤害并治疗(60/88/116/144/172 +72%AP +8%额外生命值)生命值。\n冷却时间：12/11/10/9/8\n法力消耗：60"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NunuW.png",
                    "title": "史上最大雪球！",
                    "description": "威朗普滚起一个尺寸和速度会在5秒里不停增长的雪球。当雪球碰撞到一个敌方英雄、大型野怪或墙体时，它会爆炸，对附近的敌人造成至多(180/225/270/315/360 +150%AP)魔法伤害以及至多(0.5+0.75)秒击飞效果。\n滚过小型单位时会造成(59.94/74.925/89.91/104.895/119.88 +49.95%AP)魔法伤害。\n再次激活这个技能可将雪球向前沿直线推出一段距离。\n不断向相同方向转向可持续提升威朗普的转向速率。这个加成会在变更转向方向时开始积累。\n冷却时间：14\n法力消耗：50/55/60/65/70"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NunuE.png",
                    "title": "雪球飞射",
                    "description": "努努掷出3颗雪球。[雪球飞射]可以被释放额外2次，来投掷更多的雪球。每颗雪球会对被命中的敌人造成(16/24/32/40/48 +6%AP)魔法伤害。被3颗连续的雪球所命中的敌人会被减速(30%/35%/40%/45%/50%)，持续1秒。英雄和大型野怪会变为雪缚状态。\n威朗普的回合：在3秒后，距离内的所有雪缚状态的敌人都会受到(20/30/40/50/60+80%AP)魔法伤害并被禁锢(0.5-1.5)秒。\n在进入威朗普的回合之前，努努至多可以施放3次[雪球飞射]。\n努努仅能用[雪球飞射]让相同敌人减速一次。\n冷却时间：14\n法力消耗：50"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NunuR.png",
                    "title": "绝对零度",
                    "description": "威朗普引导3秒，以创造一个强力的暴风雪。暴风雪范围内的敌人会被减缓50%/，并随着引导时长持续提升至95%。努努和威朗普会获得一层吸收(65/75/85 +30%额外生命值 +150%AP)伤害的护盾。这个护盾在接下来的3秒里持续衰减。\n当暴风雪结束时，区域内的敌人会受到至多(625/925/1275 +250%AP)魔法伤害，伤害值取决于[绝对零度]的引导时长。\n冷却时间：110/100/90\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "570.0000",
                  "hpperlevel": "82.0000",
                  "hpregen": "5.0000",
                  "hpregenperlevel": "0.8000",
                  "mp": "280.0000",
                  "mpperlevel": "42.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.5000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.2500",
                  "armor": "32.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "炼金术士",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Singed.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big27000.jpg",
                "story": "辛吉德是一位智力超群的祖安炼金术士，用自己的生命推动知识的边界。没有什么代价是他不能付出的，包括自己的理智。他的疯狂是否有迹可循？他的合剂几乎无不生效，只不过多数人都认为辛吉德已经丧失了全部人性的感知，所到之处只会留下苦难与恐惧的剧毒踪迹。",
                "usageTips": "剧毒踪迹在打钱与骚扰方面是很有效的，可以让辛吉德主宰他所在那条线的形势。",
                "battleTips": "辛吉德要接近你的队伍，才能施展技能，充分利用这一点，在攻击他的友军时，使用控制技能限制他的移动。",
                "name": "辛吉德",
                "defense": "8",
                "magic": "7",
                "difficulty": "5",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Singed_Passive.png",
                    "title": "剧毒冲流",
                    "description": "辛吉德借助附近英雄进行漂移，在经过他们时获得20%移动速度加成，持续2秒。\n每位英雄有10秒的冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PoisonTrail.png",
                    "title": "剧毒踪迹",
                    "description": "激活：辛吉德在身后留下剧毒踪迹，对经过上面的敌人造成每秒20/30/40/50/60(+40%*AP)点的魔法伤害。\n\n冷却时间（s）：0\n法力消耗：13/秒"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MegaAdhesive.png",
                    "title": "强力粘胶",
                    "description": "辛吉德在地上留下一个黏着的区域，持续3秒。区域中的敌人会被减速60%并被施加缚地效果，无法使用位移技能。\n\n如果目标被【E过肩摔】后落到辛吉德的【W强力粘胶】上，那么还会被禁锢。\n\n冷却时间（s）：17/16/15/14/13\n法力消耗： 60/70/80/90/100"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Fling.png",
                    "title": "过肩摔",
                    "description": "辛吉德将一名敌人投掷到自己身后，造成50/65/80/95/110(+75%*AP)加上相当于目标6%/6.5%/7%/7.5%/8%最大生命值的魔法伤害。\n\n如果目标被【E过肩摔】后落到辛吉德的【W强力粘胶】上，那么还会被禁锢1/1.25/1.5/1.75/2秒。\n\n冷却时间（s）：10\n法力消耗： 60/70/80/90/100"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/InsanityPotion.png",
                    "title": "疯狂药剂",
                    "description": "辛吉德喝下化学药剂，增加30/60/90法术强度、护甲、魔法抗性、移动速度、生命回复和法力回复，持续25秒。\n\n冷却时间（s）：120/110/100\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "580.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "9.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "330.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "7.5000",
                  "mpregenperlevel": "0.5500",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "3.3750",
                  "attackspeed": "0.6130",
                  "attackspeedperlevel": "1.9000",
                  "armor": "34.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "虚空恐惧",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Chogath.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big31000.jpg",
                "story": "自从科加斯出现在符文之地烈日照射之下的那一刻起，它就被永不餍足的饥饿所驱使。虚空吞噬一切生命的欲望完美地体现在科加斯的身上，它复杂的生物构造能够迅速将物质转化为身体的成长，不仅会增加肌肉的质量和密度，还能让外壳变得有如钻石般坚硬。当单纯的体型增长已经不能满足它时，这只虚空生物就会将多余的物质形成锋利的骨刺吐出体外，刺穿猎物，为稍后的盛宴进行准备。",
                "usageTips": "尽量调整普攻的角度，好让虚空尖刺能够同时击杀敌方小兵并伤害敌方英雄。",
                "battleTips": "购买一些增加生命值的物品能够降低被科加斯快速杀死的机率。",
                "name": "科加斯",
                "defense": "7",
                "magic": "7",
                "difficulty": "5",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/GreenTerror_TailSpike.png",
                    "title": "肉食者",
                    "description": "击杀一名敌人会回复20~71(1-18级 每级+3)生命值和3.3~7.5(1-18级)法力值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Rupture.png",
                    "title": "破裂",
                    "description": "科加斯使大地破裂，击飞敌人1秒，造成80/135/190/245/300（+100% *AP）魔法伤害，并使敌人减速60%，持续1.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FeralScream.png",
                    "title": "野性尖叫",
                    "description": "科加斯进行咆哮，沉默敌人1.6/1.7/1.8/1.9/2秒并造成75/125/175/225/275（+70% *AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VorpalSpikes.png",
                    "title": "恐惧之刺",
                    "description": "科加斯的下三次攻击会发射尖刺，造成22/37/52/67/82（+30% *AP）加上3%最大生命值的魔法伤害并使敌人减速30%/35%/40%/45%/50%，减速效果会在1.5秒里持续衰减。\n\n盛宴层数会提升尖刺的宽度。每层尖刺也会造成额外的0.5%最大生命值的魔法伤害。\n对小兵和野怪的百分比生命值伤害的封顶值为60。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Feast.png",
                    "title": "盛宴",
                    "description": "科加斯贪婪地吞吃一名敌人，对英雄造成300/475/650（+50%*AP +10%*额外生命值 ）额外真实伤害或对野怪和小兵造成1000（+50%*AP +10%额外生命值）真实伤害。如果这个技能击杀了敌方单位，那么科加斯会获得一层可叠加的效果，使他的体型变大且提供80/120/160最大生命值。通过吞吃小兵和非史诗级野怪的方式只能获得共6层效果。\n\n当前来自小兵和非史诗级野怪的层数：0/6"
                  }
                ],
                "data": {
                  "hp": "574.4000",
                  "hpperlevel": "80.0000",
                  "hpregen": "9.0000",
                  "hpregenperlevel": "0.8500",
                  "mp": "272.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.2060",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "69.0000",
                  "attackdamageperlevel": "4.2000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.4400",
                  "armor": "38.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "殇之木乃伊",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Amumu.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big32000.jpg",
                "story": "在远古的恕瑞玛，有一个孤独而又忧郁的灵魂，阿木木。他在世间游荡，只为找到一个朋友。他遭受了一种远古的巫术诅咒，注定忍受永世的孤单，因为被他触碰就意味着死亡，他的喜爱便是毁灭。所有自称见过阿木木的人都说他是一具活生生的死尸，身材矮小，通体捆绑着青灰色的绷带。世人围绕阿木木编造了许多神话故事、民间传说和史诗传奇。这些故事世代传颂，以至于再也没人能分得清哪些是真相，哪些是幻想。",
                "usageTips": "冷却时间减少的阿木木很强，但他很难通过装备来获取冷却缩减。击杀苍蓝雕纹魔像获得增益来减少冷却时间，而不需要牺牲装备数值。",
                "battleTips": "阿木木的绝望光环按生命比例伤害，所以购买增加生命值的物品效果并不一定理想。",
                "name": "阿木木",
                "defense": "6",
                "magic": "8",
                "difficulty": "3",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Amumu_Passive.png",
                    "title": "诅咒之触",
                    "description": "阿木木的攻击会诅咒他的敌人3秒，使其从所有来源的魔法伤害中多承受额外的10%真实伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BandageToss.png",
                    "title": "绷带牵引",
                    "description": "阿木木掷出一条绷带，然后将他拉向命中的第一个敌人，使其晕眩1秒，并造成80/130/180/230/280（+70% *AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AuraofDespair.png",
                    "title": "绝望光环",
                    "description": "开启：阿木木开始哭泣，每秒（实为每0.5秒）对附近的敌人们造成4/6/8/10/12外加0.5%/0.625%/0.75%/0.875%/1%（+0.002%*AP）最大生命值的魔法伤害并刷新诅咒。\n\n百分比生命值伤害 ：1% /1.25%/1.5%/1.75%/2%\n基础伤害 ：8/12/16/20/24"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Tantrum.png",
                    "title": "阿木木的愤怒",
                    "description": "被动：阿木木受到的物理伤害降低2/4/6/8/10（+3%*护甲 +3%*魔抗）。此外，当阿木木被一次攻击命中时，这个能的冷却时间会缩短0.5秒\n\n主动：阿木木大发脾气，对附近的敌人们造成75/95/115/135/155（+50%*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CurseoftheSadMummy.png",
                    "title": "木乃伊之咒",
                    "description": "阿木木用绷带将周围敌方单位通通缠住，将其晕眩1.5/1.75/2秒，造成150/250/350（+80%*AP）魔法伤害并施加诅咒效果。"
                  }
                ],
                "data": {
                  "hp": "613.1200",
                  "hpperlevel": "84.0000",
                  "hpregen": "9.0000",
                  "hpregenperlevel": "0.8500",
                  "mp": "287.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.3820",
                  "mpregenperlevel": "0.5250",
                  "attackdamage": "53.3800",
                  "attackdamageperlevel": "3.8000",
                  "attackspeed": "0.7360",
                  "attackspeedperlevel": "2.1800",
                  "armor": "33.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "披甲龙龟",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Rammus.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big33000.jpg",
                "story": "许多人对其崇拜敬仰，少数人对其嗤之以鼻，但所有人都对其一无所知。奇怪而有趣的生物拉莫斯就是一个谜团。他全身覆盖尖刺硬壳，人们对他的出身来历的猜测层出不穷——有人说他是半神，有人说他是神谕者，有人说他只是普通的野兽，遭遇了魔法的影响而发生彻底变异。无论真相如何，拉莫斯始终都默不作声，而且不会为任何人停留，永远都在沙漠中奔走游荡。",
                "usageTips": "地动山摇与尖刺防御可以在游戏后期用来摧毁防御塔（地动山摇对建筑物有效）。如果你在团队作战中陷入困境，可以转而攻击建筑物。",
                "battleTips": "在拉莫斯不使用尖刺防御时攻击他，因为拉莫斯不用此技能时，他的属性要远低于一般的坦克英雄。",
                "name": "拉莫斯",
                "defense": "10",
                "magic": "5",
                "difficulty": "5",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Armordillo_ScavengeArmor.png",
                    "title": "锥刺甲壳",
                    "description": "拉莫斯的普攻附带10（+10%*护甲）额外魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PowerBall.png",
                    "title": "动力冲刺",
                    "description": "拉莫斯会蜷缩为球状并不断加速，在6秒里持续获得最多150%移动速度加成。他会在与敌人碰撞后停下，对附近的敌人造成100/135/170/205/240（+100%*AP）魔法伤害以及击退效果，并使这些敌人减速40%/50%/60%/70%/80%，持续1秒。\n\n可再次激活技能来提前结束该状态并让此技能进入冷却阶段。\n此技能是一个引导类技能，并且可通过那些阻止施放的技能来打断。\n施放【动力冲刺】会取消掉【尖刺防御】并让它进入冷却阶段。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DefensiveBallCurl.png",
                    "title": "尖刺防御",
                    "description": "拉莫斯会进入一个防御姿态，最多持续6秒，在此姿态下，护甲提升30加上60%/70%/80%/90%/100%，魔抗提升10加上30%/35%/40%/45%/50%，但移动速度会减慢30%。\n\n在此期间，锥刺甲壳会造成150%伤害并且会将锥刺甲壳的伤害回敬给那些对拉莫斯进行普攻的敌人。\n\n可再次激活技能来提前结束该状态并让此技能进入冷却阶段。\n施放【尖刺防御】会取消掉【动力冲刺】并让它进入冷却阶段。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PuncturingTaunt.png",
                    "title": "狂乱嘲讽",
                    "description": "嘲讽一个敌方英雄或野怪1.25/1.5/1.75/2/2.25秒，并获得相同持续时间的20%/25%/30%/35%/40%攻击速度加成。\n\n当拉莫斯的任何其它技能处于激活状态时，狂乱嘲讽的攻击速度加成会被刷新"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Tremors2.png",
                    "title": "地动山摇",
                    "description": "撼动大地7秒，并对附近的敌人造成30/60/90（+20%*AP）魔法伤害和持续1.5秒的8%/10%/12%减速效果，最多可叠加8次。\n\n【地动山摇】会对防御塔造成200%伤害。"
                  }
                ],
                "data": {
                  "hp": "564.4800",
                  "hpperlevel": "95.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "310.4400",
                  "mpperlevel": "33.0000",
                  "mpregen": "7.8400",
                  "mpregenperlevel": "0.5000",
                  "attackdamage": "55.8800",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6560",
                  "attackspeedperlevel": "2.2150",
                  "armor": "36.0000",
                  "armorperlevel": "4.3000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "祖安狂人",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/DrMundo.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big36000.jpg",
                "story": "精神与认知彻底崩坏、杀人的欲望永不满足、浑身皮肤紫得发黑，这就是蒙多医生，这就是祖安人在漆黑的夜里不敢出门的原因。这个头脑简单的恐怖怪人似乎唯一关心的东西就是痛苦，不仅是施加痛苦，而且也是感受痛苦。他抡着一把巨大的切肉刀，举重若轻。他曾经捕捉并折磨过数十名祖安居民，因此声名狼藉。他将自己的行为称为“手术”，但却没有任何真正的目的。他残酷无情。他神出鬼没。他想去哪儿就去哪儿。另外，准确地说，他并不是一名医生。",
                "usageTips": "时机正好的背水一战可以引诱敌方英雄来攻击你，即使他们没有足够的攻击输出消灭你。",
                "battleTips": "在蒙多医生使用终极必杀技后马上和队友使用强大的攻击技能。如果不能够立刻杀死他，他会迅速回复生命值。",
                "name": "蒙多医生",
                "defense": "7",
                "magic": "6",
                "difficulty": "5",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/DrMundo_Passive.png",
                    "title": "肾上腺激素",
                    "description": "蒙多医生每5秒回复1.50%的最大生命值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/InfectedCleaverMissileCast.png",
                    "title": "病毒屠刀",
                    "description": "蒙多医生挥出他的屠刀，造成目标20%/22.5%/25%/27.5%/130%当前生命值的魔法伤害(不会小于80/130/180/230/280)和40%减速效果，持续2秒。\n\n如果屠刀命中，就会返还20/24/28/32/36生命值(如果屠刀击杀了单位，就会返还40/48/56/64/72生命值)。\n这个技能对野怪造成的伤害不会多于300/350/400/450/500。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BurningAgony.png",
                    "title": "痛苦燃烧",
                    "description": "激活：蒙多医生让自己着火，每秒对周围敌人造成40/55/70/85/100（+20%*AP）魔法伤害。\n\n在着火状态下，蒙多所受的控制技能效果减少30%的持续时间。\n\n消耗（生命值）：（10/15/20/25/30）/秒"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Masochism.png",
                    "title": "潜能激发",
                    "description": "被动：当蒙多医生承受魔法伤害或消耗生命值时，他的魔法抗性提升3%，持续2秒，最多可达6%/15%/24%/33%/42%。\n\n主动：蒙多医生的下次普攻将头槌敌人，造成额外的(3%/3.5%/4%/4.5%/5%最大生命值)物理伤害，并消耗25/35/45/55/65生命值。\n\n在激活后，蒙多医生最多获得60/90/120/150/180攻击力(不少于40/55/70/85/100)，并基于他的已损失生命值而持续5秒。\n\n每1%的已损失生命值会使这个技能提供0.2/0.35/0.5/0.65/0.8攻击力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Sadism.png",
                    "title": "背水一战",
                    "description": "蒙多医生在12秒里持续回复共(50%/75%/100%的最大生命值)生命值，并在持续期间获得15%/25%/35%移动速度。\n\n消耗（生命值） ：25%当前生命值"
                  }
                ],
                "data": {
                  "hp": "582.5200",
                  "hpperlevel": "89.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "61.2700",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.7210",
                  "attackspeedperlevel": "2.8000",
                  "armor": "36.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "熔岩巨兽",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Malphite.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big54000.jpg",
                "story": "墨菲特是一个庞大的岩石生物，为了给混乱的世界赐予秩序而不懈奋斗。他诞生之初的身份是一个石仆，侍奉着一块超乎凡人理解的石碑，名为“独石”。他用万钧元素之力维护自己的先祖，但最终遭遇了失败。在随后的毁灭中，墨菲特成为了唯一的幸存者。如今他忍受着符文之地的脆弱凡人和他们流水一般多变的性情，同时想尽办法给自己寻找一个存于世上的新位置，让自己不愧为同胞中的最后一员。",
                "usageTips": "护甲能降低花岗岩护盾在承受敌方攻击时的损耗速度，因此在敌人摧毁护盾前激活野蛮打击，可以加强护盾对物理伤害的承受能力。",
                "battleTips": "若你是物理伤害英雄，攻击墨菲特时站在友军后方。大地震颤会极大地减少你的伤害。",
                "name": "墨菲特",
                "defense": "9",
                "magic": "7",
                "difficulty": "2",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Malphite_GraniteShield.png",
                    "title": "花岗岩护盾",
                    "description": "在未受到伤害(10-6)秒后，墨菲特为自身提供(10%最大生命值）护盾值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SeismicShard.png",
                    "title": "地震碎片",
                    "description": "墨菲特朝一名敌人发出一块大地碎片，造成70/75/80/85/90(+60%AP)魔法伤害，并且使目标减速和为墨菲特提供15%/20%/25%/30%/35%移动速度，持续3秒"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Obduracy.png",
                    "title": "雷霆拍击",
                    "description": "被动：墨菲特获得(10%/15%/20%/25%/30%)护甲。这个效果会在花岗岩护盾激活时提升至(30%护甲)。\n主动：墨菲特的下次攻击造成额外的30/45/60/75/90 (+20%法术强度) (+10%额外护甲)物理伤害，并会引发一阵余震，以在一个锥形范围内造成(10/20/30/40/50+20%AP +10%护甲)物理伤害。在接下来的5秒内，他的攻击会继续引发余震。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Landslide.png",
                    "title": "大地震颤",
                    "description": "墨菲锤击地面，对周围敌人造成60/95/130/165/200 (+60%法术强度) (+30%额外护甲)魔法伤害并减少他们30%/35%/40%/45%/50%攻击速度，持续3秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/UFSlash.png",
                    "title": "势不可挡",
                    "description": "墨菲特携着山崩之力冲锋，进行不可阻挡的突进。在突进结束时，墨菲特会击飞1.5秒并造成200/300/400(+80%AP)魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "574.2000",
                  "hpperlevel": "90.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "282.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.3240",
                  "mpregenperlevel": "0.5500",
                  "attackdamage": "61.9700",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.7360",
                  "attackspeedperlevel": "3.4000",
                  "armor": "37.0000",
                  "armorperlevel": "3.7500",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "扭曲树精",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Maokai.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big57000.jpg",
                "story": "茂凯是一只内心暴怒外形魁梧的树精，不知疲倦地对抗暗影岛的骇人异象。一场魔法灾变摧毁了他的家园，同时也将他变成一股复仇的力量，使他免遭不死诅咒的，全靠他芯中灌注融合的生命之水。曾经的茂凯是一只平和的自然之灵，而现在的他则一直在暴怒地战斗，只为了将不死的灾祸逐出暗影岛，让他的家园重归往日的秀美。",
                "usageTips": "树苗可以投进草丛里，来获取强化效果——但同样不会堆叠。",
                "battleTips": "树苗会追击第一个距离它过近的敌人，但会在几秒后或与其它敌人碰撞后爆炸。要特别当心草丛里的树苗，因为它们会更加危险。",
                "name": "茂凯",
                "defense": "8",
                "magic": "6",
                "difficulty": "3",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Maokai_Passive.png",
                    "title": "吸元秘术",
                    "description": "茂凯的攻击会治疗自身（5-65↑+6%❤）生命值。这个效果有（30-20↑）秒冷却时间。\n每当茂凯释放一次技能或被一个敌方技能命中时，吸元秘术的冷却时间会缩短4秒。\n如果茂凯的生命值高于95%，则这次攻击不会使用治疗效果"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MaokaiQ.png",
                    "title": "荆棘重击",
                    "description": "茂凯将他的拳头砸入大地中，造成70/110/1550/190/230+(40%AP）魔法伤害并暂时减速敌人们。附近的敌人们还会被击退。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MaokaiW.png",
                    "title": "扭曲突刺",
                    "description": "茂凯扭曲为一团移动根须，无法被选取并且向目标突进。在抵达后，他讲禁锢目标1/1.1/1.2/1.3/1.4秒并造成70/95/120/145/170+（40%AP）魔法伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MaokaiE.png",
                    "title": "树苗投掷",
                    "description": "茂凯扔出一枚存活30秒树苗,树苗爆炸后对附近的单位造成[25/50/75/100/125]+[7/7.25/7.5/7.75/8%目标最大生命值]点魔法伤害并在2秒内减速目标35%。如果树苗被扔进了草丛，那么它们的持续时间会延长到30(+3/每100额外生命值)秒，且在爆炸之后还会对所有命中的目标在2秒内造成双倍伤害。树苗对非英雄单位的最大伤害为300，在草丛中生成时的最大伤害为600。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MaokaiR.png",
                    "title": "自然之握",
                    "description": "在大范围内召唤五个不断向前行进的根须，最大行进距离为3000,每一个根须都会对第一个命中的敌方英雄造成150/225/300(+0.75AP)点魔法伤害,并控制目标英雄0.8-2.6秒，根须行进的越远控制时间就越长，在1000距离时达到最大控制时间。(AP加成:0.75)"
                  }
                ],
                "data": {
                  "hp": "565.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "5.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "375.0000",
                  "mpperlevel": "43.0000",
                  "mpregen": "7.2000",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "63.5400",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.8000",
                  "attackspeedperlevel": "2.1250",
                  "armor": "39.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "圣锤之毅",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Poppy.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big78000.jpg",
                "story": "符文之地从来都不缺勇敢的英雄，但很少有谁能像波比一样坚毅。她一直带着奥伦的传奇圣锤，哪怕锤柄立起来有两个她那么高。这位意志坚决的约德尔人在无数个岁月中一直都在秘密地寻找“德玛西亚的英雄”，也就是传说中这把战锤的最合适的主人。在找到他之前，波比肩负起战斗的使命，用旋风般的攻击打退王国的敌人。",
                "usageTips": "被动技能【钢铁大使】的圆盾会倾向于落在墙体附近，尽量利用这点来让【E英勇冲锋】发挥作用。",
                "battleTips": "波比可以用她的【W坚定风采】来阻挡敌人的突进。",
                "name": "波比",
                "defense": "7",
                "magic": "2",
                "difficulty": "6",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Poppy_Passive.png",
                    "title": "钢铁大使",
                    "description": "波比的下次普攻会将她的圆盾给投掷出去，获得350攻击距离和20~180(1-18级)额外魔法伤害，16~8(1-18级)秒冷却时间。\n\n圆盾会落在附近的一个区域并且波比可以把它捡起来，以提供一个护盾，这个护盾可吸收(15%/17.5%/20%*生命值)(1-18级)伤害，持续3秒。敌人可通过踩在圆盾上来将它摧毁。\n\n如果攻击击杀了目标，那么圆盾会自动回到波比身上。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PoppyQ.png",
                    "title": "圣锤猛击",
                    "description": "波比猛砸地面，造成40/60/80/100/120（+90%*额外AD）加上敌人8%最大生命值的物理伤害，并留下一个不稳定区域。\n\n这个区域会使范围内的敌人减速20%/25%/30%/35%/40%并在1秒后喷发，再次造成初始伤害。\n\n波比的百分比生命值伤害对野怪的封顶值为每段50/80/110/140/170物理伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PoppyW.png",
                    "title": "坚定风采",
                    "description": "被动：波比获得(10%*护甲)护甲和(10%*魔抗)魔抗。这个加成会在波比的生命值低于40%时翻倍。\n\n主动：在接下来的2秒里，波比获得40%移动速度。当坚定风采激活时，她会中断敌人在她身边进行的突进，以造成70/110/150/190/230（+70%*AP）魔法伤害，还会对其造成缚地和25%减速效果，持续2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PoppyE.png",
                    "title": "英勇冲锋",
                    "description": "波比擒抱一名敌人，造成60/80/100/120/140（+50%*额外AD）物理伤害并将该敌人推向前方。如果波比将目标推到地形上，那么敌人会受到额外的60/80/100/120/140（+50%*额外AD）物理伤害并被晕眩1.6/1.7/1.8/1.9/2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PoppyR.png",
                    "title": "持卫的裁决",
                    "description": "首次施放：波比引导最多4秒，同时使她自身减速15%。\n\n再次施放：波比猛砸地面，发出一道冲击波，对命中的第一个英雄身边的敌人造成200/300/400（+90%*额外AD）物理伤害，并朝着他们的召唤师平台击退他们一大段距离。引导时间越久，则冲击波的长度和击退距离越长。\n\n未蓄力的【持卫的裁决】会造成半额伤害并击飞敌人0.75秒。\n\n被蓄力过的【持卫的裁决】所命中的敌人会处于不可被选取状态。"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.8000",
                  "mp": "280.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.5000",
                  "armor": "38.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "暮光之眼",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Shen.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big98000.jpg",
                "story": "在隐秘的艾欧尼亚武装力量均衡教派中，慎背负着领袖的职责和暮光之眼的称号。他力求超脱自身的情感、偏执以及自我，行走于灵魂领域和物质世界之间无人通晓的隐秘道路。为了贯彻精神与物质之间的均衡，慎用一把钢刀和一把魂刃对抗任何威胁到均衡的人。",
                "usageTips": "你的能量是可以持续快速恢复的，所以可以利用这点在与使用法力的英雄们对线时逐渐积累优势。",
                "battleTips": "一旦慎到达了6级，就要留意他用全图传送终极技能快速扭转局部战斗的局势。",
                "name": "慎",
                "defense": "9",
                "magic": "3",
                "difficulty": "4",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Shen_Passive.png",
                    "title": "忍法！气合盾",
                    "description": "在施放一次技能后，慎会获得60~111(1-18级 每级+3) （+14% 额外生命值 ）护盾值，持续2.5秒(10秒冷却时间)。如果技能成功地对一名友方英雄或敌方英雄产生作用，那么这个技能的冷却时间就会减少4/5/6/7/8(1-18级)秒。\n\n慎的查克拉的外观是一把魂刃，他可以用技能来操控它。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShenQ.png",
                    "title": "奥义！暮临",
                    "description": "慎召回他的魂刃并拖动它。如果魂刃在召回途中与敌方英雄产生了碰撞，那么该敌方英雄在逃离慎的时候会被减速15%/20%/25%/30%/35%，持续2秒。\n\n慎的下3次攻击造成10加上2%/2.5%/3%/3.5%/4%（+1.5％每100AP）目标最大生命值的额外魔法伤害。如果魂刃曾与一名敌方英雄相碰撞，那么这些攻击的伤害值会变为10加上5%/5.5%/6%/6.5%/7%（+2％每100AP）目标最大生命值，并提供+50%攻击速度加成。\n\n每次攻击会对野怪造成100%额外魔法伤害(最大伤害：120/140/160/180/200)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShenW.png",
                    "title": "奥义！魂佑",
                    "description": "慎的魂刃会创建一个持续1.75秒的防御结界。原本会命中结界内的慎或友方英雄的攻击都会被格挡。\n\n如果结界内没有任何英雄可以保护，那么魂刃将不会激活，直到一个友方英雄进入或经过2秒为止。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShenE.png",
                    "title": "奥义！影缚",
                    "description": "被动：用【E奥义！影缚】或【Q奥义！暮临】造成伤害时会回复30/35/40能量。\n\n主动：慎朝着一个方向冲刺，对沿途的敌方英雄和野怪造成60/85/110/135/160（+15% 额外生命值 ）物理伤害和1.5秒的嘲讽效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ShenR.png",
                    "title": "秘奥义！慈悲度魂落",
                    "description": "慎为目标友方英雄提供一层持续5秒的护盾，护盾最多吸收175/350/525（+135％*AP）到280/560/840（+216％*AP）伤害，基于目标已损失的生命值。在引导3秒后，慎和他的魂刃会传送到友方英雄身边。"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.7500",
                  "mp": "400.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "50.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.7510",
                  "attackspeedperlevel": "3.0000",
                  "armor": "34.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "不灭狂雷",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Volibear.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big106000.jpg",
                "story": "对于那些敬畏旧神的人，沃利贝尔是风暴的实体化身。他充满怪力、野性和倔强的坚毅，早在凡人行走于弗雷尔卓德的冻土苔原之前，他就已经存在。这片土地由他和他的半神同胞们共同创造，是他要拼死保护的东西。人类的文明以及随之而来的软弱让他积怨已久，如今他为了旧习古道而战——要让这片土地回归野性，让鲜血畅流无阻。他渴望迎战任何反对者，亮出他的尖牙、利爪和雷霆般的压制力。",
                "usageTips": "用【Q滚滚雷霆】来发起冲锋，然后用【E至尊咆哮】来让敌人减速。",
                "battleTips": "利贝尔在追杀目标时会获得极大的移动速度加成，但他在逃命的时候获得的加成很少。",
                "name": "沃利贝尔",
                "defense": "7",
                "magic": "4",
                "difficulty": "3",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/VolibearPassive.png",
                    "title": "狂雷渐起",
                    "description": "沃利贝尔在每次用攻击或技能造成伤害时都会获得5%（+0.04％*AP）攻击速度，持续6秒，至多可叠加5层。\n\n叠满5层时，沃利贝尔的双爪会激荡闪电，使他的攻击对目标和相距最近的4名敌人造成10~59(1-18级)（+40％*AP）额外魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VolibearQ.png",
                    "title": "擂首一击",
                    "description": "沃利贝尔获得10%/14%/18%/22%/26%移动速度，在朝着敌方英雄时翻倍至20%/28%/36%/44%/52%，持续4秒。在激活时，沃利贝尔的下次攻击会造成20/40/60/80/100（+100%*AD +120%额外*AD）物理伤害并使目标晕眩1秒。\n\n如果在沃利贝尔对一个目标造成晕眩效果前被敌人定身的话，那么他会因发怒而提前结束这个技能但刷新它的冷却时间。\n\n伤害收益为100%基础攻击力和120%额外攻击力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VolibearW.png",
                    "title": "暴怒撕咬",
                    "description": "沃利贝尔猛拍一名敌人，造成10/35/60/85/110（+100%*AD +6%额外生命值）物理伤害，并标记自标8秒。\n\n如果这个技能施放在一个被标记的目标身上，那么这个技能的伤害会提升至15/52.5/90/127.5/165（+150%*AD +9%额外生命值），并且沃利贝尔会回复20/35/50/65/80生命值 +他8%/10%/12%/14%/16%的已损失生命值。\n\n这个技能可施加攻击特效。\n对抗小兵和野怪时的治疗效果只有50%效能。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VolibearE.png",
                    "title": "霹天雳地",
                    "description": "沃利贝尔在目标区域召唤一道闪电箭，造成80/110/140/170/200（+80％*AP）加上11%/12%/13%/14%/15%最大生命值的魔法伤害并使敌人减速40%，持续2秒。\n\n如果沃利贝尔在闪电落点区域内，他还会获得一层（80％*AP）加上11%/12%/13%/14%/15%最大生命值的护盾，持续3秒。\n\n对非英雄单位的伤害封顶值为150/300/450/600/750。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VolibearR.png",
                    "title": "天声震落",
                    "description": "沃利贝尔变换形态并跃向一个目标区域，获得200/400/600生命值和50攻击距离，持续12秒。\n\n在着陆时，附近的敌方防御塔会失效3/4/5秒，并受到300/600/900（+125％*AP +250%*额外AD）物理伤害。附近的敌人会被减速50%，减速效果会在1秒里持续衰减。被沃利贝尔直接压住的敌人们会受到300/500/700（+125％*AP +250%*额外AD）物理伤害。"
                  }
                ],
                "data": {
                  "hp": "584.4800",
                  "hpperlevel": "86.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.6500",
                  "mp": "270.4000",
                  "mpperlevel": "30.0000",
                  "mpregen": "8.0920",
                  "mpregenperlevel": "0.6500",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.6700",
                  "armor": "35.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "北地之怒",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Sejuani.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big113000.jpg",
                "story": "瑟庄妮是凛冬之爪部族的战母。继承了寒冰血脉的她残忍无情，带领的部族也是弗雷尔卓德土地上最令人闻风丧胆的。她的部族的生存，是一场旷日持久的、毫无希望的对抗元素之力的战斗，迫使他们劫掠诺克萨斯人、德玛西亚人，还有阿瓦罗萨部族等等，以便渡过残酷的凛冬。瑟庄妮在最危险的战斗中身先士卒，骑着居瓦斯克野猪“钢鬃”冲在最前方，用她臻冰打造的链枷让敌人粉身碎骨。",
                "usageTips": "近战友军可以帮你的【E永冻领域】堆叠霜冻层数，要留意他们都在攻击谁。",
                "battleTips": "注意躲掉【W凛冬之怒】的第二次挥击，因为这次挥击会造成爆发性的伤害。",
                "name": "瑟庄妮",
                "defense": "7",
                "magic": "6",
                "difficulty": "4",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Sejuani_passive.png",
                    "title": "北地之怒",
                    "description": "在不承受来自英雄或大型野怪的伤害3秒后，瑟庄妮会免疫减速效果并获得(10+ 50%额外护甲)护甲和(10 +50%额外魔抗)魔法抗性。\n瑟庄妮对被她晕眩的敌人进行的第一次攻击会造成额外的(10%)最大生命值的魔法伤害。\n对史诗级野怪的百分比生命值伤害封顶值为300。\n在受到来自英雄或者大型野怪的伤害后，防御效果会存留2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SejuaniQ.png",
                    "title": "极寒突袭",
                    "description": "瑟庄妮冲锋，对敌人造成(80/130/180/230/280 +60%AP)魔法伤害并击飞敌人0.5秒，冲锋会在命中一名敌方英雄后停止。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SejuaniW.png",
                    "title": "凛冬之怒",
                    "description": "瑟庄妮挥击她的链枷，造成(20/25/30/35/40 +20%AP +2%生命值)物理伤害并击退小兵和野怪。然后她会再次挥击，造成(30/70/110/150/190 +60% +5.25%生命值)物理伤害并暂时减速目标75%，持续0.25秒。\n两段挥击都会施加永冻效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SejuaniE.png",
                    "title": "永冻领域",
                    "description": "被动：附近近战友方英雄的攻击会对英雄和野怪施加一层效果。\n主动：瑟庄妮对一名 带有4层此效果的敌人造成(55/105/155/205/255 +60%AP)魔法伤害并使其晕眩1秒。\n被瑟庄妮晕眩的英雄在10秒内不会获得此效果。\n被这个技能晕眩的敌人会被略微击退。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SejuaniR.png",
                    "title": "极冰寒狱",
                    "description": "瑟庄妮扔出她的极冰套索，晕眩并显形命中的第一个敌方英雄1秒并造成(125/150/175 +40%AP)魔法伤害。\n如果套索飞行了至少25%的最大距离，那么它会晕眩并显形21.5秒。它还会生成一团冰风暴来使目标附近的敌人减速80%，持续2秒，所有受影响的敌人都会受到(200/300/400 +80%AP)魔法伤害。\n被晕眩的敌人会立刻受到伤害，而风暴内的敌人会在风暴结束后受到伤害。风暴内的敌人还会被减速额外的1秒。"
                  }
                ],
                "data": {
                  "hp": "560.0000",
                  "hpperlevel": "100.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "1.0000",
                  "mp": "400.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "66.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6880",
                  "attackspeedperlevel": "3.5000",
                  "armor": "34.0000",
                  "armorperlevel": "4.2500",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "生化魔人",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Zac.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big154000.jpg",
                "story": "一滩泄漏的毒液，顺着炼金科技设施的裂缝流进了祖安的地沟区，在深处一个与世隔绝的坑洞里积成了一洼。出身虽然如此低微，但扎克却从一团蒙昧的黏液长成了一个有思想的实体，栖息在城里的管道中，偶尔露面，帮助那些无助的人，或是修缮祖安的各种公共设施。",
                "usageTips": "如果你想要维持血量的话，要特别注重粘液的拾取。",
                "battleTips": "在扎克为他的橡筋弹弓充能时，你可以用沉默、晕眩、束缚和击飞效果来干扰他。",
                "name": "扎克",
                "defense": "7",
                "magic": "7",
                "difficulty": "8",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/ZacPassive.png",
                    "title": "细胞分裂",
                    "description": "扎克每用一个技能命中一次敌人，一小块粘液就会飞溅出去。扎克可以重新吸收这些粘液，来为自己回复(5 +1%生命值)生命值。\n在阵亡时，扎克会分裂成四块细胞组织，并试图重新结合。如果在(8-4)秒后，仍然有细胞组织存活，那么他就会重生，重生时的生命值为10- 50%最大生命值(基于存活的细胞组织的生命值)。这个效果有300秒的冷却时间。\n每个细胞组织拥有扎克12%的最大生命值，以及他50%的护甲和魔法抗性。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZacQ.png",
                    "title": "延伸打击",
                    "description": "扎克伸展他的手臂，将其吸附到命中的第一个敌人身上以造成(40/55/70/85/100 +30%AP，+2.5%生命值)魔法伤害并暂时将该敌人减速。扎克的下次攻击会提升距离并且造成相同的伤害和减速。\n如果扎克用这次攻击命中了一个不同的敌人，那么他会将这两个敌人朝着彼此击飞。如果这两个敌人产生碰撞，那么他们及他们附近的敌人都会受到(40/55/70/85/100 +30%AP +2.5%生命值)魔法伤害并被暂时减速。\n法力消耗：8%当前生命值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZacW.png",
                    "title": "不稳定物质",
                    "description": "扎克的躯体进行喷溅，对附近的敌人造成(25/40/55/70/85 +(4%/5%/6%/7%/8% +0.02AP))最大生命值的魔法伤害。\n吸收粘液可使[不稳定物质]的冷却时间缩短1秒。\n对小兵和野怪的最大生命值伤害的上限为200。\n法力消耗：4%当前生命值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZacE.png",
                    "title": "橡筋弹弓",
                    "description": "开始蓄力:扎克将自身绷紧，在0.9秒里积攒-次冲刺。\n释放:扎克将自身发射出去，并击飞着陆点位置的敌人们至多1秒(基于蓄力时间)并\n对他们造成60/110/160/210/260 +90%AP)魔法伤害。扎克每命中一名敌方英雄就会生成一团额外的粘液\n可通过移动来取消。\n如果取消，会返还此技能一半的冷却时间和消耗。\n距离：1200/1350/1500/1650/1800\n法力消耗：4%当前生命值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZacR.png",
                    "title": "动感弹球",
                    "description": "被动:细胞分裂的治疗效果提升至每块细胞组织(6 +(1%/2%/3%)生命值)生命值。\n主动:扎克弹跳4次。弹跳在首次命中敌人时会将其击退并造成(140/210/280+ 40%AP)魔法伤害。后续弹跳会造成(70/105/140 +20%AP)魔法伤害并减速20%持续1秒。\n扎克在此期间持续获得至多50%移动速度并且可以在弹跳时施放不稳定物质。\n移动速度在期间内从20%持续提升到50%"
                  }
                ],
                "data": {
                  "hp": "615.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "3.4000",
                  "attackspeed": "0.7360",
                  "attackspeedperlevel": "1.6000",
                  "armor": "33.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "山隐之焰",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ornn.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big516000.jpg",
                "story": "奥恩是弗雷尔卓德的一位半神，主掌着锻造和工艺。他在名为炉乡的火山下的溶洞中凿出了一座雄伟的工坊，独自一人在里头干活。他摆弄着熔岩沸腾的坩埚，提炼矿石，打造出无与伦比的精良物件。当其他神灵——尤其是沃利贝尔，在大地上行走并且介入了凡间的事务时，奥恩就会出面，将这些鲁莽的家伙劝回各自的位置上。要么是用手里可靠的锤子，要么就是群山的烈火。",
                "usageTips": "你的技能施放顺序非常重要！尽可能将【易碎】的作用发挥至最佳。",
                "battleTips": "离墙体远一点。如果奥恩没有晕眩到你，那么他就没那么强大了。",
                "name": "奥恩",
                "defense": "9",
                "magic": "3",
                "difficulty": "5",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/OrnnP.png",
                    "title": "活体锻炉",
                    "description": "奥恩可在地图的任意位置上花费金钱来锻造非消耗品的装备。\n当奥恩到达13级时，他将能把自己的神话装备升级成杰作装备。13级后，每升1级，奥恩就可为一名友方升级一件神话装备，方法是前往友方英雄附近然后点击该友方英雄。\n奥恩使所有来源的生命值、护甲和魔抗加成额外提升10%。奥恩每将一件神话装备升级为杰作装备，就会使这一数值提升4%。\n奥恩可以直接通过他的锻造菜单或者商店菜单来锻造物品。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrnnQ.png",
                    "title": "火山突堑",
                    "description": "奥恩猛砸地面，创造一道裂隙来造成(20/45/70/95/120 +110%AD)物理伤害，以及持续2秒的40%减速效果。一根熔岩之柱会在裂隙的终点处形成，持续4秒。\n裂隙在命中一名敌方英雄后会短暂地停滞。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrnnW.png",
                    "title": "风箱炎息",
                    "description": "奥恩向前重踏，喷吐炎息使他变为不可阻挡状态，持续0.75秒。炎息最多会造成敌方(12%/13%/14%/15%/16%)最大生命值的魔法伤害。\n被最后一道炎息命中的敌人，会变为易碎效果，持续 3秒。对易碎状态的敌人施加的下一个定身类效果将使该效果的持续时间提升30%并造成目标(10%-18%)最大生命值的魔法伤害。\n奥恩的普攻会击退易碎状态下的敌人。\n对英雄和小兵的最小伤害为(80)，对野怪的最大伤害值为(250)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrnnE.png",
                    "title": "炽烈冲锋",
                    "description": "奥恩冲锋，对命中的敌人造成(80/125/170/215/260 +40%额外护甲 +40%额外魔抗)物理伤害。\n如果奥恩撞到了地形，则会引发冲击波，以击飞敌人1.25秒并将此技能的伤害施加给那些未被冲锋命中的敌人。\n冲击波会摧毁熔岩之柱和由敌方英雄创造的地形。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/OrnnR.png",
                    "title": "熔铸之神的召唤",
                    "description": "奥恩召唤一个巨大的岩浆元素来疾奔向他。被岩浆元素途径的敌人会受到(125/175/225 +20%AP)魔法伤害，基于岩浆元素的移动距离至多被减速(40%/50%/60%，持续2秒，并变为易碎状态，持续3秒。\n奥恩可以再次施放这个技能来猛冲向岩浆元素，以使它重新导向并强化它。被强化过的岩浆元素会击飞首个命中的敌方英雄1秒(对后续命中的英雄的持续时间减少至 50%)，造成(125/175/225 +20%/AP)魔法伤害并施加易碎效果。"
                  }
                ],
                "data": {
                  "hp": "590.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "9.0000",
                  "hpregenperlevel": "0.9000",
                  "mp": "340.6000",
                  "mpperlevel": "45.0000",
                  "mpregen": "8.0100",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "69.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.0000",
                  "armor": "36.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "镕铁少女",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Rell.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big526000.jpg",
                "story": "芮尔是黑色玫瑰用凶残的试验一手制造出来的产物，她是狂妄的活体武器，决心要颠覆诺克萨斯。她的童年悲惨且惊悚，那些不可言说的处理过程将她的魔法塑造成完美的武器，让她可以任意控制金属。直到最后她展开了一场暴力的逃离，从数量众多的看管者中杀出一条血路。如今，被认定为逃犯的芮尔对诺克萨斯士兵进行无差别攻击，同时寻找着曾经“学院”里的幸存者，保护柔弱者，对曾经的那些教员送去无情的死亡。",
                "usageTips": "芮尔可以通过普通攻击获取护甲和魔抗",
                "battleTips": "当心芮尔的大招，在战场中被她吸取强制位移会导致你的英雄被对方集火击杀",
                "name": "芮尔",
                "defense": "0",
                "magic": "0",
                "difficulty": "0",
                "attack": "0",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/RellP.DarkSupport.png",
                    "title": "溃敌沉力",
                    "description": "芮尔的攻击速度非常缓慢，但会暂时窃取目标的一部分护甲和魔法抗性，并基于窃取的数额来造成额外伤害。此外，芮尔可以从多个不同的敌人身上虹吸双抗，将自身的抗击能力提到极限。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RellQ.png",
                    "title": "裂阵",
                    "description": "芮尔使用长枪突刺，摧毁刺击范围内所有敌人的护盾，并造成伤害(命中首个单位后伤害会递减)。如果芮尔在突刺时与一名友军处于E技能的绑定状态，每有一名敌方英雄被长枪命中，她和友军都会回复一定的生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RellW.png",
                    "title": "驭铁术：轰落",
                    "description": "W1：驭铁术：轰落\n(只能在骑马时施放)芮尔跳向空中并将她的坐骑变为重型铠甲，获得一层巨额护盾，护盾会一直持续到被摧毁或芮尔再次骑上坐骑为止。落地时，芮尔会击飞周围的所有敌人。芮尔可以在变形期间施放E技能和大招。\n在铠甲形态下，芮尔会获得更强的抗击打能力，移动速度降低，并且会有一个移动速度封顶值。在变形后，这个技能会被更换为W2。\n\nW2：驭铁术：驾临\n(只能在步行时施放)芮尔向前冲刺，并将她的铠甲变为一匹坐骑，获得爆发性的移动速度加成。下一次攻击时，芮尔会朝目标冲锋以造成额外伤害并将敌人挑至自己身后。\n芮尔在骑乘状态下可以提升移动速度。在变形后，这个技能会被更换为W1。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RellE.png",
                    "title": "挥斥",
                    "description": "芮尔将她的一片铠甲如磁铁般绑定在目标友方英雄身上，使目标在她附近时为其提供额外的护甲和魔法抗性。\n\n芮尔可以再次施放这个技能，解除绑定，同时晕眩自己身旁以及她与被绑定队友之间的所有敌人。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RellR.png",
                    "title": "极涌",
                    "description": "芮尔喷发出有磁性的怒火，将附近的敌人们猛拽向她。之后在她周围生成一个引力场，吸引附近的敌人，持续若干秒。引力场不会打断敌人的其它行动。\n\n“黑色玫瑰想要我变成一件兵器。所以我就这样了。”"
                  }
                ],
                "data": {
                  "hp": "0.0000",
                  "hpperlevel": "0.0000",
                  "hpregen": "0.0000",
                  "hpregenperlevel": "0.0000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "0.0000",
                  "attackdamageperlevel": "0.0000",
                  "attackspeed": "0.0000",
                  "attackspeedperlevel": "0.0000",
                  "armor": "0.0000",
                  "armorperlevel": "0.0000",
                  "spellblock": "0.0000",
                  "spellblockperlevel": "0.0000",
                  "movespeed": "0.0000",
                  "attackrange": "0.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
            ]
          },
          {
            "name": "刺客",
            "heroes": [
              {
                "title": "痛苦之拥",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Evelynn.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big28000.jpg",
                "story": "在符文之地的黑暗裂缝中，恶魔伊芙琳一直在搜寻着下一个目标。她披着人类女性的撩人外表，勾引她的猎物。只要有人被她魅惑，伊芙琳就会显露出真正的形态。她会施加难以言喻的折磨，从而让自己在猎物的疼痛中获得满足。对于这个恶魔来说，这样的欢愉只是无心无邪的滥情。但是对于符文之地上的其他人，听到的则是血肉模糊的传说，提醒着人们肉欲的危险和纵欲的代价。",
                "usageTips": "【引诱】的准备时间也许看上去有点久，但它所具备的魅惑和魔抗击碎效果会为伊芙琳带来极大优势，所以等待是值得的。",
                "battleTips": "伊芙琳的很大一部分威胁都在她的魅惑效果“引诱”上。保护那些带有“引诱”标记的友军，或者，如果你被标记了，确保你的友军隔在你和伊芙琳可能发起攻击的位置之间。",
                "name": "伊芙琳",
                "defense": "2",
                "magic": "7",
                "difficulty": "10",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Evelynn_Passive.png",
                    "title": "恶魔魅影",
                    "description": "在不攻击或使用技能4秒后，伊芙琳将自身笼罩在恶魔魅影中。当低于250~590(1-18级 每级+20)（+250%*AP）生命值时，恶魔魅影每秒恢复15~75(1-18级)生命值。从6级开始，它还会提供提供伪装。\n\n受到来自英雄或防御塔的伤害，会使恶魔魅影被移除1.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EvelynnQ.png",
                    "title": "憎恨之刺",
                    "description": "伊芙琳用她的鞭子进行打击，对命中的第一个敌人造成25/30/35/40/45（+30%*AP）魔法伤害，并使伊芙琳对该敌人发起的下3次技能或攻击造成额外的10/20/30/40/50（+25%*AP）魔法伤害。伊芙琳可以再次施放这个技能最多3次。\n\n再次施放：伊芙琳发射尖刺穿过相距最近的敌人，并对命中的所有敌人造成25/30/35/40/45（+30%*AP）魔法伤害。\n\n尖刺会优先选取伊芙琳正在攻击的目标。\n\n冷却时间（s）：4\n法力消耗： 40/45/50/55/60"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EvelynnW.png",
                    "title": "引诱",
                    "description": "伊芙琳标记一名英雄或野怪5秒。如果伊芙琳用一次攻击或技能命中了该目标，那么她将抹除该印记，返还它的法力消耗，并使目标减速65%，持续0.75秒。\n\n如果标记持续了至少2.5秒，那么将它抹除还会有额外效果：\n对抗英雄时：将其魅惑1/1.25/1.5/1.75/2秒并削减25%/27.5%/30%/32.5%/35%魔法抗性，持续4秒。\n对抗野怪时：将其魅惑3/3.25/3.5/3.75/4秒并造成250/300/350/400/450（+60%*AP）魔法伤害\n\n使用这个技能不会使伊芙琳离开恶魔魅影状态\n\n\n冷却时间：15/14/13/12/11\n法力消耗（s）：60/70/80/90/100\n距离：1200/1300/1400/1500/1600"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EvelynnE.png",
                    "title": "鞭笞",
                    "description": "伊芙琳鞭笞她的目标，并造成伤害。她随后会获得短暂持续的移动速度加成。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EvelynnR.png",
                    "title": "最终抚慰",
                    "description": "伊芙琳短暂进入不可被选取状态并大肆残杀她面前区域内的敌人，然后向后位移一大段距离。"
                  }
                ],
                "data": {
                  "hp": "572.0000",
                  "hpperlevel": "84.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.7500",
                  "mp": "315.6000",
                  "mpperlevel": "42.0000",
                  "mpregen": "8.1080",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6670",
                  "attackspeedperlevel": "2.1000",
                  "armor": "37.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "恶魔小丑",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Shaco.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big35000.jpg",
                "story": "很久以前有人为一个孤独的小王子制作了一个玩物，一只附有魔法的提线人偶，它就是如今那个以杀人和破坏取乐的萨科。由于黑魔法的腐化，再加上痛失爱主，曾经友善的人偶如今只能在折磨灵魂制造苦痛的时候才能感到愉悦。小孩子的玩具和简单的戏法在他手中都有了致命的效果。而且，他觉得自己的血腥“游戏”滑稽至极——如果有谁在死寂的夜里听到了阴森的窃笑，或许那就是恶魔小丑看中了他们，作为自己新的玩物。",
                "usageTips": "使用【Q欺诈魔术】穿越地形可以帮助你干净利落地逃脱。",
                "battleTips": "如果萨科前期表现不错，把侦查守卫布置到他的野怪营地附近来对抗他是值得的。",
                "name": "萨科",
                "defense": "4",
                "magic": "6",
                "difficulty": "9",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Jester_CarefulStrikes.png",
                    "title": "背刺",
                    "description": "在目标的背后时，萨科的普攻造成额外的10~25(1-18级)（+15%*额外攻击力）物理伤害并且【E双面毒刃】造成额外的15~50(基于等级)（+10%*AP）魔法伤害。\n\n[背刺】的额外普攻伤害可以暴击。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Deceive.png",
                    "title": "欺诈魔术",
                    "description": "萨科传送到附近并进入隐形状态，持续2.5/2.75/3/3.25/3.5秒。萨科可以在使用【惊吓魔盒】或【幻像】时保持隐形状态。\n\n他在隐形期间的下一次普攻可造成25/35/45/55/65（+25%*额外攻击力）额外物理伤害（这个伤害可以暴击)。如果在目标的背后，这个攻击还能暴击，造成105%伤害。\n\n潜行一隐形：萨科只会被附近的敌方防御塔或真实视野所显形。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JackInTheBox.png",
                    "title": "惊吓魔盒",
                    "description": "萨科创造一个惊吓魔盒，魔盒会在2秒后从敌方视野中消失。它会在敌人靠近时，或是被守卫或饰品给发现时弹出，让周围的敌人慢速逃窜0.5/0.75/1/1.25/1.5秒(小兵和野怪逃窜2秒)。\n\n它的攻击会对附近所有敌人造成15/20/25/30/35（+10%*AP）魔法伤害，伤害值会在只攻击一名敌人时提升至35/50/65/80/95（+20%*AP）。它可在隐藏状态下持续40（+5%*AP）秒，或在攻击状态下持续5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TwoShivPoison.png",
                    "title": "双面毒刃",
                    "description": "被动：当双面毒刃准备就绪时，萨科的普攻会减少目标20%/22.5%/25%/27.5%/30%移动速度，持续2秒。\n\n主动：萨科扔出一把毒刃，造成70/95/120/145/170（+70%*额外攻击力 +55%*AP）魔法伤害并使目标的移动速度减少20%，持续3秒。如果目标的生命值低于30%，那么毒刃会造成150%伤害，总伤害为105/142.5/180/217.5/255（+105%*额外攻击力 +82.5%*AP）"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HallucinateFull.png",
                    "title": "幻像",
                    "description": "萨科暂时消失然后制造一个自己的幻像。幻像最多可持续18秒，并在死亡时爆炸，对附近的敌人造成150/225/300（+70%*AP）魔法伤害并生成三个立即触发的小型【惊吓魔盒】。\n\n-小型魔盒造成10/20/30（+10%*AP）伤害（如果只攻击一个敌人则为25/50/75（+15%*AP）伤害)，让敌人逃窜1秒。\n-幻像造成萨科60%的伤害并承受50%额外伤害。\n\n可以通过按住alt键的同时使用鼠标右键或再次施放此技能来控制幻像。"
                  }
                ],
                "data": {
                  "hp": "587.0000",
                  "hpperlevel": "89.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "297.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.1560",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6940",
                  "attackspeedperlevel": "3.0000",
                  "armor": "30.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "不祥之刃",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Katarina.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big55000.jpg",
                "story": "果断坚决、心狠手辣，卡特琳娜是诺克萨斯的顶尖刺客。作为传奇将军杜·克卡奥的长女，她凭借出其不意的迅猛刺杀很快声名鹊起。强烈的野心曾经驱使她挑战重兵把守的暗杀目标，甚至不惜冒险让友军暴露在危险中。不过无论是怎样的任务，卡特琳娜都会毫不迟疑地在锯刃匕首的风暴中履行自己的使命。",
                "usageTips": "拾取匕首会显著降低【E瞬步】的冷却时间。",
                "battleTips": "卡特琳娜的技能对目标造成魔法伤害，尽管她购买增加攻击的物品，你也可使用魔抗应对。",
                "name": "卡特琳娜",
                "defense": "3",
                "magic": "9",
                "difficulty": "8",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Katarina_Passive.png",
                    "title": "贪婪",
                    "description": "每当一名在过去3秒被卡特琳娜所伤害的敌方英雄阵亡时，卡特琳娜的技能的冷却时间就会减少15秒。\n如果卡特琳娜拾起一把匕首，她会用它来斩击附近的所有敌人来造成(68-240 +75%额外AD +55%AP)魔法伤害。可对命中的敌人施加攻击特效。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KatarinaQ.png",
                    "title": "弹射之刃",
                    "description": "卡特琳娜投掷一把匕首，造成75/105/135/165/195(0.3*AP)魔法伤害给目标及附近的2个敌人。匕首随后会弹落到主要目标身后的地面上。\n匕首总会落在首个目标的远侧350码处，并且无论弹射多少次都会在相同的时间内落地。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KatarinaW.png",
                    "title": "伺机待发",
                    "description": "卡特琳娜扔出一把匕首至空中并获得50%/60%/70%/80%/90%/移动速度，该移速会在1.25秒里持续衰减。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KatarinaE.png",
                    "title": "瞬步",
                    "description": "卡特琳娜霎那间闪烁到目标友军、敌人、或匕首处。如果目标是敌人，那么卡特琳娜会造成15/30/45/60/75(+0.5*AD +0.25*AP)魔法伤害—如果是其它情况，那么她会对范围内距她最近的敌人造成伤害。施加攻击特效。\n拾取一把匕首将使[E瞬步]的冷却时间减少(78%).\n卡特琳娜可以闪烁到目标附近的任何位置。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KatarinaR.png",
                    "title": "死亡莲华",
                    "description": "卡特琳娜化身为一道剑刃飓风，快速朝最近的3名敌方英雄投掷匕首，每把匕首造成(25/37.5/50 +19%AP)魔法伤害和(22.237%额外AD)物理伤害，施加25%伤害的攻击特效并施加持续时间为3秒的60%重伤效果。\n在2.5秒里持续对每个敌方英雄造成的总伤害(375/562.5/750+285%AP)魔法伤害和(379.529%额外AD)物理伤害。"
                  }
                ],
                "data": {
                  "hp": "602.0000",
                  "hpperlevel": "94.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.7000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "58.0000",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.7400",
                  "armor": "27.8800",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "永恒梦魇",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Nocturne.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big56000.jpg",
                "story": "将一切有知觉的脑海中萦绕的噩梦提取出来，进而融合成一个恶魔般的实体，这就是被人称为魔腾的存在，它已成为一种纯粹邪恶的原始力量。它的外形捉摸不定，一团无面的黑影中睁着一双冰冷的眼睛，身体两侧是一对形状恐怖的刀刃。魔腾摆脱了精神领域的束缚，进入了梦醒后的凡尘世界，寻觅那些在真正的黑暗中疯长的恐惧，作为自己的食粮。",
                "usageTips": "在千钧一发之际施放终极技能是非常划算的，即使你不能用它来突进。",
                "battleTips": "当魔腾施放终极技能时，和友军站近一点儿，这样才能一起以众敌寡！",
                "name": "魔腾",
                "defense": "5",
                "magic": "2",
                "difficulty": "4",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Nocturne_UmbraBlades.png",
                    "title": "暗影之刃",
                    "description": "每过14秒，魔腾的下次普通攻击就会对附近敌人造成(120%AD)物理伤害，并且每命中一个目标就会回复自身15-40+(15%AP)生命值。\n魔腾的每次普通攻击会使这个技能的冷却时间减少1秒(对英雄和野怪时为3秒)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NocturneDuskbringer.png",
                    "title": "梦魇之径",
                    "description": "魔腾扔出一把暗影之刃，对命中的所有敌人造成65/110/155/200/245(+0.75*额外AD)物理伤害，并留下一条持续5秒的影径。被击中的敌方英雄也会留下一条影径。\n在影径上移动时，魔腾无视任何碰撞体积，并且会提高15%/20%/25%/30%/35%的移动速度和20/30/40/50/60的攻击力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NocturneShroudofDarkness.png",
                    "title": "黑暗庇护",
                    "description": "被动：魔腾获得30%/35%/40%/45%/50%攻击速度。\n主动：魔腾制造一个能够低挡掉下一个敌人技能的魔法护盾，持续1.5秒。\n如果此技能成功地抵挡住了敌人的技能，则该技能的被动效果提升至双倍，持续5秒\n[W黑暗庇护]将在[R鬼影重重]的飞行途中保持激活状态。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NocturneUnspeakableHorror.png",
                    "title": "无言恐惧",
                    "description": "被动：魔腾在朝着被恐惧的敌人移动时会获得显著的移动速度加成。\n主动：魔腾将梦魇植入目标的大脑之中，在2秒的持续时间里共造成80/125/170/215/260(+100%AP)魔法伤害。如果整个持续过程中，目标敌人都在魔腾的作用范围内的话，那么他将会被恐惧，持续1.25/1.5/1.75/2/2.25秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NocturneParanoia.png",
                    "title": "鬼影重重",
                    "description": "魔腾减少所有敌方英雄的视野范围，并移除敌方英雄的友军视野，持续6秒。\n激活时，魔腾能突进到附近任意一位敌方英雄的身边，同时对目标造成150/275/400(+120%额外AD)物理伤害\n距离：2500/3250/4000"
                  }
                ],
                "data": {
                  "hp": "585.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "275.0000",
                  "mpperlevel": "35.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "62.0000",
                  "attackdamageperlevel": "3.1000",
                  "attackspeed": "0.7210",
                  "attackspeedperlevel": "2.7000",
                  "armor": "38.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "0.7500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "离群之刺",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Akali.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big84000.jpg",
                "story": "无论是均衡教派还是暗影之拳的称号，都已被阿卡丽抛弃，如今的阿卡丽独来独往，随时可以成为她的人民所需要的夺命武器。虽然她牢牢铭记着她从宗师慎身上学来的一切，但她效忠的保护艾欧尼亚并铲除敌人，每次一条命。或许阿卡丽的出击悄然无声，但她传达的信息将响亮无比：不听命于任何人的刺客最为可怕。",
                "usageTips": "【W我流奥义！霞阵】即使在最危险的情境下，都可为她提供保护。利用那段持续时间来积蓄能量值然后发起一记快速袭击。",
                "battleTips": "阿卡丽的【Q我流奥义！寒影】非常强大，前提是要在最大距离和最大能量时使用。在她能量值较低时与她交战，即可最大化你的胜算。",
                "name": "阿卡丽",
                "defense": "3",
                "magic": "8",
                "difficulty": "7",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Akali_P.png",
                    "title": "我流忍法！潜龙印",
                    "description": "当阿卡丽用一个技能命中一名敌方英雄时，她会在其周围显形一个持续4秒的圆环，并在朝着圆环移动时获得40%/50%/60%/70%(1-18级)移动速度。穿过圆环会使阿卡丽的下次攻击拥有双倍距离、造成额外的39~180(1-18级)（+60%*额外AD +50%*AP）魔法伤害并回复10/15/20(1-18级)能量。\n\n一旦阿卡丽穿过圆环，她在朝着敌人移动时就会获得移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AkaliQ.png",
                    "title": "我流奥义！寒影",
                    "description": "阿卡丽呈弧状掷出苦无，造成30/55/80/105/130（+65%*AD +60%*AP）魔法伤害和持续0.5秒的50%减速。\n\n在此技能满级时，此技能对小兵和野怪造成162.5 （+81.25%*AD +75%*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AkaliW.png",
                    "title": "我流奥义！霞阵",
                    "description": "阿卡丽扔出一颗烟雾弹，来释放一团在5/5.5/6/6.5/7秒里不断扩散的烟雾并为她自身提供在2秒里不断衰减的30%/35%/40%/45%/50%移动速度。\n\n在处于烟雾之中时，阿卡丽会隐形。\n\n隐形单位只会被【防御塔】或【真实视野】显形。移动速度"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AkaliE.png",
                    "title": "我流奥义！隼舞",
                    "description": "阿卡丽后空翻并掷出一枚手里剑，造成50/85/120/155/190（+35%*AD +50%*AP）魔法伤害并标记命中的第一个敌人或烟雾弹。阿卡丽可以再次施放这个技能来突进至被标记的目标处，并造成50/85/120/155/190（+35%*AD +50%*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AkaliR.png",
                    "title": "我流秘奥义！表里杀缭乱",
                    "description": "阿卡丽跃过目标敌方英雄，对沿途的所有敌人造成125/225/325（+50%*额外AD ）物理伤害。\n\n阿卡丽可以在2.5秒后再次施放以施展一次带穿刺效果的突刺，造成75/145/215（+30%*AP）到225/435/645（+90%*AP）魔法伤害(基于已损失的生命值)。\n\n对低于30%生命值的目标造成最大伤害。"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "200.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "50.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "62.4000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.2000",
                  "armor": "23.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "37.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "刀锋之影",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Talon.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big91000.jpg",
                "story": "泰隆是黑暗中的匕首，是绝无仁慈的杀手，他出手前不会有任何警告，不会留任何退路，也不会引起任何警觉。泰隆在诺克萨斯的野蛮街巷中深深烙印了自己危险的名号，在这里他被迫为了生存而战斗、杀戮、偷窃。后来恶名昭彰的杜·克卡奥家族收养了他，现在他为帝国的指挥部贡献自己的夺命手段，暗杀敌人的领袖、军官和英雄，当然也包括任何得罪了最高长官们的诺克萨斯蠢货。",
                "usageTips": "暗影突袭是很强的逃生技能，但也可以用来对敌方团队发起攻势。",
                "battleTips": "泰隆的攻击都是物理伤害。尽早出护甲装备，来对抗他的爆发伤害。",
                "name": "泰隆",
                "defense": "3",
                "magic": "1",
                "difficulty": "7",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/TalonP.png",
                    "title": "刀锋之末",
                    "description": "泰隆的技能会对英雄和大型野怪施加一层效果，持续6秒(至多可叠3层)。\n\n当泰隆攻击一个带有3层效果的目标时，他会在2秒里持续造成共75~255(1-18级)（+200%*额外AD）额外物理伤害 。\n\n持续伤害效果对野怪造成120%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TalonQ.png",
                    "title": "诺克萨斯式外交",
                    "description": "泰隆跃向一个目标并造成65/90/115/140/165（+110%*额外AD）物理伤害。如果在近战距离内使用，那么这个技能会转而进行暴击，造成125%[65/90/115/140/165（+110%*额外AD）]物理伤害。\n\n如果这个技能击杀了目标，那么泰隆会回复10~70(1-18级)生命值并返还50%冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TalonW.png",
                    "title": "斩草除根",
                    "description": "泰隆扔出一排刀刃，造成45/60/75/90/105（+40%*额外AD）物理伤害。然后刀刃会折返，造成45/70/95/120/145（+70%*额外AD）额外物理伤害和持续1秒的40%/45%/50%/55%/60%减速。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TalonE.png",
                    "title": "刺客之道",
                    "description": "泰隆翻越相距最近的地形或建筑物。泰隆无法在160/135/110/85/60秒里重复翻越相同地区的地形。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TalonR.png",
                    "title": "暗影突袭",
                    "description": "泰隆散出一圈刀刃，造成90/135/180（+100%*额外AD）物理伤害，获得40%/55%/70%移动速度，并且变为隐形状态，持续2.5秒。当隐形状态结束时，刀刃会折返至泰隆处，再次造成90/135/180（+100%*额外AD）物理伤害。\n\n如果泰隆用一次攻击或【诺克萨斯式外交】取消掉隐形状态，那么刀刃会转而折返至目标处。\n\n隐形单位只会被【防御塔】或【真实视野】显形。"
                  }
                ],
                "data": {
                  "hp": "588.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.7500",
                  "mp": "377.2000",
                  "mpperlevel": "37.0000",
                  "mpregen": "7.6000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.1000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.9000",
                  "armor": "30.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "39.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "潮汐海灵",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Fizz.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big105000.jpg",
                "story": "菲兹是一名水陆两栖的约德尔人，生活在比尔吉沃特周围的群礁之间。他会经常把迷信的船长们抛进海中的什一税捡起来物归原主。不过即使是最粗鲁的水手也知道不要招惹他，因为这里流传着的各种教训，都是因为低估了这个小鬼。经常有人误会他的行为只是海洋精灵的任性举动，而事实上他可以号令来自深渊的巨型猛兽，而且不管是盟友还是敌人他都喜欢捉弄。",
                "usageTips": "菲兹在移动时能无视单位的碰撞体积，可以跨过小兵直取敌方英雄，并用你的【W海石三叉戟】的被动效果戳他——然后在几秒后开启主动效果继续戳。",
                "battleTips": "在菲兹使用他的强化攻击后的几秒里，菲兹的攻击会变得非常致命——在他的三叉戟带着闪光时，离他远一点",
                "name": "菲兹",
                "defense": "4",
                "magic": "7",
                "difficulty": "6",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Fizz_P.png",
                    "title": "伶俐斗士",
                    "description": "菲兹处于【幽灵】状态并且减少4（+1％*AP）所受伤害(来自所有来源)。\n\n幽灵状态的单位无视其它单位的碰撞体积。\n减免伤害的占比不能超过50%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FizzQ.png",
                    "title": "淘气打击",
                    "description": "菲兹冲刺并穿过一名敌人，同时造成（100%*AD）物理伤害加上10/25/40/55/70（+55％*AP）魔法伤害。\n\n这个技能会施加攻击特效。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FizzW.png",
                    "title": "海石三叉戟",
                    "description": "被动:菲兹的攻击会使他的敌人们流血，在3秒里持续造成共20/30/40/50/60（+40％*AP）魔法伤害。\n\n主动:菲兹的下次攻击造成额外的50/70/90/110/130+50%)魔法伤害。如果这次攻击将目标击杀，那么菲兹会返还20/28/36/44/52法力并使这个技能的冷却时间缩短至1秒。如果这次攻击未能造成击杀，那么菲兹的攻击会造成额外的10/15/20/25/30（+35％*AP）魔法伤害，持续5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FizzE.png",
                    "title": "古灵/精怪",
                    "description": "菲兹跳到他的三叉戟上，变为【不可被选取】状态0.75秒，随后对附近敌人造成70/120/170/220/270（+75％*AP）魔法伤害和持续2秒的40%/45%/50%/55%/60%减速。\n\n菲兹可以在【不可被选取】状态中再次施放这个技能，以再次位移，并提前结束该状态，在一个较小的区域内造成伤害，并且不会造成减速。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/FizzR.png",
                    "title": "巨鲨强袭",
                    "description": "菲兹掷出一只小鱼来吸附到命中的第一个敌方英雄身上。这个英雄会被施加真实视野和减速，减速幅度为40%/60%/80%，基于小鱼在吸附前的位移距离。\n\n在2秒后，一头巨鲨会从目标脚下出现，击飞带着小鱼的目标1秒，击退其它单位，并基于小鱼在吸附前的位移距离造成150/250/350（+80％*AP）或225/325/425（+100％*AP）或300/400/500（+120％*AP）魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "570.0000",
                  "hpperlevel": "98.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.7000",
                  "mp": "317.2000",
                  "mpperlevel": "37.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "58.0400",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "3.1000",
                  "armor": "22.4120",
                  "armorperlevel": "3.4000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "傲之追猎者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Rengar.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big107000.jpg",
                "story": "瓦斯塔亚的雷恩加尔是一名声名远扬又凶残无比的猎手。他的人生充满着追寻猎杀危险的生物的快感。他寻遍整个世界，只为寻找他能找到的最可怕的野兽，特别是寻找任何关于卡兹克的踪迹。这头来自虚空的野兽弄瞎了他的一只眼睛。雷恩加尔追寻着猎物，不为捕食也不为荣耀，只是为了纯粹的猎杀所带来的激烈美感。",
                "usageTips": "在团战和遭遇战里，用雷恩加尔的终极技能来找到并刺杀高优先级的敌方英雄。",
                "battleTips": "在雷恩加尔的资源槽全满时，他的技能会获得一次强化。尽量在他的资源槽较低时与他对抗。",
                "name": "雷恩加尔",
                "defense": "4",
                "magic": "2",
                "difficulty": "8",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Rengar_P.png",
                    "title": "无形掠食者",
                    "description": "在草丛时，雷恩加尔的普通攻击会让他跳跃至目标敌人身边。在0层残暴值时跳跃，会生成1层残暴值。\n\n在达到4残暴值时，他的下一个技能会变成强化版。施放强化版技能时，会使雷恩加尔的移动速度提升30%、40%、50%，持续2秒。\n\n击杀敌方英雄会在雷恩加尔的【骨齿项链】上新增战利品奖励。\n骨齿项链：\n雷恩加尔把他击杀所获的战利品留在他的骨齿项链上，如果一个独特的英雄在阵亡前的1.5秒里受到过来自雷恩加尔的伤害，那么雷恩加尔就会永久提升他的额外攻击力。\n1次击杀:1%额外攻击力\n2次击杀:4%额外攻击力\n3次击杀:9%额外攻击力\n4次击杀:16%额外攻击力\n5次击杀:25%额外攻击力\n\n所有残暴值都会在离开战斗状态后立刻失去(表现为【无形掠食者】的冷却时间)."
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RengarQ.png",
                    "title": "残忍无情",
                    "description": "雷恩加尔的下两次攻击获得40%攻击速度加成。第一次攻击造成30/60/90/120/150（+100%/105%/110%/115%/120%*AD）物理伤害。\n\n残暴效果:\n此技能的伤害提升至30~235(1-18级)（+140%*AD）并会为雷恩加尔提供50%~101%(1-18级)攻击速度，持续5秒。\n\n产生1点残暴值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RengarW.png",
                    "title": "战争咆哮",
                    "description": "对附近的敌人造成50/80/110/140/170（+80％*AP）魔法伤害并治疗雷恩加尔，治疗量相当于他在过去1.5秒内受到伤害的50%。\n\n残暴效果:\n造成50~220(1-18级 每级+10)（+80％*AP）魔法伤害，并移除身上的所有控制效果，此外还会继承常规效果。\n\n在治疗来自野怪攻击的伤害时，治疗效果会提升100%。\n\n产生1点残暴值\n\n【新增】现在，W技能对野怪造成65-130 (在1-18级时)额外伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RengarE.png",
                    "title": "套索打击",
                    "description": "扔出一个套索，对命中的第一个敌人造成55/100/145/190/ 235（+80％*额外AD）物理伤害，并将目标减速30%/45%/60%/75%/90%，持续1.75秒。这个减速效果会持续衰减。\n\n残暴效果:\n造成50~305(1-18级 每级+15)（+80％*额外AD）物理伤害并禁锢目标1.75秒。\n\n产生1点残暴值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RengarR.png",
                    "title": "狩猎律动",
                    "description": "雷恩加尔获得40%/50%/60%移动速度和在2500/3000/3500码内距他最近的敌方英雄的真实视野，持续12/16/20秒。\n\n在最初的2秒后，雷恩加尔进入伪装状态，并且即使不在草丛里时也可以跃向一名敌人。跃向最近的敌人会造成(50%*AD)物理伤害并使目标的护甲减少12/18/24，持续4秒。\n\n1600码内的敌方英雄及其附近的友军都可以感知到处于【狩猎律动】状态的雷恩加尔。被【狩猎律动】所显形的敌人只要呆在追踪范围内就会处于被显形状态。\n攻击或施放大部分技能都会让【狩猎律动】结束。\n潜行 - 伪装:这个角色不会被敌方单位所看见。附近的敌方英雄和敌方防御塔会使其显形。"
                  }
                ],
                "data": {
                  "hp": "585.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "4.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6670",
                  "attackspeedperlevel": "3.0000",
                  "armor": "34.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "虚空掠夺者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Khazix.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big121000.jpg",
                "story": "虚空在成长，虚空在不断适应。在众多虚空生物中，没有哪种比卡兹克更能体现这一特性。进化的动力让这种恐怖不断变异，本能地求生并弑杀强者。只要它遇到了障碍，就会进化出新的、更有效的方法反制并杀掉猎物。卡兹克最初只是个愚钝的野兽，然而它的智力和它的形态一样获得了发展。现在，这只生物会提前计划狩猎行动，甚至懂得在猎物心中制造最真实的恐惧。",
                "usageTips": "【无形恐惧】会在卡兹克不被敌方队伍看见时激活。可以利用草丛或者【R虚空来袭】重复激活它。别忘了用普通攻击来给敌方英雄施加【无形恐惧】的效果。",
                "battleTips": "【Q品尝恐惧】会对孤立无援的目标造成额外伤害。在友军小兵，英雄或防御塔附近战斗，以获取优势。",
                "name": "卡兹克",
                "defense": "4",
                "magic": "3",
                "difficulty": "6",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Khazix_P.png",
                    "title": "无形威胁",
                    "description": "附近陷入孤立无援状态的敌人会被标记出来。卡兹克的技能会与孤立无援的目标产生交互作用。\n当卡兹克没有被敌方队伍所看见时，他会获得无形威胁，使他的下次对敌方英雄发动的普通攻击造成额外的(14-116 +40%额外AD)魔法伤害，并使目标减速25%，持续2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KhazixQ.png",
                    "title": "品尝恐惧",
                    "description": "造成(60/85/110/135/160 +130%额外AD)物理伤害，如果目标处于孤立无援状态，伤害会提升110%。\n进化收割利爪：此技能和卡兹克的普攻的距离提升50。如果目标处于孤立无援状态，返还此技能45%冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KhazixW.png",
                    "title": "虚空突刺",
                    "description": "发射爆炸性的尖刺，对名字的敌人造成(85/115/145/175/205 +100%额外AD)物理伤害。如果卡兹克处在爆炸范围内，则会回复(60/85/110/135/160 +50%AP)生命值。\n进化刺鞘：虚空突刺会呈锥形发射三个尖刺并使被命中的目标减速60%，持续2秒。使被命中的敌方英雄显形2秒。孤立无援的目标所受的减速幅度会替换为90%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KhazixE.png",
                    "title": "跃击",
                    "description": "卡兹克跳向目标区域，在着落时造成(65/100/135/170/205 +20%额外AD)物理伤害。\n进化虫翼：跃击的距离会提升200，并且在获得击杀和助攻时重置冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KhazixR.png",
                    "title": "虚空来袭",
                    "description": "被动：提升虚空来袭的等级，会允许卡兹克进化他的技能。\n主动：卡兹克变为隐形状态，持续1.25秒并再次激活[无形威胁]。在隐形状态下时，他会获得40%移速加成并无视单位的碰撞体积。\n[虚空来袭]能在初段施放后的10秒内再次施放，最多可至2此。\n进化动态遮蔽：[虚空来袭]的使用次数提升至10秒内3次，并且它的潜行时长提升至2秒。\n潜行—隐形：卡兹克只能被附近的敌方防御塔或真实视野所显形。"
                  }
                ],
                "data": {
                  "hp": "572.8000",
                  "hpperlevel": "85.0000",
                  "hpregen": "7.5000",
                  "hpregenperlevel": "0.7500",
                  "mp": "327.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.5900",
                  "mpregenperlevel": "0.5000",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "3.1000",
                  "attackspeed": "0.6680",
                  "attackspeedperlevel": "2.7000",
                  "armor": "36.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "350.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "德玛西亚之翼",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Quinn.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big133000.jpg",
                "story": "奎因是德玛西亚的游骑兵精锐，经常深入敌国腹地执行危险的任务，她和她的传奇巨鹰华洛之间存在着一种牢不可破的纽带。很多时候，他们的对手死到临头也没意识到，自己面对着的这位德玛西亚英雄，并不是在孤军奋战。战斗中的奎因灵巧敏捷，十字弓例无虚发，而华洛则会从空中标记隐蔽的敌人。两者之间默契的配合造就了战场上一对致命凶狠的搭档。",
                "usageTips": "【E旋翔掠杀】非常厉害，但使用的时候要特别谨慎，因为敌人可以在奎因发起攻势时攻击她。如果你背靠墙壁的话，有时也可以用【E旋翔掠杀】来跨越地形。",
                "battleTips": "在被标记后，要与奎因保持距离，这样她就不能占到便宜了。",
                "name": "奎因",
                "defense": "4",
                "magic": "2",
                "difficulty": "5",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Quinn_Passive.png",
                    "title": "侵扰",
                    "description": "每过8秒，华洛就会将附近的一名敌人标记为易损状态，并使其暴露在视野内，持续4秒，奎因对这名目标的下次攻击会造成(10-95 +116%)物理伤害。\n[侵扰]的冷却时间可通过暴击几率来缩短。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QuinnQ.png",
                    "title": "炫目攻势",
                    "description": "华洛沿直线飞出，在命中第一个敌人时停下，将其标记为易损状态，并大幅缩减目标视野，持续1.75秒。随后华洛会对附近的所有敌人造成((20/45/70/95/120)+(80%/90%/100%/110%/120%AD)+(50%AP)物理伤害。\n如果首要目标不是英雄，则会无法攻击1.75秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QuinnW.png",
                    "title": "敏锐感知",
                    "description": "被动：攻击一名易损目标，将会使奎因攻击速度提升(20%/35%/50%/65%/80%)，移动速度提升(20%/25%/30%/35%/40%)，持续2秒。\n主动：华洛会使周围一大片区域的战争迷雾消散，持续2秒。 "
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QuinnE.png",
                    "title": "旋翔掠杀",
                    "description": "奎因冲向一名敌人，造成(40/70/100/130/160 +20%额外AD)并让华洛将这个敌人标记为易损状态。\n在接触到 目标的同时，奎因会向后空翻，进行短暂的位移并使目标减速(50%)(减速效果会在1.5秒里持续衰减)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QuinnR.png",
                    "title": "深入敌后",
                    "description": "奎因召唤华洛下来协助她，在2秒的引导后，她们会联成一体，获得(70%/100%/130%)总移动速度加成，并且可以再次施放这个技能时或作出进攻行为时施放[鹰翼天翔]。\n鹰翼天翔：奎因和华洛会对附近的敌人造成(40%AD)物理伤害并为敌方英雄施加易损标记。施放[鹰翼天翔]或作出其他进攻行为会结束[深入敌后]。\n承受来自非小兵单位的伤害会使移动速度加成被移除3秒。"
                  }
                ],
                "data": {
                  "hp": "532.8000",
                  "hpperlevel": "85.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "268.8000",
                  "mpperlevel": "35.0000",
                  "mpregen": "6.9720",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "59.0000",
                  "attackdamageperlevel": "2.4000",
                  "attackspeed": "0.6680",
                  "attackspeedperlevel": "3.1000",
                  "armor": "28.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "影流之镰",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kayn.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big141000.jpg",
                "story": "悉达·凯隐是修行暗影魔法的精英，他战斗的意义，是为了实现自己真正的命运——为了有朝一日能够率领影流教派，开创艾欧尼亚霸业的新世代。凯隐挥舞着活体暗裔武器拉亚斯特，毫不在意它给自己身体和思想带来的腐化。这样后果只可能有两种：要么，凯隐让这把武器屈服于自己的意志；要么，这副恶毒的刀刃将他彻底吞噬，为符文之地的毁灭奏响序曲。",
                "usageTips": "在选择你的形态之前，要看好己方队伍和敌方队伍的阵容构成。 记住，附近的敌人可以看到你在哪个墙体内。",
                "battleTips": "当凯隐在你附近的地形中时，你可以在他所在的地形边缘收到一个视觉警告。 在凯隐处于【掠影步】状态下对他造成伤害，可缩短该状态的持续时间，或对他施加硬控效果（晕眩，魅惑，击飞等）来立刻终结【掠影步】状态。",
                "name": "凯隐",
                "defense": "6",
                "magic": "1",
                "difficulty": "8",
                "attack": "10",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kayn_Passive_Primary.png",
                    "title": "暗裔魔镰",
                    "description": "凯隐使用的是一把上古神兵，并且一直都在与附在武器上的暗裔灵体拉亚斯特为了身体的控制权而战斗着。一人胜，则另一人死。\n暗裔灵体拉亚斯特会在与近战敌人作战时获得成长，而狩猎脆弱的远程敌人会增强暗影刺客凯隐的力量。\n暗裔化身：治疗自身，数额相当于一部分对对方英雄造成的技能伤害值。\n暗影刺客：在与敌方英雄作战的最初几秒里。造成额外的伤害。\n\n暗影刺客：在与英雄作战的最初3秒里，造成相当于对英雄14%已造成伤害值的魔法伤害。\n在脱离与英雄战斗8秒后，可再次变为可用状态。\n\n暗裔化身：提供治疗，治疗效果相当于对英雄的40%已造成技能伤害值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaynQ.png",
                    "title": "巨镰横扫",
                    "description": "凯隐向前冲刺并打击附近的敌人。冲刺和打击都会造成(65/85/105/125/145 +65%额外AD)物理伤害。\n暗裔化身：更换为对造成目标一定百分比最大生命值的伤害。\n对小兵和野怪造成40额外物理伤害。\n\n暗影刺客：凯隐向前冲刺并打击附近的敌人。冲刺和打击都会造成(75/95/115/135/155 +65%额外AD)物理伤害。\n对小兵和野怪造成40额外物理伤害。\n\n暗裔化身：拉亚斯特向前冲刺并打击附近的敌人。冲刺和打击的都会造成()伤害+目标164.最大生命值(对野怪的最大伤害：400).\n对小兵和野怪造成40额外物理伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaynW.png",
                    "title": "利刃纵贯",
                    "description": "凯隐造成(90/135/180/225/270 +130%额外AD)物理伤害，并使命中的敌人减速90%，减速效果会在接下来的1.5秒里持续衰减。\n暗影刺客：获得额外距离并可在[利刃纵贯]期间移动。\n暗裔化身：提供一次击飞效果。\n\n暗影刺客：凯隐创造一个活体暗影，在为他施放[利刃纵贯]。造成(90/135/180/225/270 +130%额外AD)物理伤害，并使命中的敌人减速90%，减速效果会在接下来的1.5秒里持续衰减。\n\n暗裔化身：拉亚斯特会造成(90/135/180/225/270 +130%额外AD)物理伤害。被命中的敌人会被击飞1秒并被减速90%。减速效果会在接下来的1.5秒里持续衰减。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaynE.png",
                    "title": "掠影步",
                    "description": "凯隐获得40%移动速度并可在地形中穿行7秒。当他第一次进入地形时，他可治疗自身(100/115/130/145/160 +40%额外AD)生命值。\n暗影刺客：冷却时间设置为8秒，获得更多移动速度和减速免疫效果。\n在与敌方英雄作战时的最大持续时长：1.5秒。\n受到定身效果，或是连续处于地形外的时间超过1.5秒，就会立刻终结[掠影步]。\n冷却时间：21\n\n暗影刺客：凯隐获得40%移动速度，减速免疫并可在地形中穿行(7/7.5/8/8.5/9秒)。冷却时间设置为8秒，当他第一次进入地形时，他可治疗自身(100/115/130/145/160 +40%额外AD)生命值。\n在与敌方英雄作战时的最大持续时长：1.5秒。\n受到定身效果，或是连续处于地形外的时间超过1.5秒，就会立刻终结[掠影步]。\n冷却时间：8\n\n暗裔化身：拉亚斯特获得40%移动速度并可在地形中穿行(7/7.5/8/8.5/9秒)秒。当他第一次进入地形时，他可治疗自身(100/115/130/145/160 +40%额外AD)生命值。\n在与敌方英雄作战时的最大持续时长：1.5秒。\n受到定身效果，或是连续处于地形外的时间超过1.5秒，就会立刻终结[掠影步]。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaynR.png",
                    "title": "裂舍影",
                    "description": "凯隐入侵一名他近期伤害过的敌方英雄，进入不可被选取的状态，最多持续2.5秒。\n再次激活以提前结束这个效果。当[裂舍影]结束时，目标会受到(150/250/350 +175%额外AD)物理伤害。\n暗影刺客：获得额外 距离(进入和离开凯隐锁定的目标都包括在内)并在离开时刷新[暗裔魔镰]。\n暗裔化身：造成百分比最大生命值伤害并为自己提供相当于目标一部分最大生命值的治疗效果\n\n暗影刺客：凯隐入侵一名他近期伤害过的敌方英雄，进入不可被选取的状态，最多持续2.5秒。\n再次激活以提前结束这个效果。当[裂舍影]结束时，[暗裔魔镰]的冷却时间将重置，目标会受到(150/250/350 +175%额外AD)物理伤害。并且凯隐会从目标处脱离并位移一段距离。\n\n暗裔化身：拉亚斯特入侵一名他近期伤害过的敌方英雄，进入不可被选取的状态，最多持续2.5秒。再次激活以提前结束这个效果。当[裂舍影]结束时目标会受到目标(10% +0.13*额外AD)最大生命值的物理伤害，并且拉亚斯特会治疗自身，数额相当于目标(7% +0.091*额外AD)最大生命值。"
                  }
                ],
                "data": {
                  "hp": "585.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.7500",
                  "mp": "410.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "11.5000",
                  "mpregenperlevel": "0.9500",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6690",
                  "attackspeedperlevel": "2.7000",
                  "armor": "38.0000",
                  "armorperlevel": "3.3000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "影流之主",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Zed.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big238000.jpg",
                "story": "彻底抛弃了仁慈与怜悯的劫，是影流教派的领袖。他创立影流的目标是将艾欧尼亚的魔法和传统武术用于实战，驱逐诺克萨斯侵略者。在战争中，绝望指引他开启了神秘的暗影形态。这是一种恶灵魔法，虽然强大，但同时非常危险且有腐化之力。劫已经完全掌握了这种禁忌之术，用它摧毁自己眼中的威胁，维护自己的国家，以及自己的教派。",
                "usageTips": "在你使用终极技能之前，要攒好能量和技能冷却，以最大化【R禁奥义！瞬狱影杀阵】的伤害输出。",
                "battleTips": "提升攻击力的装备能提升劫的整体输出，因此在对抗劫时，护甲的作用将非常明显。",
                "name": "劫",
                "defense": "2",
                "magic": "1",
                "difficulty": "7",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/shadowninja_P.png",
                    "title": "影忍法！灭魂劫",
                    "description": "劫在攻击名生命值低于50%的目标时，他会造成额外的(6%一10%)最大生命值的魔法伤害。这个特效在10秒内只能对相同目标生效一次。\n对野怪造成100%额外伤害。\n对抗野怪时最多造成(200 -500)伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZedQ.png",
                    "title": "影奥义！诸刃",
                    "description": "劫和他的影分身起将他们的手 里剑向前方掷出，每一枚手里剑都会对命中的第一个敌人造成(80/115/150/185/220 +100% 额外AD)物理伤害，并对之后的每个敌人造成(48 +60%额外AD)物理伤害。\n法力消耗：75能量"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZedW.png",
                    "title": "影奥义！分身",
                    "description": "被动：当劫和他的影分身用相同技能击中一名敌方英雄时，劫会回复(30/35/40/45/50)能量。\n主动：劫的影分身向前突进，并在一个地方维持5秒。再次施放这个技能会让劫与这个影分身互换位置。\n每次施放技能只会回复一次能量。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZedE.png",
                    "title": "影奥义！鬼斩",
                    "description": "劫和他的影分身进行斩击，对附近的敌人造成(70/90/110/130/150 +80%额外AD)物理伤害。\n每个被劫的斩击所命中的敌方英雄都会使[影奥义！分身]的冷却时间减少2秒。\n被影分身的斩击所击中的敌人，将会被减速(20%/25%/30%/35%/40%，持续1.5秒。被多个斩击同时击中的敌人，将会被减速30%，但不会受到额外伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZedR.png",
                    "title": "禁奥义！瞬狱影杀阵",
                    "description": "劫变得不可被选取，突进向一位敌方英雄，并将其标记。3秒后，标记会触发，对目标造成(100%AD)物理伤害，加上印记激活期间劫所造成的伤害总和的(25%/40%/55%)。\n突进会留下一个影分身在身后，持续6秒。再次施放此技能会使劫与这个影分身互换位置。\n不可被选取的单位不会受到敌人的攻击或技能的影响，除非在进入此状态前已被它\n们影响。"
                  }
                ],
                "data": {
                  "hp": "584.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.6500",
                  "mp": "200.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "50.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "3.4000",
                  "attackspeed": "0.6510",
                  "attackspeedperlevel": "3.3000",
                  "armor": "32.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "345.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "时间刺客",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ekko.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big245000.jpg",
                "story": "艾克是一名来自祖安不良街区的奇才。他可以操纵时间，从而让任何处境都变得对自己有利。通过使用他自己的发明——Z型驱动——他可以探索其他平行现实的分支并创造最完美的条件。尽管他酷爱这种自由，但只要他的朋友们遇到了威胁，他就会不顾一切地去保护他们。在旁观者眼里，艾克总是能初次尝试就完成不可能之举，屡试不爽。",
                "usageTips": "如果你能够在一名敌方英雄身上触发【Z型驱动共振】，那么即使要承担一定风险也非常值得一试。它所提供的移速加成会让你轻松脱身。",
                "battleTips": "艾克在终极技能没冷却的时候非常弱。要注意他留在身后的痕迹，来判断他的终极技能是否可用。",
                "name": "艾克",
                "defense": "3",
                "magic": "7",
                "difficulty": "8",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Ekko_P.png",
                    "title": "Z型驱动共振",
                    "description": "每当艾克对相同目标进行第三次攻击或伤害性技能时，都会造成(30-140 +80%AP)额外魔法伤害。如果目标是个英雄，那么艾克自己还会获得(50%一80%)移动速度，持续(2一3)秒。这个效果在5秒里无法重复作用于相同目标。\n这个效果对野怪造成250% (无上限)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EkkoQ.png",
                    "title": "时间卷曲器",
                    "description": "艾克投出一个设备，造成(60/75/90/105/120 +40%AP)魔法伤害。在命中一名英雄或到达它的距离终点后，它会扩展为一个领域来使其中的敌人减速(32%/39%/46%/53%/60%)。在它扩展后，艾克会将其召回，造成(40/65/90/115/140 +60%AP)魔法伤害。 "
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EkkoW.png",
                    "title": "时光交错",
                    "description": "被动：艾克对生命值低于30%的敌方英雄进行的攻击会造成(3% +3%AP)已损失生命值的魔法伤害。\n主动：在1.5秒的延迟后，艾克放出一颗时空球体来使其中的敌人减速40%。如果艾克进入了球体，那么他会将其引爆，造成2.25秒晕眩并获得(80/100/120/140/160 +150%AP)护盾值。\n额外攻击伤害最少造成15伤害，并且对小兵和野怪最多造成150伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EkkoE.png",
                    "title": "相位俯冲",
                    "description": "艾克突进并强化他的下次攻击，以获得额外距离、将他传送到他的目标处、并造成额外的(50/75/100/125/150 +40%AP)魔法伤害。\n艾克可以在突进期间使用他的任意其它技能。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EkkoR.png",
                    "title": "时空断裂",
                    "description": "艾克回到过去的时间点上，进入凝滞状态同时传送回他4秒前所在的位置，并在到达后对附近的所有敌人造成(150/300/450 +150%AP)魔法伤害。此外，艾克会治疗自身(100/150/200 +60%AP)生命值，且最近4秒里每损失1%生命值就会使这个治疗效果提升3%。\n凝滞状态中的单位无法移动或行动，并且免疫伤害和不可被选取。"
                  }
                ],
                "data": {
                  "hp": "585.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "9.0000",
                  "hpregenperlevel": "0.9000",
                  "mp": "280.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "58.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6880",
                  "attackspeedperlevel": "3.3000",
                  "armor": "32.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "元素女皇",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Qiyana.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big246000.jpg",
                "story": "在丛林都市以绪奥肯中，奇亚娜谋划着自己荣登育恩塔尔塔座的无情之路。作为父母的末位继承人，她以前无古人的元素魔法技艺，傲视所有挡在面前的人。这片大地服从着奇亚娜的每一道指令，她认为自己是以绪奥肯历史上最伟大的元素使——就凭这点，她理应执掌的不仅是一座城邦，而是一个帝国。",
                "usageTips": "要注意收集相应的元素",
                "battleTips": "小心不要被齐亚娜推到墙边",
                "name": "奇亚娜",
                "defense": "2",
                "magic": "4",
                "difficulty": "8",
                "attack": "0",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Qiyana_Passive.png",
                    "title": "凌人贵气",
                    "description": "你的第一次普攻或技能会对每个敌人造成(15—83 +55%额外AD +30%AP)额外物理伤害。\n这个效果对每个目标有25秒冷却时间，但在你每次施放方圆塑令[W]来获取不同的武器附魔时，此冷却时间会重置。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QiyanaQ.png",
                    "title": "元素之怒 / 以绪塔尔之锋",
                    "description": "元素之怒：引爆你的武器附魔来让其朝着目标方向爆发，并将其消耗掉。这个弹体会在它的路径终点处或在命中第一个敌人时爆炸，造成（60/85/110/135/160 +90%额外AD)物理伤害(对后续目标造成(45/ +67.5%额外)伤害) 以及:\n寒冰附魔：禁锢敌人0.5秒，随后将其减速20%，持续1秒。\n岩石附魔：对生命值低于50%的单位造成(36/ +54%额外AD)额外物理伤害。\n野性附魔：留下一条冠盖之径，来使你潜行并提供20%移动速度。在从路径处发起攻击、离开路径或在3.5秒之后，冠盖会消逝。弹体移动多远，路径就有多长。\n以绪塔尔之锋：如果你没有任一附魔，则会朝目标方向斩击，在小范围内造成150(60/85/110/135/160 +90%额外AD)物理伤害(对后续敌人的伤害为(45/ +67.5%额外AD))。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QiyanaW.png",
                    "title": "方圆塑令",
                    "description": "主动：选择河流，墙体，或草丛作为目标。驱役来自地形的元素能量并向其冲刺，来使其为你的武器附魔，并重置元素之怒[Q]的冷却时间。\n被动:在你的武器有附魔时，你获得(5%/10%/15%/20%/25%)攻击速度并且你的攻击和基础技能造成(8/16/24/32/40 +30%AP +10%额外AD)额外魔法伤害。此外，你在非战斗状态下以及在相应的地形类型附近时获得(5%/6.25%/7.5%/8.75%/10%)移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QiyanaE.png",
                    "title": "天纵之勇",
                    "description": "朝目标敌人冲刺一段固定的距离，掠过目标并造成(60/90/120/150/180 +70%额外AD)物理伤害。\n如果在这个技能的目标是一名英雄的时候施放元素之怒/以绪塔尔之锋[Q],那么奇亚娜将自动将其瞄向她的目标。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/QiyanaR.png",
                    "title": "惊才绝景",
                    "description": "放出一道无伤害的冲击波来击退敌人并在命中墙体时停下。整块墙体随后会爆炸以晕眩附近的敌人至多1秒，并对这些敌人造成相当于他们(100/170/240 +170%额外AD)+(10%)最大生命值的物理伤害。对距离爆炸源头越远的敌人造成的\n晕眩时长越短，最低为0.5秒。\n冲击波途经的任何河流，草丛也会在短暂延迟后爆炸，造成相同伤害和1秒晕眩。来自重叠区域的爆炸不会叠加。\n利用击退效果来将敌人推到墙体、河流或草丛处来使效果最大化。"
                  }
                ],
                "data": {
                  "hp": "590.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.6500",
                  "mp": "320.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "3.1000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1000",
                  "armor": "28.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "血港鬼影",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Pyke.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big555000.jpg",
                "story": "在比尔吉沃特的屠宰码头，颇有名气的鱼叉手派克葬身在一条巨大的琢珥鱼腹内……然而，他却回来了。他在家乡的阴街陋巷中徘徊着，用超自然的手段干脆残忍地解决那些鱼肉他人的恶棍——一座因捕猎怪物而自豪的城市如今发觉自己却成了狩猎的目标。",
                "usageTips": "用蓄力版的【透骨尖钉】命中一名敌人，总会将该敌人拖拽一段相同的距离。在近战距离使用它，可将敌人拽到你身后。",
                "battleTips": "派克可从所受的来自敌方英雄的伤害中回复显著的生命值，但前提是你看不见他！",
                "name": "派克",
                "defense": "3",
                "magic": "1",
                "difficulty": "7",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/PykePassive.png",
                    "title": "溺水之幸",
                    "description": "如果派克没有被敌方看见，那么他会急速回复25%他近期所受来自英雄的伤害，最多可至()。\n此外，派克将他所有的来自任意来源(装备、符文等)的额外最大生命值上限转化为攻击力。(1攻击力每14生命值)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PykeQ.png",
                    "title": "透骨尖钉",
                    "description": "秒放：派克向前戳刺，对命中的第一个敌人造成(85/135/185/235/285 +60%额外AD)物理伤害，优先选择英雄。\n蓄力：派克扔出他的鱼叉，对命中的第一个敌人造成(85/135/185/235/285 +60%额外AD)物理伤害并将其拉向他。该目标会被减速90%，持续1秒。\n如果未成功完成引导，那么这个技能会被取消，并返还50%法力消耗。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PykeW.png",
                    "title": "幽潭潜行",
                    "description": "派克获得伪装状态并获得(40% +)不断衰减的移动速度加成，持续5秒。\n潜行-伪装：派克隐藏于视野之外，前提是敌方英雄一直在他的侦测半径之外。攻击或施放技能都会结束伪装状态。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PykeE.png",
                    "title": "魅影浪洄",
                    "description": "派克进行冲刺，并在他身后留下一个溺毙魅影。在短暂的延迟后，魅影会回到派克身边，对沿途的敌方英雄进行打击，造成(1.25)秒晕眩效果。还会对命中的英雄造成(105/135/165/195/225 +100%额外AD)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PykeR.png",
                    "title": "涌泉之恨",
                    "description": "派克对X型范围内的所有敌人发起一次打击，闪烁到目标位置并处决(220-550)(+(80%额外AD))生命值以下的敌人。未到达处决条件的敌人会受到相当于50%处决阈值的物理伤害。\n当一名敌方英雄死在X范围内(无论伤害来源是什么)时，一名助攻的友军也会获得相当于全额击杀金币的额外分红，并且派克可以在20秒内再次施放[涌泉之恨]。"
                  }
                ],
                "data": {
                  "hp": "600.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "415.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "1.0000",
                  "attackdamage": "62.0000",
                  "attackdamageperlevel": "2.0000",
                  "attackspeed": "0.6670",
                  "attackspeedperlevel": "2.5000",
                  "armor": "45.0000",
                  "armorperlevel": "5.0000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.5000",
                  "movespeed": "330.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
            ]
          },
          {
            "name": "射手",
            "heroes": [
              {
                "title": "瘟疫之源",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Twitch.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big29000.jpg",
                "story": "虽然出身只是一只瘟疫老鼠，但图奇却靠热情成为了一位污秽的鉴赏家，他可不介意弄脏自己的爪子。他使用一把炼金动力十字弩瞄准皮城人的镀金心脏，发誓要让这些生活在上层都市的人们知道他们真实的自我究竟有多么肮脏。他总是鬼鬼祟祟蹑手蹑脚，不是在地沟区翻来翻去，就是在别的地方深挖人类的垃圾堆，寻找被丢弃的宝藏……或者是一块发霉的三明治。",
                "usageTips": "图奇是游戏中最高攻击速度的英雄之一，尽量购买像黑色切割者、或智慧末刃等带有攻击附带效果的道具。",
                "battleTips": "图奇很脆弱。你可以趁他伪装状态之外时抓住他倾泻火力。",
                "name": "图奇",
                "defense": "2",
                "magic": "3",
                "difficulty": "6",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Twitch_Passive.png",
                    "title": "死亡毒液",
                    "description": "图奇的普通攻击会使目标染毒，在6秒的持续时间里每秒造成1/2/3/4/5（+3%*AP）真实伤害，最多可叠加6层。\n\n满层伤害：6/12/18/24/30（+18%*AP ）每秒，共36/72/108/144/180（+108%*AP）。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TwitchHideInShadows.png",
                    "title": "埋伏",
                    "description": "图奇进入伪装状态并获得10%移动速度加成，持续10/11/12/13/14秒。这个加成会在附近没有任何一个敌方英雄看到图奇时翻至三倍。\n\n在离开埋伏状态后，他会获得40/45/50/55/60%攻击速度加成，持续5秒。当一名感染了死亡毒液的敌方英雄死亡时，【埋伏】的冷却时间会重置。\n\n潜行-伪装：当敌方英雄持续处于图奇的侦测半径之外时，图奇不会被敌方单位所看见。攻击或施放技能就会使伪装状态结束。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TwitchVenomCask.png",
                    "title": "剧毒之桶",
                    "description": "图奇扔出一个毒性之桶来添加一层死亡毒液给所有被桶砸中的敌人，并留下一团毒雾，存留3秒。\n一直呆在毒雾中的敌人会被减少30/35/40/45/50%[(+6%*AP )%]移动速度并且会每秒受到额外的一层死亡毒液。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TwitchExpunge.png",
                    "title": "毒性爆发",
                    "description": "对附近感染了死亡毒液的所有敌人造成20/30/40/50/60物理伤害，此外，每层死亡毒液额外造成15/20/25/30/35(+35%*额外攻击力）物理伤害和33.333%*AP魔法伤害。\n\n满层伤害：20/30/40/50/60（+6*每层死亡毒液额外造成的物理伤害)物理伤害和199.998%*AP魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TwitchFullAutomatic.png",
                    "title": "火力全开",
                    "description": "在5秒的持续时间里，图奇会获得300额外攻击距离和25/40/55额外攻击力。\n在此期间，他的普通攻击附带穿刺效果，对后续目标造成的伤害减少10%，最小伤害为60%。\n\n潜行-伪装：火力全开期间不会使伪装状态结束。"
                  }
                ],
                "data": {
                  "hp": "582.0000",
                  "hpperlevel": "84.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.6000",
                  "mp": "287.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "7.2560",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "59.0000",
                  "attackdamageperlevel": "3.1100",
                  "attackspeed": "0.6790",
                  "attackspeedperlevel": "3.3800",
                  "armor": "27.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "戏命师",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Jhin.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big202000.jpg",
                "story": "作为一名心思缜密的癫狂杀手，烬坚信谋杀是一门艺术。他曾在艾欧尼亚的监狱中服刑，但却因为执政议会里涌动着的暗流而得到释放，成为了权术斗争所利用的刺客。烬将手中的枪当成画笔，尽情地挥洒他所追求的残忍艺术，让受害者肝胆俱裂，令旁观者震悚难平。他在自己制作的阴森剧目里肆意取乐，让“恐怖”二字有了最合适不过的信使。",
                "usageTips": "W致命华彩】有着难以置信的超远射程。在加入一场战斗之前，先观察下前方是否有能被你禁锢的敌人。",
                "battleTips": "【W致命华彩】只会禁锢那些在前4秒被烬的普攻、陷阱、或友方英雄给命中的敌人。",
                "name": "烬",
                "defense": "2",
                "magic": "6",
                "difficulty": "6",
                "attack": "10",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Jhin_P.png",
                    "title": "低语",
                    "description": "烬的手枪有着已被固定的攻击速度，并且在发射4次后就得重新装弹。第4颗子弹总会产生暴击并造成相当于目标(15%—25%)已损失生命值的额外物理伤害。\n此外：\n— 烬获得(4%—44% +30%暴击 +25%额外攻速)额外攻击力\n— 烬的暴击造成的伤害减少14%，但会为他提供(10% +40%额外攻速)移动速度，持续2秒。\n攻击力会从暴击几率和攻击速度加成中获益。\n移动速度会从攻击速度加成中获益。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JhinQ.png",
                    "title": "曼舞手雷",
                    "description": "烬扔出一颗手雷，手雷会造成(45/70/95/120/145 +35/42.5/50/57.5/65%AD +60%AP)物理伤害，然后弹跳到附近的一个还未被此技能命中的目标上。\n手雷最多可命中(4)个目标。手雷每击杀一个单位，它的后续弹跳伤害就会提升35%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JhinW.png",
                    "title": "致命华彩",
                    "description": "烬进行一次远程射击，对命中的第一个英雄造成(50/85/120/155/190 +50%AD)物理伤害，并对沿途的其它敌人造成(37.5/ +37.5%AD)伤害。\n如果被[W致命华彩]命中的敌方英雄在前4秒受到过来自烬的友方英雄的伤害，那么这个英雄还会被禁锢(1/1.25/1.5/1.75/2)秒，并为烬提供低语的移动速度加成。\n踩中万众倾倒陷阱的敌人也能被禁锢。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JhinE.png",
                    "title": "万众倾倒",
                    "description": "被动：被烬击杀的英雄，将在其所在位置生成并弓|爆一个莲花陷阱。\n主动：烬放置个持续3分钟的隐形莲花陷阱，陷阱会在被敌人踩中时生成一个区域，使区域内的敌人减速35%。在2秒后，陷阱会爆炸，造成(20/80/140/200/260 +120%AD +100%AP)魔法伤害。\n这个技能有2层充能。\n莲花陷阱在命中时会对非英雄单位和近期被另一个莲花陷阱减速过的英雄造成65%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JhinR.png",
                    "title": "完美谢幕",
                    "description": "烬架好武器，固定在原地，并获得4颗超级子弹。每颗超级子弹会在命中第一个敌方英雄时对其造成(50/125/200 +20%AD)到(200/500/800 + 80%AD)物理伤害(伤害会基于目标的已损失生命值而提升)并使其减速80%，持续0.5秒。第四颗子弹会暴击，造成200%伤害。"
                  }
                ],
                "data": {
                  "hp": "556.0000",
                  "hpperlevel": "91.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.5500",
                  "mp": "300.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "59.0000",
                  "attackdamageperlevel": "4.7000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "0.0000",
                  "armor": "24.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "迅捷斥候",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Teemo.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big17000.jpg",
                "story": "不惧艰难险阻、不惧坎坷危途，提莫怀着无比的热情和欢欣的精神探索着整个世界。作为一个约德尔人，他对自己的道德观坚定不移，同时对班德尔斥候的信条感到自豪，有的时候，他的热忱甚至会让他无法看到，自己行为会在更大的意义上导致什么样的后果。虽然有的人认为这支斥候小队是否真正存在还有待商榷，但有一件事是肯定的：提莫的信念绝不容小觑。",
                "usageTips": "提莫的蘑菇用来打钱是很有效的。",
                "battleTips": "提莫的毒性射击技能对被击中再退后的玩家同样有效，在你准备好反击之前要和提莫保持距离。",
                "name": "提莫",
                "defense": "3",
                "magic": "7",
                "difficulty": "6",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Teemo_P.png",
                    "title": "游击队军备",
                    "description": "如果提莫在1.5秒内静止站立且没有受到伤害，就会进入隐形状态。在草丛中时，提莫可以在移动时触发并保持隐形状态。\n通过攻击或者移动(在草丛外)打破[游击队军备]状态后，提莫会获得(20%/40%/60%/80%)攻击速度加成，持续3秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BlindingDart.png",
                    "title": "致盲吹箭",
                    "description": "造成(80/125/170/215/260 +80%AP)魔法伤害和(1.5/1.75/2/2.25/2.5)秒致盲效果。\n提莫对小兵和野怪的致盲时长是基础致盲时常的200%。\n冷却时间：8\n法力消耗：70/75/80/85/90"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MoveQuick.png",
                    "title": "小莫快跑",
                    "description": "被动：提莫的移动速度提升(10%/14%/18%/22%/26%)，若在最近5秒内受到来自敌方英雄或防御塔的伤害，则此效果移除。\n主动：提莫急速奔跑，获得此技能被动部分的双倍加成，这个加速效果在被攻击时也不会移除。\n冷却时间：17\n法力消耗：40"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ToxicShot.png",
                    "title": "毒性射击",
                    "description": "提莫的普通攻击会使目标中毒，在命中瞬间造成(10/20/30/40/50 +30%AP)魔法伤害并在之后每秒造成(6/12/18/24/30 +10%AP)魔法伤害，持续4.5秒。\n[毒性射击]的毒会对野怪造成150%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TeemoRCast.png",
                    "title": "种蘑菇",
                    "description": "将一个已储存的蘑菇作为陷阱投掷出去，如果被敌人踩到，陷阱会爆炸，将剧毒散播到附近的敌人身上，减缓他们(30%/40%/50%)的移动速度，让他们显形并在4秒里持续造成共(200/325/450 +50%AP)魔法伤害\n陷阱持续5分钟，并且需要花费1秒来部署和潜行。提莫的背包只能同时装3/4/5个蘑菇。如果一共被投掷的陷阱命中了另一个陷阱，那么它会向前弹跳3/4/5个提莫身位的距离。\n弹跳距离：400/650/900\n冷却时间：35/30/25\n法力消耗：75"
                  }
                ],
                "data": {
                  "hp": "528.0000",
                  "hpperlevel": "90.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.6500",
                  "mp": "334.0000",
                  "mpperlevel": "20.0000",
                  "mpregen": "9.6000",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "54.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6900",
                  "attackspeedperlevel": "3.3800",
                  "armor": "24.3000",
                  "armorperlevel": "3.7500",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "暗夜猎手",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Vayne.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big67000.jpg",
                "story": "肖娜·薇恩是一位无情的德玛西亚怪物猎手。终其一生，她都在寻找杀害她全家的恶魔。她的手臂上装着十字弩，心中燃烧着熊熊的复仇怒火，从暗影中射出圣银弩箭的风暴，薇恩只有在杀死那些为黑暗魔法所控制的人和生物时，才能真正感到愉悦。",
                "usageTips": "闪避突袭有很多用法，但不能穿墙。",
                "battleTips": "薇恩很脆弱——注意压制她，使其不得不谨慎作战。",
                "name": "薇恩",
                "defense": "1",
                "magic": "1",
                "difficulty": "8",
                "attack": "10",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Vayne_NightHunter.png",
                    "title": "暗夜猎手",
                    "description": "薇恩毫不留情的猎杀世间邪恶，向附近敌方英雄移动时增加30点移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VayneTumble.png",
                    "title": "闪避突袭",
                    "description": "薇恩翻滚一小段距离,在6秒内的下一次普通攻击会造成额外物理伤害。额外伤害值等同于她总攻击力的60/65/70/75/80%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VayneSilveredBolts.png",
                    "title": "圣银弩箭",
                    "description": "对同一目标每攻击或施法2次，第3次攻击或施法就会造成目标最大生命值4/6.5/9/11.5/14%的真实伤害。(对野怪最多造成200伤害)，最小伤害:50/65/80/95/110"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VayneCondemn.png",
                    "title": "恶魔审判",
                    "description": "薇恩发射巨箭，造成50/85/120/155/190物理伤害并将敌人击退。如果目标被击退至墙上或者地形边缘,则会受到额外150%的物理伤害，并被晕眩1.5秒。(AD加成:0.5)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VayneInquisition.png",
                    "title": "终极时刻",
                    "description": "薇恩获得35/50/65点额外攻击力,持续8/10/12秒，在此技能激活时,薇恩在进行闪避突袭的同时会让自己隐形1秒，并且暗夜猎手的加速效果提高至90.如果有在3秒内被薇恩攻击过的敌方英雄阵亡，那么终极时刻的持续时间会延长4秒。当终极时刻激活时,闪避突袭的冷却时间降低50%"
                  }
                ],
                "data": {
                  "hp": "515.0000",
                  "hpperlevel": "89.0000",
                  "hpregen": "3.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "231.8000",
                  "mpperlevel": "35.0000",
                  "mpregen": "6.9720",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "2.3600",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "3.3000",
                  "armor": "23.0000",
                  "armorperlevel": "3.4000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "麦林炮手",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Tristana.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big18000.jpg",
                "story": "许多约德尔人都将自己的精力花在探索发现、发明创造或者搞恶作剧上，然而崔丝塔娜则一心向往伟大勇者们的冒险故事。她听闻了太多关于符文之地的事，关于不同的势力、关于庞大的战争。崔丝塔娜相信自己也有资格成为传奇。她首次踏进了这个世界，拿着她信赖的加农炮“轰隆”，用坚定的勇气和乐观精神跳进战场。",
                "usageTips": "在你对一个敌人叠满【爆炸火花】的层数后，再用【火箭跳跃】跳到这个敌人身上，就能对这个敌人造成爆发性的伤害。",
                "battleTips": "如果你看见崔丝塔娜在交战中激活急速射击，那么晕眩她，并且后退，直到该技能消散。",
                "name": "崔丝塔娜",
                "defense": "3",
                "magic": "5",
                "difficulty": "4",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Tristana_Passive.png",
                    "title": "瞄准",
                    "description": "崔丝塔娜的攻击距离和[爆炸火花]及[毁灭射击]的施法距离增加(0-136)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TristanaQ.png",
                    "title": "急速射击",
                    "description": "主动：提升崔丝塔娜50/65/80/95/110%攻击速度，持续7秒。\n冷却时间：20/19/18/17/16"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TristanaW.png",
                    "title": "火箭跳跃",
                    "description": "崔丝塔娜朝地面开火，将自己推送至目标区域，在落地时造成(95/145/195/245/295 +50%AP)魔法伤害并减速附近敌人60%，持续(1/1.5/2/2.5/3秒)。\n获得击杀或助攻的同时，或者在英雄身上叠满[爆炸火花]层数并引爆时，[火箭跳跃]会重置冷却时间。\n冷却时间：22/20/18/16/14\n法力消耗：60"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TristanaE.png",
                    "title": "爆炸火花",
                    "description": "主动：在目标敌人或防御塔上放置一个4秒后爆炸的炸弹，造成(70/80/90/100/110 +50%额外AD +50%AP)物理伤害。崔丝塔娜的攻击和技能会使电火花的伤害提升30%，最多可提升(154/176/198/222/242+110%额外AD +110AP)。\n在叠满4层时，炸弹会立刻爆炸。如果用在防御塔上，那么爆炸半径会翻倍。\n冷却时间：16/15.5/15/14.5/14\n法力消耗：50/55/60/65/70"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TristanaR.png",
                    "title": "毁灭射击",
                    "description": "崔丝塔娜填装巨型加农炮弹攻击目标单位，这个炮弹造成造成(300/400/500 +100%AP)魔法伤害并将目标及目标周围的单位击退600/800/1000码。\n冷却时间：120/110/100\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "559.0000",
                  "hpperlevel": "88.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.6500",
                  "mp": "250.0000",
                  "mpperlevel": "32.0000",
                  "mpregen": "7.2000",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6560",
                  "attackspeedperlevel": "1.5000",
                  "armor": "26.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "战争女神",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Sivir.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big15000.jpg",
                "story": "希维尔是著名的宝藏猎人和雇佣兵队长，在恕瑞玛沙漠中进行频繁的契约交易。她的兵器是一柄颇具传奇色彩的十字刃，她曾赢得过无数次战斗，虽然她本人报价不菲，但却深得雇主青睐。她有着著名的无畏决心和无尽的野心，并且以自己的事业为傲——只要赏金够高，她就能从凶险的恕瑞玛古墓中寻回深埋于地下的宝藏。不过随着好几股远古的力量搅动着恕瑞玛的根骨，希维尔突然发现自己被裹挟着、拉扯着，卷入了截然不同的命运。",
                "usageTips": "希维尔的回旋之刃在到达最远射程后会飞回她手中，因此在扔出后你可以调整位置使它更容易命中敌人。",
                "battleTips": "回旋之刃会消耗大量法力值，躲避该技能会让希维尔受挫。如果她在扔出去时击中了你，在其返回时你要避免再次被击中。",
                "name": "希维尔",
                "defense": "3",
                "magic": "1",
                "difficulty": "4",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Sivir_Passive.png",
                    "title": "敏锐疾行",
                    "description": "希维尔在用攻击或技能对一名敌方英雄造成伤害时，会获得(35-55)移动速度，持续2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SivirQ.png",
                    "title": "回旋之刃",
                    "description": "希维尔挥出她的十字刃，对命中的所有敌人造成(35/50/65/80/95 +70%/85%/100%/115%/130%AD +50%AP)物理伤害，然后折返，再次造成伤害。这个技能每次命中非英雄单位时，对每个后续目标的伤害就会减少15%，减到40%为止。\n冷却时间：7\n法力消耗：70"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SivirW.png",
                    "title": "弹射",
                    "description": "希维尔的下3次攻击会弹射到周围目标上，对初始目标造成(100%)物理伤害，并对每个后续目标造成(30%/40%/50%/60%/70%)物理伤害\n如果某次攻击对它的首个目标产生了暴击，那么它也会对后续目标产生暴击。\n单次[弹射]不会重复命中相同目标。\n冷却时间：75\n法力消耗：12/10.5/9/7.5/6"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SivirE.png",
                    "title": "法术护盾",
                    "description": "希维尔制造一层持续1.5秒的法术屏障，来格挡一次即将到来的敌方技能。如果希维尔成功格挡了一次技能，那么就会回复110/120/130/140/150法力值。\n这个护盾可以格挡英雄技能和装备技能\n冷却时间：22/19/16/13/10"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SivirR.png",
                    "title": "狩猎",
                    "description": "被动：希维尔在弹射激活时，会获得30%/45%/60%攻击速度。\n主动：希维尔使友军振奋8秒，为附近的所有友军提供40%/50%/60%移动速度，在2/3/4秒里持续衰减。\n移动速度加成会在2/3/4秒后衰减至20%/25%/30%。\n冷却时间：120/100/80\n法力消耗：80"
                  }
                ],
                "data": {
                  "hp": "532.0000",
                  "hpperlevel": "88.0000",
                  "hpregen": "3.2500",
                  "hpregenperlevel": "0.5500",
                  "mp": "284.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.9000",
                  "attackdamage": "63.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.6000",
                  "armor": "26.0000",
                  "armorperlevel": "3.2500",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "赏金猎人",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/MissFortune.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big21000.jpg",
                "story": "以美貌闻名，但却以无情立命的莎拉是一位比尔吉沃特的船长，她在这座港镇的强硬犯罪集团中塑造了不容轻视的形象。在她还是个孩子的时候，亲眼目睹了海盗之王普朗克谋杀了自己的家人。多年以后她残忍地报仇雪恨，把他和他的旗舰连人带船一同炸飞。所有低估她的人都会发现，自己面对的是一个极具欺骗性的狡黠对手，还有可能要处理肚子里的一两颗子弹。",
                "usageTips": "在【大步流星】尚未冷却时，要尽量利用【厄运的眷顾】来最大化攻击速度加成的持续时间。",
                "battleTips": "厄运小姐的加速会被我方的伤害移除",
                "name": "厄运小姐",
                "defense": "2",
                "magic": "5",
                "difficulty": "1",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/MissFortune_W.png",
                    "title": "厄运的眷顾",
                    "description": "在攻击一个新目标时，厄运小姐的攻击会造成额外的(50%/60%/70%/80%/90%/100%/AD)物理伤害。\n对小兵造成(25%/30%/35%/40%/45%/50%AD)伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MissFortuneRicochetShot.png",
                    "title": "一箭双雕",
                    "description": "厄运小姐发射一次弹跳射击，对一个敌人及其身后的另一个敌人造成(20/40/60/80/100 +100%AD+35%AP)物理伤害。\n第二段射击可以暴击。如果第一段射击击杀了它的目标，那么第二段射击就必定暴击。\n两段射击都可以施加攻击特效。\n冷却时间：7/6/5/4/3\n法力消耗：43/46/49/52/55"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MissFortuneViciousStrikes.png",
                    "title": "大步流星",
                    "description": "被动：如果5秒内没有受到伤害，厄运小姐就会获得25移动速度。再过5秒后，这个加成会提升至(55/65/75/85/95)。\n主动：获得被动效果的全额移动速度加成并提供(40%/55%/70%/85%/100%)攻击速度加成，持续4秒。\n厄运的青睐会使这个技能的冷却时间缩短2秒。\n冷却时间：12\n法力消耗：30"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MissFortuneScattershot.png",
                    "title": "枪林弹雨",
                    "description": "厄运小姐发射一阵弹雨，来显形一个区域，在2秒里持续造成共(80/115/150/185/220 +80%AP)魔法伤害,并使敌人减速(40%/45%/50%/55%/60%)。\n冷却时间：18/16/14/12/10\n法力消耗：80"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MissFortuneBulletTime.png",
                    "title": "弹幕时间",
                    "description": "厄运小姐向前方锥形地带引导一阵弹幕，在3秒里持续发射共12/14/16波，每波弹幕造成(75%AD +20%AP)物理伤害(总共(900%/1050%/1200%AD +240%/280%/320%AP)物理伤害)。\n这个技能每一波都可以暴击以造成(120%+(8%暴击加成))伤害。\n冷却时间：120/110/100\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "541.0000",
                  "hpperlevel": "91.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.6500",
                  "mp": "325.8400",
                  "mpperlevel": "35.0000",
                  "mpregen": "8.0420",
                  "mpregenperlevel": "0.6500",
                  "attackdamage": "50.0000",
                  "attackdamageperlevel": "2.7000",
                  "attackspeed": "0.6560",
                  "attackspeedperlevel": "2.2500",
                  "armor": "28.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "寒冰射手",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ashe.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big22000.jpg",
                "story": "作为阿瓦罗萨部族的战母，寒冰血脉的艾希率领着北方人数最多的部落。她克己、智慧、忠于理想，但并不适应自己作为领袖的角色，艾希与自己血脉中蕴藏的先祖魔法相通，挽起了臻冰打造的长弓。她的族人相信她就是神话中的女英雄阿瓦罗萨的转世，在人们的追随下，艾希希望夺回那些属于部族的古代领土，从而让弗雷尔卓德再次实现统一。",
                "usageTips": "尽量预判敌人行进的方向发射魔法水晶箭 ，这样可以加大射击命中率。",
                "battleTips": "如果艾希有段时间没有施放她的【魔法水晶箭】，那么你在地图上移动时就要特别小心。",
                "name": "艾希",
                "defense": "3",
                "magic": "2",
                "difficulty": "4",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Ashe_P.png",
                    "title": "冰霜射击",
                    "description": "艾希的技能和攻击会使目标减速(20%-30%)，持续2秒，如果目标已被这个技能影响，那么艾希的攻击会造成(110% +75%暴击)伤害。\n艾希的暴击不会造成额外伤害，但会将[冰霜射击]的减速提升至(40%-60%)，在持续时间内不断衰减。\n额外伤害受益于暴击几率和暴击伤害加成。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AsheQ.png",
                    "title": "射手的专注",
                    "description": "被动：艾希的攻击提供一层持续4秒的效果。在4层时，她就可以激活这个技能。\n主动：艾希获得(20%/25%/30%/35%/40%)攻击速度并且她的攻击转而造成(105%/110%/115%/120%/125%AD)伤害，持续4秒。\n每次强化版攻击由5次小型打击组成。每次攻击只会施加1次攻击特效。\n冷却时间：0\n法力消耗：50"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Volley.png",
                    "title": "万箭齐发",
                    "description": "艾希射出一排共7支箭矢，每支箭矢造成(20/35/50/65/80 +100%AD)物理伤害。\n由于冰霜射击的存在，在命中敌方英雄时会视为暴击。\n冷却时间：14/11.5/9/6.5/4\n法力消耗：70"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/AsheSpiritOfTheHawk.png",
                    "title": "鹰击长空",
                    "description": "艾希派出一只鹰，来提供地图上任一位置的视野5秒。它还会在飞行时显形附近的区域。\n这个技能可持有2层充能(90/80/70/60/50秒充能时间)。\n法力消耗：无消耗"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EnchantedCrystalArrow.png",
                    "title": "魔法水晶箭",
                    "description": "艾希射出魔法水晶箭，晕眩命中的第一个敌方英雄，并造成(200/400/600 +100%AP)魔法伤害。晕眩时长取决于飞行距离，至多3.5秒。目标周围的敌人会受到一半伤害。\n最小晕眩时长为1秒。\n冷却时间：100/90/80\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "539.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "3.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "280.0000",
                  "mpperlevel": "32.0000",
                  "mpregen": "6.9720",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "2.9600",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "3.3300",
                  "armor": "26.0000",
                  "armorperlevel": "3.4000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "600.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "英勇投弹手",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Corki.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big42000.jpg",
                "story": "约德尔飞行员库奇最爱两件事物：一是飞行，二是自己英俊的小胡子，排名不分先后。离开班德尔城以后，他在皮尔特沃夫安家，从此爱上了这里奇异壮观的各式机器。他决定投身于飞行装置的开发事业，带领一群老练飞行员组成了一只空中防御力量，得名“尖啸之蛇”。临危不乱的库奇在他第二故乡的空域戒备巡逻，而他还从没见过什么问题是一轮导弹齐射不能解决的。",
                "usageTips": "磷光炸弹可以用来暴露躲在附近草丛里的敌人。",
                "battleTips": "小心库奇的磷光炸弹的溅射伤害，就算你躲在小兵后面也会受伤。",
                "name": "库奇",
                "defense": "3",
                "magic": "6",
                "difficulty": "6",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Corki_RapidReload.png",
                    "title": "海克斯科技军备",
                    "description": "库奇的普攻伤害会有80%转化为魔法伤害。\n在游戏时间过了最初的8分钟后，[弹药包]会出现在泉水附近。一旦被拾起，[弹药包]会持续最多60秒，并在非战斗状态下提供40%移动速度，并将[W瓦尔基里俯冲]升级为[W特别快递]。\n[弹药包]会在被库奇投出后开始装填，装填时间：4分钟"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PhosphorusBomb.png",
                    "title": "磷光炸弹",
                    "description": "库奇轰出一枚炸弹，对目标区域内的敌人们造成75/120/165/210/255(+0.7*AD)(+0.5*AP)魔法伤害。此外，爆炸会显性该区域和被爆炸命中的地方英雄6秒(不会使已潜行的单位显性)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CarpetBomb.png",
                    "title": "瓦尔基里俯冲",
                    "description": "主动：库奇进行一次短程飞行，留下一条烈焰之径，对留在其中的敌人每秒造成60/90/120/150/180(+40%AP)魔法伤害。\n特别快递：库奇进行一次长程飞行，同时丢下炸弹留下一条持续5秒的烈焰之径。烈焰之径会使留在其中的敌人减速90%并灼烧他们，每秒造成(30-100+200%额外AD +24%AP)魔法伤害。\n被库奇直接命中的敌人会被震开，并被减速和灼烧至少2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GGun.png",
                    "title": "格林机枪",
                    "description": "库奇使用格林机枪不断扫射前方的目标，持续4秒，最多造成120/170/220/370/320(+160%额外AD)伤害，并最多减少8/11/14/17/20护甲和魔法抗性。\n格林机枪的伤害有50%为物理伤害，有50%为魔法伤害。\n防御削减效果会在最后一次被格林机枪击中的2秒后消失。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/MissileBarrage.png",
                    "title": "火箭轰击",
                    "description": "主动：库奇朝目标地点发射一枚导弹，命中第一个敌人后会爆炸，对目标及目标周围的所有敌人造成90/125/160(15%/45%/75%AD + 20%AP)。\n库奇最多可挂载7枚导弹。每第3枚导弹将是超级导弹，造成200%伤害(180/250/320/+ 30%/90%/150%AD + 40%AP)。\n冷却时间：12/11/10秒充能，2秒释放间隔。"
                  }
                ],
                "data": {
                  "hp": "518.0000",
                  "hpperlevel": "87.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "350.1600",
                  "mpperlevel": "34.0000",
                  "mpregen": "7.4240",
                  "mpregenperlevel": "0.5500",
                  "attackdamage": "55.0000",
                  "attackdamageperlevel": "2.5000",
                  "attackspeed": "0.6380",
                  "attackspeedperlevel": "2.3000",
                  "armor": "28.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "皮城女警",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Caitlyn.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big51000.jpg",
                "story": "凯特琳被誉为皮尔特沃夫最顶尖的和平卫士，同时也是让这座城市真正摆脱地下隐秘犯罪的最有希望的人选。她经常和蔚联手执行任务，以冷静和沉着弥补自己搭档的鲁莽天性。虽然她的海克斯科技步枪独一无二，但凯特琳最强大的武器其实是她过人的智谋，总是能设下天罗地网，迎接任何傻到敢在进步之城作案的不法之徒。",
                "usageTips": "充分利用约德尔诱捕器，先发制人，确保在战斗过程中随时可以放置新的诱捕器。",
                "battleTips": "如果凯特琳用和平使者骚扰你，记得躲在自己的小兵后面（这样受到的伤害会更少）。",
                "name": "凯特琳",
                "defense": "2",
                "magic": "2",
                "difficulty": "6",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Caitlyn_Headshot.png",
                    "title": "爆头",
                    "description": "每攻击6次，凯特琳就会发射一次爆头。草丛中的攻击在积攒爆头次数时会算为(2)次。\n爆头造成额外的(50%AD)物理伤害。爆头对被约德尔诱捕器命中的敌人们拥有双倍距离并进一步造成(60+40%额外AD)物理伤害。爆头对被90口径绳网命中的敌人拥有双倍距离。\n爆头的伤害随等级、暴击几率和暴击伤害而增长。\n爆头对非英雄单位造成额外的(25%AD)伤害。\n对踩中陷阱的目标的额外伤害随约德尔诱捕器的技能等级而增长。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CaitlynPiltoverPeacemaker.png",
                    "title": "和平使者",
                    "description": "凯特加速转动她步枪来射出一颗穿刺弹，造成50/90/130/170/210(+130%/140%/150%/160%/170%AD)物理伤害。在子弹命中第一个单位后，它会绽开为一个更宽的弹体，造成30/54/78/102/126(+78%/84%/90%/96%/102%AD)物理伤害。被90口径绳网显形的敌人总会受到全额伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CaitlynYordleTrap.png",
                    "title": "约德尔诱捕器",
                    "description": "凯特琳设置一个陷阱来禁锢踩中它的第一个英雄1.5秒并提供该英雄的真实视野，持续3秒。陷阱持续30/35/40/45/50秒，并且同一时间至多可存在3/3/4/4/5个已激活的陷阱。这个技能有2层充能(30/24/19/15/12秒充能时间)。\n对陷阱中的目标的爆头伤害加成60/105/150/195/240\n额外攻击力收益40%/55%/70%/85%/100%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CaitlynEntrapment.png",
                    "title": "90口径绳网",
                    "description": "凯特琳发射一张绳网，并将她向后推送，绳网对命中的第一个目标造成持续1秒的50%减速和70/110/150/190/230(+80%AP)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/CaitlynAceintheHole.png",
                    "title": "让子弹飞",
                    "description": "凯特琳花费一段时间来引导并精心准备完美一击，然后她会开火，造成300/525/750(2.0*额外AD)物理伤害，但其他敌方英雄能拦截子弹。这个技能在引导期间会提供目标的真实视野。"
                  }
                ],
                "data": {
                  "hp": "481.0000",
                  "hpperlevel": "91.0000",
                  "hpregen": "3.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "313.7000",
                  "mpperlevel": "35.0000",
                  "mpregen": "7.4000",
                  "mpregenperlevel": "0.5500",
                  "attackdamage": "62.0000",
                  "attackdamageperlevel": "2.8800",
                  "attackspeed": "0.6810",
                  "attackspeedperlevel": "4.0000",
                  "armor": "28.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "650.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "探险家",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Ezreal.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big81000.jpg",
                "story": "神采奕奕的冒险家伊泽瑞尔拥有自己不知道的魔法天赋，他搜刮失落已久的古墓，触碰古老的诅咒，还举重若轻地挑战常人不可能完成的极限。他的勇气和壮举无边无际，总是喜欢随机应变地解决任何情况，一定程度上依赖他的小聪明，但更主要是依赖他神秘的恕瑞玛护手，在他的操控下释放出破坏性的奥术爆弹。有一件事可以肯定——只要伊泽瑞尔出现，那么麻烦一定接踵而至。或是还没走远。范围大概是随时随地。",
                "usageTips": "使用奥术跃迁来使你的其他技能更容易命中。",
                "battleTips": "伊泽瑞尔很脆弱，你可以优先攻击他。",
                "name": "伊泽瑞尔",
                "defense": "2",
                "magic": "6",
                "difficulty": "7",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Ezreal_RisingSpellForce.png",
                    "title": "咒能高涨",
                    "description": "伊泽瑞尔的技能在命中时会为他提供10%攻击速度，持续6秒，至多可叠加5层。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EzrealQ.png",
                    "title": "秘术射击",
                    "description": "伊泽瑞尔发射一支能量箭，对命中的第一个敌人造成20/45/70/95/120（+120%*AD +15%*AP）物理伤害并使他各个技能的冷却时间缩短1.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EzrealW.png",
                    "title": "精华跃动",
                    "description": "伊泽瑞尔发射一团魔法球体，魔法球体会附着在命中的第一个敌方英雄、建筑物或史诗级野怪上，持续4秒。如果伊泽瑞尔用技能或攻击命中该目标，就会将球体引爆，造成80/135/190/245/300（+60%*额外AD +70%/75%/80%/85%/90%*AP）魔法伤害。用技能引爆时会返还该技能的法力消耗外加60法力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EzrealE.png",
                    "title": "奥术跃迁",
                    "description": "伊泽瑞尔传送然后向最近的敌人发射一支魔法箭，造成80/130/180/230/280（+50%*额外AD +75%*AP）魔法伤害。魔法箭会优先选择受【精华跃动】影响的目标。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EzrealR.png",
                    "title": "精准弹幕",
                    "description": "伊泽瑞尔发射一道大型能量圆弧，造成350/500/650（+100%*额外AD +90%*AP）魔法伤害。圆弧对小兵和非史诗级野怪造成50%伤害。"
                  }
                ],
                "data": {
                  "hp": "500.0000",
                  "hpperlevel": "86.0000",
                  "hpregen": "4.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "375.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "8.5000",
                  "mpregenperlevel": "0.6500",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "2.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.5000",
                  "armor": "22.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "深渊巨口",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/KogMaw.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big96000.jpg",
                "story": "从艾卡西亚废墟深处的腐败裂口中，克格莫喷着进食后的胀气走入了符文之地，它是一只好奇的生物，然而身上散发着腐烂的恶臭，血盆大口里淌着腐蚀性的粘液。这只特别的虚空生物需要用牙齿和口水去理解周围的事物。虽然克格莫并非本性邪恶，但它的天真无知同样很危险，因为它的无知往往预示着一场疯狂的暴食——并不是为了填饱肚子，而是为了满足它无尽的好奇。",
                "usageTips": "克格莫使用【W生化弹幕】时，其射程比大多数英雄都远。",
                "battleTips": "克格莫没有好的逃生技能，因此他是一个好的攻击/gank对象。",
                "name": "克格莫",
                "defense": "2",
                "magic": "5",
                "difficulty": "6",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/KogMaw_IcathianSurprise.png",
                    "title": "来自艾卡西亚的惊喜",
                    "description": "在死亡后，克格莫的身体将启动一种连锁反应，并于4秒之后爆炸。对周围的敌人造成125~550(1-18级 每级+25)真实伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KogMawQ.png",
                    "title": "腐蚀唾液",
                    "description": "被动:克格莫获得15％/20%/25%/30%/35%额外攻击速度。\n\n主动:克格莫抛射出一团腐蚀性的唾沫，对命中的第一个敌人造成90/140/190/240/290（+70％*AP）魔法伤害，并击碎目标20％/22%/24%/26%/28%护甲和魔法抗性，持续4秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KogMawBioArcaneBarrage.png",
                    "title": "生化弹幕",
                    "description": "在接下来的8秒里，克格莫的普通攻击增加130/150/170/190/210攻击距离并造成目标3％/3.75%/4.5%/5.25%/6%（+1％每100AP）最大生命值的额外魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KogMawVoidOoze.png",
                    "title": "虚空淤泥",
                    "description": "对命中的敌人造成75/120/165/210/255（+50％*AP）魔法伤害并留下一条持续4秒的酸性路径，使路径内的敌人减少20％/28％/36％/44％/52％移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KogMawLivingArtillery.png",
                    "title": "活体大炮",
                    "description": "克格莫发射一枚射程极远的活体炮弹，对生命值在40％以上的敌人造成100/140/180 （+35％*AP +65％*额外AD）额外到150/210/270（+52.5％*AP +97.5％*额外AD）额外魔法伤害（基于已损失的生命值）。\n\n如果敌人的生命值低于40％，那么该敌人就会受到200/280/360（+70％*AP +130％*额外AD）额外魔法伤害。\n\n在施放后8秒内的每次后续施放，都会多消耗40法力值（最大值：400）。\n\n范围：1300/1550/1800"
                  }
                ],
                "data": {
                  "hp": "534.0000",
                  "hpperlevel": "88.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.5500",
                  "mp": "325.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.7500",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.1100",
                  "attackspeed": "0.6650",
                  "attackspeedperlevel": "2.6500",
                  "armor": "24.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "法外狂徒",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Graves.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big104000.jpg",
                "story": "马尔科姆·格雷福斯是有名的佣兵、赌徒和窃贼，凡是他到过的城邦或帝国，都在通缉悬赏他的人头。虽然他脾气暴躁，但却非常讲究黑道的义气，他的双管散弹枪“命运”就经常用来纠正背信弃义之事。几年前他和老搭档崔斯特·菲特冰释前嫌，如今二人一同在比尔吉沃特的地下黑道纷争中再次如鱼得水。",
                "usageTips": "可以用【E快速拔枪】来接近敌人，然后用【Q大号铅弹】来造成巨额的伤害。",
                "battleTips": "格雷福斯主要造成物理伤害，所以护甲是一个非常有效的反制手段。",
                "name": "格雷福斯",
                "defense": "5",
                "magic": "3",
                "difficulty": "3",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/GravesTrueGrit.png",
                    "title": "新命运",
                    "description": "格雷福斯的囊弹枪拥有一些独特性能：\n\n- 它只有2颗子弹并且打完后就必须得重新装填。攻击速度会略微缩短装填时间，但会显著缩短开火间隔。\n\n- 每次攻击会发射4颗弹丸。对同一目标的第一颗弹丸会造成(70％~100%*AD)(1-18级)物理伤害，并且每颗后续弹丸造成该物理伤害的1/3。作为常规暴击效果的替代，暴击会将弹丸的数量提升至6颗并让每颗弹丸造成的伤害提高30%。对建筑造成的伤害减少25%。\n\n- 弹丸在命中第一个单位后就会停下。会击退被多颗弹丸命中的非英雄单位。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GravesQLineSpell.png",
                    "title": "穷途末路",
                    "description": "格雷福斯发射一个火药卷，造成45/60/75/90/105（+80%*额外AD）物理伤害。在2秒或与地形碰撞之后，它会爆炸，对沿途和爆炸附近的敌人们再次造成85/120/155/190/225（+40%/ 70% /100%/130%/160%*额外AD）物理伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GravesSmokeGrenade.png",
                    "title": "烟幕弹",
                    "description": "格雷福斯生成一团黑色烟幕，持续4秒，使其中的敌人减速50%并无法看到区域之外。初始冲击造成60/110/160/210/260（+60％*AP）魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GravesMove.png",
                    "title": "快速拔枪",
                    "description": "格雷福斯突进并装填一颗子弹进他的觳弹枪中。他还会每4秒获得一层效果(最多8层)或是在朝着敌人突进时获得2层效果。每层效果为他提供6/9/12/15/18护甲。层数会在对非小兵单位造成伤害时刷新。\n\n格雷福斯的攻击每有一颗弹丸命中敌人，就会使这个技能缩短0.5秒冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/GravesChargeShot.png",
                    "title": "终极爆弹",
                    "description": "格雷福斯射出一枚爆破弹，并因后坐力而向后位移。爆破弹对命中的第一敌人造成250/400/550（+150%*额外AD）物理伤害。在命中一名敌方英雄，或者达到最大射程之后，终极爆弹将会向外爆裂，造成200/320/440（+120%*额外AD）物理伤害。"
                  }
                ],
                "data": {
                  "hp": "555.0000",
                  "hpperlevel": "92.0000",
                  "hpregen": "8.0000",
                  "hpregenperlevel": "0.7000",
                  "mp": "325.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "68.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.4750",
                  "attackspeedperlevel": "2.6000",
                  "armor": "33.0000",
                  "armorperlevel": "3.4000",
                  "spellblock": "32.0000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "425.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "惩戒之箭",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Varus.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big110000.jpg",
                "story": "韦鲁斯是古老暗裔的一员。身为一名冷血的杀手，他最爱的就是用箭矢折磨敌人。先让他们失心发疯，再了结他们的性命。韦鲁斯俊美非常，虽然在大暗裔战争结束后便遭囚禁，但却在几百年后成功逃脱，寄宿于两位艾欧尼亚猎人再造的血肉之躯中。这二人无意之间释放了韦鲁斯，从此便背上了那把蕴含着韦鲁斯精魄的长弓。如今的韦鲁斯开始残酷地报复那些囚禁他的人，但他体内纠缠的两个凡人灵魂却在阻挠他的每一步。",
                "usageTips": "尽早点出枯萎箭袋，有助于压制敌方英雄，以及补刀。",
                "battleTips": " 如果你被施加了枯萎效果，那么韦鲁斯的技能就会对你造成额外伤害。",
                "name": "韦鲁斯",
                "defense": "3",
                "magic": "4",
                "difficulty": "2",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/VarusPassive.png",
                    "title": "复仇之欲",
                    "description": "韦鲁斯在每击杀一名敌人后，就会获得(20% +15% 额外攻速)攻击速度。参与击杀英雄后会提供(40% +30%额外攻速)攻击速度作为替代。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VarusQ.png",
                    "title": "穿刺之箭",
                    "description": "开始虚拟：韦鲁斯为一次强力射击深深挽弓，自我减速220%。在4秒后，如果未将箭射出去，则韦鲁斯会取消这个技能并返还其50%法力消耗。\n释放：韦鲁斯将箭射出，造成((10) +83.3%/86.6%90%/93.3%/96.6%)物理伤害，每命中一个敌人就会降低15%(最低将至33%)。伤害和枯萎的引爆效果会基于蓄力时间至多提升50%(最大值为(15) +125%/1.3%/1.35%/1.4%/1.45)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VarusW.png",
                    "title": "枯萎箭袋",
                    "description": "被动：韦鲁斯的攻击造成额外的(7/8/9/10/11 +25%AP)魔法伤害并施加一层持续6秒的枯萎(最多至3层)。\n韦鲁斯的其他技能会引爆枯萎层数来造成每次(3%/3.5%/4%/4.5%/5% +2%AP)最大生命值伤害(9%/10.5%/12%/13.5%/15% +6%)最大生命值伤害。引爆英雄和史诗级野怪身上的枯萎时也会使他基础技能的冷却时间缩短，数额为每层12%基础冷却时间。\n主动：韦鲁斯的下一个穿刺之箭会造成额外的(6%-14%)最大生命值的魔法伤害，并且基于蓄力时间不断提升，至多可达(9%-21%)最大生命值伤害。\n来自枯萎爆炸的被动伤害对野怪的封顶值为360伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VarusE.png",
                    "title": "恶灵箭雨",
                    "description": "韦鲁斯射出一阵箭雨，造成(60/100/140/180/220 +60%额外AD)物理伤害，并污染地面4秒钟，减速敌人(25%/30%/35%/40%/45%)并施加40%重伤。\n重伤会降低治疗效果和生命回复的效能。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/VarusR.png",
                    "title": "腐败锁链",
                    "description": "韦鲁斯猛射出一根藤蔓，禁锢命中的第一个敌方英雄2秒并造成(150/200/250 +100%AP)魔法伤害。被禁锢的敌人们会在此期间持续获得3层枯萎。\n随后腐败藤蔓会从它的目标处向附近未被感染的英雄蔓延。如果它接触到了未被感染的英雄，就会使其承受相同的伤害并被禁锢。"
                  }
                ],
                "data": {
                  "hp": "500.0000",
                  "hpperlevel": "89.0000",
                  "hpregen": "3.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "360.0000",
                  "mpperlevel": "33.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.1100",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "3.0000",
                  "armor": "27.0000",
                  "armorperlevel": "3.4000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "575.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "荣耀行刑官",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Draven.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big119000.jpg",
                "story": "在诺克萨斯，有一种战士被称为“清算人”。他们在竞技场里互相厮杀，以鲜血作为赌注进行力量的考验，不过没有任何人能像德莱文那样受人追捧。曾经参过军的他，发现竞技场的观众们尤为喜爱他的耀武扬威和刻意表演，和他使用飞斧的无匹技艺。这种狂傲的完美带来的赞叹令他上瘾，因此德莱文发誓要不惜代价打败任何对手，以此确保自己的名字在帝国之中永世传唱。",
                "usageTips": "如果德莱文呆在原地不动，旋转飞斧将会掉落在他所在的位置附近。也许会直接落到他身上，也许会在左边或右边。",
                "battleTips": "朝着德莱文旋转飞斧即将着陆的位置施放技巧射击。",
                "name": "德莱文",
                "defense": "3",
                "magic": "1",
                "difficulty": "8",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Draven_passive.png",
                    "title": "德莱文联盟",
                    "description": "德莱文在每次击杀一个非英雄单位或防御塔、接住一把旋转飞斧时会获得1层效果，并且在不接丢一把旋转飞斧的前提下连杀6个小兵时会获得2层效果。\n德莱文在击杀一名英雄后，会获得25+2X效果层数的金币。德莱文在阵亡时会损失75%的层数。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DravenSpinning.png",
                    "title": "旋转飞斧",
                    "description": "德莱文准备好一把旋转飞斧，使他的下一次攻击造成额外的(35/40/45/50/55 +65%/75%/85%/95%/105%额外AD)物理伤害然后弹到空中。如果德莱文接住了它，那么他会准备好另一把旋转飞斧。\n德莱文可以同时持有2把旋转飞斧。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DravenFury.png",
                    "title": "血性冲刺",
                    "description": "德莱文进入幽灵状态，获得在1.5秒里持续衰减的(50%/55%/60%/65%/70%)移动速度和持续3秒的(20%/25%/30%/35%/40%)攻击速度。\n当德莱文接住一把旋转飞斧时，这个技能的冷却时间就会刷新。\n[幽灵]状态的单位能够无视其他单位的碰撞体积。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DravenDoubleShot.png",
                    "title": "开道利斧",
                    "description": "德莱文抛掷一柄开山斧，造成(75/110/145/180/215 +50%额外AD)物理伤害，击退，以及持续2秒的(20%/25%/30%/35%/40%)减速。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/DravenRCast.png",
                    "title": "冷血追命",
                    "description": "德莱文猛掷出两把巨斧来造成(175/275/375 +(110%/130%/150%)额外AD)物理伤害。在命中一名英雄或再次施放之后，巨斧会掉转方向并返回德莱文处。巨斧每命中一个敌人就会降低8%伤害，最低降至40%。\n伤害衰减会在折返时重置。"
                  }
                ],
                "data": {
                  "hp": "574.0000",
                  "hpperlevel": "88.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.7000",
                  "mp": "360.5600",
                  "mpperlevel": "39.0000",
                  "mpregen": "8.0420",
                  "mpregenperlevel": "0.6500",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "3.6100",
                  "attackspeed": "0.6790",
                  "attackspeedperlevel": "2.7000",
                  "armor": "29.0000",
                  "armorperlevel": "3.3000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "虚空之女",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kaisa.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big145000.jpg",
                "story": "在孩童时期就被虚空夺走的卡莎，凭着纯粹的固执和意志力活了下来。她的历练让她成为了一位夺命的猎手，或者也有人会称她为黑暗未来的使者。她与一副有生命的虚空生物甲壳形成了一种不得安宁的共生状态，而很快她就将面临一个重大的抉择，究竟是原谅那些称她为怪物的凡人并协力抵御压境的黑暗……还是干脆忘记，放任虚空吞噬这个已将她抛弃的世界。",
                "usageTips": "尝试抓住对方落单的输出，并用艾卡西亚暴雨来将其轰杀。 与你的队友一起发动你的终极技能，并利用你的被动来充分输出伤害。 确保购买那些能够至少进化你1到2个技能的装备。",
                "battleTips": "卡莎非常善于带走落单的敌人。要对付她就得保持抱团。 卡莎很容易就会在对抗法师和超远程输出时陷入射程劣势。 请时刻保证自己已在盲区布置了守卫，这样一来就能看到卡莎的到来了。",
                "name": "卡莎",
                "defense": "5",
                "magic": "3",
                "difficulty": "6",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kaisa_Passive.png",
                    "title": "体表活肤",
                    "description": "苛性伤口—卡莎的普通攻击会叠加电浆，持续4秒并造成(4-10 +10%AP)((+1.00-5.00 +2.5%AP)X层数)魔法伤害。在4层时，卡莎的攻击会引爆电浆，造成额外伤害，伤害值为(15% +0.025*AP)的目标已损失生命值(对野怪的最大值：400)。\n友军施放在英雄身上的定身效果也会提供1层电浆。\n活体武器—卡莎的战斗服会适应她所选择的战斗风格并进化她的技能，具体进化哪个技能是基于来自装备和英雄等级的永久属性。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaisaQ.png",
                    "title": "艾卡西亚暴雨",
                    "description": "卡莎发射6发弹体并在附近的敌人中间四散，每一发造成(45/61.25/77.5/93.75/110)(+0.4AD)(+0.25AP)物理伤害。\n有额外的弹体命中相同英雄或野怪时，会造成25%伤害(最大值：)\n活体武器—100额外攻击力—艾卡西亚暴雨发射12发弹体。\n低于35%生命值的小兵受到200%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaisaW.png",
                    "title": "虚空索敌",
                    "description": "卡莎发射一道虚空震波，在命中第一个敌人时提供其真实视野，并触发2层电浆，然后基于目标身上的层数造成魔法伤害。\n最小伤害()\n最大伤害()\n活体武器 —100法术强度(当前：)—虚空索敌触发3层电浆并在命中英雄时返还70%冷却时间。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaisaE.png",
                    "title": "极限超载",
                    "description": "卡莎花费0.92秒来极限超载她的虚空能量。她在超载蓄力期间获得98%移动速度并在超载持续期间获得(40%/50%/60%/70%/80%/)攻击速度，持续4秒。\n活体武器 — 100%攻击速度(目前：) - 极限超载提供隐形，持续0.5秒。\n普通攻击会使极限超载的冷却时间减少0.5秒，可通过攻击速度来改进极限超载的施放时间和移动速度加成。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KaisaR.png",
                    "title": "猎手本能",
                    "description": "卡莎跃迁到一个位置上，该位置处于一名身上带有电浆效果的地方英雄的附近，并获得一个可吸收(75/100/125 +(100%/150%/200%AD) +75%AP)伤害的护盾，该护盾持续2秒。\n距离：1500/2250/3000"
                  }
                ],
                "data": {
                  "hp": "571.0000",
                  "hpperlevel": "86.0000",
                  "hpregen": "3.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "344.8800",
                  "mpperlevel": "38.0000",
                  "mpregen": "8.2000",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "59.0000",
                  "attackdamageperlevel": "1.7000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "1.8000",
                  "armor": "28.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "永猎双子",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kindred.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big203000.jpg",
                "story": "千珏，作为象征死亡的一对精魂，他们互相独立，却从未分离。对于坦然接受命运的人来说，羊灵的长弓可以痛快地送他们离开生者的乐园，而妄图逃脱宿命的人则由狼灵追捕，痛苦地倒在他有力的撕咬下。虽然在符文之地上到处是关于千珏的不同传言，但每个人在临终时都会看到死亡的真正面孔。至于是哪一面，完全出自个人的抉择。",
                "usageTips": "在打野时，利用攻击间隔进行移动，会有助于你避免受伤并从【W狼灵狂热】中获得更多治疗效果。要谨慎地挑选你想要追杀的狩猎目标；尽可能多地干掉狩猎目标，是通向胜利的关键。",
                "battleTips": "千珏是个脆皮英雄——对他们施加压力，他们就会被迫小心翼翼。",
                "name": "千珏",
                "defense": "2",
                "magic": "2",
                "difficulty": "4",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kindred_Passive.png",
                    "title": "千珏之印",
                    "description": "羊灵可通过点击物品栏上方的英雄肖像来选择-一个英雄进行狩猎。\n狼灵会周期性地陷入饥饿状态并狩猎敌方野区的野怪。\n每次击杀或参与击杀被狩猎的目标都会视为完成一次狩猎，并强化千珏的基础技能。最先的4次成功的狩猎，会使千珏的攻击距离提升75。在最先的4次成功的狩猎后，每3次成功的狩猎会使干珏的攻击距离提升25。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KindredQ.png",
                    "title": "乱箭之舞",
                    "description": "羊灵进行翻滚，并朝附近的敌人发射最多 3支箭矢，造成(60/80/100/120/140 +65%额外AD)物理伤害，并获得25%攻击速度加成，持续4秒。\n施放狼灵狂热或翻滚到它的效果内，会使这个技能的冷却时间减少至(4/3.5/3/2.5/2)秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KindredW.png",
                    "title": "狼灵狂热",
                    "description": "被动：在千珏移动和攻击的同时，他们会积攒[猎手英姿]的层数，最大值为100。在满层时，羊灵的下一次普攻将基于已损失的生命值来至多治疗千珏。\n主动：狼灵划出一块领地，并攻击领地内的所有附近的敌人，持续8.5秒。羊灵会将狼灵重新导向至新目标并对其发起攻击。如果羊灵离开了狼灵的领地，那么狼灵就会终止攻击并回到羊灵的身上。\n狼灵的攻击会造成魔法伤害，伤害值相当于(25/30/35/40/45 +20%AD)加上目标1.5%当前生命值。\n狼灵的攻击会基于羊灵的攻击速度而变快。他的共济会残废野怪。造成50%提升伤害并使目标野怪的移动速度减少50%，持续2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KindredEWrapper.png",
                    "title": "横生惧意",
                    "description": "使一名敌人致残，让目标的移动速度减少50%，持续1秒。\n在羊灵对目标又进行了两次攻击后，她的第三次攻击会指挥狼灵扑向敌人，造成(80/100/120/140/160 +80%AD)加上目标8.0%已损失生命值的物理伤害。\n狼灵的攻击可暴击目标造成50%提升伤害，前提是目标生命值低于(15%)+(可通过暴击几率提升)。\n对野怪最大伤害：300"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KindredR.png",
                    "title": "羊灵生息",
                    "description": "羊灵朝脚下的土地进行为时4秒的赐福，创造一个无论是我方的还是敌方的生灵们都不会死亡的区域。范围内的单位在生命值降至10%后，都会免疫任何进一步的伤害或治疗效果。\n当赐福结束后，范围内的所有生灵都会获得(250/325/400)治疗效果。"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "300.0000",
                  "mpperlevel": "35.0000",
                  "mpregen": "7.0000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "65.0000",
                  "attackdamageperlevel": "2.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.5000",
                  "armor": "29.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "暴走萝莉",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Jinx.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big222000.jpg",
                "story": "神经狂躁、冲动任性、劣迹斑斑……金克丝出身自祖安，生来就爱不计后果地大搞破坏。她就是一座人形自走军火库，所经之处必定会留下夺目的火光和震耳的爆炸。金克丝最讨厌无聊，所以不管她去到哪里，混乱和骚动就会如期而至，这就是她留下的“到此一游”。",
                "usageTips": "火箭并不总是最佳选择！金克丝的轻机枪在预热完毕后，也拥有难以置信的威力。当敌方英雄快要抓到你时，就换成轻机枪射个痛快吧！",
                "battleTips": "金克丝的轻机枪需要花费时间来预热，才能发挥全部实力。如果你看到她在远处发射火箭，就要尽量突进过去将她放倒。",
                "name": "金克丝",
                "defense": "2",
                "magic": "4",
                "difficulty": "6",
                "attack": "9",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Jinx_Passive.png",
                    "title": "罪恶快感",
                    "description": "当一名英雄、一个史诗级野怪或一座建筑物在被金克丝造成伤害后的3秒内被击杀或摧毁的话，金克丝就会获得不断衰减的175%移动速度和15%攻击速度，持续6秒。\n这个效果在触发时可让金克丝突破攻击速度上限。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JinxQ.png",
                    "title": "枪炮交响曲！",
                    "description": "金克丝可在[鱼骨头—火箭发射器]和[砰砰枪—轻机枪]之间切换武器。\n在使用火箭发射器时，金克丝的攻击会对目标和目标附近的敌人造成(110%AD)物理伤害，提升(75/100/125/150/175)攻击距离，消耗法力，但所享的攻速加成减少25%。\n在使用轻机枪时，金克丝的攻击提供攻击速度，持续2.5秒，可叠加至多3次(最大攻速加成为30%/55%/80%/105%/130%)\n轻机枪的叠加效果一次只会消散一层，并且 在金克丝切换至火箭发射器后，只有第一次攻击会享受攻速加成。\n法力消耗：每发火箭20法力"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JinxW.png",
                    "title": "震荡电磁波！",
                    "description": "金克丝发射一束震荡波，对命中的第一个目标造成(10/60/110/160/210 +160%AD)物理伤害，使其减速(30%/40%/50%/60%/70%)并显形，持续2秒。\n[震荡电磁波]的施放时间会随着金克丝攻击速度的增加而缩短。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JinxE.png",
                    "title": "嚼火者手雷！",
                    "description": "金克丝扔出3颗持续5秒的嚼火者手雷，手雷会在接触到敌方英雄时爆炸，将其禁锢1.5秒并对附近敌人造成(70/120/170/220/270 +100%/AP)魔法伤害。\n嚼火者手雷可以中断敌人的突进。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/JinxR.png",
                    "title": "超究极死神飞弹！",
                    "description": "金克丝发射一枚 飞弹，在首次命中敌方英雄后爆炸，造成(25/40/55 +15% 额外AD)到(250/350/450 +150%额外AD)+(25%%/30%/35%)已损失生命值的物理伤害，飞弹在飞行时间超过1秒后会持续提升伤害。目标附近的敌人受到80%伤害。"
                  }
                ],
                "data": {
                  "hp": "581.0000",
                  "hpperlevel": "84.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.5000",
                  "mp": "245.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "6.7000",
                  "mpregenperlevel": "1.0000",
                  "attackdamage": "57.0000",
                  "attackdamageperlevel": "3.4000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.0000",
                  "armor": "28.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "圣枪游侠",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Lucian.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big236000.jpg",
                "story": "卢锡安，一名光明哨兵，一个狩猎不死亡灵的残酷猎手，使用一双圣物手枪进行无情的追踪与杀灭。当怨灵锤石夺走他妻子的生命后，卢锡安踏上了复仇之路。但即便她已重获新生，他的怒火也没有平息。无情而又固执的卢锡安将不惜一切代价，保护生者，对抗黑雾中那亡故已久的恐怖。",
                "usageTips": "热诚烈弹会呈星形爆炸。试着找一个前置目标，然后用爆炸后的流弹命中敌方英雄。",
                "battleTips": "透体圣光不会给卢锡安额外的攻击距离。他仍然需要寻找一个目标来触发圣光。注意预判卢锡安将会选择的角度，就会比较容易躲掉透体圣光。",
                "name": "卢锡安",
                "defense": "5",
                "magic": "3",
                "difficulty": "6",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Lucian_Passive.png",
                    "title": "圣光银弹",
                    "description": "每使用一个技能时，卢锡安的下次普通攻击都会连射2次。\n第二次射击会对敌方英雄和建筑物造成(50%AD)物理伤害，附加攻击特效，并且在暴击时造成(87.5AD)伤害。第二次射击会对小兵造成(100%AD)物理伤害，在暴击时提升至(175%AD)伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LucianQ.png",
                    "title": "透体圣光",
                    "description": "射出一束穿透性的圣光穿过一个目标单位，对一条直线上的敌人造成(95/130/165/200/235 +(60%/75%/90%/105%/120%AD))物理伤害。\n此技能的施放时间会随卢锡安的等级提升而略微降低。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LucianW.png",
                    "title": "热诚烈弹",
                    "description": "发射一枚飞弹，飞弹会在命中敌人时或者到达终点时呈星形爆炸。爆炸会造成(75/110/145/185/215 +90%AP)魔法伤害，将敌人标记6秒，并暂时使敌人显形。\n在卢锡安或他的友军对标记 的敌人造成伤害时，卢锡安会获得(60/65/70/75/80)移动速度加成，此加成持续1秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LucianE.png",
                    "title": "冷酷追击",
                    "description": "卢锡安冲刺一小段距离\n一旦[圣光银弹]命中了一名敌人，[E冷酷追击]的冷却时间就会减少1秒(在以地方英雄为目标时，翻倍至2秒)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LucianR.png",
                    "title": "圣枪洗礼",
                    "description": "卢锡安在自由移动的同时，朝着一个方向猛射3秒。这些子弹会与命中的第一个敌方单位进行碰撞，每一发能造成(20/40/60 +10%AP +25%AD)物理伤害。（22/28/34)次射击的总伤害(440/560/680 +220%AP +550%AD)物理伤害。圣枪洗礼对小兵造成200%伤害。"
                  }
                ],
                "data": {
                  "hp": "571.0000",
                  "hpperlevel": "86.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.6500",
                  "mp": "348.8800",
                  "mpperlevel": "38.0000",
                  "mpregen": "8.1760",
                  "mpregenperlevel": "0.7000",
                  "attackdamage": "64.0000",
                  "attackdamageperlevel": "2.7500",
                  "attackspeed": "0.6380",
                  "attackspeedperlevel": "3.3000",
                  "armor": "28.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "沙漠玫瑰",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Samira.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big360000.jpg",
                "story": "莎弥拉以寸步不让的自信直视死亡，无论走到哪里都要寻找刺激。生于恕瑞玛的她在很小的时候就被摧毁了家园，后来受到了诺克萨斯的感召，在那里，她成为了英姿飒爽的孤胆女将，专门处理最高规格的凶险任务。莎弥拉使用黑火药双枪外加一把特殊制程工艺的大刀，她在生死存亡的关头尤为勇猛，以闪钢与烈焰终结任何拦路者。",
                "usageTips": "使用沙漠玫瑰时多使用combo",
                "battleTips": "沙漠玫瑰比较怕gank",
                "name": "莎弥拉",
                "defense": "0",
                "magic": "0",
                "difficulty": "0",
                "attack": "0",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/SamiraP.Samira.png",
                    "title": "悍勇本色",
                    "description": "莎弥拉可通过用不同的技能或攻击对敌方英雄造成伤害来构建一次连招。 每个与前一个不同的技能或攻击都会提升她的评价等级，从E到S(共6级)。每个评级会为莎弥拉提供3.5%移动速度。\n\n莎弥拉在近战距离的攻击会造成额外的(7.5%AD+2 -19)魔法伤害， 基于目标的已损失生命值至多提升至 (15%AD+4-38)。\n\n莎弥拉对被定身的敌人打出的攻击会使她突进到她的攻击距离处，并击飞该敌人0.5秒。\n\n莎弥拉的冲刺距离为650= (650 - 950 )码. \n\n【已更新】现在，不再击飞被定身的敌人，而是让已经被击飞的敌人在空中的停留时间延长至少0.5秒。莎弥拉依然可以对被定身的敌人进行一次普攻来突进至距离内。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SamiraQ.png",
                    "title": "交火",
                    "description": "莎弥拉进行一次射击，对命中的第一个敌人造成( 0/5/ 10/15/20 +66.6%AD)物理伤害 。如果朝着近战距离内的一名敌人施放这个技能，那么莎弥拉会转而用她的剑进行斩击，造成( 0/5/ 10/15/20  +100%AD)物理伤害。\n\n这两种打击能够暴击(造成25%额外伤害)并施加生命偷取。\n\n如果在狂飙期间施放此技能，那么莎弥拉将在冲刺完成时打击她沿途的所有敌人。\n\nBUG修复：现在，用Q技能暴击命中敌人时，将造成正确的额外伤害值"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SamiraW.png",
                    "title": "锋旋",
                    "description": "莎弥拉斩击她的周围1秒，对敌人造成2段伤害，每段为(20/35/50/65/80+80%AD)理伤害并摧毁任何进入该区域的敌方飞行道具。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SamiraE.png",
                    "title": "狂飙",
                    "description": "莎弥拉冲刺并越过一名敌人或友军，斩击沿途的敌人来造成 (50 +20%AD)魔法伤害并获得20%攻击速度，持续3秒。\n\n参与击杀敌方英雄后，会刷新这个技能的冷却时间。\n攻击速度[20%/25%/30%/35%/40%]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SamiraR.png",
                    "title": "炼狱扳机",
                    "description": "莎弥拉只能在她当前的评价等级为S时使用这个技能。\n\n莎弥拉的武器倾泻出大量的子弹，狂野地在2秒里持续对她周围的所有敌人进行10次射击，每次射击造成29= (0/ 10/20 +50%AD)物理伤害并施加生命偷取。这些射击也可以产生暴击。\n\n暴击时的生命偷取效能：66.6%"
                  }
                ],
                "data": {
                  "hp": "0.0000",
                  "hpperlevel": "0.0000",
                  "hpregen": "0.0000",
                  "hpregenperlevel": "0.0000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "0.0000",
                  "attackdamageperlevel": "0.0000",
                  "attackspeed": "0.0000",
                  "attackspeedperlevel": "0.0000",
                  "armor": "0.0000",
                  "armorperlevel": "0.0000",
                  "spellblock": "0.0000",
                  "spellblockperlevel": "0.0000",
                  "movespeed": "0.0000",
                  "attackrange": "0.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "复仇之矛",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Kalista.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big429000.jpg",
                "story": "卡莉丝塔是充满复仇怨念的幽灵，是不灭的复仇之魂，是召唤自暗影岛的噩梦，专门猎杀背信弃义之人。因遭人背叛而受害的人会以血泪盼望着复仇，但只有甘愿献上自己灵魂的人，才能获得卡莉丝塔的回应。所有被卡莉丝塔盯上的人都将不可避免地遭遇不幸，因为这位死亡猎手完成誓约的方式只有一种，那就是她灵魂标枪上的冷酷灵火。",
                "usageTips": "撕裂是一个有用的补刀手段，因为它会在击杀一个单位后重置冷却时间。",
                "battleTips": "卡莉丝塔的机动能力取决于她的攻击。这意味着她在射程内没有敌人时，机动能力会很低，并且如果她的攻速被减慢了，那么她在一场战斗中的位移距离也会被降低不少。",
                "name": "卡莉丝塔",
                "defense": "2",
                "magic": "4",
                "difficulty": "7",
                "attack": "8",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Kalista_Passive.png",
                    "title": "武术姿态",
                    "description": "如果卡莉丝塔在普攻或穿刺的起手阶段被下达了移动指令，那么她会在发起进攻的同时朝着这个移动指令的方向位移小段距离。\n\n位移距离会随着鞋子品质的提高而成长。\n\n卡莉丝塔的普攻有一些独特的缺点。它们:\n:-无法被取消;\n会在目标离开视野后落空;\n\n-造成的伤害只有总攻击力的90%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KalistaMysticShot.png",
                    "title": "穿刺",
                    "description": "掷出一根狭长、快速的长矛，对命中的第个敌 人造成( 20/85/ 150/215/280+69)物理伤害。触发武术姿态(被动)，哨兵(W)，撕裂(E).\n\n如果击杀了一个目标，穿刺的飞行道具就会继续向前飞行，将撕裂效果的所有层数传递给下个目标。\n法力消耗伤害[ 50/55/60/65/ 70 ]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KalistaW.png",
                    "title": "哨兵",
                    "description": "被动:当卡莉丝塔和她的誓约者都用普攻击中了相同的目标时，她会造成相当于目标14%/15%/16%/17%/18%最大生命值的魔法伤害。10秒内无法重复作用于相同目标。\n\n主动:派出一个灵魂哨兵去巡视一个不可见的地区。被发现的敌方英雄会被暴露4秒。哨兵持续3个来回。\n\n卡莉丝塔每90秒获得一层哨兵充能。\n充能时间[ 90/80/ 70/60/50 ]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KalistaExpungeWrapper.png",
                    "title": "撕裂",
                    "description": "被动:卡莉丝塔的长矛会刺穿她们的目标，并在目标身上停留4秒。\n\n主动:将长矛从附近目标身上扯出，造成(20/30/40/50/60 +60%金 )物理伤害并减少目标10%的移动速度，持续2秒。\n\n每根额外的长矛造成24 = (10/ 15/20/25/30 + 20%AD)物理伤害。\n\n如果撕裂击杀了至少-个单位， 那么它的冷却时间会得到重置，并且它会返还10法力。\n\n撕裂对史诗级野怪造成50%伤害。\n移动减速[10%/18%/26%/34%/42%」"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KalistaRx.png",
                    "title": "命运的召唤",
                    "description": "将卡莉丝塔的誓约者拉向她。在最多4秒的持续时间里，誓约者不可被选取且进入平静状态。\n\n誓约者可以通过鼠标点击来飞向目标区域，在命中第个敌方英雄时停下， 将小范围内 的所有敌人击退。\n\n卡莉丝塔与誓约者之间的距离必须在1100码内，才能使用这个技能。\n冷却时间[ 150/ 120/90 ]\n击飞时长[ 1.5/1.75/2 ]"
                  }
                ],
                "data": {
                  "hp": "534.0000",
                  "hpperlevel": "100.0000",
                  "hpregen": "3.7500",
                  "hpregenperlevel": "0.5500",
                  "mp": "250.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "6.3000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "69.0000",
                  "attackdamageperlevel": "4.0000",
                  "attackspeed": "0.6940",
                  "attackspeedperlevel": "4.0000",
                  "armor": "21.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "逆羽",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Xayah.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big498000.jpg",
                "story": "身为瓦斯塔亚的志士，霞要掀起一场革命来拯救她的族群。她身法敏捷又慧心独具，凭借锋芒逼人的羽刃，扫除任何异己。霞与她的灵魂伴侣洛并肩作战，共同守护他们日渐衰落的部族，同时韬光养晦，希望终有一天能率领全族重夺昔日荣光。",
                "usageTips": "霞可以使用【R暴风羽刃】来躲避几乎所有技能，同时创造成吨的羽毛。尽量把这个技能在攻击和防御两方面的特点都利用好。",
                "battleTips": "霞的倒钩只会禁锢那些被3支以上羽毛所命中的目标。",
                "name": "霞",
                "defense": "6",
                "magic": "1",
                "difficulty": "5",
                "attack": "10",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/XayahPassive.png",
                    "title": "锐切",
                    "description": "在使用一个技能后，霞的下3次攻击将对沿途的所有目标造成30%/伤害并留下一支持续6秒的羽毛\n情人跃：洛和霞可以一起回城。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XayahQ.png",
                    "title": "双刃",
                    "description": "霞扔出两把匕首，每把造成(45/65/85/105/125 +50%额外AD)物理伤害，然后留下两支羽毛。每把匕首对首个目标之后的后续目标造成22.5/32.5/42.5/52.5/62.5 +25%额外AD)伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XayahW.png",
                    "title": "致死羽衣",
                    "description": "霞创造一道持续4秒的羽刃风暴，来为她提供(35%/40%/45%/50%/55%)攻击速度并使她的攻击发射一支造成20%伤害的次级羽刃。\n如果次级羽刃命中了一名敌方英雄，那么她自身会获得30%移动速度，持续1.5,秒。\n如果洛在附近，那么他也会获得这个技能的效果，但他只会在霞命中了一个目标英雄时获得移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XayahE.png",
                    "title": "倒钩",
                    "description": "霞将所有羽毛召回至她的位置，每支造成(55/65/75/85/95 +60%额外AD)物理伤害，可通过(50%暴击)来提升。如果有3支或以上的羽毛命中了一名敌人，那么该敌人就会被禁锢1.25秒。\n这个技能对小兵造成50%伤害。\n敌人每被一支额外的羽毛命中，所受的后续羽毛伤害就会降低5%，最低降至10%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/XayahR.png",
                    "title": "暴风羽刃",
                    "description": "霞跃向空中，变为不可选取状态，持续1.5秒。然后她会从天降下匕首雨，造成(125/250/375 +100%额外AD)物理伤害并留下一连串的羽毛。\n不可选取的单位不会受到敌人的攻击或者技能的影响，除非在进入此状态前已被它们影响。\n幽灵状态的单位无视 其他单位的碰撞体积。\n霞在空中时可以移动。"
                  }
                ],
                "data": {
                  "hp": "561.0000",
                  "hpperlevel": "86.0000",
                  "hpregen": "3.2500",
                  "hpregenperlevel": "0.7500",
                  "mp": "340.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.2500",
                  "mpregenperlevel": "0.7500",
                  "attackdamage": "60.0000",
                  "attackdamageperlevel": "2.9000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.3000",
                  "armor": "25.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "残月之肃",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Aphelios.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big523000.jpg",
                "story": "如月影般神出鬼没、时刻剑拔弩张的厄斐琉斯沉默地弑杀一切自身信仰的敌人。他的语言只有精确的瞄准和枪火。虽然他饮下了让他失声的毒药，但他也因此得到了妹妹拉露恩的引导，从遥远的神庙中将月石打造的各种武器送到他手里。只要头顶的明月依然皎洁，厄斐琉斯就永不孤单。",
                "usageTips": "厄斐琉斯的每件武器都有不同的优点，因此要尽量寻找利于你当前武器发挥的输出环境。",
                "battleTips": "厄斐琉斯的每件武器都有不同的缺点，尽量挑他拿到利于你的英雄发挥的武器时进攻。要当心紫色的重力枪，它能禁锢你。",
                "name": "厄斐琉斯",
                "defense": "2",
                "magic": "1",
                "difficulty": "10",
                "attack": "6",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ApheliosP.png",
                    "title": "传知者与真知者",
                    "description": "残月神兵:\n厄斐琉斯拥有5件由他妹妹拉露恩所制的皎月武器。他同一时间可以使用2件: 1件主武器和1件副武器。每件武器都有1种独特的普攻方式和1个独特的主动技能[Q]。攻击和技能都会消耗该武器的弹药。在弹药耗尽后，拉露恩会回收该武器,并召唤下一件武器给厄斐琉斯。\n厄斐琉斯不需要使用技能点来提升技能等级，而是会将技能点用在能永久提升属性的赐福上。\n攻击力[Q] 0/24\n攻击速度[W] 0%/36%\n穿甲[E] 0/18\n皎月武器：\n通碧(步枪)：长距离攻击。\n断魄(短镰枪)：生命偷取和移动速度。\n坠明(加农炮)：控制。\n萤焰(喷火器)：群体伤害。\n折镜(飞轮刃)：近距离的高额伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ApheliosQ_ClientTooltipWrapper.png",
                    "title": "武器技能",
                    "description": "通碧(步枪)：还魂月闪\n主动：进行一次长距离射击，对命中的第一个敌人造成((60-160) +42%额外AD +100%AP)物理伤害并将其标记。\n冷却时间：(10-8)秒。10层充能，60法力值。\n断魄(短镰枪)：擘分对影\n主动：获得(20% -20%+1%)移动速度并在接下来的1.75秒里用两件武器射击距离最近的敌人。射击(6)次，共造成(60 +10%额外AD)物理伤害。优先以英雄为目标。施加攻击特效，但攻击特效仅造成25%伤害。\n冷却时间：(10-8)秒。10层充能，60法力值。\n坠明(加农炮)：地霜暗蚀\n主动：抹除这件武器在目标上的减速效果，同时使目标禁锢1秒并造成(50-110 +32%额外AD +70%AP)魔法伤害。\n冷却时间：(12-10)秒。10层充能，60法力值。\n萤焰(喷火器)：地霜暝涌\n主动：喷出一股火焰，造成(25-65 +72%额外AD +70%AP)物理伤害。随后，用你的副手武器攻击命中的所有敌人。\n冷却时间：(9-6)秒。10层充能，60法力值。\n折镜(飞轮刃)：还魂驻灵\n主动：部署一个月之驻灵，它装备着厄斐琉斯的副手武器，持续20秒。驻灵们会在附近有敌人接近时激活并射击敌人，每次射击造成(25-85 +45%额外AD +50%AP)物理伤害，激活后持续4秒。驻灵可从攻击速度和暴击中获益。\n冷却时间：(9-6)秒。10层充能，60法力值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ApheliosW.png",
                    "title": "月相轮转",
                    "description": "通碧：被动：在装备时为厄斐琉斯提供+100攻击距离。\n使用通碧的技能会标记目标4.5秒，并提供目标的视野。厄斐琉斯可以用它的副手武器从远处攻击一名被标记的目标；这次攻击会消耗掉所有附近的印记，每个印记造成(15/ +20%额外AD)额外伤害。如果厄斐琉斯的副手武器是[通碧]，那么他会使用他的主手武器作为替代。\n断魄：被动：用这把武器造成的实际伤害将有(3%-20%)转化为治疗效果。断魄可以进行过量治疗，至多为他提供(10-140 +6%生命值)生命值的护盾。护盾至多存在30秒。\n坠明：被动：用这个武器造成的伤害会使目标减速(30%)，在3.5秒内持续衰减。\n萤焰：被动：弹体会在一个区域内造成伤害。主要目标会受到(110%AD)伤害，其他目标会受到(75-100%)伤害*(对小兵的伤害值为30%)。\n折镜：被动：折镜的攻击弹体会回弹并且重置厄斐琉斯的攻击，但他在弹体处于飞行途中时无法攻击。回弹速度可通过攻击速度提升(600+75%额外攻速)。\n当厄斐琉斯的其他技能使用折镜时，这些技能会生成小型飞轮刃，持续5秒。用折镜进行攻击时会猛掷所有小型飞轮刃来造成额外伤害。额外伤害的初始值为(24%AD)物理伤害。并且后续的小型飞轮刃在命中时会有伤害衰减(最小值：(5%)伤害。)持续时长会在攻击英雄时刷新。可以暴击。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ApheliosE_ClientTooltipWrapper.png",
                    "title": "武器队列系统",
                    "description": "厄斐琉斯没有E技能。这个技能栏位会显示拉露恩将为他提供的下一件武器。武器序列在开始时是固定的，但可以随着游戏时间来变更——在一件武器弹药耗尽后，就会进入序列的末端。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ApheliosR.png",
                    "title": "清辉夜凝",
                    "description": "通碧(步枪)：发射一道浓缩爆裂的月光，在命中一位敌方英雄时爆炸，以在一个范围内造成(125/175/225 +20%额外AD +100%AP)物理伤害。\n随后厄斐琉斯会用他的主手武器攻击所有英雄。(施加攻击特效。暴击造成120%伤害)。\n通碧加成：用这种方式添加的标记，在被消耗时，每个标记会造成额外的(40/70/100)物理伤害。\n断魄(短镰枪)：发射一道浓缩爆裂的月光，在命中一位敌方英雄时爆炸，以在一个范围内造成(125/175/225 +20%额外AD +100%AP)物理伤害。\n随后厄斐琉斯会用他的主手武器攻击所有英雄。(施加攻击特效。暴击造成120%伤害)。\n断魄加成：厄斐琉斯回复(250/375/500)生命值。\n坠明(加农炮)：发射一道浓缩爆裂的月光，在命中一位敌方英雄时爆炸，以在一个范围内造成(125/175/225 +20%额外AD +100%AP)物理伤害。\n随后厄斐琉斯会用他的主手武器攻击所有英雄。(施加攻击特效。暴击造成120%)伤害。\n坠明加成：减速强度提升至(99%)。地霜暗蚀对这些目标的禁锢效果提升至1.25秒。\n萤焰(喷火器)：发射一道浓缩爆裂的月光，在命中一位敌方英雄时爆炸，以在一个范围内造成(125/175/225 +20%额外AD +100%AP)物理伤害。\n随后厄斐琉斯会用他的主手武器攻击所有英雄。(施加攻击特效。暴击造成120%伤害)。\n萤焰加成：初段爆炸造成的额外的(50/100/150 +25%额外AD)物理伤害，并且后续的攻击会呈环状爆炸，每次攻击对次级目标造成(85%)伤害。\n折镜(飞轮刃)：发射一道浓缩爆裂的月光，在命中一位敌方英雄时爆炸，以在一个范围内造成(125/175/225 +20%额外AD +100%AP)物理伤害。\n随后厄斐琉斯会用他的主手武器攻击所有英雄。(施加攻击特效。暴击造成120%伤害)。\n折镜加成：召唤一个额外的4小型飞轮刃。"
                  }
                ],
                "data": {
                  "hp": "500.0000",
                  "hpperlevel": "86.0000",
                  "hpregen": "3.2500",
                  "hpregenperlevel": "0.5500",
                  "mp": "348.0000",
                  "mpperlevel": "42.0000",
                  "mpregen": "6.5000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "57.0000",
                  "attackdamageperlevel": "2.2000",
                  "attackspeed": "0.6400",
                  "attackspeedperlevel": "2.1000",
                  "armor": "28.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "26.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
            ]
          },
          {
            "name": "辅助",
            "heroes": [
              {
                "title": "众星之子",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Soraka.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big16000.jpg",
                "story": "索拉卡是来自巨神峰彼端天界维度的流浪者。她放弃了不朽的神格，保护凡间的种族免遭他们自身暴力本能的伤害。她对自己遇见的每个人都施以同情与仁慈——即使是那些对她心存恶念的人也不例外。虽然索拉卡见证了这世上如此多的苦痛与挣扎，但她依然相信符文之地的人们依然有更多潜力尚未发现。",
                "usageTips": "索拉卡是个很强大的盟友，用她的治疗技能让己方团队不停前进。",
                "battleTips": "在团战中，如果索拉卡冲到前线治疗她的队友，那么就集中火力攻击索拉卡。",
                "name": "索拉卡",
                "defense": "5",
                "magic": "7",
                "difficulty": "3",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Soraka_Passive.png",
                    "title": "拯救",
                    "description": "索拉卡在朝着生命值低于40%的友军英雄移动时会获得70%移动速度加成。(该友方英雄与索拉卡的距离必须在2500以内)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SorakaQ.png",
                    "title": "流星坠落",
                    "description": "索拉卡召唤下一颗流星，从索拉卡所处的位置落向目标地点。站在爆炸范围内的敌人会受到(85/120/155/190/225 + 35%AP)魔法伤害并被减速30%，持续1.5秒\n如果此技能命中一个敌方英雄，那么索拉卡会获得持续2.5秒的活力焕发效果，在这个效果持续期间为索拉卡持续回复共(50/60/70/80/90 + 30%AP)生命值，并提供不断衰减的(15%/17.5%/20%/22.5%/25%)移动速度加成。\n冷却时间：8/7/6/5/4\n法力消耗：45/50/55/60/65"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SorakaW.png",
                    "title": "星之灌注",
                    "description": "回复一名友方英雄(80/115/150/185/220 +60%AP)生命值。\n如果在身上带有活力焕发效果时释放，那么最大生命值消耗会降低(80%/85%/90%/95%/100%)，并且也会为目标提供活力焕发效果，在2.5秒的持续时间里共回复(90 +30%AP)生命值并提供在持续期间不断衰减的25%移动速度加成。\n如果索拉卡低于(5%)生命值(她5%的最大生命值)则不能释放此技能。\n冷却时间：6/5/4/3/2\n法力消耗：40/45/50/55/60"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SorakaE.png",
                    "title": "星体结界",
                    "description": "在目标区域创造一个持续1.5秒的结界，对施法半径内的敌方英雄造成(70/95/120/145/170 +40%AP)魔法伤害。敌方英雄在结界内将一直被沉默，直到走出结界为止。\n当结界消失时，所有仍在结界内的敌方英雄将被禁锢1秒，并受到(70/95/120/145/170 +40%/AP)魔法伤害。\n冷却时间：20/19/18/17/16\n法力消耗：70/75/80/85/90"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SorakaR.png",
                    "title": "祈愿",
                    "description": "召唤神圣的能量来为所有友方英雄回复(150/250/350 +55%AP)生命值。祈愿会对每个生命值低于40%的友方英雄提升50%的回复效果(225/375/525 +82.5%)。\n冷却时间：160/145/130\n法力消耗：100"
                  }
                ],
                "data": {
                  "hp": "535.0000",
                  "hpperlevel": "74.0000",
                  "hpregen": "2.5000",
                  "hpregenperlevel": "0.5000",
                  "mp": "425.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "11.5000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "50.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1400",
                  "armor": "32.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "时光守护者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Zilean.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big26000.jpg",
                "story": "基兰曾是一位强大的艾卡西亚法师，在目睹了家园被虚空毁灭以后，他开始执迷于时间的流逝。他甚至没有时间为这场灾难感到悲哀，立刻就召唤了远古的时间魔法，预测全部的发展结局。从实际结果来说，他已成为不朽的存在。如今的基兰在过去、现在、未来之间漂泊，弯折、扭曲自己周围的时间，追寻那稍纵即逝的关键时刻，逆转时光，阻止艾卡西亚的毁灭。",
                "usageTips": "你可以连续使用【Q定时炸弹】和【W穿梭未来】来快速地将两颗定时炸弹放到一个目标上。放置第二颗炸弹会引爆第一颗，并晕眩附近的敌人。",
                "battleTips": "如果你能跟上基兰的速度，你可以等到他的终极技能效果消逝之后，再发出夺命一击。",
                "name": "基兰",
                "defense": "5",
                "magic": "8",
                "difficulty": "6",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Zilean_Passive.png",
                    "title": "瓶中时光",
                    "description": "基兰将时光储存为经验值，频率为每5秒(2-6)经验值。当他拥有足够的经验值来使一名友方英雄升级时，他就能通过右键点击该友方英雄来传授经验值，并且基兰自身也会获得等值的经验值。冷却时间为120秒。\n无法在战斗中传授经验值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZileanQ.png",
                    "title": "定时炸弹",
                    "description": "基兰在目标地点上投掷一个定时炸弹。炸弹会附着在第一个靠近它周围小范围内的单位上。\n3秒后炸弹会爆炸，造成(75/115/165/230/300 +90%AP)魔法伤害。\n对已被附着了一颗炸弹的单位放置第二颗炸弹时，就会立刻引爆第一个炸弹，并使爆炸范围内的所有敌人晕眩(1.1/1.2/1.3/1.4/1.5)秒。\n炸弹会优先附着英雄和已被附着了一颗炸弹的敌人。\n冷却时间：10/9.5/9/8.5/8\n法力消耗：60/65/70/75/80"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZileanW.png",
                    "title": "穿梭未来",
                    "description": "基兰倒转时间，使他其他基础技能的冷却时间缩短10秒。\n冷却时间：14/12/10/8/6\n法力消耗：35"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TimeWarp.png",
                    "title": "时光发条",
                    "description": "基兰使一名敌方英雄减速(40%/55%/70%/85%/99%)或为一名友方英雄提供(40%/55%/70%/85%/99%)移动速度，持续2.5秒\n冷却时间：15\n法力消耗：50"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ChronoShift.png",
                    "title": "时光倒流",
                    "description": "基兰提供一个保护性的时间符文给一名友方英雄，持续5秒。如果该目标将要阵亡，那么符文会回溯其时间线，使其凝滞3秒，然后将其复活并回复(600/850/1100 +200%AP)生命值。\n凝滞状态中的单位无法移动或行动，并且免疫伤害和不可选取。\n冷却时间：120/90/60\n法力消耗：125/150/175"
                  }
                ],
                "data": {
                  "hp": "504.0000",
                  "hpperlevel": "82.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5000",
                  "mp": "452.0000",
                  "mpperlevel": "30.0000",
                  "mpregen": "11.3350",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "51.6400",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1300",
                  "armor": "24.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "唤潮鲛姬",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Nami.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big267000.jpg",
                "story": "娜美是一名强大年轻的瓦斯塔亚海族。当鲛人族与巨神族之间自古订立的契约被打破，她是第一个离开海洋、踏上陆地的人。她别无选择，只能挺身而出担此重任，完成神圣的仪式从而确保族人的安全。在这崭新时代的混乱浪潮中，娜美用无比的决心和无畏的斗志面对未知的明天，用手中的唤潮者之杖召唤来自海洋的力量。",
                "usageTips": "在交战中对敌方英雄使用【W冲击之潮】，将会让团战走势朝着有利于你的方向倾斜。",
                "battleTips": "碧波之牢是一个非常强力的技能，但冷却时间很长。如果娜美将这个技能放空，那么就可以趁机获取优势了。",
                "name": "娜美",
                "defense": "3",
                "magic": "7",
                "difficulty": "5",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/NamiPassive.png",
                    "title": "踏浪之行",
                    "description": "每当娜美的技能命中友方英雄时，他们都会获得(45 +20%AP)移动速度， 持续1.5秒。\n\n娜美的进攻型技能如果命中了友军，也会为该友军提供强化。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NamiQ.png",
                    "title": "碧波之牢",
                    "description": "娜美掷出一个泡泡，晕眩敌人1. 5秒并造成[ 75/ 130/ 185/ 240/295）\n魔法伤害。 "
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NamiW.png",
                    "title": "冲击之潮",
                    "description": "娜美释放一股在友方英雄和敌方英雄之间交替弹跳的水流。每个目标只会被弹到一次，并且水流至多可弹3个目标。\n\n-为友方英雄回复 (60/85/ 110/ 135/ 160 +30%AP)生命值并弹向附近的一名敌方英雄身上。-给敌方英雄造成 (70/ 110/ 150/ 190/230 +50% AP)魔法伤害并弹向附近的一名友方英雄身上。每次弹跳会使伤害和治疗效果修正 (-15% +0.075% AP)。\n\n只会弹向可见的敌人。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NamiE.png",
                    "title": "唤潮之佑",
                    "description": "娜美强化一位友方英雄的下3次普通攻击和技能，持续6秒，使其可以使目标减速(15%/20%/25%/30%/35%\n +0.05%AP)， 持续1秒，并造成额外的(25/40/55/ 70/85 +20%AP )魔法伤害。\n在强化群体技能时会对非英雄单位造成 (33%66%)伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NamiR.png",
                    "title": "怒涛之啸",
                    "description": "娜美召唤一阵怒涛，击飞敌人0.5秒，减速敌人50%/60%/70%并造成 (150/ 250/350+60%AP)魔法伤害。减速的持续时间会基于怒涛的行进距离而提升，至多持续4秒。"
                  }
                ],
                "data": {
                  "hp": "475.0000",
                  "hpperlevel": "74.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "365.0000",
                  "mpperlevel": "43.0000",
                  "mpregen": "11.5000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "51.2080",
                  "attackdamageperlevel": "3.1000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "2.6100",
                  "armor": "29.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "涤魂圣枪",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Senna.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big235000.jpg",
                "story": "在孩童时期就受到诅咒的赛娜，注定被超自然的黑雾不断侵扰，于是她加入了一个名叫“光明哨兵”的神圣教团，进行勇猛的反击。但最后她却在战斗中殒命，灵魂被囚禁在残酷怨灵锤石的灯笼中。拒绝失去希望的赛娜，在灯笼中学会了如何使用黑雾，并重获新生，接受了永久的改变。如今的赛娜同时使用黑暗和光明的力量，要用黑雾自己的力量终结黑雾——每一次圣物武器的炮火，都救赎着雾中的迷魂。",
                "usageTips": "赛娜可以收集灵魂来加强自己，注意在对线过程中多收集灵魂",
                "battleTips": "赛娜的攻击速度非常慢，前期很弱",
                "name": "赛娜",
                "defense": "2",
                "magic": "6",
                "difficulty": "7",
                "attack": "7",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Senna_Passive.Senna.png",
                    "title": "赦除",
                    "description": "赛娜可以统计由阵亡敌人生成的灵魂，以吸收它们的黑雾。它还可以从被她用攻击或技能命中2次的敌方英雄身上吸取黑雾，同时造成额外的(1-16)%当前生命值的物理伤害。\n每层黑雾提供0.75攻击力。每20层黑雾提供25攻击距离和10%暴击几率。35%溢出的暴击几率会被转化为生命偷取。\n赛娜的攻击需要更长时间来开火，命中时造成额外(20%AD)物理伤害，并暂时为他提供目标(10%-20%)的移动速度。\n赛娜每(6-4)秒内仅能在相同英雄身上吸取1次迷雾。\n被赛娜击杀的小兵生成灵魂的几率会更低。英雄、大型野怪和非赛娜所杀的大型小兵总会生成灵魂。\n赛娜的攻击力不会在升级时成长。她的暴击造成的伤害会减少14%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Senna_Q.Senna.png",
                    "title": "黑暗洞灭",
                    "description": "赛娜发射一束洞灭之影来洞穿一名友军或一名敌人，对敌人造成(40/70/100/130/160 +40%额外AD)物理伤害并给友方英雄回复(40/60/80/100/120 +25%AP +40%额外AD)生命值。\n攻击会使这个技能的冷却时间缩短1秒。\n这个技能的施放距离与赛娜的攻击距离相匹配，并且它的施放时间可通过攻击速度来缩短，它可以选取任一单位为目标并且会对敌方英雄施加攻击特效。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Senna_W.Senna.png",
                    "title": "无尽厮守",
                    "description": "赛娜向前发射一道黑雾，对命中的第一个敌人造成(70/115/160/205/250 +70%额外AD)物理伤害。在1秒的延迟后，对目标及目标附近的其他敌人造成(1.25/1.5/1.75/2/2.25)秒禁锢效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Senna_E.Senna.png",
                    "title": "黑雾咒附",
                    "description": "赛娜融入一团黑雾，持续(6/6.5/7/7.5/8)秒，并变为幽魂。进入黑雾的友方英雄会变成伪装状态，并在离开时变成幽魂。幽魂获得20%移动速度，无法被选中，并且在附近没有地方英雄时会隐藏自己的身份。\n敌人能够看到幽魂在哪里 ，但不知道幽魂是谁。除此之外，它们会被视作伪装状态。\n伪装的单位不会被敌人看见，除非有一名敌方英雄在其侦测半径中。\n无法被选中的单位不会成为被攻击的目标或需要选中目标来施放的技能的目标。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Senna_R.Senna.png",
                    "title": "暗影燎原",
                    "description": "赛娜发射一道光束，对命中的所有敌方英雄造成(250/375/500 +50%AP +100%额外AD)物理伤害。在更广范围内被命中的友方英雄会获得(120/160/200 +40%AP +150%)护盾值，持续3秒。\n护盾值的强度取决于赛娜已收集黑雾的数量。"
                  }
                ],
                "data": {
                  "hp": "520.0000",
                  "hpperlevel": "75.0000",
                  "hpregen": "3.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "350.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "11.5000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "50.0000",
                  "attackdamageperlevel": "0.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "4.0000",
                  "armor": "28.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "600.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "魔法猫咪",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Yuumi.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big350000.jpg",
                "story": "作为一只来自班德尔城的魔法猫咪，悠米曾是一名约德尔魔女的守护灵，她的主人名叫诺拉。而当主人神秘消失以后，悠米就成为了《门扉魔典》的守护者，这是诺拉留下的一本有灵性的书，他们一起穿越书页中的传送门，共同寻找诺拉。渴望被宠爱的悠米在她的旅途中寻找着友善的同伴，为同伴们提供闪光护盾和坚决意志作为保护。虽然魔典竭尽全力让她把注意力留在最初的任务上，但悠米经常会被世俗的安逸所吸引，比如打盹和吃鱼。但安逸过后，她总是会回归自己的任务，找寻自己的朋友。",
                "usageTips": "使用猫咪的时候可以及时跳到队友身上规避伤害",
                "battleTips": "猫咪被控制时无法进入它的队友身上，请把控制技能留给她",
                "name": "悠米",
                "defense": "1",
                "magic": "8",
                "difficulty": "2",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YuumiP.png",
                    "title": "连打带挡",
                    "description": "悠米对英雄进行的攻击会为她提供(60 - 400 + 30%AP护盾值并回复(25-100+8% )法力。这个效果拥有 (18-6)秒的冷却时间。\n\n这个护盾会跟随悠米到被打破为止，并且在悠米有护盾时，将会保护被悠米附身的友方英雄。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YuumiQ.png",
                    "title": "摸鱼飞弹",
                    "description": "悠米召唤个飘忽不定的 飞弹，可对命中的首 个敌人造成(40/ 70/ 100/ 130/ 160/ 190+30%AP )魔法伤害。如果飞弹在命中前飞行了1秒，那么它会转而造成(45/85/ 125/ 165/205/245+40% AP)外加2%当前生命值的魔法伤害，并减速英雄20%，持续1秒。\n\n如果在附身时施放，那么悠米可以使用她的鼠标来控制飞弹。\n当前生命值百分( 2%/3.2%/4 4%/5.6%/6.8% /8% ]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YuumiW.png",
                    "title": "悠米出动！",
                    "description": "被动:悠米为她附身的友方英雄提升12/14/16/ 18/20+12%适应之力，并为她自身提供等额的适应之力。\n\n主动:悠米突进到一名友方英雄处并附身到其身上。当悠米附身时，她会跟随她搭档的移动并且不可被选取(防御塔除外)。\n\n悠米在受到定身类效果后，她的悠米出动!会进入一个长达5秒的冷却期。\n\n这个技能拥有 (10 - 0公)秒冷却时间，但悠米在准备就绪时，可以随时切换附身的队友，或是解除附身。\n\n悠米的技能将使用她附身友军的位置，而非她自身的。\n\n悠米将在该友军使用召唤师技能[传送]时解除附身。\n百分比适应之力[12%/14%/16%/18%/20%]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YuumiE.png",
                    "title": "旺盛精力",
                    "description": "悠米自身回复(70/ 105/ 140/ 175/210 +40%AP )生命值并获得(15+10% AP )%移动速度和25%攻击速度，持续3秒。如果悠米处在附身状态，这个技能会转而影响该友方英雄。\n攻击速度[25%/30%/35%/40%/45%)"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/YuumiR.png",
                    "title": "魔典终章",
                    "description": "悠米引导3.5秒，发射7道可造成(60/80/ 100 +20% Ap)魔法伤害的波纹。 任何被3道波纹命中的敌方英雄都会被禁锢1.75秒。\n\n在引导期间，悠米可以移动并施放悠米出动!和旺盛精力。\n\n后续波纹造成 (30 +10%AP)魔法伤害。 对单个敌人可能造成的最大伤害为(240 +80%Ap) "
                  }
                ],
                "data": {
                  "hp": "480.0000",
                  "hpperlevel": "70.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "400.0000",
                  "mpperlevel": "45.0000",
                  "mpregen": "10.0000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "55.0000",
                  "attackdamageperlevel": "3.1000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.0000",
                  "armor": "25.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "25.0000",
                  "spellblockperlevel": "0.3000",
                  "movespeed": "330.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "荆棘之兴",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Zyra.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big143000.jpg",
                "story": "婕拉诞生于一次远古的巫术灾难，她是获得了实体形态的自然之怒，是拥有诱人外观的植物与人类的混合体。她的每一个脚步都在点燃新的生命。在她眼里，瓦洛兰的众多凡人都只不过供她播种的猎物，用夺命的尖刺杀死他们也是一件不值一提的小事。虽然她的真正目的还是个谜，但婕拉一直在世界上流浪，肆意放纵自己占领土地的欲望，同时扼杀自己领地上的所有其他生命。",
                "usageTips": "在你的技能施放之后，再往你的技能路径上播下种子，可以确保你的种子能够百分百变成植物。",
                "battleTips": "你可以通过踩掉婕拉的种子来摧毁它们。如果她想在你要这么做时把种子长成植物，那么就在最后关头撤退。",
                "name": "婕拉",
                "defense": "3",
                "magic": "8",
                "difficulty": "7",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/ZyraP.png",
                    "title": "荆棘花园",
                    "description": "婕拉每(13-9)秒就会在她周围生成持续30秒的种子，她同一时间可存在8颗已种植的种子。如果一名敌方英雄踩到了种子，那么种子会阵亡。婕拉躲在草丛里时不会生成种子。\n这些种子可通过她的其他技能来使用。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZyraQ.png",
                    "title": "致命棘刺",
                    "description": "婕拉使稠密的藤蔓们扩散并爆炸成尖刺，造成(60/95/130/165/200 +60%AP)魔法伤害。\n如果这个技能在一颗种子附近施放，那么这颗种子就会长成荆棘喷射者，造成(20-100 +15%AP)魔法伤害，并持续8秒。荆棘喷射者拥有575攻击距离。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZyraW.png",
                    "title": "狂野生长",
                    "description": "婕拉种下一颗持续60秒的种子。这些种子在被敌方英雄踩到后会提供该英雄的真实视野，持续2秒，但仍然会被摧毁。\n这个技能有2层充能，充能时间为20秒。击杀一个敌人会使充能时间缩短20%。参与击杀英雄、大型野怪和击杀大型小兵会转而使充能时间缩短100%。\n种子充能速率：20/18/16/14/12\n法力消耗：1颗种子"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZyraE.png",
                    "title": "缠绕之根",
                    "description": "婕拉向前方放出藤蔓，禁锢(1/1.25/1.5/1.75/2)秒并造成(60/105/150/195/240 +50%AP)魔法伤害。\n如果这个技能在一颗种子附近，那么这颗种子就会长成藤蔓鞭击者，造成(20-100 +15%AP)魔法伤害并持续8秒。藤蔓鞭击者拥有400攻击距离，并且它们的攻击会施加持续2秒的25%减速。来自多个藤蔓鞭击者的减速可以至多叠加2次。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ZyraR.png",
                    "title": "绞杀之藤",
                    "description": "婕拉召唤自然之怒，长出一个扭曲的密林，造成(180/265/350 +70%AP)魔法伤害。在2秒后，藤蔓会向上方抽击，使敌人击飞1秒。\n婕拉在密林中的植物们会被激怒，重置持续时间，获得50%生命值并且攻击会造成50%额外伤害。"
                  }
                ],
                "data": {
                  "hp": "504.0000",
                  "hpperlevel": "79.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5000",
                  "mp": "418.0000",
                  "mpperlevel": "25.0000",
                  "mpregen": "13.0000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "53.3760",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.1100",
                  "armor": "29.0000",
                  "armorperlevel": "3.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "340.0000",
                  "attackrange": "575.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "星籁歌姬",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Seraphine.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big147000.jpg",
                "story": "萨勒芬妮生在皮城，但父母都是祖安人。她能听到其他人的灵魂——整个世界都在对她唱歌，而她也回以自己的歌声。虽然她小的时候曾被这些声音压得不堪重负，但如今她从这些声音中汲取灵感，将混乱变为协奏。她为这对姊妹城市演唱，提醒这里的人们，他们并不孤单，他们凝聚起来会更强大，在她眼中，他们的潜力是无限的。",
                "usageTips": "萨拉芬尼的大招可以通过队友来延长",
                "battleTips": "萨拉芬尼没有任何逃命技能",
                "name": "萨勒芬妮",
                "defense": "0",
                "magic": "0",
                "difficulty": "0",
                "attack": "0",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Seraphine_Passive.EllipsisMage.png",
                    "title": "星光漫射",
                    "description": "萨勒芬妮每第三次释放的普通技能将会产生回响，以自动施放第二次。\n此外，萨勒芬妮会从她的友军身上汲取灵感，每当她施放一个技能时，都会在附近的每位友方英雄身上生成一个音符，(音符持续6秒并且每位友军可叠加至多4层)。每个音符都会使萨勒芬妮提升25攻击距离并造成额外的(5-20 +6%/7%/8%/9%AP)魔法伤害，之后音符会被消耗掉。\n音符会对非小兵的单位造成前一个音符的95%伤害。\n音符对小兵造成300%伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SeraphineQ.png",
                    "title": "清籁穿云",
                    "description": "萨勒芬妮放出一道清澈的音符，造成(55/70/85/100/115 +40%/45%/50%/55%/60%AP)魔法伤害，基于目标的已损失生命值而提升，至多可对25%生命值以下的目标造成(82.5/105/127.5/150/172.5 +60%/67.5%/75%/82.5%/90%AP)伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SeraphineW.png",
                    "title": "聚和心声",
                    "description": "萨勒芬妮用歌曲激励她附近的友方英雄，为他自己提供(20% +0.04*AP啊、)不断衰减的移动速度，为她的友军提供(8% +0.016*AP)%移动速度，并为每个人提供(60-120 +30%)护盾值，持续2.5秒。\n如果萨勒芬妮身上已有护盾，那么她会号召友军加入她，在2.5秒延迟后，附近的每有一位友方英雄，就会为他们回复(5 +1.125*AP)%已损失生命值。\n这个技能只会治疗一次，即使是双重施放。\n\n【新增】现在，萨勒芬妮给自己施加的护盾值提升50% (现在，护盾值为90-180 (基于等级) (+45%法术强度))"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SeraphineE.png",
                    "title": "增幅节拍",
                    "description": "萨勒芬妮释放出一道强力音波，对一条直线上的敌人造成(60/80/100/120/140 +35%/AP)魔法伤害和持续1秒的99%减速效果。\n对已被减速带敌人们造成禁锢效果，对已被定身的敌人们造成晕眩效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SeraphineR.png",
                    "title": "炫音返场",
                    "description": "萨勒芬妮登台演出，投射出一股令人着迷的力量，来造成(1.25/1.5/1.75)秒魅惑效果和(150/200/250 +60%AP)魔法伤害。\n任何被命中的英雄(包括友方英雄)都会成为表演的一部分，延长这个技能的距离。\n友方英雄会获得满层的音符。"
                  }
                ],
                "data": {
                  "hp": "0.0000",
                  "hpperlevel": "0.0000",
                  "hpregen": "0.0000",
                  "hpregenperlevel": "0.0000",
                  "mp": "0.0000",
                  "mpperlevel": "0.0000",
                  "mpregen": "0.0000",
                  "mpregenperlevel": "0.0000",
                  "attackdamage": "0.0000",
                  "attackdamageperlevel": "0.0000",
                  "attackspeed": "0.0000",
                  "attackspeedperlevel": "0.0000",
                  "armor": "0.0000",
                  "armorperlevel": "0.0000",
                  "spellblock": "0.0000",
                  "spellblockperlevel": "0.0000",
                  "movespeed": "0.0000",
                  "attackrange": "0.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "仙灵女巫",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Lulu.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big117000.jpg",
                "story": "璐璐是一位约德尔女巫，最著名的能力是召来梦境般的幻觉和奇异的小动物，小仙灵皮克斯就是她云游符文之地的伙伴。璐璐可以突发奇想地重塑现实，改变世界的本质结构，改造这个平凡的物质领域中任何让她感觉是限制的东西。虽然其他人对她的魔法颇有微词，往好听了说是一种异象，往难听了说是一种危害。但是璐璐始终认为，一点魔力的启发对任何人都没有坏处。",
                "usageTips": "【闪耀长枪】可以从古怪的角度发射出去，而这取决于你的指针位置——让你的指针靠近皮克斯和璐璐，会使它的作用范围发生极大改变。",
                "battleTips": "璐璐的仙灵所发出的飞弹可以被拦截——躲在你的小兵后面，来躲避仙灵额外的攻击。",
                "name": "璐璐",
                "defense": "5",
                "magic": "7",
                "difficulty": "5",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Lulu_PixFaerieCompanion.png",
                    "title": "皮克斯，仙灵伙伴",
                    "description": "皮克斯朝着璐璐正在攻击的目标发射一阵由3颗飞弹组成的弹幕，总共造成(15-117 +15%AP)魔法伤害。\n这些飞弹会自动寻敌，但会被其他单位所格挡。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuluQ.png",
                    "title": "闪耀长枪",
                    "description": "璐璐和皮克斯各自发射一个穿刺飞弹，对命中的第一个敌人造成(70/105/140/175/210 +50%AP)魔法伤害，并对后续的所有敌人造成(31/80/105/129/154 +35%AP)魔法伤害。\n每次释放此技能时，最多只会对每名敌人造成共计(80/115/150/185/220 +50%AP)伤害。\n\n【新增】现在，如果用两个飞弹命中目标，则会造成25%额外伤害\n【新增】现在，对小兵造成70%伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuluW.png",
                    "title": "奇思妙想",
                    "description": "对友军施放：目标友军会增长(30 +5%AP)移动速度和(20%/23.75%/27.5%/31.25%/35)攻击速度，持续(3/3.25/3.5/3.75/4)秒。\n对敌人施放：将一名敌方英雄变形(1.25/1.5/1.75/2/2.25)秒，让他无法攻击或施法，并将它的基础移动速度减少60。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuluE.png",
                    "title": "帮忙，皮克斯！",
                    "description": "对友军施放：命令皮克斯跳到一名友军身上，之后会跟随并协助目标(而不是璐璐)进行攻击，持续6秒。如果友军目标是英雄，那么皮克斯会为目标提供1层抵挡(80/120/160/200/240 +60%AP)伤害的护盾，持续2.5秒。\n对敌人施放：皮克斯对目标敌人造成(80/120/160/200/240 +40%AP)魔法伤害，之后会跟随并提供目标的真实视野，持续4秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LuluR.png",
                    "title": "狂野生长",
                    "description": "璐璐让她的友军变大，并击飞目标附近的敌人。在7秒的持续时间里，她的友军会获得(300/450/600 +50%AP)生命值加成，并使附近的敌军减速(30%/45%/60%)。"
                  }
                ],
                "data": {
                  "hp": "525.0000",
                  "hpperlevel": "74.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.6000",
                  "mp": "350.0000",
                  "mpperlevel": "55.0000",
                  "mpregen": "11.0000",
                  "mpregenperlevel": "0.6000",
                  "attackdamage": "47.0000",
                  "attackdamageperlevel": "2.6000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.2500",
                  "armor": "29.0000",
                  "armorperlevel": "3.7000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "星界游神",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Bard.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big432000.jpg",
                "story": "巴德是星界彼端的旅者，是奇缘巧遇的使者。他艰难地维护着宇宙的平衡，从而让生命能够抵御无情的混乱。符文之地有许多人传唱关于他的歌谣，内容里流露出对他超凡本质的猜想，而所有这些歌谣都会提及同一件事：这位星界游荡者总是会被强大的魔法圣物所吸引。一群木灵欢唱乐团始终围绕在巴德身边充当小帮手，他的行为绝不会被误会带有任何恶意，因为他永远都是在为更大的良善尽职尽责，只不过他的方式不为常人理解。",
                "usageTips": "收集调和之音来增强木灵们的攻击，是非常重要的，但不要怠慢了你的对线搭档！可以尝试带着另一个友军用你的【神奇旅程】在某条战线上来一次盛大登场。",
                "battleTips": "巴德的对手也可以通过他的【神奇旅程】进行传送。你可以尾随他一起进门，如果你认为安全的话。",
                "name": "巴德",
                "defense": "4",
                "magic": "5",
                "difficulty": "9",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Bard_Passive.png",
                    "title": "旅者的召唤",
                    "description": "木灵:巴德吸引小型灵体来强化他的普攻。随着巴德不断地收集调和之音，这些木灵会提供新的效果。\n\n-木灵目前造成40 (+0)魔法伤害。每8秒出现一个新的木灵，最多可同时存在1个木灵。\n\n调和之音:远古的调和之音会随机出现，用来让巴德收集。这些调和之音会提供经验值，12%的最大法力值，以及脱战状态下的移动速度加成层数。调和之音可存留10分钟。\n\n已收集0个调和之音再收集5个调和之音 ，就能让木灵的攻击附带持续1秒的25%减速效果并多造成12伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BardQ.png",
                    "title": "星界束缚",
                    "description": "巴德发射一束能量弹，对1或2名敌人造成( 80/125/ 170/215/260+0)魔法伤害。被命中的第一个目标会 被减速60%，持续1秒。\n\n如果能量弹命中了另一个敌人或墙体，则被命中的敌人都会被晕眩1秒。\n减速时长1/1.2/1.4/1.6/1.8 \n晕眩时长[ 1/1.2/1.4/1.6/1.8 ]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BardW.png",
                    "title": "游神圣坛",
                    "description": "巴德升起一座治疗圣坛，圣坛会为前来拜访的友军回复( 30/60/90/ 120/ 150+0)到(55/95/ 135/ 175/215+0)生命(在圣坛出现10秒时达到最大值)。圣坛还会为受影响的英雄提供30%的移动速度加成，这个加成会在1.5秒里持续衰减。\n\n巴德最多可同时拥有3座圣坛，圣坛会持续到被一名友方英雄拜访或被一名敌方英雄摧毁为止。\n\n处于激活状态的圣坛数: 0/3"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BardE.png",
                    "title": "神奇旅程",
                    "description": "巴德在附近的地形上开启一道单向传送带。友军和敌人都可以在靠近入口时通过右键点击来使用传送带，但友军的传送速度比敌人快33%。\n\n传送带会在10秒后消失。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BardR.png",
                    "title": "调和命运",
                    "description": "巴德朝目标区域发出一道魔法能量弧光。在弧光着陆后，目标区域内的所有英雄、小兵、野怪、防御塔都会进入凝滞状态，变得免疫伤害、不可被选取并且无法进行任何动作，持续2.5秒。\n\n就连史诗级野怪也会进入凝滞状态，尽管它们免疫控制效果。"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "89.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "350.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.4500",
                  "attackdamage": "52.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.0000",
                  "armor": "34.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "330.0000",
                  "attackrange": "500.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "琴瑟仙女",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Sona.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big37000.jpg",
                "story": "娑娜是德玛西亚的一流弦乐演奏家，“叆华”的优美和弦与多变曲调就是她唯一的语言。这种文雅的举止让她深得上流社会的宠爱，不过也有其他人怀疑她如魔咒般的旋律其实是在施放魔法——而魔法可是德玛西亚的大忌。娑娜始终对外人一言不发，但她最亲密的同伴却不知为何能完全理解她。娑娜弹拨的和声不仅能够安抚受伤的盟友，而且还能打击掉以轻心的敌人。",
                "usageTips": "在娑娜的光环激活时，要确保紧靠队友来提供效果，同时也要避免被敌人攻击到。",
                "battleTips": "看到娑娜时要散开，避免她施法让你和友军跳舞。",
                "name": "娑娜",
                "defense": "2",
                "magic": "8",
                "difficulty": "4",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Sona_Passive_Charged.png",
                    "title": "能量和弦",
                    "description": "娑娜每施放3次基础技能后，她的下次攻击就会造成额外的20~240(1-18级)（+20%*AP） 魔法伤害，并基于娑娜最后激活的乐章来附加特殊效果。\n\n- 英勇赞美诗：这次攻击造成额外的28~336(1-18级)（+28% *AP）魔法伤害。\n- 坚毅咏叹调：目标造成的伤害减少25%（ +0.04%*AP），持续3秒。\n- 迅捷奏鸣曲：目标被减速40% （+0.04%*AP）， 持续2秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SonaQ.png",
                    "title": "英勇赞美诗",
                    "description": "娑娜对相距最近的2个敌人造成40/70/100/130/160（+40%*AP）魔法伤害，优先选取英雄为目标。然后她会开始一段新的旋律。\n\n旋律：娑娜获得一个持续3秒的光环，可使友方英雄的下次攻击造成额外的10/15/20/25/30（+20%*AP）魔法伤害。每次施放期间，每为一位友方英雄提供这个效果，将返还娑娜30法力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SonaW.png",
                    "title": "坚毅咏叹调",
                    "description": "主动：娑娜为自身和一名附近的友方英雄回复30/50/70/90/110（+20%*AP）生命值，优先选择伤势最重的英雄。然后她会开始一段新的旋律。\n\n旋律：娑娜获得一个持续3秒的光环，可为友方英雄提供25/50/75/100/125（+30%*AP）护盾，持续1.5秒。每次施放期间，每为一位友方英雄提供这个效果，将返还娑娜30法力。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SonaE.png",
                    "title": "迅捷奏鸣曲",
                    "description": "主动：娑娜为自身提供20%（+2%*AP）移动速度，持续7秒或受到伤害为止。然后她会开始一段新的旋律。\n\n旋律：娑娜获得一个持续3秒的光环，可为友方英雄提供10%/11%/12%/13%/14%（+0.02%*AP）移动速度，持续3秒。每次施放期间，每为一位友方英雄提供这个效果，将返还娑娜30法力。\n\n娑娜的自身移动速度提升将总会持续至少3秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SonaR.png",
                    "title": "狂舞终乐章",
                    "description": "被动：娑娜的基础技能的基础冷却时间缩短10%/25%/40%。\n\n主动：娑娜弹出一段不可抗拒的音符，晕眩敌方英雄并迫使他们跳舞1.5秒，并造成150/250/350（+50%*AP）魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "482.3600",
                  "hpperlevel": "77.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "340.6000",
                  "mpperlevel": "45.0000",
                  "mpregen": "11.5000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "49.0000",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "2.3000",
                  "armor": "28.0000",
                  "armorperlevel": "3.3000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "325.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "风暴之怒",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Janna.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big40000.jpg",
                "story": "风暴是她的武器，符文之地是她的家园。神秘的迦娜是风元素的精灵，保护着祖安城内无依无靠的人们。有人相信她的诞生是源于符文之地水手们的祈愿，他们会祈祷友善的风伴他们渡过险恶的海域，战胜无情的风暴。后来她的眷顾和庇护被召唤到了祖安深处，在那里，迦娜成为了无助之人的希望灯塔。没人知道她会在何时何地出现，但大多数时候，她的到来都意味着援手。",
                "usageTips": "风暴之眼可以用于友方的防御塔。",
                "battleTips": "保留一个打断技能给迦娜的终极技能。",
                "name": "迦娜",
                "defense": "5",
                "magic": "7",
                "difficulty": "7",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Janna_Tailwind.png",
                    "title": "顺风而行",
                    "description": "迦娜和朝她移动的友军获得8%移动速度\n迦娜的攻击和和风守护会造成额外的6/9=(25%/35%额外移速)魔法伤害\n移动速度伤害收益会在10级时提升"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/HowlingGale.png",
                    "title": "飓风呼啸",
                    "description": "迦娜改变气压和温度，在目标区域召唤小型风暴，风暴体积随时间增大。她可以再次施放该技能来释放风暴。风暴会朝向施放方向飞行，对沿途的所有单位造成伤害和击飞效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/SowTheWind.png",
                    "title": "和风守护",
                    "description": "被动：当这个技能可用时，迦娜获得6/7/8/9/10( +2%)移动速度并处在幽灵状态。\n主动：迦娜的风灵打击一名敌人，造成2秒的24%/28%/32%/36%/40%( +6%)%减速和55/85/115/145/175(+50%)魔法伤害\n幽灵状态的单位无视其他单位的碰撞体积。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/EyeOfTheStorm.png",
                    "title": "风暴之眼",
                    "description": "迦娜为一个友军或防御塔提供在5秒里持续衰减的80/115/150/185/220(+70%AP)护盾值。在护盾存在时，目标会获得10//177.5/25/32.5/40(10%AP)攻击力。\n每当迦娜减速或浮空一名敌方英雄时，她就会使这个技能的冷却时间缩短20%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ReapTheWhirlwind.png",
                    "title": "复苏季风",
                    "description": "迦娜召唤一阵魔法季风，击退附近的敌人们然后在3秒里持续治疗附近的友军共300=(300+150%AP)生命值。移动或者使用技能都会提前结束这阵季风。\n击退无法将敌人推过墙体。\n每秒治疗100/150/200"
                  }
                ],
                "data": {
                  "hp": "500.0000",
                  "hpperlevel": "70.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "350.0000",
                  "mpperlevel": "64.0000",
                  "mpregen": "11.5000",
                  "mpregenperlevel": "0.4000",
                  "attackdamage": "46.0000",
                  "attackdamageperlevel": "1.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.9500",
                  "armor": "28.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "315.0000",
                  "attackrange": "550.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "天启者",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Karma.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big43000.jpg",
                "story": "要说哪位英雄能代表艾欧尼亚的精神传统，没有谁比卡尔玛更合适。她是一个古代灵魂在当代的化身，经历过无数次转世，每次获得新生都会继承以前的全部记忆，同时也被赐予常人无法理解的力量。她在最近一次遭遇危难之时倾尽全力引领她的人民，但她知道，要获得和平与和谐，就必须付出重大代价——既是对她自己，也是对她深爱的土地。",
                "usageTips": "聚能之炎鼓励激进型的打法。找机会把技能和普攻扔到你的对手身上，来降低梵咒的冷却时间，并保持攻击的态势。",
                "battleTips": "卡尔玛的被动技能，会在她用技能和普攻命中敌人时减少梵咒的冷却时间。不要给她白打你的机会。",
                "name": "卡尔玛",
                "defense": "7",
                "magic": "8",
                "difficulty": "5",
                "attack": "1",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Karma_Passive.png",
                    "title": "聚能之炎",
                    "description": "卡尔玛每用技能伤害到敌方英雄时，梵咒的冷却时间就会缩短(2-5)秒(普通攻击可缩短1秒)。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarmaQ.png",
                    "title": "心灵烈焰",
                    "description": "卡尔玛向前方发射一枚灵能法球。这枚法球会在命中第一个敌人时爆炸，造成90/135/180/225/270(+0.4*AP)魔法伤害和35%减速效果，持续1.5秒。\n梵咒增强—灵光闪耀：造成25/75/125/175(+0.3*AP)额外魔法伤害，并留下一圈火环，使敌人减速50%。在1.5秒后火环会喷发，对范围内的敌人造成35/140/245/350(+0.6*AP)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarmaSpiritBind.png",
                    "title": "坚定不移",
                    "description": "在卡尔玛和一个敌方英雄或野怪之间搭起一条灵链，提供真实视野并造成40/65/90/115/140(+0.45AP)魔法伤害。如果灵链在2秒后任未被破坏，那么目标英雄就会被束缚在原地1.4/1.55/1.7/1.85/2秒并受到额外的40/65/90/115/140(+0.45AP)魔法伤害。\n焚咒增效—焕发：卡尔玛治疗自身20%(+0.1*AP)的已损失生命值。如果灵链未被破坏或目标死亡，禁锢时长会提升0.5/0.75/1/1.25秒，并且卡尔玛治疗自身20%(+0.1*AP)的已损失生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarmaSolKimShield.png",
                    "title": "鼓舞",
                    "description": "目标友军获得一层持续2.5秒的护盾和持续1.5秒的40%移动速度加成。护盾能吸收80/120/160/200/240(+0.5*AP)伤害。\n梵咒增效—蔑视：护盾满溢着能量，吸收额外的25/80/135/190(+0.5*AP)伤害。她的目标周围的友方英雄会获得一层护盾，护盾生命值相当于初始目标的30%护盾值。所有获得了蔑视护盾的英雄并获得35%的移动速度加成，持续1.5秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/KarmaMantra.png",
                    "title": "梵咒",
                    "description": "卡尔玛在8秒内释放的下个技能会获得强化，造成额外特效。\n灵光闪耀：造成额外魔法伤害，并留下一圈火环，减慢敌人的速度并造成额外伤害。\n焕发：卡尔玛治疗自身的一部分已损失生命值。如果灵链没有被打破，那么禁锢时间变久并且卡尔玛会再次获得治疗效果。\n蔑视：护盾变强并且目标周围的友方英雄也会获得护盾和移动速度加成。"
                  }
                ],
                "data": {
                  "hp": "534.0000",
                  "hpperlevel": "95.0000",
                  "hpregen": "5.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "374.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "11.5000",
                  "mpregenperlevel": "0.5000",
                  "attackdamage": "53.5440",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.3000",
                  "armor": "26.0000",
                  "armorperlevel": "3.8000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "525.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "瓦洛兰之盾",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Taric.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big44000.jpg",
                "story": "塔里克是保护者星灵，用超乎寻常的力量守护着符文之地的生命、仁爱以及万物之美。塔里克由于渎职而被放逐，离开了祖国德玛西亚，前去攀登巨神峰寻找救赎，但他找到的却是来自星界的更高层的召唤。现在的塔里克与古代巨神族的神力相融合，以瓦洛兰之盾的身份，永不疲倦地警惕着阴险狡诈的虚空腐化之力。",
                "usageTips": "【正气凌人】的冷却缩减部分，让诸如【冰霜之心】、【冰脉护手】和【振奋盔甲】等冷却缩减装在塔里克身上尤显强大。",
                "battleTips": "塔里克的终极技能【R宇宙之辉】会在生效前有段很长的延迟时间。尽量快速做出判断，来看看是否能够脱离战斗或在这段延迟时间内击杀掉他的友军。",
                "name": "塔里克",
                "defense": "8",
                "magic": "5",
                "difficulty": "3",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Taric_Passive.png",
                    "title": "正气凌人",
                    "description": "每次释放技能会强化塔里克的下2次普工，以造成(21-89 +15%额外护甲)额外魔法伤害，使他的基础技能的冷却时间减少1秒，并且可以快速地连续攻击。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaricQ.png",
                    "title": "星光之触",
                    "description": "花费全部充能数来治疗附近的友方英雄每层充能提供30/60/90/120/150(+0.2*AP)(+1%最大生命值)治疗效果，最多在满层时提供30/60/90/120/150(+0.2*AP)(+1%最大生命值)治疗效果。\n最大充能层数：1/2/3/4/5\n[正气凌人]的强化攻击会提供1层[星光之触]充能。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaricW.png",
                    "title": "坚毅壁垒",
                    "description": "塔里克的技能还会从附近一名被[W坚毅壁]所保护的友方英雄处释放。\n被动：[W坚毅壁垒]可提升(塔里克10/11/12/13/14%的护甲)护甲\n主动：用[W坚毅壁垒]祝福一名友军，并为其武装上一层持续2.5秒的护盾，护盾可吸收的伤害值相当于目标8/9/10/11/12%的最大生命值。[W坚毅壁垒]可一直持续到选择一个新的目标为止。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaricE.png",
                    "title": "炫光",
                    "description": "准备释放一束星光，在1秒的延迟后，造成90/130/170/210/250(+0.5*AP)(+0.3*护甲)魔法伤害并使敌人晕眩1.25秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TaricR.png",
                    "title": "宇宙之辉",
                    "description": "在2.5秒延迟后，放射一道宇宙能量到附近的友方英雄身上，让他们免疫伤害2.5秒。"
                  }
                ],
                "data": {
                  "hp": "575.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "6.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "300.0000",
                  "mpperlevel": "60.0000",
                  "mpregen": "8.5000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "55.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.0000",
                  "armor": "40.0000",
                  "armorperlevel": "3.4000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "340.0000",
                  "attackrange": "150.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "魂锁典狱长",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Thresh.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big412000.jpg",
                "story": "暴虐又狡猾的锤石是一个来自暗影岛的亡灵，野心勃勃、不知疲倦。他曾经是无数奥秘的看守，在一种超越生死的力量下骨肉瓦解，而现在他则使用自己独创的钻心痛苦缓慢地折磨并击溃其他人，以此作为自己存在下去的手段。被他迫害的人需要承受远超死亡的痛苦，因为锤石会让他们的灵魂也饱尝剧痛，将他们的灵魂囚禁在自己的灯笼中，经受永世的折磨。",
                "usageTips": "锤石不用亲自击杀单位也可以收集灵魂。规划好你的地图走位，来尽可能多地接近死亡，从而让你最有效率地收集灵魂。",
                "battleTips": "锤石依靠收集灵魂来增加他的生存能力和伤害。在他想去收集灵魂时，给他点颜色看看。",
                "name": "锤石",
                "defense": "6",
                "magic": "6",
                "difficulty": "7",
                "attack": "5",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Thresh_Passive.png",
                    "title": "地狱诅咒",
                    "description": "锤石在走过已死敌人身边的时候，会收集他们的灵魂，同时永久地为他提供护甲和法术强度。不过，锤石升级时不会获得护甲。\n\n英雄和大型小兵总会掉落一个灵魂，而小型小兵只会有时掉落一个。 史诗级野怪掉落2个灵魂。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ThreshQ.png",
                    "title": "死亡判决",
                    "description": "锤石扔出他的镰刀，对命中的第个敌人造成(80/ 120/ 160/ 200/240  +0)魔法伤害，提供真实视野并将其拉向他，持续1.5秒。\n\n再次激活这个技能，会将锤石拉向被捆绑的敌人。\n\n当[死亡判决]命中敌人时，它的冷却时间会减少3秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ThreshW.png",
                    "title": "魂引之灯",
                    "description": "锤石朝目标地点扔出灯笼。如果灯笼附近的一名友军点击了灯笼，则他会将灯笼捡起来，然后锤石会将该友军和灯笼起拉回身边。\n\n灯笼会为锤石和靠近灯笼的一名友军提供一层持续4秒、 可吸收最多( 60/ 100/ 140/ 180/220+0)伤害的护盾。护盾的生命值会随着锤石收集的灵魂数而增长。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ThreshE.png",
                    "title": "厄运钟摆",
                    "description": "被动:锤石的普攻造成0到( 100% / 125%/ 150%/ 175% / 200% +0)额外魔法伤害，该伤害会在非攻击状态下逐渐提升(相当于已收集的灵魂数加上最多100%的总攻击力)。\n\n主动:沿着从锤石身后到他面前的一条直线造成(65/95/ 125/ 155/ 185+0)魔法伤害。被命中的敌人会被推向挥舞的方向，然后被减速20%，持续1秒。\n\n面朝敌人施法会将敌人推走。背对敌人施法会将敌人拉近。\n伤害减速[20%/ 25% / 30%/ 35% / 40%]"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/ThreshRPenta.png",
                    "title": "幽冥监牢",
                    "description": "锤石制造一个环绕自身的幽冥监牢。穿过墙壁的敌方英雄会遭受( 250/ 400/550+0)魔法伤害，并被减速99%，持续2秒，同时打破该面墙壁。\n\n一旦有一面墙壁被打破，剩余墙壁就不会造成伤害，并且减速时长减半。在技能作用期间，一个敌人不会同时受到多个墙壁的影响。"
                  }
                ],
                "data": {
                  "hp": "560.5200",
                  "hpperlevel": "93.0000",
                  "hpregen": "7.0000",
                  "hpregenperlevel": "0.5500",
                  "mp": "273.9200",
                  "mpperlevel": "44.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "56.0000",
                  "attackdamageperlevel": "2.2000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "3.5000",
                  "armor": "28.0000",
                  "armorperlevel": "0.0000",
                  "spellblock": "30.0000",
                  "spellblockperlevel": "0.5000",
                  "movespeed": "335.0000",
                  "attackrange": "450.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "深海泰坦",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Nautilus.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big111000.jpg",
                "story": "早在比尔吉沃特立起第一座码头的时候，就有一个孤独的传说。身着铁甲的巨人诺提勒斯在蓝焰岛附近的黑暗水域中徘徊。他心里记恨着一桩不可原谅的背叛，毫无预警地出手。他甩动巨大的船锚，拯救落难的可怜虫，或是将贪婪的人拖进末日。据说，没有缴“比尔吉沃特什一税”的人就是他的目标。他会带着他们和自己一起沉入波涛——相当于一个铁板钉钉的提醒，没人能逃脱深海的制裁。",
                "usageTips": "在抓人的时候，可以用【Q疏通航道】来瞄准附近的地形，接着使用【E暗流涌动】，以获取更高的命中率。",
                "battleTips": "如果诺提勒斯贴着你放【E暗流涌动】的话，最好先呆在原地一段时间再逃跑。逃得太早的话，会跑到第二阶段的爆炸区域，使你受到额外的伤害以及减速效果。",
                "name": "诺提勒斯",
                "defense": "6",
                "magic": "6",
                "difficulty": "6",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Nautilus_StaggeringBlow.png",
                    "title": "排山倒海",
                    "description": "诺提勒斯对一名目标发起的第一次攻击会造成(8-100+100%AD)物理伤害并将其禁锢(0.8-1.5秒)。\n这个特效只能在6秒内对相同目标生效一次。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NautilusAnchorDrag.png",
                    "title": "疏通航道",
                    "description": "诺提勒斯向前猛掷他的船锚。如果它命中了一个敌方单位，诺提勒斯就会将他自己和目标吸到一起，同时造成(70/115/160/205/250 +90%AP)魔法伤害，并将目标暂时晕眩。如果它命中了地形，诺提勒斯则会把自己拉到船锚处。\n如果诺提勒斯勾中了地形，那么这么技能的冷却时间会降低50，并且返还50%法力消耗。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NautilusPiercingGaze.png",
                    "title": "泰坦之怒",
                    "description": "诺提勒斯获得一层持续6秒的(45/55/65/75/85 +(9%/10%/11%/12%/13%生命值))护盾。当护盾存在时，诺提勒斯的攻击会在2秒里对目标及目标周围的敌人持续造成共(30/40/50/60/70 +20%AP)额外魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NautilusSplashZone.png",
                    "title": "暗流涌动",
                    "description": "诺提勒斯在他身边生成3道爆炸波纹，每道爆炸波纹都会对范围内的敌人们造成(55/85/115/145/175 +30%AP)魔法伤害并使他们减速(30%/35%/40%/45%/50%)，在1.25秒里持续衰减。\n后续爆炸波纹造成的伤害减少50%。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/NautilusGrandLine.png",
                    "title": "深海冲击",
                    "description": "诺提勒斯发射一道冲击波来追击一名敌方英雄，造成(150/275/400 +80%AP)魔法伤害，击飞目标并将其晕眩(1/1.5/2)秒。冲击波还会击飞和晕眩沿途经过的其他敌人，并造成(125/175/225 +40%AP)魔法伤害。"
                  }
                ],
                "data": {
                  "hp": "576.4800",
                  "hpperlevel": "86.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "400.0000",
                  "mpperlevel": "47.0000",
                  "mpregen": "8.6260",
                  "mpregenperlevel": "0.5000",
                  "attackdamage": "61.0000",
                  "attackdamageperlevel": "3.3000",
                  "attackspeed": "0.7060",
                  "attackspeedperlevel": "1.0000",
                  "armor": "39.0000",
                  "armorperlevel": "3.7500",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "325.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "曙光女神",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Leona.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big89000.jpg",
                "story": "蕾欧娜是被灌注了烈阳之火的烈阳教派圣殿武士，用天顶之刃和破晓之盾守护着巨神峰。她的皮肤闪烁着星火，她体内天界星灵的力量透过她的双眼炯炯燃烧。蕾欧娜身披金色铠甲，背负着沉重的上古知识，为一些人带来启示，为另一些人带去死亡。",
                "usageTips": "带头冲锋，在友军攻击目标前用日光标记目标。",
                "battleTips": "当蕾欧娜释放日蚀时，你有3秒的时间躲避其造成的伤害。",
                "name": "蕾欧娜",
                "defense": "8",
                "magic": "3",
                "difficulty": "4",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/LeonaSunlight.png",
                    "title": "日光",
                    "description": "蕾欧娜的各个技能会标记敌人1.5秒，其他友方英雄在对被标记的敌人造成伤害时，会消耗该印记以造成额外的25~144(1-18级 每级+7)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeonaShieldOfDaybreak.png",
                    "title": "破晓之盾",
                    "description": "蕾欧娜的下次攻击会晕眩目标1秒，并造成额外的10/35/60/85/110（+30%*AP）魔法伤害。\n\n必须在6秒内打出这次攻击，否则它就会消退。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeonaSolarBarrier.png",
                    "title": "日蚀",
                    "description": "蕾欧娜举起她的盾牌，使即将到来的伤害降低8/12/16/20/24并获得20/25/30/35/40 （+20%*额外护甲）护甲和20/25/30/35/40（+20%*额外魔抗）魔法抗性， 持续3秒。之后，她的盾牌会爆炸，对附近的敌人造成60/95/130/165/200（+40%*AP）魔法伤害。如果她命中了至少一名敌人，那么她会将护甲和魔法抗性加成延续额外的3秒。\n\n伤害降低效果无法将伤害降至50%以下。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeonaZenithBlade.png",
                    "title": "天顶之刃",
                    "description": "蕾欧娜用光明之剑进行猛刺，造成50/90/130/170/210（+40%*AP）魔法伤害。最后一个被命中的敌方英雄将会被禁锢0.5秒，并且蕾欧娜将会冲向该英雄。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/LeonaSolarFlare.png",
                    "title": "日炎耀斑",
                    "description": "蕾欧娜召唤一道太阳光束，造成100/175/250（+80%*AP）魔法伤害并使敌人减速80%，持续1.5秒。处于区域正中的敌人会受到晕眩效果而非减速效果。"
                  }
                ],
                "data": {
                  "hp": "576.1600",
                  "hpperlevel": "87.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.8500",
                  "mp": "302.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "60.0400",
                  "attackdamageperlevel": "3.0000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "2.9000",
                  "armor": "47.0000",
                  "armorperlevel": "3.6000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "蒸汽机器人",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Blitzcrank.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big53000.jpg",
                "story": "布里茨是来自祖安的一个巨大的、几乎坚不可摧的机械体，最初被制造出来的目的是为了处理有毒废料。然而他觉得自己存在的意义太过狭隘，于是就改装了自己的形态，以便更好地效力于地沟区的孱弱人群。布里茨无私地使用自己的力量和钢铁之躯保护其他人，伸出长长的机械援手，或者发出能量脉冲，制服任何带来麻烦的人。",
                "usageTips": "使用布里茨的抓取将一名敌人拉入你们的防御塔射程内，继而一记能量铁拳可以让防御塔对他们进行多发射击。",
                "battleTips": "布里茨的被动技能【法力屏障】能够在他濒死时提供一个吸收伤害的护盾。",
                "name": "布里茨",
                "defense": "8",
                "magic": "5",
                "difficulty": "4",
                "attack": "4",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Blitzcrank_ManaBarrier.png",
                    "title": "法力屏障",
                    "description": "布里茨在生命值跌到30%以下时会获得(30%法力值)护盾值。这个效果的冷却时间为90秒。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RocketGrab.png",
                    "title": "机械飞爪",
                    "description": "布里茨射出他的右拳，将命中的第一个敌人拉拽向他并造成70/120/170/220/270(+100%AP)魔法伤害"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/Overdrive.png",
                    "title": "过载运转",
                    "description": "布里茨进行超级充能，获得持续衰减的70%/75%/80%/85%/90%/移动速度和30%/38%/46%/54%/62%攻击速度，持续5秒，持续期间过后，布里茨会减速30%，持续1.5秒。\n移动速度会在2.5秒里持续衰减至10%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/PowerFist.png",
                    "title": "能量铁拳",
                    "description": "布里茨给他的拳头充能，使他的下次攻击可以击飞并造成(200%AD)物理伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/StaticField.png",
                    "title": "静电力场",
                    "description": "被动：在这个技能可以使用时，闪电会充盈着布里茨的双拳，标记他攻击的敌人。在1秒后，被标记的敌人会被震击，受到50/100/150(+30%AP)魔法伤害。\n主动：布里茨进行过度充能，对附近的敌人造成250/375/500(+100%AP)魔法伤害和0.75秒沉默效果。敌人的护盾也会被摧毁。\n敌人的护盾也会被摧毁。\n敌人可累计至多3层来自被动部分的标记。"
                  }
                ],
                "data": {
                  "hp": "582.6000",
                  "hpperlevel": "95.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "0.7500",
                  "mp": "267.2000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.5000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "61.5400",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6250",
                  "attackspeedperlevel": "1.1300",
                  "armor": "37.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "325.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "弗雷尔卓德之心",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Braum.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big201000.jpg",
                "story": "拥有大块肌肉和更大颗心脏的布隆，是弗雷尔卓德的一个受人爱戴的英雄。弗雷尔卓德北部的任何一家蜜酒坊里都有人会致敬他传奇般的强壮，据说他曾在一夜之间扫平一整片橡树森林，还曾用拳头把一整座山打成碎石子。一扇附有魔法的秘库大门被他拿在手中当做盾牌，布隆在北方的冻土上漫游，小胡子勾勒出的微笑和他的肌肉块头一样大， 真诚友善地帮助所有危难之中的人。",
                "usageTips": "和你的队友一起叠加【震荡猛击】效果，鼓励他们对被标记的目标进行普攻。",
                "battleTips": "布隆必须用【寒冬之咬】或普攻来施加第一层【震荡猛击】效果。如果你被标记了，那就在承受另外的3次攻击之前退出战斗区域，从而避免被击晕。",
                "name": "布隆",
                "defense": "9",
                "magic": "4",
                "difficulty": "3",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Braum_Passive.png",
                    "title": "震荡猛击",
                    "description": "布隆的攻击会施加持续4秒的震荡猛击。带有震荡猛击的敌人会被任何友军的攻击施加额外的层数。\n在4层时，目标会被晕眩(1.25-1.75)秒，并受到(26-196)魔法伤害。在接下来的(8-6)秒里，该目标不会被施加层数，但布隆的攻击会对其造成额外的(5-39)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BraumQ.png",
                    "title": "寒冬之咬",
                    "description": "布隆驱动盾牌里的冷冻冰块，对命中的第个敌人造成(75/125/175/225/275 +2.5%生命值)魔法伤害和在2秒里持续衰减的70%减速。\n施加一层震荡猛击。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BraumW.png",
                    "title": "挺身而出",
                    "description": "布隆跃向一名友方英雄或小兵。在抵达时，布隆为目标提供(10/14/18/22/26+ 12%额外护甲)护甲和(10/14/18/22/26 +12% 额外魔法抗性)魔法抗性，持续3秒。布隆为他自己提供(10/14/18/22/26 +36%额外护甲)护甲和(10/14/18/22/26 +36% 额外魔法抗性)魔法抗性，持续时间相同。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BraumE.png",
                    "title": "坚不可摧",
                    "description": "布隆将他的盾牌举起(3/3.25/3.5/3.75/4)秒，拦截来自选定方向的所有敌方弹体，使其命中布隆然后被摧毁。布隆格挡的首个弹体不会造成伤害，并且后续弹体造成的伤害降低(30%/32.5%/35%/37.5%/40%)。\n布隆在举盾时获得10%移动速度。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/BraumRWrapper.png",
                    "title": "冰川裂隙",
                    "description": "布隆猛砸地面并向前发出一道裂隙，来击飞沿途和布隆身边的敌人，并造成(150/300/450 +60%AP)魔法伤害。被命中的第一个目标会被击飞0.3到(1.25/1.5)秒，取决于其与布隆之间的距离。被命中的其他目标也会被击飞0.3秒。\n裂隙还会留下一个持续4秒的地带，可使敌人减速(40%/50%/60%)。"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "98.0000",
                  "hpregen": "8.5000",
                  "hpregenperlevel": "1.0000",
                  "mp": "310.6000",
                  "mpperlevel": "45.0000",
                  "mpregen": "6.0000",
                  "mpregenperlevel": "0.8000",
                  "attackdamage": "55.3760",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6440",
                  "attackspeedperlevel": "3.5000",
                  "armor": "47.0000",
                  "armorperlevel": "4.0000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "125.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "河流之王",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/TahmKench.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big223000.jpg",
                "story": "塔姆·肯奇在历史上有许多不同的名字，他游历于符文大陆的各条水道，用大意之人的悲惨喂养自己贪得无厌的食欲。虽然他的外表可能富有古怪的魅力和得意，但他在物质领域的漫游只是为了寻找毫无戒心的猎物。他的舌头像巨大的鞭子，即使是全副武装的重甲士兵也会被他从十几步以外的距离击晕，而如果跌入了他隆隆作响的肚子里，就相当于掉进了九死一生的绝命深渊。",
                "usageTips": "在辅助时，你最重要的一个功能就是把脆皮友军带到安全的地方。要注意【大快朵颐】的距离和冷却时间，并进行相应的走位！",
                "battleTips": "当你看到塔姆使用【厚实表皮】获取护盾时，要记住他是以无法获得这个技能的治疗效果为代价的。只有在【厚实表皮】冷却完毕后，他才会积累新的灰色生命值。利用这点来创造优势吧！",
                "name": "塔姆",
                "defense": "9",
                "magic": "6",
                "difficulty": "5",
                "attack": "3",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/TahmKench_P.png",
                    "title": "培养品味",
                    "description": "普攻会造成(4%生命值)额外魔法伤害，对英雄时最多可叠加至3层。\n在3层时，对这个英雄施放的[巨舌鞭笞]和[大快朵颐]获得强化效果。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TahmKenchQ.png",
                    "title": "巨舌鞭笞",
                    "description": "对命中的第一个敌人造成(80/130/180/230/280 +70%AP)魔法伤害，并使目标减速(30%/40%/50%/60%/70%)，持续3秒。身上叠了3层[培养品味]的英雄还会被晕眩2秒。\n在你的舌头飞在半空中时施放[大快朵颐]，可以让你吞噬远处的野怪/小兵。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TahmKenchW.png",
                    "title": "大快朵颐",
                    "description": "吞噬一个目标4秒(吞噬敌方英雄时的时长减半)。敌人会受到相当于目标(60/105/150/195/240 +(11%)+(2%)最大生命值的魔法伤害)(对野怪的最大伤害值为：500)。\n敌方英雄：需要身上叠了3层[培养品味]效果才能被吞噬。当含着任一英雄时，塔姆会被减速95%且被缚地。\n小兵和野怪：再次激活来喷吐口里的单位，对命中的目标造成((60/105/150/195/240 +60%AP)魔法伤害。\n在对敌方使用时，返回此技能一半的冷却时间和法力消耗。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TahmKenchE.png",
                    "title": "厚实表皮",
                    "description": "被动：在这个技能未开启时时所承受伤害的45%会转化为灰色生命值。一旦灰色生命值开始衰减，那么灰色生命值的(30%—100%)就会转回正常生命值。\n主动：将你所有灰色生命值转化为一个持续2秒的护盾。\n伤害转灰色生命值的转化率：45%/50%/55%/60%/65%"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/TahmKenchNewR.png",
                    "title": "深渊潜航",
                    "description": "开始进入最多6秒的引导。在这段时间里，一名友方英雄可以通过右键点击塔姆来加入这次潜航。此外，可以再次激活这个技能来独自潜航。再次激活这个技能或一名友方英雄加入进来，那么塔姆就会朝着目标区域进行潜航。\n任何来自英雄的伤害都会打断引导。\n在与英雄作战的友方英雄无法 搭乘[深渊潜航]。\n“小子，世界就是一条河流，而我是它的国王。没有哪里是我没去过的；没有哪里是我不能去的。”\n距离：2500/5500/8500"
                  }
                ],
                "data": {
                  "hp": "600.0000",
                  "hpperlevel": "100.0000",
                  "hpregen": "6.5000",
                  "hpregenperlevel": "0.5500",
                  "mp": "325.0000",
                  "mpperlevel": "40.0000",
                  "mpregen": "8.0000",
                  "mpregenperlevel": "1.0000",
                  "attackdamage": "56.0000",
                  "attackdamageperlevel": "3.2000",
                  "attackspeed": "0.6580",
                  "attackspeedperlevel": "2.5000",
                  "armor": "47.0000",
                  "armorperlevel": "3.5000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "175.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              },
              {
                "title": "幻翎",
                "avatar": "https://game.gtimg.cn/images/lol/act/img/champion/Rakan.png",
                "banner": "https://game.gtimg.cn/images/lol/act/img/skin/big497000.jpg",
                "story": "对洛特兰部落而言，瓦斯塔亚的洛是臭名远播的捣蛋鬼，同时也是有史以来最为出色的战舞舞者。他风流潇洒，魅力无穷，同时却令人难以捉摸。在艾欧尼亚高地的居民眼中，“洛”这个名字一旦出现，就一定会带来热闹非凡的节日庆典、热火朝天的狂欢派对和杂乱无章的音乐。很少人知道，这个精力充沛的浪荡子和叛逆的霞是一对儿，而他的一举一动就是为了全力配合她的行动。",
                "usageTips": "洛需要附近有友军才能让他的技能运作起来。 洛的突进速度可通过他的移动速度来提升。使用爆表的速度来让你的敌人大吃一惊！ 当你放任危险到来时，就知道危险有多好玩了。",
                "battleTips": "只要拥有快速生效的控制技能，就能很好地应对洛。 如果洛的附近没有友军，那么他的机动能力就会被严重抑制。所以尽量在洛落单时抓他。",
                "name": "洛",
                "defense": "4",
                "magic": "8",
                "difficulty": "5",
                "attack": "2",
                "skills": [
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/passive/Rakan_P.png",
                    "title": "异色羽裳",
                    "description": "每过 (40.0 - 14.5)秒， 洛会获得(33 - 254 +90% AP)护盾值。每当洛的攻击和技能在命中敌方英雄时，就会减少这个技能1秒冷却时间。\n\n情人跃-洛和霞可以一起回城。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RakanQ.png",
                    "title": "微光飞翎",
                    "description": "洛掷出一支魔羽，对命中的第一个敌人造成(70/ 115/ 160/ 205/250 +60% AP)魔法伤害。\n\n如果魔羽命中了一个英雄或史诗级野怪，洛会在3秒后或触碰到-名友方英雄后，为自己和附近的友方回复(18 - 120公 + 70%AP)生命值。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RakanW.png",
                    "title": "盛大登场",
                    "description": "洛进行冲刺，然后旋至空中，击飞1秒并造成 （70/125/ 180/ 235/290 + 70%AP)魔法伤害。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RakanE.png",
                    "title": "轻舞成双",
                    "description": "洛冲向一名友方英雄，为其提供40= (40 +80% AP)护盾值，持续3秒。洛可以在5秒内再次施放次这个技能。\n\n施放距离会在对霞施放时提升。"
                  },
                  {
                    "abilityIconPath": "https://game.gtimg.cn/images/lol/act/img/spell/RakanR.png",
                    "title": "惊鸿过隙",
                    "description": "洛获得75%移动速度，持续4秒。洛触碰到敌人时会造成 ( 100/ 200/ 300 +50% AP)魔法伤害，并在首次命中时造成持续1秒的魅惑效果。被触碰的首个敌方英雄，会为洛提供持续衰减的150%移动速度。\n魅惑时长[1/1.25/1.5 ]"
                  }
                ],
                "data": {
                  "hp": "540.0000",
                  "hpperlevel": "85.0000",
                  "hpregen": "5.0000",
                  "hpregenperlevel": "0.5000",
                  "mp": "315.0000",
                  "mpperlevel": "50.0000",
                  "mpregen": "8.7500",
                  "mpregenperlevel": "0.5000",
                  "attackdamage": "62.0000",
                  "attackdamageperlevel": "3.5000",
                  "attackspeed": "0.6350",
                  "attackspeedperlevel": "3.0000",
                  "armor": "32.0000",
                  "armorperlevel": "3.9000",
                  "spellblock": "32.1000",
                  "spellblockperlevel": "1.2500",
                  "movespeed": "335.0000",
                  "attackrange": "300.0000",
                  "crit": "0.0000",
                  "critperlevel": "0.0000"
                }
              }
            ]
          }
        ]
        for (let cat of rawData){
            //找到当前数据在数据库中对应的分类
            const category = await Category.findOne({
                name: cat.name
            })
            cat.heroes = cat.heroes.map(hero=>{
                hero.categories = [category]
                return hero
            })
            await Hero.insertMany(cat.heroes)
        }
        res.send (await Hero.find())
    })
    //装备录入接口
    router.get('/items/init', async(req,res)=>{
        await Item.deleteMany({})
        const rawData =[
            {
                "name":"消耗品",
                "items":[
                    {
                        "itemId": "2052",
                        "name": "魄罗佳肴",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2052.png",
                        "description": "用一勺美味来招待附近的一只魄罗。",
                        "plainText": "用一勺美味来招待附近的一只魄罗。",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "2403",
                        "name": "小兵去质器",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2403.png",
                        "description": "主动 - 消耗：击杀目标线上小兵(10秒)。",
                        "plainText": "",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "2419",
                        "name": "始动的秒表",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2419.png",
                        "description": "在对局开始14分钟后变为一个秒表。你每次参与击杀后都会使这个时间点提前2分钟。该秒表可在合成进阶装备时贡献250金。(【秒表】一般会贡献650金)",
                        "plainText": "",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3330",
                        "name": "草间人",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3330.png",
                        "description": "不能售出主动 - 饰品：放置一个持续130 - 300 (基于等级)秒的草间人，并且现身于敌人视野时会对敌人做出和费德提克一样的动作。每115 - 30 (基于等级)秒储存1层充能，至多可储存2层充能。敌方英雄在接近草间人时会将其激活，使该草间人假装进行一个随机的动作，然后该草间人会四分五裂。",
                        "plainText": "周期性地放置一个侦查守卫",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3340",
                        "name": "侦查守卫",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3340.png",
                        "description": "主动 - 饰品：在地上放置一个可持续90 - 120秒的侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存2个侦察守卫，每240 - 120秒生成1个新的守卫。 ",
                        "plainText": "周期性地放置一个侦查守卫",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3363",
                        "name": "远见改造",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3363.png",
                        "description": "主动 - 饰品：显形4000码内的一个区域并放置一个可见且脆弱的守卫。友军无法将这个守卫作为召唤师技能或技能的目标(198 - 99秒冷却时间)。",
                        "plainText": "提升施放距离并揭示目标区域",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3364",
                        "name": "神谕透镜",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3364.png",
                        "description": "主动 - 饰品：扫描你的周围，预警隐藏的敌方单位，并使隐形的陷阱显形，并使附近的敌方侦察守卫显形(并暂时失效)10秒(90 - 60秒冷却时间)。",
                        "plainText": "持续期间可瘫痪附近隐形的守卫和陷阱",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3400",
                        "name": "你也有份",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3400.png",
                        "description": "主动 - 消耗：提供0金币。在派克用终极技能处决一名敌方英雄时，给予一名友军的额外金币。如果没有友军参与到这次击杀中，那么派克会留下这份金币！",
                        "plainText": "",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3513",
                        "name": "先锋之眼",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3513.png",
                        "description": "主动 - 消耗：在1秒后碾碎先锋之眼，同时召唤出峡谷先锋来对敌方防御塔进行攻坚。虚空掠影：【先锋之眼】的持有者拥有强化回城。【先锋之眼】如果在240秒内没有被使用，就会迷失到虚空中去。",
                        "plainText": "先锋之眼——来自虚空的礼物。",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3599",
                        "name": "卡莉斯塔的黑色长矛",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3599.png",
                        "description": "主动 - 消耗：选择一位友军进行绑定，让该友军在该局的剩余时间里成为誓约者。誓约者在你附近时，你和誓约者彼此都会得到强化。",
                        "plainText": "卡莉丝塔用来与誓约者进行连接的长矛。",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3901",
                        "name": "随意开火",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3901.png",
                        "description": "加农炮幕会以持续提升的速率发射（在持续期间额外发射6波）需要500银蛇币",
                        "plainText": "加农炮幕获得额外的波数",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3902",
                        "name": "死亡之女",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3902.png",
                        "description": "加农炮幕在炮幕的中心额外发射一颗巨型炮弹，造成300%真实伤害并使范围内的敌人减速60%，持续1.5秒。需要500银蛇币",
                        "plainText": "加农炮幕发射一颗巨型加农炮弹",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "3903",
                        "name": "鼓舞士气",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3903.png",
                        "description": "加农炮幕范围内的友军会获得30%移动速度，持续2秒。需要500银蛇币",
                        "plainText": "加农炮幕会使友军加速",
                        "sell": "0",
                        "total": "0"
                    },
                    {
                        "itemId": "2003",
                        "name": "生命药水",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2003.png",
                        "description": "主动 - 消耗：饮用药水，以在15秒里持续回复共150生命值。你最多可携带5瓶生命药水。",
                        "plainText": "饮用后持续回复生命值",
                        "sell": "20",
                        "total": "50"
                    },
                    {
                        "itemId": "2010",
                        "name": "永续意志夹心饼干",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2010.png",
                        "description": "主动 - 消耗：食用饼干，以在5秒里持续回复共10%的已损失生命值和法力值。消耗或出售一块饼干，都会永久提供50最大法力值。 ",
                        "plainText": "",
                        "sell": "30",
                        "total": "75"
                    },
                    {
                        "itemId": "2055",
                        "name": "控制守卫",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2055.png",
                        "description": "主动 - 消耗：放置一个强大的控制守卫来提供附近区域的视野。这个设备还会使隐形的陷阱显形，使伪装的敌人们显形，并使敌方守卫显形和失效。 你至多可以携带2个控制守卫。控制守卫不会使其它控制守卫失效。",
                        "plainText": "用来使范围内的守卫和隐形陷阱失效。",
                        "sell": "30",
                        "total": "75"
                    },
                    {
                        "itemId": "2138",
                        "name": "钢铁合剂",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2138.png",
                        "description": "主动 - 消耗：饮用以获得300生命值，25%韧性，并提升英雄体型，持续3分钟。在合剂生效期间，移动时会在身后留下一条道路，友方英雄在这条道路上会提升15%移动速度。饮用一瓶不同的合剂将替换掉现有合剂的效果。",
                        "plainText": "临时提升防御属性。留下一条让友军跟随的道路。",
                        "sell": "200",
                        "total": "500"
                    },
                    {
                        "itemId": "2139",
                        "name": "巫术合剂",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2139.png",
                        "description": "主动 - 消耗：饮用以获得50法术强度和15%法力回复，持续3分钟。在合剂生效期间，对英雄或防御塔造成伤害时会造成25额外真实伤害(5秒冷却时间)。需要英雄等级达到9级或以上才能购买。【巫术合剂】的真实伤害效果在攻击防御塔时没有冷却时间。饮用一瓶不同的合剂将替换掉现有合剂的效果。",
                        "plainText": "临时提供法术强度，并对英雄和防御塔造成额外伤害。",
                        "sell": "200",
                        "total": "500"
                    },
                    {
                        "itemId": "2140",
                        "name": "愤怒合剂",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2140.png",
                        "description": "主动 - 消耗：饮用以获得30攻击力和15%物理吸血(对英雄)，持续3分钟。饮用一瓶不同的合剂将替换掉现有合剂的效果。",
                        "plainText": "临时提升攻击力，并在对英雄造成物理伤害时治愈自身。",
                        "sell": "200",
                        "total": "500"
                    }
                ]
            },
            {
                "name":"基础",
                "items":[
                    {
                        "itemId": "1006",
                        "name": "治疗宝珠",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1006.png",
                        "description": "50%基础生命回复",
                        "plainText": "略微提升生命回复",
                        "sell": "105",
                        "total": "150"
                    },
                    {
                        "itemId": "2031",
                        "name": "复用型药水",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2031.png",
                        "description": "主动 - 消耗：消耗一层充能，以在12秒里持续回复共125生命值。最多可持有2层充能，并且每当你造访商店时，都会将充能数填满。",
                        "plainText": "持续回复生命。在商店填满充能。",
                        "sell": "60",
                        "total": "150"
                    },
                    {
                        "itemId": "1004",
                        "name": "仙女护符",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1004.png",
                        "description": "50%基础法力回复",
                        "plainText": "略微提升法力回复",
                        "sell": "175",
                        "total": "250"
                    },
                    {
                        "itemId": "1001",
                        "name": "鞋子",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1001.png",
                        "description": "25移动速度",
                        "plainText": "略微提升移动速度",
                        "sell": "210",
                        "total": "300"
                    },
                    {
                        "itemId": "1029",
                        "name": "布甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1029.png",
                        "description": "15护甲",
                        "plainText": "略微提升护甲",
                        "sell": "210",
                        "total": "300"
                    },
                    {
                        "itemId": "1042",
                        "name": "短剑",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1042.png",
                        "description": "12%攻击速度",
                        "plainText": "略微提升攻击速度",
                        "sell": "210",
                        "total": "300"
                    },
                    {
                        "itemId": "2422",
                        "name": "有点神奇之鞋",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2422.png",
                        "description": "25移动速度提供额外的10移动速度。通过【有点神奇之鞋】合成的鞋类装备都会保持这个移动速度加成。",
                        "plainText": "",
                        "sell": "210",
                        "total": "300"
                    },
                    {
                        "itemId": "1027",
                        "name": "蓝水晶",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1027.png",
                        "description": "250法力",
                        "plainText": "提升法力值",
                        "sell": "245",
                        "total": "350"
                    },
                    {
                        "itemId": "1035",
                        "name": "灰烬小刀",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1035.png",
                        "description": "10%全能吸血(对野怪)烙印：对野怪造成伤害时会灼烧它们，在5秒里持续造成共(60 + 30%法术强度 + 5%额外攻击力 + 2%额外生命值)魔法伤害。挑战之途：在使用5次【惩戒】后，会将这件装备消耗掉，并将你的【惩戒】升级为挑战惩戒。挑战惩戒会标记英雄4秒。在此期间，你会在2.5秒里持续造成48 - 125(基于等级)额外真实伤害。你所受的来自被标记英雄的伤害降低20%。狩猎人：击杀大型野怪会提供额外经验值。取食于野：在野区或河道时，每秒至多回复8 - 18(基于等级)法力值。吞噬这件装备时会永久提供它的所有效果。如果你从小兵处获得的金币多于你从野怪处获得的金币，那么来自小兵的金币和经验将被大幅降低。群体攻击的治疗效果不会降低。 ",
                        "plainText": "",
                        "sell": "140",
                        "total": "350"
                    },
                    {
                        "itemId": "1036",
                        "name": "长剑",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1036.png",
                        "description": "10攻击力",
                        "plainText": "略微提升攻击力",
                        "sell": "245",
                        "total": "350"
                    },
                    {
                        "itemId": "1039",
                        "name": "冰雹刀刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1039.png",
                        "description": "10%全能吸血(对野怪)烙印：对野怪造成伤害时会灼烧它们，在5秒里持续造成共(60 + 30%法术强度 + 5%额外攻击力 + 2%额外生命值)魔法伤害。深寒之途：在使用5次【惩戒】后，会将这件装备消耗掉，并将你的【惩戒】升级为深寒惩戒。深寒惩戒对敌方英雄造成20 - 156真实伤害(基于等级)并窃取其20%移动速度，持续2秒。狩猎人：击杀大型野怪会提供额外经验值。取食于野：在野区或河道时，每秒至多回复8 - 18(基于等级)法力值。吞噬这件装备时会永久提供它的所有效果。如果你从小兵处获得的金币多于你从野怪处获得的金币，那么来自小兵的金币和经验将被大幅降低。群体攻击的治疗效果不会降低。 ",
                        "plainText": "提供对野怪的伤害和在野区中的法力回复。",
                        "sell": "140",
                        "total": "350"
                    },
                    {
                        "itemId": "1082",
                        "name": "黑暗封印",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1082.png",
                        "description": "15法术强度40生命值荣耀：每次击杀提供2层，每次助攻提供1层(至多可达10层)。每次死亡损失4层。畏惧：每层荣耀提供5法术强度。已获得的荣耀层数可在这件装备和梅贾的窃魂卷之间保存。",
                        "plainText": "提供法术强度和法力。在你击杀敌方英雄时提升威力。",
                        "sell": "140",
                        "total": "350"
                    },
                    {
                        "itemId": "1028",
                        "name": "红水晶",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1028.png",
                        "description": "150生命值",
                        "plainText": "提升生命值",
                        "sell": "280",
                        "total": "400"
                    },
                    {
                        "itemId": "1056",
                        "name": "多兰之戒",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1056.png",
                        "description": "15法术强度70生命值专注：攻击对小兵附带额外的5物理伤害。虹吸：击杀一名小兵时会回复6法力值。如果你无法回复法力值，那么会回复3生命值作为替代。",
                        "plainText": "施法型英雄的优秀起始装备",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3070",
                        "name": "女神之泪",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3070.png",
                        "description": "240法力专注：攻击对小兵附带额外的5物理伤害。法力积攒：用一个技能击中一个目标时，会消耗一层充能并提供3额外法力值，至多提供360额外法力值。如果目标是英雄，则该次提供的额外法力值翻倍。每8秒获得一层新的法力积攒充能(至多4层)。",
                        "plainText": "在花费法力时提升最大法力值",
                        "sell": "280",
                        "total": "400"
                    },
                    {
                        "itemId": "3850",
                        "name": "窃法之刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3850.png",
                        "description": "8法术强度10生命值50%基础法力回复2金币/10秒贡品：当附近有一名友方英雄时，用技能和普攻对敌方英雄或防御塔造成伤害时，会提供20金币。这个效果每30秒最多触发3次。任务：使用这个装备赚取500金币来将其转变为冰霜之牙，并提供主动 - 布置守卫。这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "通过对敌方英雄造成伤害来赚取金币并升级",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3851",
                        "name": "冰霜之牙",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3851.png",
                        "description": "15法术强度70生命值75%基础法力回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 贡品：当附近有一名友方英雄时，用技能和普攻对敌方英雄或防御塔造成伤害时，会提供20金币。这个效果每30秒最多触发3次。任务：使用这个装备赚取1000金币来将其转变为极冰碎片。这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3853",
                        "name": "极冰碎片",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3853.png",
                        "description": "40法术强度75生命值100%基础法力回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3854",
                        "name": "钢铁护肩",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3854.png",
                        "description": "通过在同伴并肩时处决小兵来赚取金币并升级。 任务：使用这个装备赚取500金币来将其转变为符钢肩甲，并提供主动-布置守卫。",
                        "plainText": "通过在同伴并肩时处决小兵来赚取金币并升级。 任务：使用这个装备赚取500金币来将其转变为符钢肩甲，并提供主动-布置守卫。",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3855",
                        "name": "符钢肩甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3855.png",
                        "description": "6攻击力100生命值50%基础生命回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 战利品：附近有一名友方英雄时，普攻会处决低于0%最大生命值的小兵。击杀一位小兵会为距离最近的友方英雄提供等额的击杀金币。这些效果每0秒获得一层充能(最多拥有0层充能)。任务：使用这个装备赚取1000金币来将其转变为白岩肩铠。这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3857",
                        "name": "白岩肩铠",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3857.png",
                        "description": "15攻击力250生命值100%基础生命回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3858",
                        "name": "圣物之盾",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3858.png",
                        "description": "通过在同伴并肩时处决小兵来赚取金币并升级。 任务：使用这个装备赚取500金币来将其转变为巨神峰圆盾，并提供主动-布置守卫。",
                        "plainText": "通过在同伴并肩时处决小兵来赚取金币并升级。 任务：使用这个装备赚取500金币来将其转变为巨神峰圆盾，并提供主动-布置守卫。",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3859",
                        "name": "巨神峰圆盾",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3859.png",
                        "description": "10法术强度100生命值50%基础生命回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 战利品：附近有一名友方英雄时，普攻会处决低于0%最大生命值的小兵。击杀一位小兵会为距离最近的友方英雄提供等额的击杀金币。这些效果每0秒获得一层充能(最多拥有0层充能)。任务：使用这个装备赚取1000金币来将其转变为山脉壁垒。这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3860",
                        "name": "山脉壁垒",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3860.png",
                        "description": "20法术强度250生命值100%基础生命回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3862",
                        "name": "幽魂镰刀",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3862.png",
                        "description": "5攻击力10生命值25%基础法力回复2金币/10秒贡品：当附近有一名友方英雄时，用技能和普攻对敌方英雄或防御塔造成伤害时，会提供20金币。这个效果每30秒最多触发3次。任务：使用这个装备赚取500金币来将其转变为鬼影新月，并提供主动 - 布置守卫。这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "通过对敌方英雄造成伤害来赚取金币并升级",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3863",
                        "name": "鬼影新月",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3863.png",
                        "description": "10攻击力60生命值50%基础法力回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 贡品：当附近有一名友方英雄时，用技能和普攻对敌方英雄或防御塔造成伤害时，会提供20金币。这个效果每30秒最多触发3次。任务：使用这个装备赚取1000金币来将其转变为黑雾巨镰。这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "3864",
                        "name": "黑雾巨镰",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3864.png",
                        "description": "20攻击力75生命值100%基础法力回复3金币/10秒主动 - 守卫：在地上放置一个侦察守卫，对敌方隐形但会为你的队伍提供附近区域的视野。至多可储存0个侦察守卫，并在访问商店时补满。 这件装备会在你击杀了过多数量的小兵时降低来自小兵的金币。",
                        "plainText": "",
                        "sell": "160",
                        "total": "400"
                    },
                    {
                        "itemId": "1052",
                        "name": "增幅典籍",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1052.png",
                        "description": "20法术强度",
                        "plainText": "略微提升法术强度",
                        "sell": "305",
                        "total": "435"
                    },
                    {
                        "itemId": "1033",
                        "name": "抗魔斗篷",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1033.png",
                        "description": "25魔法抗性",
                        "plainText": "略微提升魔法抗性",
                        "sell": "315",
                        "total": "450"
                    },
                    {
                        "itemId": "1054",
                        "name": "多兰之盾",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1054.png",
                        "description": "80生命值专注：攻击对小兵会附带额外的5物理伤害。复原力：每5秒回复6生命值。耐久：在受到来自一名敌方英雄、大型野怪或史诗级野怪的伤害后，在8秒里至多回复40生命值。回复额会在你生命值较低时提升。被远程英雄持有时，或受到群体伤害或周期型伤害时，耐久的效能为66%。",
                        "plainText": "优秀的防御型起始装备",
                        "sell": "180",
                        "total": "450"
                    },
                    {
                        "itemId": "1055",
                        "name": "多兰之刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1055.png",
                        "description": "8攻击力80生命值好战者：获得2.5%全能吸血。在造成群体伤害或通过宠物造成伤害时，全能吸血只有33%效能。",
                        "plainText": "普攻型英雄的优秀起始装备",
                        "sell": "180",
                        "total": "450"
                    },
                    {
                        "itemId": "1083",
                        "name": "萃取",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1083.png",
                        "description": "7攻击力攻击时每一击回复3生命值。收割：击杀1个线上小兵时提供1额外金币。击杀100个线上小兵时会立刻提供额外的350额外金币，并使收割失效。",
                        "plainText": "提供攻击力并在攻击时附带生命偷取—击杀小兵提供额外金币",
                        "sell": "180",
                        "total": "450"
                    },
                    {
                        "itemId": "2033",
                        "name": "腐败药水",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2033.png",
                        "description": "主动 - 消耗：消耗一层充能，以在12秒里持续回复共125生命值和75法力值。在此期间，伤害型技能和攻击会灼烧敌方英雄，在3秒里持续造成共15(如果你无法回复法力值，则为20)魔法伤害。至多可持有3层充能并会在访问商店时填满。如果由群体伤害或周期性伤害触发，那么腐败伤害降低至50%。",
                        "plainText": "持续回复生命和法力并提升战斗力—可在商店填满充能",
                        "sell": "200",
                        "total": "500"
                    },
                    {
                        "itemId": "3009",
                        "name": "轻灵之靴",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3009.png",
                        "description": "60移动速度所受的移动减速效果的强度降低25%。",
                        "plainText": "增强移动速度并抵抗减速效果",
                        "sell": "630",
                        "total": "900"
                    },
                    {
                        "itemId": "3158",
                        "name": "明朗之靴",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3158.png",
                        "description": "20技能急速45移动速度提供12%召唤师技能急速。“这个物品是为了庆祝在2010年12月10日艾欧尼亚和诺克萨斯的重赛中，艾欧尼亚取得胜利。”",
                        "plainText": "提升移动速度和冷却缩减",
                        "sell": "630",
                        "total": "900"
                    },
                    {
                        "itemId": "3117",
                        "name": "疾行之靴",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3117.png",
                        "description": "25移动速度当处于非战斗状态至少5秒时，使这个装备的效果提升至115。",
                        "plainText": "脱离战斗后，显著增强移动速度",
                        "sell": "700",
                        "total": "1000"
                    },
                    {
                        "itemId": "3006",
                        "name": "狂战士胫甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3006.png",
                        "description": "35%攻击速度45移动速度",
                        "plainText": "增强移动速度和攻击速度",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "3020",
                        "name": "法师之靴",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3020.png",
                        "description": "18法术穿透45移动速度",
                        "plainText": "增强移动速度和魔法伤害",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "3111",
                        "name": "水银之靴",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3111.png",
                        "description": "25魔法抗性45移动速度30%韧性韧性会减少晕眩、减速、嘲讽、恐惧、沉默、致盲、变形和定身效果的持续时间。它对浮空或压制效果无效。",
                        "plainText": "提升移动速度并降低限制效果的时长",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "3047",
                        "name": "铁板靴",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3047.png",
                        "description": "20护甲45移动速度使即将到来的攻击伤害降低12%。",
                        "plainText": "增强移动速度并减少即将到来的普攻伤害",
                        "sell": "770",
                        "total": "1100"
                    },
                ]
            },
            {
                "name":"史诗",
                "items":[
                    {
                        "itemId": "1018",
                        "name": "灵巧披风",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1018.png",
                        "description": "15%暴击几率",
                        "plainText": "提升暴击几率",
                        "sell": "420",
                        "total": "600"
                    },
                    {
                        "itemId": "2420",
                        "name": "秒表",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2420.png",
                        "description": "主动 -  凝滞：一次性使用，在2.5秒里免疫伤害且不可被选取，但在此期间里无法采取任何其它行动(转变为一个破损的秒表)。",
                        "plainText": "主动施放来变得无敌但无法行动",
                        "sell": "260",
                        "total": "650"
                    },
                    {
                        "itemId": "2421",
                        "name": "破损的秒表",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2421.png",
                        "description": "支离破碎的时间：【秒表】目前是破损状态，但仍然可以用于升级。在打破一个【秒表】后，商店主人就只会卖给你破损的秒表了。",
                        "plainText": "升级为秒表",
                        "sell": "260",
                        "total": "650"
                    },
                    {
                        "itemId": "2423",
                        "name": "完美时机秒表",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2423.png",
                        "description": "主动 -  凝滞：一次性使用，在2.5秒里免疫伤害且不可被选取，但在此期间里无法采取任何其它行动(转变为一个破损的秒表)。",
                        "plainText": "激活以免疫伤害但无法进行任何行动",
                        "sell": "260",
                        "total": "650"
                    },
                    {
                        "itemId": "3801",
                        "name": "晶体护腕",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3801.png",
                        "description": "200生命值50%基础生命回复",
                        "plainText": "提供生命值和生命回复",
                        "sell": "455",
                        "total": "650"
                    },
                    {
                        "itemId": "2015",
                        "name": "基舍艾斯碎片",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2015.png",
                        "description": "15%攻击速度盈能：移动和攻击将生成一次【盈能】攻击。突震：盈能攻击会造成额外的80魔法伤害。",
                        "plainText": "提供攻击速度和可充能的魔法打击",
                        "sell": "490",
                        "total": "700"
                    },
                    {
                        "itemId": "3057",
                        "name": "耀光",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3057.png",
                        "description": "咒刃：施放技能后，你的下一次普通攻击会因强化而附带额外的(100%基础攻击力)物理伤害(1.5秒冷却时间)。 ",
                        "plainText": "在施法后，为下次普攻提供加成效果",
                        "sell": "490",
                        "total": "700"
                    },
                    {
                        "itemId": "1031",
                        "name": "锁子甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1031.png",
                        "description": "40护甲",
                        "plainText": "显著提升护甲",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "3066",
                        "name": "带翼的月板甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3066.png",
                        "description": "150生命值飞翔：提供5%移动速度。",
                        "plainText": "",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "3067",
                        "name": "燃烧宝石",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3067.png",
                        "description": "200生命值10技能急速",
                        "plainText": "提升生命值和冷却缩减",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "3076",
                        "name": "棘刺背心",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3076.png",
                        "description": "35护甲荆棘：在被一次攻击命中后，对攻击者造成(3 + 10%额外护甲)魔法伤害并且如果目标是英雄，还会施加40%重伤效果，持续3秒。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "3114",
                        "name": "禁忌雕像",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3114.png",
                        "description": "10%治疗和护盾强度50%基础法力回复",
                        "plainText": "提升治疗和护盾强度、法力回复和冷却缩减",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "3123",
                        "name": "死刑宣告",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3123.png",
                        "description": "15攻击力撕裂：对敌方英雄造成物理伤害时会施加持续3秒的40%重伤效果。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "用于对抗拥有高生命回复的敌人",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "3916",
                        "name": "湮灭宝珠",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3916.png",
                        "description": "30法术强度恶咒缠身：对英雄造成魔法伤害时会施加持续3秒的40%重伤效果。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "提升魔法伤害",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "6677",
                        "name": "狂怒小刀",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6677.png",
                        "description": "25%攻击速度怨怒：你的暴击几率会被转化为攻击特效伤害。已转化的每20%暴击几率提供35攻击特效。",
                        "plainText": "",
                        "sell": "560",
                        "total": "800"
                    },
                    {
                        "itemId": "1026",
                        "name": "爆裂魔杖",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1026.png",
                        "description": "40法术强度",
                        "plainText": "适度提升法术强度",
                        "sell": "595",
                        "total": "850"
                    },
                    {
                        "itemId": "3113",
                        "name": "以太精魂",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3113.png",
                        "description": "30法术强度滑行：提供5%移动速度。",
                        "plainText": "提升法术强度和移动速度",
                        "sell": "595",
                        "total": "850"
                    },
                    {
                        "itemId": "1037",
                        "name": "十字镐",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1037.png",
                        "description": "25攻击力",
                        "plainText": "适度提升攻击力",
                        "sell": "613",
                        "total": "875"
                    },
                    {
                        "itemId": "1011",
                        "name": "巨人腰带",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1011.png",
                        "description": "350生命值",
                        "plainText": "显著提升生命值",
                        "sell": "630",
                        "total": "900"
                    },
                    {
                        "itemId": "1053",
                        "name": "吸血鬼节杖",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1053.png",
                        "description": "15攻击力10%生命偷取",
                        "plainText": "普攻回复生命值",
                        "sell": "630",
                        "total": "900"
                    },
                    {
                        "itemId": "1057",
                        "name": "负极斗篷",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1057.png",
                        "description": "50魔法抗性",
                        "plainText": "适度提升魔法抗性",
                        "sell": "630",
                        "total": "900"
                    },
                    
                    {
                        "itemId": "3024",
                        "name": "冰川圆盾",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3024.png",
                        "description": "20护甲250法力10技能急速",
                        "plainText": "提升护甲和冷却缩减",
                        "sell": "630",
                        "total": "900"
                    },
                    {
                        "itemId": "3108",
                        "name": "恶魔法典",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3108.png",
                        "description": "35法术强度10技能急速",
                        "plainText": "提升法术强度和冷却缩减",
                        "sell": "630",
                        "total": "900"
                    },
                    
                    {
                        "itemId": "3191",
                        "name": "探索者的护臂",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3191.png",
                        "description": "20法术强度15护甲女巫之道：击杀一个单位提供1护甲(最多提供30护甲)。",
                        "plainText": "提升护甲和法术强度",
                        "sell": "630",
                        "total": "900"
                    },
                    
                    {
                        "itemId": "4642",
                        "name": "班德尔玻璃镜",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4642.png",
                        "description": "20法术强度10技能急速50%基础法力回复",
                        "plainText": "",
                        "sell": "665",
                        "total": "950"
                    },
                    {
                        "itemId": "1043",
                        "name": "反曲之弓",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1043.png",
                        "description": "25%攻击速度镶钢：攻击附带15物理伤害攻击特效。",
                        "plainText": "显著提升攻击速度",
                        "sell": "700",
                        "total": "1000"
                    },
                    {
                        "itemId": "3082",
                        "name": "守望者铠甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3082.png",
                        "description": "40护甲坚如磐石：使所受的来自普攻的伤害至多降低(0.5%最大生命值)，至多为该次攻击伤害的40%。",
                        "plainText": "",
                        "sell": "700",
                        "total": "1000"
                    },
                    
                    {
                        "itemId": "3086",
                        "name": "狂热",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3086.png",
                        "description": "18%攻击速度15%暴击几率狂热：提供7%移动速度。",
                        "plainText": "略微提升暴击几率，移动速度和攻击速度",
                        "sell": "735",
                        "total": "1050"
                    },
                    {
                        "itemId": "3145",
                        "name": "海克斯科技发电机",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3145.png",
                        "description": "40法术强度转速加快：对一名敌方英雄造成伤害时会附带额外的50 - 125 (基于等级)魔法伤害(40秒冷却时间)。",
                        "plainText": "提升法术强度。普攻会周期性附带额外魔法伤害。",
                        "sell": "735",
                        "total": "1050"
                    },
                    
                    {
                        "itemId": "3044",
                        "name": "净蚀",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3044.png",
                        "description": "15攻击力200生命值顽强：在你对敌方英雄造成物理伤害后，于6秒内持续回复共2%最大生命值。远程英雄的回复效果降低至50%。",
                        "plainText": "攻击和击杀会给予爆发性的加速效果",
                        "sell": "770",
                        "total": "1100"
                    },
                    
                    {
                        "itemId": "3051",
                        "name": "缚炉之斧",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3051.png",
                        "description": "15攻击力15%攻击速度灵光：攻击一名单位会提供(20 | 远程为10)移动速度，持续2秒。",
                        "plainText": "",
                        "sell": "770",
                        "total": "1100"
                    },
                    
                    {
                        "itemId": "3133",
                        "name": "考尔菲德的战锤",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3133.png",
                        "description": "25攻击力10技能急速",
                        "plainText": "攻击力和冷却缩减",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "3134",
                        "name": "锯齿短匕",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3134.png",
                        "description": "30攻击力凿击：提供10穿甲",
                        "plainText": "提升攻击力和穿甲",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "4638",
                        "name": "戒备眼石",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4638.png",
                        "description": "25技能急速奥术窖藏：这件装备至多可储存3个已购买的控制守卫。注视：使你的侦察守卫和控制守卫的放置上限提升1。可从【侦察守卫饰品】和唯一：辅助装备中放置侦察守卫。",
                        "plainText": "",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "4641",
                        "name": "萌动眼石",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4641.png",
                        "description": "奥术窖藏：这件装备可以至多储存3个已购买的控制守卫。盛开帝国：一旦你已经放置了20个侦察守卫，这件装备就会转变为戒备眼石。可从【侦察守卫饰品】和升级版唯一：辅助装备中放置侦察守卫。",
                        "plainText": "",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "6660",
                        "name": "斑比的熔渣",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6660.png",
                        "description": "300生命值献祭：承受或造成伤害时会使你开始每秒对附近敌人造成(15 + 1%额外生命值)魔法伤害(对小兵提升25%，对野怪提升25%)，持续3秒。",
                        "plainText": "",
                        "sell": "770",
                        "total": "1100"
                    },
                    {
                        "itemId": "3077",
                        "name": "提亚马特",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3077.png",
                        "description": "25攻击力顺劈：攻击会对附近的其它敌人造成至多(60%攻击力)物理伤害。对相距最远的敌人造成(12%攻击力)的最小伤害。",
                        "plainText": "近战攻击会命中身边的敌人",
                        "sell": "840",
                        "total": "1200"
                    },
                    {
                        "itemId": "4632",
                        "name": "翠绿屏障",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4632.png",
                        "description": "25法术强度25魔法抗性适应：每过60秒，提供3魔法抗性(最大值为15)。受到魔法伤害时，会使下次提升所需的时间缩短。缩短的时间(以秒为单位)相当于5%的伤害值。",
                        "plainText": "",
                        "sell": "840",
                        "total": "1200"
                    },
                    {
                        "itemId": "6029",
                        "name": "铁刺鞭",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6029.png",
                        "description": "30攻击力主动 - 新月：对附近敌人单位造成(75%攻击力)物理伤害。低于40%生命值的小兵和野怪受到200%伤害(15秒冷却时间，可通过技能急速缩短)。",
                        "plainText": "",
                        "sell": "840",
                        "total": "1200"
                    },
                    {
                        "itemId": "1058",
                        "name": "无用大棒",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1058.png",
                        "description": "60法术强度",
                        "plainText": "极大提升法术强度",
                        "sell": "875",
                        "total": "1250"
                    },
                    {
                        "itemId": "3211",
                        "name": "幽魂斗篷",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3211.png",
                        "description": "250生命值25魔法抗性无实体：在受到来自英雄的伤害后， 提供150%基础生命回复，最多持续10秒(持续时间随受到的伤害值提升)。",
                        "plainText": "增强防御属性并在受到伤害时提供回复",
                        "sell": "875",
                        "total": "1250"
                    },
                    {
                        "itemId": "4630",
                        "name": "枯萎珠宝",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4630.png",
                        "description": "25法术强度15%法术穿透",
                        "plainText": "",
                        "sell": "875",
                        "total": "1250"
                    },
                    {
                        "itemId": "1038",
                        "name": "暴风之剑",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/1038.png",
                        "description": "40攻击力",
                        "plainText": "显著提升攻击力",
                        "sell": "910",
                        "total": "1300"
                    },
                    {
                        "itemId": "3140",
                        "name": "水银饰带",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3140.png",
                        "description": "30魔法抗性主动 - 水银：移除你英雄身上的所有控制效果(浮空除外)(90秒冷却时间)。",
                        "plainText": "主动施放来移除所有控制效果",
                        "sell": "910",
                        "total": "1300"
                    },
                    {
                        "itemId": "3155",
                        "name": "海克斯饮魔刀",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3155.png",
                        "description": "20攻击力35魔法抗性救主灵刃：在受到将使你的生命值跌到30%以下的魔法伤害时，提供110 - 280(基于等级)魔法伤害护盾，持续3秒(90秒冷却时间)。",
                        "plainText": "提升攻击力和魔法抗性",
                        "sell": "910",
                        "total": "1300"
                    },
                    {
                        "itemId": "3802",
                        "name": "遗失的章节",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3802.png",
                        "description": "40法术强度300法力10技能急速启蒙：当你升级后，在3秒里持续回复共20%最大法力值。",
                        "plainText": "升级时回复法力值。",
                        "sell": "910",
                        "total": "1300"
                    },
                    {
                        "itemId": "4635",
                        "name": "榨血睥睨",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4635.png",
                        "description": "20法术强度150生命值10%全能吸血",
                        "plainText": "",
                        "sell": "910",
                        "total": "1300"
                    },
                    {
                        "itemId": "6670",
                        "name": "正午箭袋",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6670.png",
                        "description": "30攻击力15%攻击速度精密：攻击会对小兵和野怪额外造成20物理伤害。",
                        "plainText": "",
                        "sell": "910",
                        "total": "1300"
                    },
                    {
                        "itemId": "3035",
                        "name": "最后的轻语",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3035.png",
                        "description": "20攻击力20%护甲穿透",
                        "plainText": "用于对抗拥有高护甲的敌人",
                        "sell": "1015",
                        "total": "1450"
                    },
                    {
                        "itemId": "3105",
                        "name": "军团圣盾",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3105.png",
                        "description": "30护甲30魔法抗性10技能急速",
                        "plainText": "提供护甲和魔法抗性",
                        "sell": "1050",
                        "total": "1500"
                    }
                ]
            },
            
            {
                "name":"传说",
                "items":[
                    {
                        "itemId": "2051",
                        "name": "守护者号角",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2051.png",
                        "description": "150生命值复原力：每5秒回复20生命值。不屈不挠：格挡来自英雄的攻击和技能的15伤害(对抗持续伤害技能时的效能为25%)。传说：这个单位会被视为一件传说装备。",
                        "plainText": "坦克型英雄的优秀起始装备",
                        "sell": "665",
                        "total": "950"
                    },
                    {
                        "itemId": "3112",
                        "name": "守护者法球",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3112.png",
                        "description": "40法术强度150生命值复原力：每5秒回复10法力。如果你不能获得法力值，则转而回复15生命值。传说：这个单位会被视为一件传说装备。",
                        "plainText": "法师型英雄的优秀起始装备",
                        "sell": "665",
                        "total": "950"
                    },
                    {
                        "itemId": "3177",
                        "name": "守护者之刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3177.png",
                        "description": "30攻击力150生命值15技能急速传说：这个单位会被视为一件传说装备。",
                        "plainText": "普攻型英雄的优秀起始装备",
                        "sell": "665",
                        "total": "950"
                    },
                    {
                        "itemId": "3184",
                        "name": "守护者战锤",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3184.png",
                        "description": "25攻击力150生命值10%生命偷取传说：这个单位会被视为一件传说装备。",
                        "plainText": "普攻型英雄的优秀起始装备",
                        "sell": "665",
                        "total": "950"
                    },
                    {
                        "itemId": "3041",
                        "name": "梅贾的窃魂卷",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3041.png",
                        "description": "20法术强度100生命值荣耀：每次击杀提供4层，每次助攻提供2层(至多可达25层)。每次死亡损失10层。畏惧：每层荣耀提供5法术强度。如果你拥有至少10层荣耀，就会提供10%移动速度。已获得的荣耀层数可在这件装备和黑暗封印之间保存。",
                        "plainText": "击杀和助攻提供法术强度",
                        "sell": "1120",
                        "total": "1600"
                    },
                    {
                        "itemId": "3011",
                        "name": "炼金科技纯化器",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3011.png",
                        "description": "50法术强度15技能急速100%基础法力回复芙顶之毒：对英雄们造成魔法伤害时会施加40%重伤效果，持续3秒。使英雄们定身时会施加60%重伤效果作为替代。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "",
                        "sell": "1610",
                        "total": "2300"
                    },
                    {
                        "itemId": "3107",
                        "name": "救赎",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3107.png",
                        "description": "20%治疗和护盾强度200生命值15技能急速100%基础法力回复主动 - 干涉：选择5500码内的一个区域作为目标。在2.5秒后，召下一道光束来治疗友军们180 - 360(随友军等级增长)生命值，并灼烧敌方英雄们相当于其10%最大生命值的真实伤害(120秒冷却时间)。这件装备可以在死亡时施放。对近期被其它干涉效果影响的目标造成的伤害和治疗效果降低50%。加成效果的强度是基于该友方英雄的等级。",
                        "plainText": "主动使用以在一个区域内治愈友军和伤害敌军",
                        "sell": "1610",
                        "total": "2300"
                    },
                    {
                        "itemId": "3109",
                        "name": "骑士之誓",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3109.png",
                        "description": "400生命值10技能急速300%基础生命回复主动 - 立誓：指定一名友方英雄作为你的誓约者(60秒冷却时间)。牺牲：当你的誓约者友军在附近时，将其所受的15%伤害转移到你身上，并且如果其生命值低于50%，你就会在朝着其移动时获得35%移动速度。同一个英雄同一时间只能被一个【骑士之誓】所连接。如果你的生命值低于30%，伤害转移就会停止。",
                        "plainText": "与一名友方英雄搭档来保护彼此",
                        "sell": "1610",
                        "total": "2300"
                    },
                    {
                        "itemId": "3222",
                        "name": "米凯尔的祝福",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3222.png",
                        "description": "20%治疗和护盾强度50魔法抗性15技能急速100%基础法力回复主动 - 纯化：移除一名友方英雄身上的所有控制效果(击飞和压制除外)并为其回复100 - 200(基于友军等级)生命值(120秒冷却时间)。",
                        "plainText": "主动施放来移除一名友军身上的限制效果",
                        "sell": "1610",
                        "total": "2300"
                    },
                    {
                        "itemId": "3504",
                        "name": "炽热香炉",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3504.png",
                        "description": "60法术强度10%治疗和护盾强度100%基础法力回复圣洁化：对一名友方英雄施加治疗和护盾时，会使你们两个都获得6秒的强化，使你们获得10% - 30%(基于友军等级)攻击速度并且攻击时附带5 - 20(基于友军等级)魔法伤害攻击特效。加成效果的强度是基于该友方英雄的等级。",
                        "plainText": "施加于其他单位的护盾和治疗效果还会为你和目标提供攻击速度并使他们的攻击在命中时造成额外魔法伤害。",
                        "sell": "1610",
                        "total": "2300"
                    },
                    {
                        "itemId": "4643",
                        "name": "警觉眼石",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4643.png",
                        "description": "40技能急速10%移动速度奥术窖藏：这件装备至多可储存3个已购买的控制守卫。注视：使你的侦察守卫和控制守卫的放置上限提升1。需要戒备眼石来购买。",
                        "plainText": "",
                        "sell": "1610",
                        "total": "2300"
                    },
                    {
                        "itemId": "6616",
                        "name": "流水法杖",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6616.png",
                        "description": "60法术强度10%治疗和护盾强度100%基础法力回复湍流：当你为一名友方英雄提供治疗或护盾效果时，你和你的目标都会获得15%移动速度和20 - 40(友军)法术强度，持续3秒。加成效果的强度是基于该友方英雄的等级。",
                        "plainText": "你的治疗和护盾会缩短控制效果并提供移动速度",
                        "sell": "1610",
                        "total": "2300"
                    },
                    {
                        "itemId": "3050",
                        "name": "基克的聚合",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3050.png",
                        "description": "250生命值25护甲250法力20技能急速主动 - 导管：指定一名帮手(60秒冷却时间)。聚合：在你定身一名敌人后的8秒里，你的帮手的技能和攻击(攻击特效)对这个敌人附带额外的(30 - 70(基于等级) + 1.5%生命值 + 7.5%法术强度)魔法伤害。同一个英雄同一时间只能被一个【基克的聚合】所连接。",
                        "plainText": "在你施放终极技能时，为你和你的队友提供加成。",
                        "sell": "1680",
                        "total": "2400"
                    },
                    {
                        "itemId": "2065",
                        "name": "舒瑞娅的战歌",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/2065.png",
                        "description": "350生命值20技能急速5%移动速度50%基础法力回复主动 - 鼓舞：为你和附近的友方英雄提供60%不断衰减的移动速度，持续4秒。对敌方英雄发起的下3次来自英雄的攻击或技能会造成额外的35 - 55(基于友军等级)魔法伤害(90秒冷却时间)。神话被动：你每装备一件传说装备，就会获得2.5%移动速度。加成效果的强度是基于该友方英雄的等级。",
                        "plainText": "激活以加速附近的友军",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3033",
                        "name": "凡性的提醒",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3033.png",
                        "description": "20攻击力25%攻击速度20%暴击几率7%移动速度脓毒：对敌方英雄造成物理伤害时会施加40%重伤效果，持续3秒。对一名敌方英雄连续命中3次攻击时，会使该英雄身上的这个效果增强至60%重伤效果，持续到这个效果消逝为止。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "用于对抗拥有高生命回复和高护甲的敌人",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3046",
                        "name": "幻影之舞",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3046.png",
                        "description": "40%攻击速度20%暴击几率7%移动速度幽影华尔兹：普攻会提供幽灵状态和额外的7%移动速度，持续3秒。此外，进行5次普攻后，还会使【幽影华尔兹】提供相同持续时间的40%攻击速度。幽灵状态的单位无视其它单位的碰撞体积。",
                        "plainText": "攻击敌方英雄时移动得更快，并在低血量时提供一层护盾。",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3085",
                        "name": "卢安娜的飓风",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3085.png",
                        "description": "40%攻击速度20%暴击几率7%移动速度风怒：你的普通攻击会朝目标附近的至多2个敌人发射弩箭，每支弩箭造成(40%()攻击力)物理伤害。这些弩箭能够附带攻击特效并且可以暴击。这件装备仅远程英雄可用。",
                        "plainText": "远程攻击会对目标身边的2个敌人发射弩箭",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3094",
                        "name": "疾射火炮",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3094.png",
                        "description": "35%攻击速度20%暴击几率7%移动速度盈能：移动和攻击将生成一次【盈能】攻击。神射手：你的【盈能】攻击造成120额外魔法伤害。此外，【盈能】攻击至多获得35%额外攻击距离。攻击距离的提升不能多于150码。",
                        "plainText": "移动时积攒充能来释放一次攻城炮击",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3102",
                        "name": "女妖面纱",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3102.png",
                        "description": "65法术强度45魔法抗性10技能急速废除：提供一层法术护盾来格挡下一个敌方技能(40秒冷却时间)。如果你在此装备冷却完毕之前受到来自英雄的伤害，那么它的冷却时间会重新开始计算。",
                        "plainText": "周期性阻挡敌方技能",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3135",
                        "name": "虚空之杖",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3135.png",
                        "description": "65法术强度40%法术穿透",
                        "plainText": "提升魔法伤害",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3157",
                        "name": "中娅沙漏",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3157.png",
                        "description": "65法术强度45护甲10技能急速主动 - 凝滞：在2.5秒里免疫伤害且不可被选取，但在此期间里无法采取任何其它行动(120秒冷却时间)。",
                        "plainText": "主动施放来变得无敌但无法行动",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3165",
                        "name": "莫雷洛秘典",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3165.png",
                        "description": "70法术强度250生命值苦难：对敌方英雄造成魔法伤害时会施加40%重伤效果，持续3秒。如果目标的生命值低于50%，那么这个效果会提升至60%重伤。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "提升魔法伤害",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "3043",
                        "name": "魔切",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3043.png",
                        "description": "35攻击力860法力15技能急速敬畏：提供相当于2.5%最大法力值的额外攻击力。冲击：以英雄为目标时，技能和攻击(攻击特效)会附带额外的(2.5%最大法力值)物理伤害。 ",
                        "plainText": "",
                        "sell": "1820",
                        "total": "2600"
                    },
                    {
                        "itemId": "3001",
                        "name": "深渊面具",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3001.png",
                        "description": "350生命值60魔法抗性损毁：定身一名敌方英雄时会使其所受伤害提升10%，持续4秒。",
                        "plainText": "附近敌人承受更多魔法伤害",
                        "sell": "1890",
                        "total": "2700"
                    },
                    {
                        "itemId": "3075",
                        "name": "荆棘之甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3075.png",
                        "description": "350生命值60护甲荆棘：在被一次攻击命中后，对攻击者造成(10 + 10%额外护甲)魔法伤害并且如果目标是英雄，还会施加40%重伤效果，持续3秒。定身敌方英雄时还会施加60%重伤效果，持续3秒。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "",
                        "sell": "1890",
                        "total": "2700"
                    },
                    {
                        "itemId": "3095",
                        "name": "岚切",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3095.png",
                        "description": "40攻击力15%攻击速度20%暴击几率盈能：移动和攻击将生成一次【盈能】攻击。麻痹：你的【盈能】攻击造成120额外魔法伤害。此外，【盈能】攻击对敌人造成75%减速，持续0.5秒。",
                        "plainText": "大幅强化其它盈能效果。",
                        "sell": "1890",
                        "total": "2700"
                    },
                    {
                        "itemId": "3110",
                        "name": "冰霜之心",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3110.png",
                        "description": "80护甲400法力20技能急速凛冬之抚：使附近敌人的攻击速度降低15%。坚如磐石：使所受的来自普攻的伤害至多降低(0.5%最大生命值)，至多为该次攻击伤害的40%。",
                        "plainText": "巨幅提升护甲并减慢敌人的普攻",
                        "sell": "1890",
                        "total": "2700"
                    },
                    {
                        "itemId": "3143",
                        "name": "兰顿之兆",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3143.png",
                        "description": "250生命值80护甲10技能急速主动：谦卑：暂时使附近敌人减速并使其攻击力降低10%，暴击伤害降低 20%，持续4秒(60秒冷却时间)。坚如磐石：使所受的来自普攻的伤害至多降低(0.5%最大生命值)，至多为该次攻击伤害的40%。",
                        "plainText": "显著提升防御属性，主动施放来减速身边的敌人",
                        "sell": "1890",
                        "total": "2700"
                    },
                    {
                        "itemId": "6609",
                        "name": "炼金朋克链锯剑",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6609.png",
                        "description": "45攻击力200生命值15技能急速劈削：对敌方英雄造成物理伤害时会施加40%重伤效果，持续3秒。如果目标的生命值低于50%，那么重伤效果会提升至60%。重伤会降低治疗效果和生命回复的效能。",
                        "plainText": "",
                        "sell": "1890",
                        "total": "2700"
                    },
                    {
                        "itemId": "3026",
                        "name": "守护天使",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3026.png",
                        "description": "40攻击力40护甲挽救恩赐：你的英雄在受到致命伤害时，会在凝滞4秒后原地复活，恢复50%基础生命值和30%最大法力值(300秒冷却时间)。",
                        "plainText": "周期性地在英雄死亡时将其复活",
                        "sell": "1120",
                        "total": "2800"
                    },
                    {
                        "itemId": "3124",
                        "name": "鬼索的狂暴之刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3124.png",
                        "description": "40%攻击速度20%暴击几率怨怒：你的暴击几率会被转化为攻击特效伤害。已转化的每20%暴击几率提供40攻击特效。沸腾打击：每第三次普攻会施加2次攻击特效。",
                        "plainText": "提供攻击速度、护甲穿透和法术穿透。",
                        "sell": "1960",
                        "total": "2800"
                    },
                    {
                        "itemId": "3179",
                        "name": "黯影阔剑",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3179.png",
                        "description": "55攻击力12穿甲15技能急速封锁：在被一个敌方守卫发现时，使你附近的所有敌方陷阱显形并使敌方守卫失效，持续8秒(45秒冷却时间)。普通攻击会立刻击杀被显形的陷阱，并对守卫造成三倍伤害。",
                        "plainText": "周期性地提供陷阱和守卫侦测能力",
                        "sell": "1960",
                        "total": "2800"
                    },
                    {
                        "itemId": "6695",
                        "name": "巨蛇之牙",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6695.png",
                        "description": "60攻击力18穿甲掠盾者：攻击和技能对带护盾的目标们造成额外的(50+40%额外攻击力)物理伤害。额外伤害不会作用于带有仅格挡魔法伤害护盾的目标。",
                        "plainText": "",
                        "sell": "1960",
                        "total": "2800"
                    },
                    {
                        "itemId": "3004",
                        "name": "魔宗",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3004.png",
                        "description": "35攻击力500法力15技能急速敬畏：提供相当于2.5%最大法力值的额外攻击力。法力积攒：用普攻或一个技能击中一个目标时，会消耗一层充能并提供3额外法力值。如果目标是英雄，则该次提供的额外法力值翻倍。一旦提供了360额外法力值，这个装备就会转变为魔切。每8秒提供一层新的法力积攒(至多4层)。",
                        "plainText": "基于最大法力值提升攻击力",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "3036",
                        "name": "多米尼克领主的致意",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3036.png",
                        "description": "35攻击力20%暴击几率25%护甲穿透巨人杀手：对最大生命值高于你的敌方英雄至多造成15%额外物理伤害。当最大生命值差异大于2000时，即可达到最大伤害值提升。",
                        "plainText": "用于对抗拥有高生命值和高护甲的敌人",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "3042",
                        "name": "魔切",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3042.png",
                        "description": "35攻击力860法力15技能急速敬畏：提供相当于2.5%最大法力值的额外攻击力。冲击：以英雄为目标时，技能和攻击(攻击特效)会附带额外的(2.5%最大法力值)物理伤害。 ",
                        "plainText": "",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "3065",
                        "name": "振奋盔甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3065.png",
                        "description": "450生命值40魔法抗性10技能急速100%基础生命回复无拘活力：使你所受的全部治疗和护盾的效果提升25%。",
                        "plainText": "提升生命值和治疗效果",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "3508",
                        "name": "夺萃之镰",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3508.png",
                        "description": "55攻击力20%暴击几率20技能急速咒刃：施放技能后，你的下一次普通攻击会因强化而附带额外的(100%基础攻击力+40%额外攻击力)物理伤害并回复3%最大法力值(1.5秒冷却时间)。",
                        "plainText": "提供暴击伤害。在施放技能后，造成额外伤害并回复法力值。",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "3742",
                        "name": "亡者的板甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3742.png",
                        "description": "475生命值40护甲5%移动速度沉船者：在移动时，至多积攒60额外移动速度。你的下一次攻击会释放掉已积攒的移动速度来造成至多100魔法伤害。在移动速度满层时，如果持有者是近战，那么该次攻击还会减速目标50%，持续1秒。“只有一种方法能让你从我这里拿到这件盔甲……” - 被遗忘的名字",
                        "plainText": "在你四处移动时积攒气势，然后猛击敌人。",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "3814",
                        "name": "夜之锋刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3814.png",
                        "description": "50攻击力10穿甲325生命值废除：提供一层【法术护盾】来格挡下一个敌方技能(40秒冷却时间)。如果你在这件装备冷却完毕之前受到伤害，那么它的冷却时间会重新开始计算。",
                        "plainText": "周期性地阻挡敌方技能",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "4401",
                        "name": "自然之力",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4401.png",
                        "description": "350生命值60魔法抗性5%移动速度吸收：受到技能伤害时会为你提供6移动速度和4魔法抗性，持续5秒(可叠加至多5层；所受的每个独特的技能都会提供1层)。",
                        "plainText": "移动速度，魔法抗性，以及最大生命回复",
                        "sell": "2030",
                        "total": "2900"
                    },
                    {
                        "itemId": "3003",
                        "name": "大天使之杖",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3003.png",
                        "description": "65法术强度500法力敬畏：提供相当于3%额外法力值的法术强度。法力积攒：用一个技能击中一个目标时，会消耗一层充能并提供3额外法力值。如果目标是英雄，则该次提供的额外法力值翻倍。一旦提供了360额外法力值，这个装备就会转变为炽天使之拥。每8秒提供一层新的法力积攒(至多4层)。",
                        "plainText": "基于最大法力值提升法术强度",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3040",
                        "name": "炽天使之拥",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3040.png",
                        "description": "65法术强度860法力敬畏：提供额外的相当于5%额外法力值的法术强度。最高天：使你提升5%(+2.5%每100法术强度)总法力值。",
                        "plainText": "",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3083",
                        "name": "狂徒铠甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3083.png",
                        "description": "800生命值10技能急速200%基础生命回复狂徒之心：如果你至少拥有3000最大生命值，并且在6秒(非英雄单位为3秒)内没有受到伤害，就会每秒回复5%最大生命值。",
                        "plainText": "提供巨量生命值和生命回复",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3100",
                        "name": "巫妖之祸",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3100.png",
                        "description": "80法术强度10%移动速度咒刃：施放技能后，你的下一次普通攻击会因强化而附带额外的(150%基础攻击力+40%法术强度)魔法伤害(2.5秒冷却时间)。",
                        "plainText": "在施法后，为下次普攻提供加成效果",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3115",
                        "name": "纳什之牙",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3115.png",
                        "description": "100法术强度50%攻击速度艾卡西亚之咬：攻击附带(15+20%法术强度 )魔法伤害攻击特效。",
                        "plainText": "提升攻击力，法术强度和冷却缩减",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3116",
                        "name": "瑞莱的冰晶节杖",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3116.png",
                        "description": "90法术强度350生命值凝霜：伤害型技能会使敌人减速30%，持续1秒。",
                        "plainText": "技能使敌人减速",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3139",
                        "name": "水银弯刀",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3139.png",
                        "description": "40攻击力20%暴击几率30魔法抗性主动 - 水银：移除所有控制效果(浮空除外)，并提供50%移动速度，持续1秒(90秒冷却时间)。",
                        "plainText": "主动施放来移除所有控制效果并提供巨幅移速",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3142",
                        "name": "幽梦之灵",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3142.png",
                        "description": "60攻击力18穿甲主动 - 鬼魂步伐：提供20%移动速度和幽灵状态，持续6秒(45秒冷却时间)。鬼影萦绕：提供非战斗状态下的40移动速度。幽灵状态的单位无视其它单位的碰撞体积。。",
                        "plainText": "主动施放来显著提升移动速度",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3181",
                        "name": "血色之刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3181.png",
                        "description": "50攻击力10穿甲12物理吸血暴怒：当附近的可见敌方英雄数量在1或以下时，获得8穿甲和20% - 80%攻击速度，如果有其他英雄近身，那么这个攻速加成会在3秒内持续衰减。",
                        "plainText": "在猎杀落单的敌方英雄时提升攻击速度和穿甲",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "4628",
                        "name": "视界专注",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4628.png",
                        "description": "100法术强度亢奋射击：用一个无目标类技能对相距750码之外的一名英雄造成伤害或定身时，会使该英雄显形并使你对其造成的伤害提升10%，持续6秒。触发亢奋射击的该次技能也会享受到这个伤害提升效果。宠物和非定身类陷阱不会触发这个效果。领域类技能只有在初始放置时会触发这个效果。距离是从技能施放位置处进行测量的。",
                        "plainText": "定身一名英雄时会引发闪电袭击该英雄",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "4629",
                        "name": "星界驱驰",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4629.png",
                        "description": "75法术强度200生命值30技能急速咒舞：用技能造成伤害时会提供(10+20%技能急速)移动速度，持续4秒。",
                        "plainText": "巨幅冷却缩减",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "4637",
                        "name": "恶魔之拥",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4637.png",
                        "description": "70法术强度350生命值亚扎卡纳的凝视：对一名敌人造成技能伤害时会使其每秒受到相当于其1.2%最大生命值的魔法伤害，持续4秒。在此期间，如果该敌人是一名英雄，你会获得10护甲和魔法抗性(每名受影响的额外敌方英雄会使这个效果+2.5护甲和魔法抗性)。 ",
                        "plainText": "",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "6035",
                        "name": "密银黎明",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6035.png",
                        "description": "35攻击力200生命值35魔法抗性主动 - 水银：移除所有控制效果(浮空除外)，并提供40%韧性和40%减速抵抗，持续3秒(90秒冷却时间)。",
                        "plainText": "",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "6676",
                        "name": "收集者",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6676.png",
                        "description": "55攻击力20%暴击几率12穿甲死与税：如果你造成的伤害将使一名敌方英雄的生命值跌到5%以下，那么会直接将其处决。击杀英雄时会为你提供额外的25金币。",
                        "plainText": "",
                        "sell": "2100",
                        "total": "3000"
                    },
                    {
                        "itemId": "3053",
                        "name": "斯特拉克的挑战护手",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3053.png",
                        "description": "50攻击力400生命值嗜血：在对抗一名敌方英雄时造成或受到伤害后，都会在6秒里持续回复共(2%最大生命值 | 远程为1.2%最大生命值)。救主灵刃：在受到将使你的生命值跌到30%以下的伤害时，提供200护盾值(每层嗜血提升8%最大生命值 | 远程为4.8%最大生命值)，持续5秒(60秒冷却时间)。嗜血至多可叠加5层；每个敌方英雄提供1层。远程英雄的治疗效果和额外护盾会降低至60%。",
                        "plainText": "提供抵抗大额爆发伤害的护盾",
                        "sell": "2170",
                        "total": "3100"
                    },
                    {
                        "itemId": "3091",
                        "name": "智慧末刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3091.png",
                        "description": "30攻击力40%攻击速度50魔法抗性喧争：攻击附带额外的15 - 80 (基于等级)魔法伤害攻击特效并提供20移动速度，持续2秒。",
                        "plainText": "普攻造成魔法伤害并抓回你的生命值。",
                        "sell": "2170",
                        "total": "3100"
                    },
                    {
                        "itemId": "3156",
                        "name": "玛莫提乌斯之噬",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3156.png",
                        "description": "50攻击力50魔法抗性15技能急速救主灵刃：在受到将使你的生命值跌到30%以下的魔法伤害时，提供(200 + 20%最大生命值)魔法伤害护盾，持续5秒(60秒冷却时间)。",
                        "plainText": "生命值很低时提供攻击力",
                        "sell": "2170",
                        "total": "3100"
                    },
                    {
                        "itemId": "6333",
                        "name": "死亡之舞",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6333.png",
                        "description": "50攻击力40护甲15技能急速无视痛苦：所受的35%(远程使用者为15%)物理伤害会以流血形式在3秒里持续扣除。蔑视：参与击杀英雄后会净化无视痛苦的剩余伤害，并为你提供30%移动速度，持续2秒并在此期间持续回复共10%最大生命值。",
                        "plainText": "",
                        "sell": "2170",
                        "total": "3100"
                    },
                    {
                        "itemId": "3153",
                        "name": "破败王者之刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3153.png",
                        "description": "40攻击力25%攻击速度12%生命偷取雾之锋：攻击附带额外的物理伤害攻击特效，相当于(近战携带者为10% | 远程携带者为6%)目标当前生命值。虹吸：攻击一名敌方英雄3次后，会造成40 - 150(基于等级)魔法伤害并偷取其25%移动速度，持续2秒(20秒冷却时间)。雾之锋对野怪和小兵的最大伤害为60。装备对近战携带者和远程携带者会有不同的性能。",
                        "plainText": "造成基于目标生命值的伤害，并能偷取移动速度",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "3071",
                        "name": "黑色切割者",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3071.png",
                        "description": "40攻击力300生命值25技能急速切割：对一名英雄造成物理伤害时会施加4%护甲削减，持续6秒(至多24%护甲削减)。屠夫：伤害型技能和攻击(攻击特效)会对被完全切割的敌人们造成额外的5%已损失生命值的物理伤害。持续伤害造成40%屠夫伤害(0.5秒冷却时间)。",
                        "plainText": "对敌人造成物理伤害时会减少目标的护甲",
                        "sell": "2310",
                        "total": "3300"
                    },
                    {
                        "itemId": "3074",
                        "name": "贪欲九头蛇",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3074.png",
                        "description": "65攻击力20技能急速15%全能吸血顺劈：攻击和技能会对命中目标附近的其它敌人造成至多(60%攻击力)物理伤害。顺劈对处于其距离边缘的敌人造成(12%攻击力)的最小伤害，每次攻击或技能仅能命中每个目标1次，并且不会被周期性伤害触发。",
                        "plainText": "近战攻击会命中身边的敌人，造成伤害并回复生命值",
                        "sell": "2310",
                        "total": "3300"
                    },
                    {
                        "itemId": "3193",
                        "name": "石像鬼石板甲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3193.png",
                        "description": "60护甲60魔法抗性15技能急速主动 - 坚不可摧：获得在2.5秒里持续衰减的(100 + 100%额外生命值)护盾值，并在此期间体型增长25%(90秒冷却时间)。巩固：在受到来自一名英雄的伤害时，提供一层3%额外护甲和3%额外魔法抗性，持续6秒至多5层；每个英雄可提供1层。",
                        "plainText": "附近有多名敌人时极大提升防御属性",
                        "sell": "2310",
                        "total": "3300"
                    },
                    {
                        "itemId": "3748",
                        "name": "巨型九头蛇",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3748.png",
                        "description": "30攻击力500生命值巨像：提供相当于1%最大生命值的攻击力。顺劈：攻击附带额外的(5 + 1.5%最大生命值)物理伤害攻击特效，并生成一道冲击波来对目标身后的敌人们造成(40 + 3%最大生命值)物理伤害。远程英雄造成75%伤害。攻击特效伤害也可作用于建筑物。",
                        "plainText": "造成基于携带者最大生命值的群体伤害",
                        "sell": "2310",
                        "total": "3300"
                    },
                    {
                        "itemId": "3031",
                        "name": "无尽之刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3031.png",
                        "description": "70攻击力20%暴击几率完美：如果你拥有至少60%暴击几率，就会获得35%暴击伤害。",
                        "plainText": "巨幅加强暴击",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "3072",
                        "name": "饮血剑",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3072.png",
                        "description": "55攻击力20%暴击几率20%生命偷取脓水护盾：来自攻击的生命偷取现在可以对你进行过量治疗。溢出的生命值会储存为50 - 350(基于等级)护盾值，如果你在之前的25秒里没有造成或承受任何伤害，那么这个护盾会缓慢衰减。",
                        "plainText": "提供攻击力，生命偷取且生命偷取可过量治疗",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "6675",
                        "name": "纳沃利迅刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6675.png",
                        "description": "60攻击力20%暴击几率30技能急速灵巧打击：你的攻击在暴击时会使你的基础技能缩短20%的剩余冷却时间。",
                        "plainText": "",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "6694",
                        "name": "赛瑞尔达的怨恨",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6694.png",
                        "description": "45攻击力30%护甲穿透20技能急速严寒：伤害型技能会使敌人减速30%，持续1秒。",
                        "plainText": "",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "3089",
                        "name": "灭世者的死亡之帽",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3089.png",
                        "description": "120法术强度魔法乐章：使你的总法术强度提升35%。",
                        "plainText": "巨幅提升法术强度",
                        "sell": "2660",
                        "total": "3800"
                    }
                ]
            },
        
            {
                "name":"神话",
                "items":[
                    {
                        "itemId": "3190",
                        "name": "钢铁烈阳之匣",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3190.png",
                        "description": "200生命值20技能急速30护甲30魔法抗性主动 - 虔诚：为附近友军提供230 - 385(基于友军等级)护盾，在2.5秒里持续衰减(90秒冷却时间)。奉献：为附近友方英雄提供5护甲和魔法抗性。神话被动：你每装备一件传说装备，就会获得2护甲/魔法抗性加成至奉献。加成效果的强度是基于该友方英雄的等级。",
                        "plainText": "主动施放来为附近友军提供吸收伤害的护盾",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "4005",
                        "name": "帝国指令",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4005.png",
                        "description": "40法术强度200生命值20技能急速100%基础法力回复协同开火：技能在减速或定身一名英雄时会造成36 - 60(基于等级)额外魔法伤害并将其标记4秒。友方英雄的伤害会引爆标记，造成额外的90 - 150(基于友军等级)魔法伤害并为你和该友军都提供20%移动速度，持续2秒(每个英雄6秒冷却时间)。神话被动：你每装备一件传说装备，就会获得15法术强度。加成效果的强度是基于该友方英雄的等级。",
                        "plainText": "将伤害延后。",
                        "sell": "1750",
                        "total": "2500"
                    },
                    {
                        "itemId": "6617",
                        "name": "月石再生器",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6617.png",
                        "description": "40法术强度200生命值20技能急速100%基础法力回复星光恩典：在战斗中对一名英雄(包括友军)施放一次技能或攻击后，会给附近伤势最重的友方英雄回复70 - 100(基于友军等级)生命值(2秒冷却时间)。每与英雄战斗1秒，这个治疗效果就会提升12.5%(最多提升50%)。神话被动：你每装备一件传说装备，就会获得 5技能急速。加成效果的强度是基于该友方英雄的等级。",
                        "plainText": "你的治疗和护盾的冷却时间更快并且对低生命值英雄造成更多效果",
                        "sell": "1750",
                        "total": "2500"
                    },
                   
                    {
                        "itemId": "3068",
                        "name": "日炎圣盾",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3068.png",
                        "description": "350生命值30护甲30魔法抗性15技能急速献祭：承受或造成伤害会使你开始每秒对附近敌人造成(20 - 40(基于等级) + 1%额外生命值)魔法伤害(对小兵提升25%，对野怪提升100%)，持续3秒。用这个效果对敌方英雄或史诗级野怪造成伤害时会添加一层效果，使后续的献祭伤害提升10%，持续5秒(最多6层)。火焰触摸：在最大献祭层数时，你的攻击会灼烧附近的敌人来每秒造成你的献祭伤害，持续3秒。神话被动：你每装备一件传说装备，就会获得5技能急速。",
                        "plainText": "高护甲。持续对附近敌人造成伤害。定身敌人时可释放一道伤害性的火焰波",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "3152",
                        "name": "海克斯科技火箭腰带",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3152.png",
                        "description": "90法术强度350生命值15技能急速主动 - 海克斯魔弹：朝着目标方向冲刺，释放一道魔法弹圆弧，造成(125+15%法术强度)魔法伤害。随后，在朝着敌方英雄移动时提供50%移动速度，持续2秒(40秒冷却时间)。神话被动：你每装备一件传说装备，就会获得 5法术穿透。超音速的冲刺无法穿过地形。",
                        "plainText": "主动施放来向前冲刺并释放一阵炫目的轰炸",
                        "sell": "2240",
                        "total": "3200"
                    },
                   
                    {
                        "itemId": "4633",
                        "name": "峡谷制造者",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4633.png",
                        "description": "80法术强度150生命值15技能急速15%全能吸血虚空腐蚀：在对敌方英雄造成伤害时，每过1秒，就会造成2%额外伤害(最大值为10%)。在满层时，该额外伤害会转而造成真实伤害。神话被动：你每装备一件传说装备，就会获得5%法术穿透。",
                        "plainText": "",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "4636",
                        "name": "暗夜收割者",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4636.png",
                        "description": "90法术强度300生命值15技能急速灵魂撕裂：对一名英雄造成伤害时会造成额外的125+15%法术强度的魔法伤害并为你提供25%移动速度，持续1.5秒(对每个英雄有40秒冷却时间)。神话被动：你每装备一件传说装备，就会获得5技能急速。对一名未受影响的英雄造成伤害时会延长移动速度加成的持续时间。",
                        "plainText": "",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "6662",
                        "name": "霜火护手",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6662.png",
                        "description": "350生命值50护甲25魔法抗性15技能急速献祭：承受或造成伤害会使你开始每秒对附近敌人造成(20 - 40 (基于等级)+1%额外生命值)魔法伤害(对小兵提升25%，对野怪提升150%)，持续3秒。雪缚：普通攻击会生成一个持续1.5秒的冰霜领域(4秒冷却时间)。穿过领域的敌人会被减速25%(已通过你的最大生命值提升)。神话被动：你每装备一件传说装备，就会获得100生命值和6%体型。",
                        "plainText": "高魔法抗性。被动减速附近敌人。当附近有技能释放时，释放一道能量波来造成伤害和减速。",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "6664",
                        "name": "涡轮炼金罐",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6664.png",
                        "description": "350生命值25护甲50魔法抗性15技能急速主动 - 极限超载：在朝着敌方英雄或敌方防御塔移动时会提供75%移动速度，持续4秒。一旦贴近一名敌方英雄(或在4秒后)，你就会放出一道冲击波，使附近敌方英雄减速40%，持续1.5秒(90秒冷却时间)。献祭：承受或造成伤害时会使你开始每秒对附近敌人造成(20 - 40 (基于等级) + 1%额外生命值)魔法伤害(对小兵提升25%，对野怪提升150%)，持续3秒。神话被动：你每装备一件传说装备，就会获得5%韧性和减速抗性。",
                        "plainText": "定身敌人以获得一层护盾。激活以更快跑向敌人。",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "6691",
                        "name": "德拉克萨的暮刃",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6691.png",
                        "description": "60攻击力18穿甲20技能急速夜行者：攻击一名敌方英雄时会附带额外的(65+25%额外攻击力)物理伤害(15秒冷却时间)。如果持有者是一名近战英雄，这个攻击还会使目标减速99%，持续0.25秒。如果一名在过去3秒内曾被你造成过伤害的英雄阵亡，就会刷新这个冷却时间并获得持续1.5秒的隐形状态。神话被动：你每装备一件传说装备，就会获得5技能急速。",
                        "plainText": "",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "6692",
                        "name": "星蚀",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6692.png",
                        "description": "55攻击力18穿甲10%全能吸血永升之月：在1.5秒内用2次独立的攻击或技能命中一名英雄时，会造成额外的6%最大生命值的物理伤害，并为你提供15%移动速度和(150 + 40%额外攻击力)护盾值(远程英雄为100 + 30%额外攻击力)，持续2秒。(8秒冷却时间，远程英雄为16秒冷却时间)。神话被动：你每装备一件传说装备，就会获得4%护甲穿透。",
                        "plainText": "",
                        "sell": "2240",
                        "total": "3200"
                    },
                    {
                        "itemId": "6693",
                        "name": "暗行者之爪",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6693.png",
                        "description": "60攻击力21穿甲20技能急速主动 - 沙之挥击：冲刺并穿过目标敌人，造成(100 + 30%额外攻击力)物理伤害。接下来的3秒里，你对目标造成的伤害提升15%(60秒冷却时间)。神话被动：你每装备一件传说装备，就会获得 5穿甲",
                        "plainText": "",
                        "sell": "2240",
                        "total": "3200"
                    },
                    
                    {
                        "itemId": "6630",
                        "name": "渴血战斧",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6630.png",
                        "description": "45攻击力400生命值20技能急速150%基础生命回复主动 - 饥渴斩击：对附近的敌人们造成(100%攻击力)物理伤害。每命中一个敌方英雄就会回复(25%攻击力 + 12%已损失生命值)(15秒冷却时间，可被技能急速缩短)。怨恨：每5%已损失生命值提供1%攻击力(最多提供15%)。神话被动：你每装备一件传说装备，就会获得 5技能急速。",
                        "plainText": "",
                        "sell": "2310",
                        "total": "3300"
                    },
                    {
                        "itemId": "6631",
                        "name": "挺进破坏者",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6631.png",
                        "description": "45攻击力20%攻击速度300生命值20技能急速主动 - 蹒跚斩击：突进一小段距离并对附近的敌人们造成(100%攻击力)物理伤害，使他们减速60%，减速效果在2秒里持续衰减(20秒冷却时间，可通过技能急速缩短)。英勇步态：造成物理伤害时会提供30移动速度，持续2秒。神话被动：你每装备一件传说装备，就会获得3%移动速度。【蹒跚斩击】的突进无法穿过地形。",
                        "plainText": "",
                        "sell": "2310",
                        "total": "3300"
                    },
                    {
                        "itemId": "6632",
                        "name": "神圣分离者",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6632.png",
                        "description": "咒刃：释放技能后，下一次普攻会造成额外伤害，如果目标是英雄，会回复生命值。 神话被动：你每装备一件传说装备，就会获得5%护甲穿透和5%法术穿透。",
                        "plainText": "咒刃：释放技能后，下一次普攻会造成额外伤害，如果目标是英雄，会回复生命值。 神话被动：你每装备一件传说装备，就会获得5%护甲穿透和5%法术穿透。",
                        "sell": "2310",
                        "total": "3300"
                    },
                    {
                        "itemId": "3078",
                        "name": "三相之力",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/3078.png",
                        "description": "25攻击力35%攻击速度200生命值20技能急速三重打击：攻击会提供25移动速度，持续3秒。如果目标是英雄，则提升你6%基础攻击力，持续3秒，至多可叠加5次(最多提升：30%攻击力)。咒刃：施放技能后，你的下一次攻击因强化而附带额外的(200%基础攻击力)物理伤害(1.5秒冷却时间)。神话被动：你每装备一件传说装备，就会获得10攻击速度。",
                        "plainText": "成吨的伤害",
                        "sell": "2333",
                        "total": "3333"
                    },
                    
                    {
                        "itemId": "6653",
                        "name": "兰德里的苦楚",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6653.png",
                        "description": "80法术强度600法力20技能急速折磨：你的技能伤害会使敌人们灼烧，每秒造成(15+1.5%法术强度)+1%最大生命值的魔法伤害，持续4秒。在灼烧时，这些敌人每秒损失5%魔法抗性(至多削减15%)。神话被动：你每装备一件传说装备，就会获得5技能急速。",
                        "plainText": "在战斗中充能以造成高额持续伤害，尤其是在对抗耐久型敌人时",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "6655",
                        "name": "卢登的激荡",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6655.png",
                        "description": "80法术强度6法术穿透600法力20技能急速回声：伤害型技能会对目标和3个附近的敌人造成额外的(100+10%法术强度)魔法伤害并为你提供15%移动速度，持续2秒(10秒冷却时间)。神话被动：你每装备一件传说装备，就会获得5法术穿透。 ",
                        "plainText": "高额爆发伤害，擅长对抗脆皮型敌人",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "6656",
                        "name": "永霜",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6656.png",
                        "description": "80法术强度200生命值600法力20技能急速主动 - 冰川覆盖：在一个锥形范围内造成(100 + 30%法术强度)魔法伤害，并使其中的敌人减速65%，持续1.5秒。锥形范围中心的敌人会被禁锢而非被减速(20秒冷却时间)。神话被动：你每装备一件传说装备，就会获得15法术强度。",
                        "plainText": "",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "6671",
                        "name": "狂风之力",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6671.png",
                        "description": "60攻击力20%攻击速度20%暴击几率主动 - 暴雨：朝着目标方向冲刺，同时对你终点附近生命值最低的敌人(优先英雄)发射3个弹体。共造成(180 - 220(基于等级) + 45%额外攻击力)魔法伤害，对低生命值的目标时至多提升50%(60秒冷却时间)。神话被动：你每装备一件传说装备，就会获得3%移动速度弹体伤害在敌人生命值低于30%时达到最大值。【暴雨】的冲刺无法穿过地形。",
                        "plainText": "",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "6672",
                        "name": "海妖杀手",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6672.png",
                        "description": "65攻击力25%攻击速度20%暴击几率放倒它： 每第三次攻击造成额外的(60+45%额外攻击力)真实伤害。神话被动：你每装备一件传说装备，就会获得10%攻击速度。",
                        "plainText": "",
                        "sell": "2380",
                        "total": "3400"
                    },
                    {
                        "itemId": "6673",
                        "name": "不朽盾弓",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/6673.png",
                        "description": "50攻击力15%攻击速度20%暴击几率12%生命偷取救主灵刃：在受到将使你的生命值跌到30%以下的伤害时，提供250 - 700护盾值，持续3秒。此外，提供15%生命偷取，持续8秒(90秒冷却时间)。神话被动：你每装备一件传说装备，就会获得5攻击力和50生命值。",
                        "plainText": "",
                        "sell": "2380",
                        "total": "3400"
                    },
                    
                    {
                        "itemId": "4403",
                        "name": "金铲铲",
                        "iconPath": "https://game.gtimg.cn/images/lol/act/img/item/4403.png",
                        "description": "70攻击力120法术强度50%攻击速度30%暴击几率250生命值30护甲30魔法抗性250法力20技能急速10%移动速度10%生命偷取100%基础生命回复100%基础法力回复正在搞事：你正处在永久的火力全开中！",
                        "plainText": "它什么都做得到！",
                        "sell": "5241",
                        "total": "7487"
                    }
                ]
            }
        ]
        for (let cat of rawData){
            //找到当前数据在数据库中对应的分类
            const category = await Category.findOne({
                name: cat.name
            })
            //将每一条装备加上品质属性
            cat.items = cat.items.map(item=>{
                item.quality = category
                return item
            })
            await Item.insertMany(cat.items)
        }
        res.send (await Item.find())
    })
    //英雄列表展示接口
    router.get('/heroes/list', async(req,res)=>{
        const parent = await Category.findOne({
            name: '英雄分类'
        })
        //聚合查询，可以同时执行好几次查询，最终得到想要的结果
        const cats = await Category.aggregate([
            {$match:{ parent: parent._id}},
            {
                $lookup:{
                from:'heroes',
                localField:'_id',
                foreignField:'categories',
                as:'heroList'
                }
            },
            // {
            //     $addFields:{
            //         heroList:{$slice: ['$newsList',5]}
            //     }
            // }
        ])
        res.send(cats)
    })
    
    //装备列表展示接口
    router.get('/items/list', async(req,res)=>{
        const parent = await Category.findOne({
            name: '装备分类'
        })
        //聚合查询，可以同时执行好几次查询，最终得到想要的结果
        const cats = await Category.aggregate([
            {$match:{ parent: parent._id}},
            {
                $lookup:{
                from:'items',
                localField:'_id',
                foreignField:'quality',
                as:'itemList'
                }
            },
            // {
            //     $addFields:{
            //         heroList:{$slice: ['$newsList',5]}
            //     }
            // }
        ])
        res.send(cats)
    })
    //文章详情接口
    router.get('/articles/:id', async(req,res)=>{
        const data= await Article.findById(req.params.id)
        res.send(data)
    })
    //英雄详情接口
    //:id 用形式参数和实际参数来理解，这里:id 代表形参，实际参数从前端传来。 argument 实参 parameter 形参
    router.get('/heroes/:id' ,async(req,res)=>{
        const data =await Hero
        .findById(req.params.id)
        .populate('categories')
        .lean() //.lean()转化为json对象
        res.send(data)
    })
    app.use('/web/api', router)
}