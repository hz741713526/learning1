module.exports = options =>{
    return async (req,res,next)=>{
        const modelName = require('inflection').classify(req.params.resource) //将小写复数变为大写的单数
        req.Model =require(`../models/${modelName}`)
        next ()
    }
}