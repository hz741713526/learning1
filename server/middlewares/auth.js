module.exports = options =>{
    const AdminUser = require('../models/AdminUser')
    const jwt= require('jsonwebtoken')
    const assert= require('http-assert')
    return async(req,res,next)=>{
        const token = String(req.headers.authorization || '').split(' ').pop()//前端大写 后端小写
        assert(token, 401, '请先登录 (请提供jwt token)')
        const {id} = jwt.verify(token, req.app.get('secret'))
        assert(token, 401, '请先登录 (无效的jwt token )')
        req.user =await AdminUser.findById(id) //只有req res的对象才可以拿到其他地方使用。 表示客户端请求的用户
        assert(req.user, 401, '请先登录')
        await next()
    }
}