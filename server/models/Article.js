const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    title:{type: String},
    body:{type: String},
    //用数组表示多个分类的关联
    categories:[{type: mongoose.SchemaTypes.ObjectId, ref:'Category'}],
},{
    timestamps:true
})

module.exports = mongoose.model('Article',schema)