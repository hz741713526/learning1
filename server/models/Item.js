const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    itemID:{type:Number},
    name:{type: String},
    iconPath:{type: String},
    description:{type: String},
    quality:{type: mongoose.SchemaTypes.ObjectId, ref:'Category'},
    total:{type:Number},
    sell:{type:Number},
    plainText:{type:String},

})

module.exports = mongoose.model('Item',schema)