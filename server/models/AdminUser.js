const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    username:{type: String},
    password:{
        select:false,
        type: String, set(val){
        return require('bcrypt').hashSync(val,10) //生成val的散列值 ,数字代表安全程度
    }},
})

module.exports = mongoose.model('AdminUser',schema)