const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    name:{type: String},
    fullName:{type: String},
    avatar:{type: String},
    banner:{type: String},
    title:{type: String},
    //用数组表示多个分类的关联
    categories:[{type: mongoose.SchemaTypes.ObjectId, ref:'Category'}],
    data:{
        hp:{type:Number},
        hpperlevel:{type:Number},
        hpregen:{type:Number},
        hpregenperlevel:{type:Number},
        mp:{type:Number},
        mpperlevel:{type:Number},
        mpregen:{type:Number},
        mpregenperlevel:{type:Number},
        attackdamage:{type:Number},
        attackdamageperlevel:{type:Number},
        attackspeed:{type:Number},
        attackspeedperlevel:{type:Number},
        armor:{type:Number},
        armorperlevel:{type:Number},
        spellblock:{type:Number},
        spellblockperlevel:{type:Number},
        movespeed:{type:Number},
        attackrange:{type:Number},
        crit:{type:Number},
        critperlevel:{type:Number},
    },

    attack:{type:Number},
    defense:{type:Number},
    magic:{type:Number},
    difficulty:{type:Number},

    skills:[{
        abilityIconPath:{type:String},
        title:{type:String},
        description:{type:String},
        tips:{type:String},
    }],
    items1:[{type:mongoose.SchemaTypes.ObjectId, ref:'Item'}],
    items2:[{type:mongoose.SchemaTypes.ObjectId, ref:'Item'}],
    usageTips:{type: String},
    battleTips:{type: String},
    story:{type: String},
})

module.exports = mongoose.model('Hero',schema , 'heroes')