const express = require('express')
const app =express ()

app.set('secret','!!!123qweQWE')
app.use(require('cors')())
app.use(express.json())
app.use('/uploads',express.static(__dirname + '/uploads'))//express 静态文件托管

require('./routes/admin')(app)//请求admin时 传回app， 在admin中就也可以使用这个app
require('./plugins/db')(app)
require('./routes/web')(app)
app.listen(3000,()=>{
    console.log('http://localhost:3000')
})